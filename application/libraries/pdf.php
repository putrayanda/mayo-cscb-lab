<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('tcpdf/tcpdf.php');
/**
 * TCPDF  Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	PDF Generator
 * @author		TCPDF
 */
class Pdf extends TCPDF {

	public function __construct()
	{
		parent::__construct();
	}
}
