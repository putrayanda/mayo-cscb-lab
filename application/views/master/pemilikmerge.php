<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<style>
	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}

	table {
		table-layout: fixed;
	}

	.modal-dialog .select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pemilik
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Pemilik Utama</span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/pemilik_search/">
					<div class="form-group row">
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari Nama" name="cr_nama"  id="cr_nama" /></div>
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari No. HP"  name="cr_no_hp"  id="cr_no_hp"/></div>
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari Email" name="cr_email"  id="cr_email"/></div>
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari NIK" name="cr_nik"  id="cr_nik" /></div>
						<div class="col-lg-2 mb-5">
							<button class="btn btn-success mr-2 col-lg-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_pemilik" data-toggle="table" data-height="500" data-show-columns="false" data-search="true" data-show-toggle="false" data-pagination="false" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pem_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="250">Nama Pemilik</th>
							<th data-sortable="true" data-width="150">Komunitas</th>
							<th data-sortable="true" data-width="175">NIK</th>
							<th data-sortable="true" data-width="250">Email</th>
							<th data-sortable="false" data-width="225">No HP/WA</th>
							<th data-sortable="false" data-width="175">TTL</th>
							<th data-sortable="false" data-width="75" data-align="center">JK</th>
							<th data-sortable="false" data-width="125" data-align="center">Foto Profile</th>
							<th data-sortable="false" data-width="125" data-align="center">Foto Identitas</th>
							<th data-sortable="false" data-width="600">Alamat</th>
							<th data-sortable="false" data-width="75" data-align="center">Aktif ?</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Hewan</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= $offset;
						foreach($query_pemilik->result() as $row_pemilik){
							$no++;
							$pem_id		= $row_pemilik->pem_id;
							if($rNum == $pem_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pemilik->pem_asal == 1){
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pemilik->ref_asal_pem_ket.'</span>';
							}
							else{
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pemilik->ref_asal_pem_ket.'</span>';
							}

							if($row_pemilik->pem_gender == 'P'){
								$gender = '<i class="fas fa-male"></i>';
							}
							else{
								$gender = '<i class="fas fa-female"></i>';
							}

							if($row_pemilik->pem_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
							<td><?php echo $pem_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_pemilik->pem_nama; ?></td>
							<td><?php echo $asal; ?></td>
							<td><?php echo $row_pemilik->pem_nik; ?></td>
							<td><?php echo $row_pemilik->pem_email; ?></td>
							<td><?php echo $row_pemilik->pem_no_hp_wa; ?></td>
							<td><?php echo $row_pemilik->ttl; ?></td>
							<td><?php echo $gender; ?></td>
							<td><a class="ml-2 mr-2" data-caption="Foto Profile" data-fancybox="gallery" href="assets/file/foto_pemilik/<?php echo $row_pemilik->pem_photo ?>" >Lihat</a></td>
							<td><a class="ml-2 mr-2" data-caption="Foto Profile" data-fancybox="gallery" href="assets/file/foto_identitas/<?php echo $row_pemilik->pem_photo_identitas ?>" >Lihat</a></td>
							<td><?php echo $row_pemilik->alamat_lengkap; ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo $row_pemilik->count_pasien; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			<?php
				if (isset($pagination)){
					echo $pagination ;
				}
			?>
			</div>
		</div>
	</div>
</div>
<?php
if($rNum > 0){
?>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pemilik
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Pemilik Yang Ingin Digabung</span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/pemilik_merge_search/">
					<input type="hidden" name="rNum"  id="rNum" value="<?php echo $rNum;?>" />
					<div class="form-group row">
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari Nama" name="cr_nama"  id="cr_nama" /></div>
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari No. HP"  name="cr_no_hp"  id="cr_no_hp"/></div>
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari Email" name="cr_email"  id="cr_email"/></div>
						<div class="col-lg-2 mb-5"><input type="text" class="form-control" placeholder="Cari NIK" name="cr_nik"  id="cr_nik" /></div>
						<div class="col-lg-2 mb-5">
							<button class="btn btn-success mr-2 col-lg-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<form action="<?= base_url().$class.'/'.$method?>/crud/" method="post" class="form-horizontal form-label-left" novalidate >
				<table id="table_pemilik_merge" data-toggle="table" data-height="500" data-show-columns="false" data-search="true" data-show-toggle="false" data-pagination="false" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_merge_pem_id" data-visible="false">ID</th>
							<th data-sortable="false" data-width="85" data-align="center">Merge</th>
							<th data-sortable="true" data-width="250">Nama Pemilik</th>
							<th data-sortable="true" data-width="150">Komunitas</th>
							<th data-sortable="true" data-width="175">NIK</th>
							<th data-sortable="true" data-width="250">Email</th>
							<th data-sortable="false" data-width="225">No HP/WA</th>
							<th data-sortable="false" data-width="175">TTL</th>
							<th data-sortable="false" data-width="75" data-align="center">JK</th>
							<th data-sortable="false" data-width="125" data-align="center">Foto Profile</th>
							<th data-sortable="false" data-width="125" data-align="center">Foto Identitas</th>
							<th data-sortable="false" data-width="600">Alamat</th>
							<th data-sortable="false" data-width="75" data-align="center">Aktif ?</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Hewan</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_pemilik_merge->result() as $row_pemilik_merge){
							$no++;
							$pem_id		= $row_pemilik_merge->pem_id;

							if($row_pemilik_merge->pem_asal == 1){
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pemilik_merge->ref_asal_pem_ket.'</span>';
							}
							else{
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pemilik_merge->ref_asal_pem_ket.'</span>';
							}

							if($row_pemilik_merge->pem_gender == 'P'){
								$gender = '<i class="fas fa-male"></i>';
							}
							else{
								$gender = '<i class="fas fa-female"></i>';
							}

							if($row_pemilik_merge->pem_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> ">
							<td><?php echo $pem_id; ?></td>
							<td><input type="hidden" name="row_id_<?php echo $no; ?>" id="row_id_<?php echo $no; ?>" value="<?php echo $pem_id; ?>" /><input type="checkbox" name="checked_<?php echo $no; ?>" value="t" /></td>
							<td><?php echo $row_pemilik_merge->pem_nama; ?></td>
							<td><?php echo $asal; ?></td>
							<td><?php echo $row_pemilik_merge->pem_nik; ?></td>
							<td><?php echo $row_pemilik_merge->pem_email; ?></td>
							<td><?php echo $row_pemilik_merge->pem_no_hp_wa; ?></td>
							<td><?php echo $row_pemilik_merge->ttl; ?></td>
							<td><?php echo $gender; ?></td>
							<td><a class="ml-2 mr-2" data-caption="Foto Profile" data-fancybox="gallery" href="assets/file/foto_pemilik/<?php echo $row_pemilik_merge->pem_photo ?>" >Lihat</a></td>
							<td><a class="ml-2 mr-2" data-caption="Foto Profile" data-fancybox="gallery" href="assets/file/foto_identitas/<?php echo $row_pemilik_merge->pem_photo_identitas ?>" >Lihat</a></td>
							<td><?php echo $row_pemilik_merge->alamat_lengkap; ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo $row_pemilik_merge->count_pasien; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
			<div class="card-body">
					<input type="hidden" class="form-control" id="count_dt" name="count_dt" value="<?php echo $no ?>">
					<input type="hidden" name="rNum"  id="rNum" value="<?php echo $rNum;?>" />
					<div class="form-group row">
						<div class="col-lg-4 mb-5">&nbsp;</div>
						<div class="col-lg-4 mb-5">
							<button class="btn btn-success mr-2 col-lg-12" type="submit" > Merge
								<i class="fa fa-search"></i>
							</button>
						</div>
						<div class="col-lg-4 mb-5">&nbsp;</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
}
?>
<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
	
<script type="text/javascript">
	$('#table_pemilik').on('dbl-click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_pem_id);
	});


	Fancybox.bind('[data-fancybox="gallery"]', {
	  //dragToClose: false,
	  Thumbs: false,

	  Image: {
		zoom: false,
		click: false,
		wheel: "slide",
	  },

	  on: {
		// Move caption inside the slide
		reveal: (f, slide) => {
		  slide.$caption && slide.$content.appendChild(slide.$caption);
		},
	  },
	});
</script>
