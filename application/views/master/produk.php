<style>
table {
	table-layout: fixed;
}
.select2-container {
width: 100% !important;
padding: 0;
}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Produk
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Produk</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($readonly_menu == 'f'){
						if($rNum > 0){
					?>
							<a href="<?php echo base_url().$class.'/'.$method;?>/form/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
								<i class="fas fa-box icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="#" class="btn btn-info font-weight-bolder" data-toggle="modal" data-target="#formAddDetailModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Detail
							</a>
							&nbsp;&nbsp;
						<?php
							if($ref_prod_aktif =='t'){
						?>
								<a href="<?php echo base_url().$class.'/'.$method;?>/disabled/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
									<i class="fas fa-box icon-md"></i>
									Non Aktifkan
								</a>
						<?php 
							}
							else{
						?>
								<a href="<?php echo base_url().$class.'/'.$method;?>/enabled/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
									<i class="fas fa-box icon-md"></i>
									Aktifkan
								</a>
						<?php
							}
						?>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/delete/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-box icon-md"></i>
								Hapus
							</a>
					<?php
						}
						else{
					?>
							<a href="<?php echo base_url().$class.'/'.$method;?>/form" class="btn btn-primary font-weight-bolder fix150" >
								<i class="fas fa-box icon-md"></i>
								Tambah Data
							</a>					
					<?php
						}
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<table id="table_hd" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="200">Jenis Produk</th>
							<th data-sortable="true" data-width="150">Kode Produk</th>
							<th data-sortable="true" data-width="300">Nama Produk</th>
							<th data-sortable="true" data-width="300">Nama Alias/Di Invoice</th>
							<th data-sortable="true" data-width="100" data-align="right">Min Stok</th>
							<th data-sortable="true" data-width="175" data-align="center">Satuan Beli</th>
							<th data-sortable="true" data-width="175" data-align="right">Rasio Beli - Stok</th>
							<th data-sortable="true" data-width="175" data-align="center">Satuan Stok/Jual</th>
							<th data-sortable="true" data-width="175" data-align="right">Rasio Stok - Pakai</th>
							<th data-sortable="true" data-width="175" data-align="center">Satuan Pakai</th>
							<th data-sortable="true" data-width="150" data-align="right">Harga Jual</th>
							<th data-sortable="true" data-width="200" data-align="right">Biaya Rata2 Pakai/Harga Beli</th>
							<th data-sortable="true" data-width="300">Akun Pendapatan</th>
							<th data-sortable="true" data-width="300">Akun Biaya</th>
							<th data-sortable="true" data-width="300">Akun Persediaan</th>
							<th data-sortable="true" data-width="100" data-align="center">Klinik</th>
							<th data-sortable="true" data-width="100" data-align="center">Lab & Rad</th>
							<th data-sortable="true" data-width="100" data-align="center">Petcare</th>
							<th data-sortable="true" data-width="100" data-align="center">Apotek</th>

							<th data-sortable="true" data-width="85" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_hd){
							$no++;
							$ref_prod_id		= $row_hd->ref_prod_id;
							if($rNum == $ref_prod_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							($row_hd->ref_prod_aktif == 't') ? $status = '<i class="fas fa-thumbs-up">' : $status = '<i class="fas fa-thumbs-down"></i>';
							($row_hd->ref_prod_klinik == 't') ? $checkKlinik = 'x' : $checkKlinik = '';
							($row_hd->ref_prod_labrad == 't') ? $checkLabrad = 'x' : $checkLabrad = '';
							($row_hd->ref_prod_petcare == 't') ? $checkPetcare = 'x' : $checkPetcare = '';
							($row_hd->ref_prod_apotek == 't') ? $checkApotek = 'x' : $checkApotek = '';

						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $ref_prod_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_hd->ref_jns_prod_nama; ?></td>
							<td><?php echo $row_hd->ref_prod_kode; ?></td>
							<td><?php echo $row_hd->ref_prod_nama; ?></td>
							<td><?php echo $row_hd->ref_prod_nama_invoice; ?></td>
							<td><?php echo $row_hd->ref_prod_min_stok; ?></td>
							<td><?php echo $row_hd->sat_beli; ?></td>
							<td><?php echo $row_hd->ref_prod_rasio_beli_stok; ?></td>
							<td><?php echo $row_hd->sat_pakai; ?></td>
							<td><?php echo $row_hd->ref_prod_rasio_stok_pakai; ?></td>
							<td><?php echo $row_hd->sat_stok; ?></td>
							<td><?php echo number_format($row_hd->ref_prod_harga, 2, '.', ','); ?></td>
							<td><?php echo number_format($row_hd->ref_prod_harga_beli, 2, '.', ','); ?></td>
							<td><?php echo $row_hd->coa_pndptn; ?></td>
							<td><?php echo $row_hd->coa_biaya; ?></td>
							<td><?php echo $row_hd->coa_prsdn; ?></td>
							<td><?php echo $checkKlinik; ?></td>
							<td><?php echo $checkLabrad; ?></td>
							<td><?php echo $checkPetcare; ?></td>
							<td><?php echo $checkApotek; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<?php 
				if($rNum > 0){
				?>
				<table id="table_dt" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-sortable="false" data-width="65" data-align="center">Action</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="200">Jenis Produk</th>
							<th data-sortable="true" data-width="300">Nama Produk</th>
							<th data-sortable="true" data-width="150">Satuan Beli</th>
							<th data-sortable="true" data-width="170" data-align="center">Rasio Beli - Pakai</th>
							<th data-sortable="true" data-width="150" data-align="right">Harga Rata2</th>
							<th data-sortable="true" data-width="150" data-align="right">Jumlah Pakai</th>
							<th data-sortable="true" data-width="150">Satuan Pakai</th>
							<th data-sortable="true" data-width="250">Akun Biaya</th>
							<th data-sortable="true" data-width="150" data-align="right">Biaya</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_dt->result() as $row_dt){
							$no++;
							$ref_prod_dt_id	= $row_dt->ref_prod_dt_id;
							$rasio			= 1/$row_dt->ref_prod_rasio_stok_pakai;
							$harga_rata2	= ($rasio * $row_dt->ref_prod_harga_beli) * $row_dt->ref_prod_dt_jumlah_pakai;

						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td>
								<span class="dropdown">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu">
										<a class="dropdown-item del_detail" data-detail="<?php echo $ref_prod_dt_id; ?>"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus Detail</a>
									</div>
								</span>
							</td>
							<td><?php echo $no?></td>
							<td><?php echo $row_dt->ref_jns_prod_nama; ?></td>
							<td><?php echo $row_dt->ref_prod_nama; ?></td>
							<td><?php echo $row_dt->sat_beli; ?></td>
							<td>1:<?php echo $row_dt->ref_prod_rasio_stok_pakai; ?></td>
							<td><?php echo number_format($row_dt->ref_prod_harga_beli, 2, '.', ',');?></td>
							<td><?php echo number_format($row_dt->ref_prod_dt_jumlah_pakai, 2, '.', ','); ?></td>
							<td><?php echo $row_dt->sat_pakai; ?></td>
							<td><?php echo $row_dt->coa_biaya; ?></td>
							<td><?php echo number_format($harga_rata2, 2, '.', ','); ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<?php
				}
				?>				
			</div>
		</div>
		<!--end::Card-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
<!-- Modal Detail -->
<div class="modal fade" id="formAddDetailModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_ras" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_detail/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelRas">Tambah Produk Detail</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Produk Detail:</label>
							<select class="form-control select2" name="inp_dt_prod"  id="inp_dt_prod" required >
								<option label="Label"></option>
								<?=$comboProduk;?>
							</select>
						</div>
						<div class="form-group">
							<label>Jumlah Pakai:</label>
							<input type="number" class="form-control text-right" placeholder="Isi Jumlah Pakai" name="inp_dt_jml"  id="inp_dt_jml" value="" step=".001" required/>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_ras" name="submit_crud_ras" value="simpan_ras">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<script type="text/javascript">
	<?php
		if($readonly_menu == 'f'){
	?>
		$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
			$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
		});
	<?php
	}
	?>

	$('#inp_dt_prod').select2({
		placeholder: "Pilih Produk Detail",
		allowClear: true
	});	
		

	jQuery(document).ready(function() {
		$('.del_detail').click(function(){
			var rNum	= <?php echo $rNum?>;
			var rNum2	= $(this).attr('data-detail');
			Swal.fire({
				title: "Anda Yakin?",
				text: "Anda tidak akan dapat mengembalikan ini!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Ya, hapus!"
			}).then(function(result) {
				if(result.value==true){
					window.location.href = "<?php echo base_url().$class.'/'.$method ?>/del_detail/?rNum="+rNum+"&rNum2="+rNum2;
				}
			});
		}); 
	});
</script>
