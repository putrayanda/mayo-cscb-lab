<style>
table {
	table-layout: fixed;
}

.modal-dialog .select2-container {
    width: 100% !important;
    padding: 0;
}
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Hewan/Pasien
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Hewan/Pasien</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum > 0){
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pasien/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fas fa-edit icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<?php
						if($pas_aktif =='t'){
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/disabled_pasien/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-heart-broken icon-md"></i>
							RIP
						</a>
					<?php 
						}
						else{
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/enabled_pasien/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-heart icon-md"></i>
							Batal RIP
						</a>
					<?php
						}
					?>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete_pasien/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
						<i class="fas fa-minus-circle icon-md"></i>
						Hapus
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pasien" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fas fa-plus-circle icon-md"></i>
						Tambah Data
					</a>
					
					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/pasien_search/">
					<div class="form-group row">
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari No. RM"  name="cr_rm"  id="cr_rm"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Nama"  name="cr_nama"  id="cr_nama"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Pemilik"  name="cr_pemilik"  id="cr_pemilik"/></div>
						<div class="col-lg-3 mb-5">
							<button class="btn btn-success mr-2" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_pasien" data-toggle="table" data-height="500" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pas_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="125">No. RM</th>
							<th data-sortable="true" data-width="225">Nama Hewan</th>
							<th data-sortable="true" data-width="200">Pemilik Utama</th>
							<th data-sortable="true" data-width="135">Spesies</th>
							<th data-sortable="true" data-width="100" data-align="center">JK</th>
							<th data-sortable="true" data-width="135">Jenis Hewan</th>
							<th data-sortable="true" data-width="135">Ras Hewan</th>
							<th data-sortable="true" data-width="135">Warna Hewan</th>
							<th data-sortable="true" data-width="140">Umur</th>
							<th data-sortable="true" data-width="85" data-align="right">Berat (Kg)</th>
							<th data-sortable="true" data-width="135">Tanda Khusus</th>
							<th data-sortable="true" data-width="85" data-align="center">RIP ?</th>
							<th data-sortable="true" data-width="200">Pemilik</th>
							<th data-sortable="true" data-width="125" data-align="center">Jml Pemilik</th>
							<th data-sortable="true" data-width="125" data-align="center">Jml Tele</th>
							<th data-sortable="true" data-width="125" data-align="center">Jml RJ</th>
							<th data-sortable="true" data-width="125" data-align="center">Jml RI</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= $offset;
						foreach($query_pasien->result() as $row_pasien){
							$no++;
							$pas_id		= $row_pasien->pas_id;
							$pas_aktif	= $row_pasien->pas_aktif;
							if($rNum == $pas_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pasien->ref_gender_nama == 'J'){
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pasien->ref_gender_nama.'</span>';
							}
							else{
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pasien->ref_gender_nama.'</span>';
							}


							if($pas_aktif == 't'){
								$status = '';
								$pas_nama = $row_pasien->pas_nama;
							}
							else{
								$status = '<i class="fas fa-check icon-nm"></i>';
								$pas_nama = '<strike>'.$row_pasien->pas_nama.'</strike>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
							<td><?php echo $pas_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_pasien->pas_mrn?></td>
							<td><?php echo $pas_nama; ?></td>
							<td><?php echo $row_pasien->pem_nama; ?><br />(<?php echo $row_pasien->pem_no_hp_wa; ?>)</td>
							<td><?php echo $row_pasien->ref_spesies_nama; ?></td>
							<td><?php echo $gender; ?></td>
							<td><?php echo $row_pasien->ref_jns_hwn_nama; ?></td>
							<td><?php echo $row_pasien->ref_ras_nama; ?></td>
							<td><?php echo $row_pasien->ref_warna_ket; ?></td>
							<td><?php echo $row_pasien->umur; ?></td>
							<td><?php echo $row_pasien->pas_berat; ?></td>
							<td><?php echo $row_pasien->ref_tnd_khss_ket; ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo $row_pasien->nama_pemilik; ?></td>
							<td><?php echo $row_pasien->count_pemilik; ?></td>
							<td><?php echo $row_pasien->jumlah_telemedicine; ?></td>
							<td><?php echo $row_pasien->jumlah_kunjungan; ?></td>
							<td><?php echo $row_pasien->jumlah_rawatinap; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>	
		<?php
			if (isset($pagination)){
				echo $pagination ;
			}
		?>
			</div>
		</div>
	</div>
</div>
<?php
if($rNum > 0){
?>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pemilik
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Pemilik Hewan/Pasien Tersebut</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum2 > 0){
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pemilik/?rNum=<?php echo $rNum?>&rNum2=<?php echo $rNum2?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-edit icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete_pemilik_pasien/?rNum=<?php echo $rNum?>&rNum2=<?php echo $rNum2?>" class="btn btn-danger font-weight-bolder fix150" >
						<i class="fas fa-user-times icon-md"></i>
						Hapus
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pemilik/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-plus icon-md"></i>
						Tambah Data
					</a>
					
					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<table id="table_pemilik" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pem_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="250">Nama Pemilik</th>
							<th data-sortable="true" data-width="150">Komunitas</th>
							<th data-sortable="true" data-width="175">NIK</th>
							<th data-sortable="true" data-width="250">Email</th>
							<th data-sortable="false" data-width="225">No HP/WA</th>
							<th data-sortable="false" data-width="170">TTL</th>
							<th data-sortable="false" data-width="75" data-align="center">JK</th>
							<th data-sortable="false" data-width="600">Alamat</th>
							<th data-sortable="false" data-width="75" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_pemilik->result() as $row_pemilik){
							$no++;
							$pem_id		= $row_pemilik->pem_id;
							if($rNum2 == $pem_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pemilik->pem_asal == 1){
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pemilik->ref_asal_pem_ket.'</span>';
							}
							else{
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pemilik->ref_asal_pem_ket.'</span>';
							}

							if($row_pemilik->pem_gender == 'P'){
								$gender = '<i class="fas fa-male"></i>';
							}
							else{
								$gender = '<i class="fas fa-female"></i>';
							}

							if($row_pemilik->pem_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
							<td><?php echo $pem_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_pemilik->pem_nama; ?></td>
							<td><?php echo $asal; ?></td>
							<td><?php echo $row_pemilik->pem_nik; ?></td>
							<td><?php echo $row_pemilik->pem_email; ?></td>
							<td><?php echo $row_pemilik->pem_no_hp_wa; ?></td>
							<td><?php echo $row_pemilik->ttl; ?></td>
							<td><?php echo $gender; ?></td>
							<td><?php echo $row_pemilik->alamat_lengkap; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>								
			</div>
		</div>
	</div>
</div>
<?php
}
?>

<script type="text/javascript">
	$('#table_pasien').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_pas_id);
	});

	$('#table_pemilik').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum=<?php echo $rNum;?>&rNum2='+row.row_pem_id);
	});
</script>
