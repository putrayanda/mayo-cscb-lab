<style>
.select2-container {
width: 100% !important;
padding: 0;
}
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<div class="row">
			<!-- SPESIES -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Spesies <span class="d-block text-muted pt-2 font-size-sm">Informasi Spesies Hewan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddSpesiesModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_spesies/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddSpesiesModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_spesies" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_spes_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Spesies</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_spesies->result() as $row_spes){
									$no++;
									$ref_spesies_id	= $row_spes->ref_spesies_id;
									if($rNum == $ref_spesies_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_spesies_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_spes->ref_spesies_nama; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- WARNA -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Warna <span class="d-block text-muted pt-2 font-size-sm">Informasi Warna</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum2 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddWarnaModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_warna/?rNum=<?php echo $rNum2?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddWarnaModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_warna" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_wrn_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Warna</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_warna->result() as $row_wrn){
									$no++;
									$ref_warna_id	= $row_wrn->ref_warna_id;
									if($rNum2 == $ref_warna_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_warna_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_wrn->ref_warna_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- RAS -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Ras <span class="d-block text-muted pt-2 font-size-sm">Informasi Ras Hewan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum3 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddRasModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_ras/?rNum3=<?php echo $rNum3?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddRasModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_ras" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_ras_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" data-width="150" >Spesies</th>
									<th data-sortable="true" >Ras</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_ras->result() as $row_ras){
									$no++;
									$ref_ras_id	= $row_ras->ref_ras_id;
									if($rNum3 == $ref_ras_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_ras_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_ras->ref_spesies_nama; ?></td>
									<td><?php echo $row_ras->ref_ras_nama; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- DETAIL RAS -->
			<?php
			/*
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Detail Ras <span class="d-block text-muted pt-2 font-size-sm">Informasi Detail Ras Hewan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum4 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddRasDtModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_ras_dt/?rNum4=<?php echo $rNum4?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddRasDtModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_ras_dt" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_ras_dt_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Ras</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_ras_detail->result() as $row_ras_dt){
									$no++;
									$ref_ras_dt_id	= $row_ras_dt->ref_ras_dt_id;
									if($rNum4 == $ref_ras_dt_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_ras_dt_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_ras_dt->ref_ras_dt_nama; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			*/
			?>
			<!-- TANDA KHUSUS -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Tanda Khusus <span class="d-block text-muted pt-2 font-size-sm">Informasi Tanda Khusus</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum2 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddTandaModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_warna/?rNum=<?php echo $rNum2?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddTandaModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_tanda" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_tnd_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Tanda Khusus</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_tanda->result() as $row_tnd){
									$no++;
									$ref_tnd_khss_id	= $row_tnd->ref_tnd_khss_id;
									if($rNum5 == $ref_tnd_khss_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_tnd_khss_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_tnd->ref_tnd_khss_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>

				</div>
				<!--end::Card-->
			</div>
		</div>
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<!-- Modal Spesies -->
<div class="modal fade" id="formAddSpesiesModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_spesies" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_spesies/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelSpesies">Tambah Spesies</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Spesies:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Spesies" name="inp_nama_spesies"  id="inp_nama_spesies" value="<?php echo $ref_spesies_nama;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_spesies" name="submit_crud_spesies" value="simpan_spesies">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Warna -->
<div class="modal fade" id="formAddWarnaModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_warna" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_warna/?rNum2=<?php echo $rNum2?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelWarna">Tambah Warna</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Warna:</label>
							<input type="text" class="form-control" placeholder="Isi Warna" name="inp_nama_warna"  id="inp_nama_warna" value="<?php echo $ref_warna_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_warna" name="submit_crud_warna" value="simpan_warna">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Ras -->
<div class="modal fade" id="formAddRasModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_ras" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_ras/?rNum3=<?php echo $rNum3?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelRas">Tambah Ras</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Spesies:</label>
							<select class="form-control select2" name="inp_spesies_ras"  id="inp_spesies_ras" required >
								<option label="Label"></option>
								<?=$comboSpesiesRas;?>
							</select>
						</div>
						<div class="form-group">
							<label>Ras:</label>
							<input type="text" class="form-control" placeholder="Isi Ras" name="inp_nama_ras"  id="inp_nama_ras" value="<?php echo $ref_ras_nama;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_ras" name="submit_crud_ras" value="simpan_ras">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Ras Detail -->
<div class="modal fade" id="formAddRasDtModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_ras_dt" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_ras_dt/?rNum3=<?php echo $rNum3?>&rNum4=<?php echo $rNum4?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelRasDt">Tambah Ras Detail</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Ras Detail:</label>
							<input type="text" class="form-control" placeholder="Isi Ras Detail" name="inp_nama_ras_dt"  id="inp_nama_ras_dt" value="<?php echo $ref_ras_dt_nama;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_ras_dt" name="submit_crud_ras_dt" value="simpan_ras_dt">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>
<!-- Modal Tanda -->
<div class="modal fade" id="formAddTandaModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_tanda" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_tanda/?rNum2=<?php echo $rNum2?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelTanda">Tambah Tanda Khusus</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Tanda Khusus:</label>
							<input type="text" class="form-control" placeholder="Isi Tanda" name="inp_nama_tanda"  id="inp_nama_tanda" value="<?php echo $ref_tnd_khss_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_tanda" name="submit_crud_tanda" value="simpan_tanda">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#table_spesies').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_spes_id);
	});

	$('#table_warna').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum2='+row.row_wrn_id);
	});

	$('#table_ras').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum3='+row.row_ras_id);
	});

	$('#table_ras_dt').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum3=<?php echo $rNum3 ?>&rNum4='+row.row_ras_dt_id);
	});

	$('#table_tanda').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum5='+row.row_tnd_id);
	});

	$('#inp_spesies_ras').select2({
		placeholder: "Pilih Ras",
		allowClear: true
	});		
</script>
