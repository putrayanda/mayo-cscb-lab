<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Input Pasien
					<i class="mr-2"></i>
					<small class="">Untuk menambah Pasien/Hewan baru yang dimiliki</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
					<div class="btn-group">
						<button type="button" class="btn btn-primary font-weight-bolder">
						<i class="ki ki-check icon-sm"></i>Save Form</button>
						<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
						<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
							<ul class="nav nav-hover flex-column">
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_continue_button">
										<i class="nav-icon flaticon2-reload"></i>
										<span class="nav-text">Save &amp; continue</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_new_button">
										<i class="nav-icon flaticon2-add-1"></i>
										<span class="nav-text">Save &amp; add new</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_exit_button">
										<i class="nav-icon flaticon2-power"></i>
										<span class="nav-text">Save &amp; exit</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form id="form_input" role="form" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_pasien/?rNum=<?php echo $rNum?>">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<div class="form-group row">
						<div class="col-lg-12" align="center">
							<div class="image-input image-input-outline" id="kt_image_1">
								<div class="image-input-wrapper" style="background-image: url(<?php echo $pas_foto?>)"></div>
								<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
									<i class="fa fa-pen icon-sm text-muted"></i>
									<input type="file" id="profile_avatar" name="profile_avatar" accept=".png, .jpg, .jpeg" />
									<input type="hidden" id="profile_avatar_remove" name="profile_avatar_remove" />
								</label>
								<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
									<i class="ki ki-bold-close icon-xs text-muted"></i>
								</span>
							</div>
							<span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>No Rekam Medis:</label>
							<input type="text" class="form-control" placeholder="Isi MRN" name="inp_mrn"  id="inp_mrn" value="<?php echo $pas_mrn;?>" <?php echo $readonly_mrn;?>/>
							<input type="hidden" id="mrn_exist" name="mrn_exist" value="">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6 typeahead" id="d_cr_nama">
							<label>Pemilik:</label>
							<select class="form-control select2" name="inp_pemilik"  id="inp_pemilik" required >
								<option label="Label"></option>
								<?=$comboPemilik;?>
							</select>
						</div>
						<div class="col-lg-6">
							<label>Nama Pasien/Hewan:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Lengkap" name="inp_nama"  id="inp_nama" value="<?php echo $pas_nama;?>" required/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Spesies:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-chain"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_spesies"  id="inp_spesies" required >
									<option label="Label"></option>
									<?=$comboSpesies;?>
								</select>
							</div>
						</div>
						<div class="col-lg-2">
							<label>Berat (Kg)</label>
							<input type="number" class="form-control"  name="inp_berat"  id="inp_berat" step="0.01" value="<?php echo $pas_berat;?>" required/>
						</div>
						<div class="col-lg-2">
							<label>Jenis Kelamin:</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_jk"  id="inp_jantan" <?php echo $checkedJantan;?> value="J"/>Jantan
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_jk"  id="inp_betina" <?php echo $checkedBetina;?> value="B" />Betina
								<span></span></label>
							</div>
						</div>
						<div class="col-lg-2">
							<label>Jenis:</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_jns"  id="inp_jns_ras" <?php echo $checkedRas;?> value="1"/>Ras
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_jns"  id="inp_jsn_dome" <?php echo $checkedDomestik;?> value="2" />Domestik
								<span></span></label>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Ras:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-chain"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_ras"  id="inp_ras" required >
									<option label="Label"></option>
									<?=$comboRas;?>
								</select>
							</div>
						</div>
						<div class="col-lg-2">
							<label>Tgl. Lahir:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Tanggal Lahir" name="inp_tgl_lahir"  id="inp_tgl_lahir" value="<?php echo $pas_tanggal_lahir;?>" required />
							</div>
						</div>
						<div class="col-lg-2">
							<label>Umur Tahun</label>
							<input type="number" class="form-control"  name="inp_umur_tahun"  id="inp_umur_tahun" value="<?php echo $pas_umur_tahun;?>" required/>
						</div>
						<div class="col-lg-2">
							<label>Bulan</label>
							<input type="number" class="form-control"  name="inp_umur_bulan"  id="inp_umur_bulan" value="<?php echo $pas_umur_bulan;?>" required/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Warna:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-chain"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_warna"  id="inp_warna" required >
									<option label="Label"></option>
									<?=$comboWarna;?>
								</select>
							</div>
						</div>
						<div class="col-lg-2">
							<label>Terakhir Vaksin:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Terakhir Vaksin" name="inp_last_vaksin"  id="inp_last_vaksin" value="<?php echo $pas_last_vaksin;?>" />
							</div>
						</div>

						<div class="col-lg-4">
							<label>Tanda Khusus:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-chain"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_tanda_khusus"  id="inp_tanda_khusus" required >
									<option label="Label"></option>
									<?=$comboTndKhusus;?>
								</select>
							</div>
						</div>
					</div>
				</form>  
			</div>
		</div>
	</div>
</div>

<script>
var arrows;
if (KTUtil.isRTL()) {
	arrows = {
		leftArrow: '<i class="la la-angle-right"></i>',
		rightArrow: '<i class="la la-angle-left"></i>'
	}
} else {
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
}

jQuery(document).ready(function() {

	$('#inp_last_vaksin').datepicker({
		rtl: KTUtil.isRTL(),
		orientation: "top left",
		todayHighlight: true,
		templates: arrows,
		format: 'dd-mm-yyyy',
	});

	$('#inp_tgl_lahir').datepicker({
		rtl: KTUtil.isRTL(),
		orientation: "top left",
		todayHighlight: true,
		templates: arrows,
		format: 'dd-mm-yyyy',
	});

	$('#inp_tgl_lahir').blur(function(){
		var today = new Date();
		var d1 = $('#inp_tgl_lahir').val();
		var d2 = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
		if(d1 != ""){
			var form_data = {};
			$.ajax({
				url:"<?php echo base_url() ?>/Ajax/get_data/umur_bulan/?d1="+d1+"&d2="+d2,
				type: 'POST',
				data: form_data,
				success: function(data){
					if($.parseJSON(data)['data'] != null){
						var total_bulan = $.parseJSON(data)['data'].umur;
						var umur_tahun	= Math.floor(total_bulan/12);
						var umur_bulan	= total_bulan%12;
						 $('#inp_umur_tahun').val(umur_tahun);
						 $('#inp_umur_bulan').val(umur_bulan);
					}
				}
			});
		}
	});

	$('#inp_umur_tahun').blur(function(){
		var tahun = $('#inp_umur_tahun').val();
		var bulan = $('#inp_umur_bulan').val();
		if(tahun== '' || tahun=='undefine'){
			tahun= 0;
		}
		if(bulan== '' || bulan=='undefine'){
			bulan= 0;
		}

		if(tahun != "" || bulan != ""){
			var form_data = {};
			$.ajax({
				url:"<?php echo base_url() ?>/Ajax/get_data/get_tanggal/?tahun="+tahun+"&bulan="+bulan,
				type: 'POST',
				data: form_data,
				success: function(data){
					if($.parseJSON(data)['data'] != null){
						var tanggal_lahir = $.parseJSON(data)['data'].tanggal_lahir;
						$('#inp_tgl_lahir').val(tanggal_lahir);
					}
				}
			});
		}
	});

	$('#inp_umur_bulan').blur(function(){
		var tahun = $('#inp_umur_tahun').val();
		var bulan = $('#inp_umur_bulan').val();
		if(tahun== '' || tahun=='undefine'){
			tahun= 0;
		}
		if(bulan== '' || bulan=='undefine'){
			bulan= 0;
		}

		if(tahun != "" || bulan != ""){
			var form_data = {};
			$.ajax({
				url:"<?php echo base_url() ?>/Ajax/get_data/get_tanggal/?tahun="+tahun+"&bulan="+bulan,
				type: 'POST',
				data: form_data,
				success: function(data){
					if($.parseJSON(data)['data'] != null){
						var tanggal_lahir = $.parseJSON(data)['data'].tanggal_lahir;
						$('#inp_tgl_lahir').val(tanggal_lahir);
					}
				}
			});
		}
	});

	$('#inp_mrn').blur(function(){
		var form_data = {};
		var mrn = $('#inp_mrn').val();
		$.ajax({
			url:"<?php echo base_url().$class.'/'.$method;?>/get_data_mrn/?mrn="+mrn+"&rNum=<?php echo $rNum?>",
			type: 'POST',
			data: form_data,
			success: function(data){
				var data = $.parseJSON(data)['data'];
				if(data === null){
					$( "#mrn_exist" ).val(0);
					$( "#message_exist" ).remove();
				}
				else{
					$( "#mrn_exist" ).val(data.pas_id);
					$( "#message_exist" ).remove();
					$( "#inp_mrn" ).after('<div id="message_exist" class="fv-plugins-message-container"><div data-field="inp_mrn" data-validator="stringLength" class="fv-help-block">No Rekam Medis tersebut sudah terdaftar</div></div>');
				}
			}
		});
	});

	$('#inp_pemilik').select2({
		placeholder: "Pilih Pemilik",
		allowClear: true
	});

	$('#inp_spesies').select2({
		placeholder: "Pilih Spesies",
		allowClear: true
	});

	$('#inp_spesies').on('change', function(){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_ras",
				type: "POST",
				data: {'sel_id' : sel_id},
				dataType: 'json',
				success: function(data){
					$('#inp_ras').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

	$('#inp_ras').select2({
		placeholder: "Pilih Ras",
		allowClear: true
	});

	$('#inp_warna').select2({
		placeholder: "Pilih Warna",
		allowClear: true
	});

	$('#inp_tanda_khusus').select2({
		placeholder: "Pilih Tanda Khusus",
		allowClear: true
	});

    const save_continue_button	= document.getElementById('save_continue_button');
    const save_new_button		= document.getElementById('save_new_button');
    const save_exit_button		= document.getElementById('save_exit_button');
	const inputForm = document.getElementById('form_input');
    const fv = FormValidation.formValidation(inputForm, {
        fields: {
            inp_pemilik: {
				validators: {
					notEmpty: {
                        message: 'Pemilik wajib diisi'
					},
				}
            },
            inp_nama: {
                validators: {
                    notEmpty: {
                        message: 'Nama Lengkap wajib diisi'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'Nama Lengkap minimal 3 karakter dan maksimal 30 karakter'
                    }
                }
            },
			inp_spesies: {
				validators: {
					notEmpty: {
                        message: 'Spesies wajib diisi'
					},
				}
			},
			inp_tgl_lahir: {
				validators: {
					notEmpty: {
                        message: 'Tgl. Lahir wajib diisi'
					},
				}
			}
        },
        plugins: {
			trigger: new FormValidation.plugins.Trigger(),
			bootstrap: new FormValidation.plugins.Bootstrap(),

        },
    }).on('core.form.validating', function() {
    });

    save_continue_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			var mrn = $( "#mrn_exist" ).val();
			if(mrn > 0){
				status = '';
				$( "#message_exist" ).remove();
				$( "#inp_mrn" ).after('<div id="message_exist" class="fv-plugins-message-container"><div data-field="inp_mrn" data-validator="stringLength" class="fv-help-block">No Rekam Medis tersebut sudah terdaftar</div></div>');
			}
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_continue';
				document.forms["form_input"].submit();
			}
        });
    });

    save_new_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			var mrn = $( "#mrn_exist" ).val();
			if(mrn > 0){
				status = '';
				$( "#message_exist" ).remove();
				$( "#inp_mrn" ).after('<div id="message_exist" class="fv-plugins-message-container"><div data-field="inp_mrn" data-validator="stringLength" class="fv-help-block">No Rekam Medis tersebut sudah terdaftar</div></div>');
			}
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_new';
				document.forms["form_input"].submit();
			}
        });
    });

    save_exit_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			var mrn = $( "#mrn_exist" ).val();
			if(mrn > 0){
				status = '';
				$( "#message_exist" ).remove();
				$( "#inp_mrn" ).after('<div id="message_exist" class="fv-plugins-message-container"><div data-field="inp_mrn" data-validator="stringLength" class="fv-help-block">No Rekam Medis tersebut sudah terdaftar</div></div>');
			}
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_exit';
				document.forms["form_input"].submit();
			}
        });
    });
});

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}
</script>
<script src="assets/js/pages/crud/file-upload/image-input.js?v=7.0.4"></script>

