<style>
table {
	table-layout: fixed;
}
.select2-container {
width: 100% !important;
padding: 0;
}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Kode Rekening
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Kode Rekening (Chart of Account)</span></h3>
				</div>
			</div>
			<div class="card-body">
				<table id="table_hd" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[500, 1000, 5000]" data-page-size="500" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th colspan="11">Kode akun</th>
							<th data-align="center">Klinik</th>
							<th data-align="center">Lab & Rad</th>
							<th data-align="center">Petcare</th>
							<th data-align="center">Apotek</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($query_hd->result() as $row_hd){
						?>
						<tr class="tr-class">
							<td><?php echo $row_hd->kd_rkng_id; ?></td>
							<td colspan="15"><?php echo $row_hd->kd_rkng_kode; ?> - <?php echo $row_hd->kd_rkng_nama; ?></td>
						</tr>
						<?php
							$query_child1	= $this->db->query("SELECT * FROM kode_rekening WHERE kd_rkng_parent =".$row_hd->kd_rkng_id." ORDER BY kd_rkng_kode");
							foreach($query_child1->result() as $row_child1){
						?>
							<tr class="tr-class">
								<td><?php echo $row_child1->kd_rkng_id; ?></td>
								<td></td>
								<td colspan="14"><?php echo $row_child1->kd_rkng_kode; ?> - <?php echo $row_child1->kd_rkng_nama; ?></td>
							</tr>
						<?php
								$query_child2	= $this->db->query("SELECT * FROM kode_rekening WHERE kd_rkng_parent =".$row_child1->kd_rkng_id." ORDER BY kd_rkng_kode");
								foreach($query_child2->result() as $row_child2){
						?>
								<tr class="tr-class">
									<td><?php echo $row_child2->kd_rkng_id; ?></td>
									<td></td>
									<td class="text-center">
										<span class="dropdown">
											<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
											  <i class="la la-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="#" data-toggle="modal" data-target="#formCoaModal" data-tier="3" data-coa-parent="<?php  echo $row_child2->kd_rkng_id; ?>" data-coa-id=""><i class="fas fa-pen-square"></i>&nbsp;&nbsp;Tambah</a>
												<a class="dropdown-item" href="#" data-toggle="modal" data-target="#formCoaModal" data-tier="3" data-coa-id="<?php  echo $row_child2->kd_rkng_id; ?>"><i class="fas fa-pen-square"></i>&nbsp;&nbsp;Ubah</a>
												<a class="dropdown-item delete" data-coa-id="<?php echo $row_child2->kd_rkng_id ?>"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus</a>
											</div>
										</span>
									</td>
									<td colspan="13"><?php echo $row_child2->kd_rkng_kode; ?> - <?php echo $row_child2->kd_rkng_nama; ?></td>
								</tr>
						<?php
									$query_child3	= $this->db->query("SELECT * FROM v_kode_rekening WHERE kd_rkng_parent =".$row_child2->kd_rkng_id." ORDER BY kode4");
									foreach($query_child3->result() as $row_child3){
										($row_child3->kd_rkng_klinik == 't') ? $checkKlinik = 'x' : $checkKlinik = '';
										($row_child3->kd_rkng_labrad == 't') ? $checkLabrad = 'x' : $checkLabrad = '';
										($row_child3->kd_rkng_petcare == 't') ? $checkPetcare = 'x' : $checkPetcare = '';
										($row_child3->kd_rkng_apotek == 't') ? $checkApotek = 'x' : $checkApotek = '';
						?>
									<tr class="tr-class">
										<td><?php echo $row_child3->kd_rkng_id; ?></td>
										<td></td>
										<td></td>
										<td class="text-center">
											<span class="dropdown">
												<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
												  <i class="la la-ellipsis-h"></i>
												</a>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#" data-toggle="modal" data-target="#formCoaModal" data-tier="4" data-coa-id="<?php  echo $row_child3->kd_rkng_id; ?>"><i class="fas fa-pen-square"></i>&nbsp;&nbsp;Ubah</a>
													<a class="dropdown-item delete" data-coa-id="<?php echo $row_child3->kd_rkng_id ?>"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus</a>
												</div>
											</span>
										</td>
										<td colspan="8"><?php echo $row_child3->kd_rkng_kode; ?> - <?php echo $row_child3->kd_rkng_nama; ?></td>
										<td><?php echo $checkKlinik; ?></td>
										<td><?php echo $checkLabrad; ?></td>
										<td><?php echo $checkPetcare; ?></td>
										<td><?php echo $checkApotek; ?></td>
									</tr>
						<?php
									
									}

								}
							}
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
		<!--end::Card-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<!-- MODAL COA CHILD 3 -->
<div class="modal fade" id="formCoaModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_coa" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud/">
				<div class="modal-header">
					<h5 class="modal-title" id="modalCoa">Kode Akun</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body" id="modal_body"></div>
				<div class="modal-footer">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_hd" name="submit_simpan_hd" value="simpan_hd">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<script type="text/javascript">

	jQuery(document).ready(function() {
		$('#formCoaModal').on('show.bs.modal', function(e) {
			var tier		= $(e.relatedTarget).data('tier');
			var coa_parent	= $(e.relatedTarget).data('coa-parent');
			var coa_id		= $(e.relatedTarget).data('coa-id');
			$.ajax({url: '<?php echo base_url().$class;?>/modalcoa/?tier='+tier+'&coa_parent='+coa_parent+'&coa_id='+coa_id, success: function(result){
				$("#modal_body").html(result);
	
				const inputForm = document.getElementById('form_header');
				const fv = FormValidation.formValidation(inputForm, {
					fields: {
						 inp_nama: {
							validators: {
								notEmpty: {
									message: 'Nama Akun wajib diisi'
								}
							}
						},
					},
					plugins: {
						trigger: new FormValidation.plugins.Trigger(),
						bootstrap: new FormValidation.plugins.Bootstrap(),

					},
				}).on('core.form.validating', function() {
				});

				const save_exit_button	= document.getElementById('submit_simpan_hd');
				save_exit_button.addEventListener('click', function() {
					fv.validate().then(function(status) {
						if(status=='Valid'){
							document.getElementById("action_crud").value = 'save_exit';
							document.forms["form_coa"].submit();
						}
					});
				});
			}});
		});

		$('.delete').click(function(){
			var coa_id	= $(this).attr('data-coa-id');
			Swal.fire({
				title: "Anda Yakin?",
				text: "Anda tidak akan dapat mengembalikan ini!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Ya, hapus!"
			}).then(function(result) {
				if(result.value==true){
					window.location.href = "<?php echo base_url().$class.'/'.$method ?>/delete/?coa_id="+coa_id;
				}
			});
		}); 

	});
		
</script>
