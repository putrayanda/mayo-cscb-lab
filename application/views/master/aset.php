<style>
table {
	table-layout: fixed;
}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Aset
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Kepemilikan Aset</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum > 0){
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fas fa-car icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<?php
						if($aktiva_aktif =='t'){
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/disabled/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-car icon-md"></i>
							Non Aktifkan
						</a>
					<?php 
						}
						else{
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/enabled/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-car icon-md"></i>
							Aktifkan
						</a>
					<?php
						}
					?>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
						<i class="fas fa-car icon-md"></i>
						Hapus
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fas fa-car icon-md"></i>
						Tambah Data
					</a>					
					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<table id="table_hd" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="125">No. Aset</th>
							<th data-sortable="true" data-width="300">Nama Aset</th>
							<th data-sortable="true" data-width="200">Jenis Aset</th>
							<th data-sortable="true" data-width="200">Organisasi</th>
							<th data-sortable="true" data-width="200">Cabang</th>
							<th data-sortable="true" data-width="150">Tgl. Perolehan</th>
							<th data-sortable="true" data-width="150" data-align="right">Nilai Perolehan</th>
							<th data-sortable="true" data-width="150" data-align="right">Umur Ekonomis</th>
							<th data-sortable="true" data-width="125" data-align="right">Nilai Sisa</th>
							<th data-sortable="true" data-width="125" data-align="right">Umur Aktiva</th>
							<th data-sortable="true" data-width="125" data-align="right">Nilai Buku</th>
							<th data-sortable="true" data-width="125" data-align="right">Nilai Susut</th>
							<th data-sortable="true" data-width="225">Akun Biaya Penyusutan</th>
							<th data-sortable="true" data-width="225">Akun Akumulasi Penyusutan</th>
							<th data-sortable="true" data-width="85" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_hd){
							$no++;
							$aktiva_id		= $row_hd->aktiva_id;
							if($rNum == $aktiva_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}


							if($row_hd->aktiva_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $aktiva_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_hd->aktiva_nomor; ?></td>
							<td><?php echo $row_hd->aktiva_nama; ?></td>
							<td><?php echo $row_hd->jenis_aset_nama; ?></td>
							<td><?php echo $row_hd->org_nama; ?></td>
							<td><?php echo $row_hd->ref_cab_nama; ?></td>
							<td><?php echo $row_hd->aktiva_tanggal_peroleh; ?></td>
							<td><?php echo number_format($row_hd->aktiva_nilai_peroleh, 2, '.', ','); ?></td>
							<td><?php echo $row_hd->aktiva_umur_ekonomis; ?></td>
							<td><?php echo number_format($row_hd->aktiva_nilai_sisa, 2, '.', ','); ?></td>
							<td><?php echo number_format($row_hd->umur_aktiva, 2, '.', ','); ?></td>
							<td><?php echo number_format($row_hd->aktiva_nilai_buku, 2, '.', ','); ?></td>
							<td><?php echo number_format($row_hd->nilai_susut, 2, '.', ','); ?></td>
							<td><?php echo $row_hd->biaya_susut_nama; ?></td>
							<td><?php echo $row_hd->akum_susut_nama; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
		<!--end::Card-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<script type="text/javascript">
	$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
	});
</script>
