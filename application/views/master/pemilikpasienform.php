<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Input Pemilik
					<i class="mr-2"></i>
					<small class="">Untuk menambah Pemilik baru pada hewan tersebut</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method.'/?rNum='.$rNum;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
					<div class="btn-group">
						<button type="button" class="btn btn-primary font-weight-bolder">
						<i class="ki ki-check icon-sm"></i>Save Form</button>
						<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
						<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
							<ul class="nav nav-hover flex-column">
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_continue_button">
										<i class="nav-icon flaticon2-reload"></i>
										<span class="nav-text">Save &amp; continue</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_new_button">
										<i class="nav-icon flaticon2-add-1"></i>
										<span class="nav-text">Save &amp; add new</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_exit_button">
										<i class="nav-icon flaticon2-power"></i>
										<span class="nav-text">Save &amp; exit</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form id="form_input" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_pemilik/?rNum=<?php echo $rNum?>&rNum2=<?php echo $rNum2?>">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<input type="hidden" id="inp_pem_id" name="inp_pem_id"  value=""/>
					
					<?php
					if(empty($rNum2)){
					?>
					<div class="form-group row">
						<div class="col-lg-3">
							<label>Status :</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_status"  id="inp_status_baru" value="1" checked="checked" />Baru
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_status"  id="inp_status_lama" value="2" />Lama
								<span></span></label>
							</div>
						</div>
						<div class="col-lg-3 typeahead" id="d_cr_nama">
							<label>Cari Pemilik:</label>
							<input type="text" class="form-control" placeholder="Cari Berdasarkan Nama" name="cr_nama"  id="cr_nama" value=""/>
						</div>
						<div class="col-lg-3 typeahead" id="d_cr_nik">
							<label>&nbsp;</label>
							<input type="text" class="form-control" placeholder="Cari Berdasarkan NIK" name="cr_nik"  id="cr_nik" value=""/>
						</div>
						<div class="col-lg-3 typeahead" id="d_cr_hp">
							<label>&nbsp;</label>
							<input type="text" class="form-control" placeholder="Cari Berdasarkan No HP" name="cr_hp"  id="cr_hp" value=""/>
						</div>
					</div>
					<?php
					}
					?>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Nama Lengkap:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Lengkap" name="inp_nama"  id="inp_nama" value="<?php echo $pem_nama;?>" required/>
						</div>
						<div class="col-lg-3">
							<label>NIK:</label>
							<input type="text" class="form-control" placeholder="Isi NIK" name="inp_nik"  id="inp_nik" value="<?php echo $pem_nik;?>" required/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Alamat Email:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-at"></i>
									</span>
								</div>
								<input type="email" class="form-control" placeholder="Isi Email" name="inp_email"  id="inp_email" value="<?php echo $pem_email;?>"/>
							</div>
						</div>
						<div class="col-lg-6">
							<label>No. HP/WA:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-phone"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi No. HP" name="inp_hp"  id="inp_hp" value="<?php echo $pem_no_hp_wa;?>" required/>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-3">
							<label>Tempat:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-map-marker"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Tempat Lahir" name="inp_tmpt_lhr"  id="inp_tmpt_lhr" value="<?php echo $pem_tempat_lahir;?>" required/>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Tanggal Lahir:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Tanggal Lahir" name="inp_tgl_lhr"  id="inp_tgl_lhr" value="<?php echo $pem_tanggal_lahir;?>" required />
							</div>
						</div>
						<div class="col-lg-3">
							<label>Jenis Kelamin:</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_jk"  id="inp_pria" <?php echo $checkedPria;?> value="P"/>Pria
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_jk"  id="inp_wanita" <?php echo $checkedWanita;?> value="W" />Wanita
								<span></span></label>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Asal:</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_asal"  id="inp_non" <?php echo $checkedNonKom;?> value="1"/>Non Komunitas
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_asal"  id="inp_kom" <?php echo $checkedKom;?> value="2" />Komunitas
								<span></span></label>
							</div>
						</div>

					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-12">
							<label>Alamat:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-map-marker"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Alamat" name="inp_almt"  id="inp_almt" value="<?php echo $pem_alamat;?>" required/>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Propinsi:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-map-marker"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_prop"  id="inp_prop" required >
									<option label="Label"></option>
									<?=$comboPropinsi;?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<label>Kota:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-map-marker"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_kota"  id="inp_kota" required >
									<option label="Label"></option>
									<?=$comboKota;?>
								</select>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Kecamatan:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-map-marker"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_kcmtn"  id="inp_kcmtn" required >
									<option label="Label"></option>
									<?=$comboKecamatan;?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
				</form>  
			</div>
		</div>
	</div>
</div>

<script>
var arrows;
if (KTUtil.isRTL()) {
	arrows = {
		leftArrow: '<i class="la la-angle-right"></i>',
		rightArrow: '<i class="la la-angle-left"></i>'
	}
} else {
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
}

jQuery(document).ready(function() {
	$('#d_cr_nama').hide();
	$('#d_cr_nik').hide();
	$('#d_cr_hp').hide();

	$("input[name='inp_status']").change(function(){
		if($(this).val() == 2){
			$('#d_cr_nama').show();
			$('#d_cr_nik').show();
			$('#d_cr_hp').show();
		}
		else{
			$("#inp_pem_id").val(0);
			$('#d_cr_nama').hide();
			$('#d_cr_nik').hide();
			$('#d_cr_hp').hide();
		}
	});

	var substringMatcher = function(strs) {
		return function findMatches(q, cb) {
			var matches, substringRegex;

			// an array that will be populated with substring matches
			matches = [];

			// regex used to determine if a string contains the substring `q`
			substrRegex = new RegExp(q, 'i');

			// iterate through the pool of strings and for any string that
			// contains the substring `q`, add it to the `matches` array
			$.each(strs, function(i, str) {
				if (substrRegex.test(str)) {
					matches.push(str);
				}
			});

			cb(matches);
		};
	};

	var pem_nama = [<?= $var_pem_nama; ?>];
	$('#cr_nama').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'Nama',
		source: substringMatcher(pem_nama)
	});

	jQuery('#cr_nama').on('typeahead:selected', function (e, value) {
		var negara = 1;
		var form_data = {};
		$.ajax({
			url:"<?php echo base_url().$class.'/'.$method;?>/get_data_nama/?nama="+value,
			type: 'POST',
			data: form_data,
			success: function(data){

				$("#inp_pem_id").val($.parseJSON(data)['data'].pem_id);
				$("#inp_nama").val($.parseJSON(data)['data'].pem_nama);
				$("#inp_nik").val($.parseJSON(data)['data'].pem_nik);
				$("#inp_email").val($.parseJSON(data)['data'].pem_email);
				$("#inp_hp").val($.parseJSON(data)['data'].pem_no_hp_wa);
				$("#inp_tmpt_lhr").val($.parseJSON(data)['data'].pem_tempat_lahir);
				$("#inp_tgl_lhr").val($.parseJSON(data)['data'].pem_tanggal_lahir);
				$("#inp_almt").val($.parseJSON(data)['data'].pem_alamat);
			
				$("input[name=inp_jk][value="+$.parseJSON(data)['data'].pem_gender+"]").attr("checked", "checked");
				$("input[name=inp_asal][value="+$.parseJSON(data)['data'].pem_asal+"]").attr("checked", "checked");

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_propinsi",
					type: "POST",
					data: {'sel_id' : negara,'set_id' : $.parseJSON(data)['data'].ref_prop_id},
					dataType: 'json',
					success: function(data){
						$('#inp_prop').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_kota",
					type: "POST",
					data: {'sel_id' : $.parseJSON(data)['data'].ref_prop_id,'set_id' : $.parseJSON(data)['data'].ref_kota_id},
					dataType: 'json',
					success: function(data){
						$('#inp_kota').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_kcmtn",
					type: "POST",
					data: {'sel_id' : $.parseJSON(data)['data'].ref_kota_id,'set_id' : $.parseJSON(data)['data'].pem_kecamatan},
					dataType: 'json',
					success: function(data){
						$('#inp_kcmtn').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				//$("#inp_prop").val($.parseJSON(data)['data'].ref_prop_id).change();
				//$("#inp_kota").val($.parseJSON(data)['data'].ref_kota_id).change();
				//$("#inp_kcmtn").val($.parseJSON(data)['data'].pem_kecamatan).change();
			}
		});
	});

	var pem_nik = [<?= $var_pem_nik; ?>];
	$('#cr_nik').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'NIK',
		source: substringMatcher(pem_nik)
	});

	jQuery('#cr_nik').on('typeahead:selected', function (e, value) {
		var negara = 1;
		var form_data = {};
		$.ajax({
			url:"<?php echo base_url().$class.'/'.$method;?>/get_data_nik/?nik="+value,
			type: 'POST',
			data: form_data,
			success: function(data){

				$("#inp_pem_id").val($.parseJSON(data)['data'].pem_id);
				$("#inp_nama").val($.parseJSON(data)['data'].pem_nama);
				$("#inp_nik").val($.parseJSON(data)['data'].pem_nik);
				$("#inp_email").val($.parseJSON(data)['data'].pem_email);
				$("#inp_hp").val($.parseJSON(data)['data'].pem_no_hp_wa);
				$("#inp_tmpt_lhr").val($.parseJSON(data)['data'].pem_tempat_lahir);
				$("#inp_tgl_lhr").val($.parseJSON(data)['data'].pem_tanggal_lahir);
				$("#inp_almt").val($.parseJSON(data)['data'].pem_alamat);
			
				$("input[name=inp_jk][value="+$.parseJSON(data)['data'].pem_gender+"]").attr("checked", "checked");
				$("input[name=inp_asal][value="+$.parseJSON(data)['data'].pem_asal+"]").attr("checked", "checked");

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_propinsi",
					type: "POST",
					data: {'sel_id' : negara,'set_id' : $.parseJSON(data)['data'].ref_prop_id},
					dataType: 'json',
					success: function(data){
						$('#inp_prop').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_kota",
					type: "POST",
					data: {'sel_id' : $.parseJSON(data)['data'].ref_prop_id,'set_id' : $.parseJSON(data)['data'].ref_kota_id},
					dataType: 'json',
					success: function(data){
						$('#inp_kota').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_kcmtn",
					type: "POST",
					data: {'sel_id' : $.parseJSON(data)['data'].ref_kota_id,'set_id' : $.parseJSON(data)['data'].pem_kecamatan},
					dataType: 'json',
					success: function(data){
						$('#inp_kcmtn').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				//$("#inp_prop").val($.parseJSON(data)['data'].ref_prop_id).change();
				//$("#inp_kota").val($.parseJSON(data)['data'].ref_kota_id).change();
				//$("#inp_kcmtn").val($.parseJSON(data)['data'].pem_kecamatan).change();
			}
		});
	});

	var pem_no_hp_wa = [<?= $var_pem_no_hp_wa; ?>];
	$('#cr_hp').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'HP',
		source: substringMatcher(pem_no_hp_wa)
	});

	jQuery('#cr_hp').on('typeahead:selected', function (e, value) {
		var negara = 1;
		var form_data = {};
		$.ajax({
			url:"<?php echo base_url().$class.'/'.$method;?>/get_data_nohp/?nohp="+value,
			type: 'POST',
			data: form_data,
			success: function(data){

				$("#inp_pem_id").val($.parseJSON(data)['data'].pem_id);
				$("#inp_nama").val($.parseJSON(data)['data'].pem_nama);
				$("#inp_nik").val($.parseJSON(data)['data'].pem_nik);
				$("#inp_email").val($.parseJSON(data)['data'].pem_email);
				$("#inp_hp").val($.parseJSON(data)['data'].pem_no_hp_wa);
				$("#inp_tmpt_lhr").val($.parseJSON(data)['data'].pem_tempat_lahir);
				$("#inp_tgl_lhr").val($.parseJSON(data)['data'].pem_tanggal_lahir);
				$("#inp_almt").val($.parseJSON(data)['data'].pem_alamat);
			
				$("input[name=inp_jk][value="+$.parseJSON(data)['data'].pem_gender+"]").attr("checked", "checked");
				$("input[name=inp_asal][value="+$.parseJSON(data)['data'].pem_asal+"]").attr("checked", "checked");

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_propinsi",
					type: "POST",
					data: {'sel_id' : negara,'set_id' : $.parseJSON(data)['data'].ref_prop_id},
					dataType: 'json',
					success: function(data){
						$('#inp_prop').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_kota",
					type: "POST",
					data: {'sel_id' : $.parseJSON(data)['data'].ref_prop_id,'set_id' : $.parseJSON(data)['data'].ref_kota_id},
					dataType: 'json',
					success: function(data){
						$('#inp_kota').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_kcmtn",
					type: "POST",
					data: {'sel_id' : $.parseJSON(data)['data'].ref_kota_id,'set_id' : $.parseJSON(data)['data'].pem_kecamatan},
					dataType: 'json',
					success: function(data){
						$('#inp_kcmtn').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});

				//$("#inp_prop").val($.parseJSON(data)['data'].ref_prop_id).change();
				//$("#inp_kota").val($.parseJSON(data)['data'].ref_kota_id).change();
				//$("#inp_kcmtn").val($.parseJSON(data)['data'].pem_kecamatan).change();
			}
		});
	});

	$('#inp_tgl_lhr').datepicker({
		rtl: KTUtil.isRTL(),
		orientation: "top left",
		todayHighlight: true,
		templates: arrows,
		format: 'dd/mm/yyyy',
	});

	$('#inp_prop').select2({
		placeholder: "Pilih Propinsi",
		allowClear: true
	});

	$('#inp_kota').select2({
		placeholder: "Pilih Kota/Kabupaten",
		allowClear: true
	});

	$('#inp_kcmtn').select2({
		placeholder: "Pilih Kecamatan",
		allowClear: true
	});		

	$('#inp_prop').on('change', function(id){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_kota",
				type: "POST",
				data: {'sel_id' : sel_id, 'set_id' : 0},
				dataType: 'json',
				success: function(data){
					$('#inp_kota').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

	$('#inp_kota').on('change', function(){
		var sel_id = $(this).val();
		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_kcmtn",
				type: "POST",
				data: {'sel_id' : sel_id, 'set_id' : 0},
				dataType: 'json',
				success: function(data){
					$('#inp_kcmtn').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

	$( "#inp_email" ).blur(function() {
		var rNum		= "<?php echo $rNum?>";
		var inp_email	= $( "#inp_email" ).val();
		var form_data	= {};
		$.ajax({
			url:"<?php echo base_url() ?>/Ajax/get_data/pemilik_by_email/?email="+inp_email+"&rNum="+rNum,
			type: 'POST',
			data: form_data,
			success: function(data){
				if($.parseJSON(data)['data'] != null){
					if($.parseJSON(data)['data'].pem_id > 0){
						$( "#div_email" ).after('<div class="fv-plugins-message-container"><div data-field="inp_email" data-validator="emailAddress" class="fv-help-block">Email Sudah Terdaftar</div></div>');
						$( "#inp_email" ).addClass( "is-invalid" );
						//$( "#inp_email" ).val("");
					}
				}
			}
		});
	});

    const save_continue_button	= document.getElementById('save_continue_button');
    const save_new_button		= document.getElementById('save_new_button');
    const save_exit_button		= document.getElementById('save_exit_button');
	const inputForm = document.getElementById('form_input');
    const fv = FormValidation.formValidation(inputForm, {
        fields: {
            inp_nama: {
                validators: {
                    notEmpty: {
                        message: 'Nama Lengkap wajib diisi'
                    }
                }
            },
			inp_email: {
				validators: {
					emailAddress: {
						message: 'The value is not a valid email address'
					}
				}
			},
            inp_nik: {
                validators: {
                    notEmpty: {
                        message: 'NIK wajib diisi'
                    }
                }
            },

			inp_tgl_lhr: {
				validators: {
					notEmpty: {
                        message: 'Tgl Lahir wajib diisi'
					},
				}
			},
			inp_almt: {
				validators: {
					notEmpty: {
                        message: 'Alamat wajib diisi'
					},
				}
			},
			inp_kcmtn: {
				validators: {
					notEmpty: {
                        message: 'Kecamatan wajib diisi'
					},
				}
			},
        },
        plugins: {
			trigger: new FormValidation.plugins.Trigger(),
			bootstrap: new FormValidation.plugins.Bootstrap(),

        },
    }).on('core.form.validating', function() {
    });

    save_continue_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_continue';
				document.forms["form_input"].submit();
			}
        });
    });

    save_new_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_new';
				document.forms["form_input"].submit();
			}
        });
    });

    save_exit_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_exit';
				document.forms["form_input"].submit();
			}
        });
    });

});
</script>
