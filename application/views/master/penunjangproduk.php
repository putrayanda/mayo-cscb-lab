<style>
.select2-container {
width: 100% !important;
padding: 0;
}
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<div class="row">
			<!-- JENIS PRODUK -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Jenis Produk <span class="d-block text-muted pt-2 font-size-sm">Informasi Jenis Produk</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddJenisModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_jenis/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddJenisModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_jenis" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_jns_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Jenis Produk</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_jenis->result() as $row_jns){
									$no++;
									$ref_jns_prod_id	= $row_jns->ref_jns_prod_id;
									if($rNum == $ref_jns_prod_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_jns_prod_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_jns->ref_jns_prod_nama; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- SATUAN -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Satuan <span class="d-block text-muted pt-2 font-size-sm">Informasi Satuan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum2 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddSatuanModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_satuan/?rNum=<?php echo $rNum2?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddSatuanModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_satuan" data-toggle="table" data-height="250" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_sat_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Satuan</th>
									<th data-sortable="true" >Kode</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_satuan->result() as $row_sat){
									$no++;
									$ref_sat_id	= $row_sat->ref_sat_id;
									if($rNum2 == $ref_sat_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_sat_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_sat->ref_sat_ket; ?></td>
									<td><?php echo $row_sat->ref_sat_kode; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
		</div>
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<!-- Modal Jenis Produk -->
<div class="modal fade" id="formAddJenisModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_jenis" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_jenis/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelJenis">Tambah Jenis Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Jenis Produk:</label>
							<input type="text" class="form-control" placeholder="Isi Jenis Produk" name="inp_nama_jenis"  id="inp_nama_jenis" value="<?php echo $ref_jns_prod_nama;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_jenis" name="submit_crud_jenis" value="simpan_jenis">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Satuan -->
<div class="modal fade" id="formAddSatuanModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_satuan" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_satuan/?rNum2=<?php echo $rNum2?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelSatuan">Tambah Satuan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Satuan:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Satuan" name="inp_nama_satuan"  id="inp_nama_satuan" value="<?php echo $ref_sat_ket;?>" required/>
						</div>
						<div class="form-group">
							<label>Kode Satuan:</label>
							<input type="text" class="form-control" placeholder="Isi Kode Satuan" name="inp_kode_satuan"  id="inp_kode_satuan" value="<?php echo $ref_sat_kode;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_satuan" name="submit_crud_satuan" value="simpan_satuan">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>



<script type="text/javascript">
	$('#table_jenis').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_jns_id);
	});

	$('#table_satuan').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum2='+row.row_sat_id);
	});

</script>
