<link href="assets/bower_components/bootstrap-table/bootstrap-table.css" rel="stylesheet" type="text/css">
<link href="assets/bower_components/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<script src="assets/bower_components/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
<style>
	.select2-container {
	width: 100% !important;
	padding: 0;

	.kv-file-upload{
		color:#fff !important;
		visibility: hidden !important;
	}

	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}
}
</style>
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Input Aset
					<i class="mr-2"></i>
					<small class="">Untuk menambah/mengubah aset</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
					<div class="btn-group">
						<button type="button" class="btn btn-primary font-weight-bolder">
						<i class="ki ki-check icon-sm"></i>Save Form</button>
						<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
						<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
							<ul class="nav nav-hover flex-column">
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_continue_button">
										<i class="nav-icon flaticon2-reload"></i>
										<span class="nav-text">Save &amp; continue</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_new_button">
										<i class="nav-icon flaticon2-add-1"></i>
										<span class="nav-text">Save &amp; add new</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_exit_button">
										<i class="nav-icon flaticon2-power"></i>
										<span class="nav-text">Save &amp; exit</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form id="form_input" role="form" method="post"  enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud/?rNum=<?php echo $rNum?>">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<div class="form-group row">
						<div class="col-md-4 col-lg-4">
							<label>Cabang:</label>
							<div class="input-group ">
								<select class="form-control select2" name="inp_cab_aset"  id="inp_cab_aset" required >
									<option label="Label"></option>
									<?=$comboCabang;?>
								</select>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<label>Jenis Aset:</label>
							<div class="input-group ">
								<select class="form-control select2" name="inp_jenis_aset"  id="inp_jenis_aset" required >
									<option label="Label"></option>
									<?=$comboJenisAset;?>
								</select>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<label>Nama Aset:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Aset" name="inp_nama_aset"  id="inp_nama_aset" value="<?php echo $aktiva_nama;?>" required/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-md-2 col-lg-2">
							<label>Nomor Aset:</label>
							<input type="text" class="form-control" placeholder="Isi Nomor Aset" name="inp_no_aset"  id="inp_no_aset" value="<?php echo $aktiva_nomor;?>" required/>
						</div>
						<div class="col-md-2 col-lg-2">
							<label>Tgl. Perolehan:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-calendar"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Tgl. Perolehan" name="inp_tgl_oleh_aset"  id="inp_tgl_oleh_aset" value="<?php echo $aktiva_tanggal_peroleh;?>" required />
							</div>
						</div>
						<div class="col-md-2 col-lg-2">
							<label>Nilai Perolehan:</label>
							<input style="text-align:right;" type="number" class="form-control" placeholder="Isi Nilai Perolehan" name="inp_nilai_oleh_aset"  id="inp_nilai_oleh_aset" value="<?php echo $aktiva_nilai_peroleh;?>" required/>
						</div>
						<div class="col-md-2 col-lg-2">
							<label>Nilai Sisa:</label>
							<input style="text-align:right;" type="number" class="form-control" placeholder="Isi Nilai Sisa" name="inp_nilai_sisa_aset"  id="inp_nilai_sisa_aset" value="<?php echo $aktiva_nilai_sisa;?>" required/>
						</div>
						<div class="col-md-2 col-lg-2">
							<label>Umur Ekonomis:</label>
							<input style="text-align:right;" type="number" class="form-control" placeholder="Isi Umur Ekonomis" name="inp_umur_eko_aset"  id="inp_umur_eko_aset" value="<?php echo $aktiva_umur_ekonomis;?>" required/>
						</div>
						<div class="col-md-2 col-lg-2">
							<label>Nilai Buku</label>
							<input style="text-align:right;" type="number" class="form-control" placeholder="Isi Nilai Buku" name="inp_nilai_buku_aset"  id="inp_nilai_buku_aset" value="<?php echo $aktiva_nilai_buku;?>" required/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-md-4 col-lg-4">
							<label>Kode Biaya Penyusutan:</label>
							<div class="input-group ">
								<select class="form-control select2" name="inp_coa_biaya_aset"  id="inp_coa_biaya_aset" required >
									<option label="Label"></option>
									<?=$comboBiayaSusut;?>
								</select>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<label>Kode Akumulasi Penyusutan:</label>
							<div class="input-group ">
								<select class="form-control select2" name="inp_coa_akum_aset"  id="inp_coa_akum_aset" required >
									<option label="Label"></option>
									<?=$comboAkumSusut;?>
								</select>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<label>Susutkan?:</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_susut"  id="inp_ya_susut" <?php echo $checkedYaSusut;?> value="T"/>Ya
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_susut"  id="inp_tidak_susut" <?php echo $checkedTidakSusut;?> value="F" />Tidak
								<span></span></label>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-md-12 col-lg-12">
							<label>Foto Aset:</label>
							<div class="file-loading">
								<input type="file" name="file_hasil[]" id="file_hasil" multiple data-allowed-file-extensions='[ "jpg", "jpeg", "png", "mkv", "mp4"]' />
							</div>
							</br>
							<div class="d-flex justify-content-center">
								<?php
								foreach($query_foto->result() as $row_foto){
									$aktfv_id = $row_foto->aktfv_id;
									if(preg_match('/^.*\.(mp4|mkv)$/i', $row_foto->aktfv_rm_url)) {
										$thumbnail = "https://localhost/cscb.leonvets.id/assets/file/foto_aset/videos.png";
									}
									else{
										$thumbnail = $row_foto->aktfv_rm_url;
									}
								?>
								<div>
									<div align="center">
										<a href="<?php echo base_url().$class.'/'.$method.'/del_foto/?rNum='.$rNum.'&rNum2='.$aktfv_id ?>" class="btn btn-icon btn-outline-danger btn-xs"><i class="fas fa-trash"></i></a>
									</div>
									<a class="ml-2 mr-2" data-caption="<?php echo $row_foto->aktfv_rm_keterangan ?>" data-fancybox="gallery" href="<?php echo $row_foto->aktfv_rm_url ?>"  >
										<img width="150px" class="rounded" src="<?php echo $thumbnail ?>" />
									</a>
								</div>
								<?php
								}
								?>
							</div>
						</div>
					</div>
 					<div class="separator separator-dashed my-2"></div>
				</form>  
			</div>
		</div>
	</div>
</div>

<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
<script src="assets/js/pages/crud/file-upload/image-input.js?v=7.0.4"></script>
<script>
var arrows;
if (KTUtil.isRTL()) {
	arrows = {
		leftArrow: '<i class="la la-angle-right"></i>',
		rightArrow: '<i class="la la-angle-left"></i>'
	}
} else {
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
}

jQuery(document).ready(function() {
	
	$('#inp_tgl_oleh_aset').datepicker({
		rtl: KTUtil.isRTL(),
		orientation: "top left",
		todayHighlight: true,
		templates: arrows,
		format: 'dd/mm/yyyy',
	});

	$('#inp_cab_aset').select2({
		placeholder: "Pilih Cabang",
		allowClear: true
	});

	$('#inp_jenis_aset').select2({
		placeholder: "Pilih Jenis Aset",
		allowClear: true
	});

	$('#inp_coa_biaya_aset').select2({
		placeholder: "Pilih COA Biaya Penyusutan",
		allowClear: true
	});

	$('#inp_coa_akum_aset').select2({
		placeholder: "Pilih COA Akumulasi Penyusutan",
		allowClear: true
	});

	$("#file_hasil").fileinput({
		'theme': 'explorer-fas',
		'showUpload': false
	});

    const save_continue_button	= document.getElementById('save_continue_button');
    const save_new_button		= document.getElementById('save_new_button');
    const save_exit_button		= document.getElementById('save_exit_button');
	const inputForm = document.getElementById('form_input');
    const fv = FormValidation.formValidation(inputForm, {
        fields: {
            inp_nama_aset: {
                validators: {
                    notEmpty: {
                        message: 'Nama Aset wajib diisi'
                    },
                    stringLength: {
                        min: 5,
                        message: 'Nama Aset minimal 5 karakter'
                    }
                }
            },
			inp_tgl_oleh_aset: {
				validators: {
					notEmpty: {
                        message: 'Tgl Perolehan wajib diisi'
					},
				}
			},
        },
        plugins: {
			trigger: new FormValidation.plugins.Trigger(),
			bootstrap: new FormValidation.plugins.Bootstrap(),

        },
    }).on('core.form.validating', function() {
    });

    save_continue_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_continue';
				document.forms["form_input"].submit();
			}
        });
    });

    save_new_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_new';
				document.forms["form_input"].submit();
			}
        });
    });

    save_exit_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_exit';
				document.forms["form_input"].submit();
			}
        });
    });

});
Fancybox.bind('[data-fancybox="gallery"]', {
  //dragToClose: false,
  Thumbs: false,

  Image: {
	zoom: false,
	click: false,
	wheel: "slide",
  },

  on: {
	// Move caption inside the slide
	reveal: (f, slide) => {
	  slide.$caption && slide.$content.appendChild(slide.$caption);
	},
  },
});

</script>
