<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Input Dokter
					<i class="mr-2"></i>
					<small class="">Untuk menambah/mengubah data Dokter</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
					<div class="btn-group">
						<button type="button" class="btn btn-primary font-weight-bolder">
						<i class="ki ki-check icon-sm"></i>Save Form</button>
						<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
						<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
							<ul class="nav nav-hover flex-column">
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_continue_button">
										<i class="nav-icon flaticon2-reload"></i>
										<span class="nav-text">Save &amp; continue</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_new_button">
										<i class="nav-icon flaticon2-add-1"></i>
										<span class="nav-text">Save &amp; add new</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_exit_button">
										<i class="nav-icon flaticon2-power"></i>
										<span class="nav-text">Save &amp; exit</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form id="form_input" role="form" method="post"  enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud/?rNum=<?php echo $rNum?>">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<div class="form-group row">
						<div class="col-lg-4 col-sm-4">
							<label>Nama Dokter:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Dokter" name="inp_nama"  id="inp_nama" value="<?php echo $dok_nama;?>" required/>
						</div>
						<div class="col-lg-2 col-sm-2">
							<label>Jenis Kelamin:</label>
							<div class="radio-inline">
								<label class="radio radio-primary">
								<input type="radio" name="inp_jk"  id="inp_pria" <?php echo $checkedPria;?> value="P"/>Pria
								<span></span></label>
								<label class="radio radio-success">
								<input type="radio" name="inp_jk"  id="inp_wanita" <?php echo $checkedWanita;?> value="W" />Wanita
								<span></span></label>
							</div>
						</div>
						<div class="col-lg-3 col-sm-3">
							<label>Email:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-at"></i>
									</span>
								</div>
								<input type="email" class="form-control" placeholder="Isi Email" name="inp_email"  id="inp_email" value="<?php echo $dok_email;?>" required/>
							</div>
						</div>
						<div class="col-lg-3 col-sm-3">
							<label>No. HP/WA:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="fas fa-phone"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi No. HP/WA" name="inp_telp"  id="inp_telp" value="<?php echo $dok_no_hp_wa;?>" required/>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-12">
							<label>Alamat:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="la la-map-marker"></i>
									</span>
								</div>
								<input type="text" class="form-control" placeholder="Isi Alamat" name="inp_almt"  id="inp_almt" value="<?php echo $dok_alamat;?>" required/>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-4 col-sm-4">
							<label>Propinsi:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="fas fa-city"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_prop"  id="inp_prop" required >
									<option label="Label"></option>
									<?=$comboPropinsi;?>
								</select>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<label>Kota:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="fas fa-map-marked-alt"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_kota"  id="inp_kota" required >
									<option label="Label"></option>
									<?=$comboKota;?>
								</select>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<label>Kecamatan:</label>
							<div class="input-group ">
								<div class="input-group-prepend">
									<span class="input-group-text">
										<i class="fas fa-map-marker-alt"></i>
									</span>
								</div>
								<select class="form-control select2" name="inp_kcmtn"  id="inp_kcmtn" required >
									<option label="Label"></option>
									<?=$comboKecamatan;?>
								</select>
							</div>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
				</form>  
				<!--end::Form-->
			</div>
		</div>
		<!--end::Card-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<script>
var arrows;
if (KTUtil.isRTL()) {
	arrows = {
		leftArrow: '<i class="la la-angle-right"></i>',
		rightArrow: '<i class="la la-angle-left"></i>'
	}
} else {
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
}

jQuery(document).ready(function() {
	

	$('#inp_prop').select2({
		placeholder: "Pilih Propinsi",
		allowClear: true
	});

	$('#inp_kota').select2({
		placeholder: "Pilih Kota/Kabupaten",
		allowClear: true
	});

	$('#inp_kcmtn').select2({
		placeholder: "Pilih Kecamatan",
		allowClear: true
	});		

	$('#inp_prop').on('change', function(){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_kota",
				type: "POST",
				data: {'sel_id' : sel_id},
				dataType: 'json',
				success: function(data){
					$('#inp_kota').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

	$('#inp_kota').on('change', function(){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_kcmtn",
				type: "POST",
				data: {'sel_id' : sel_id},
				dataType: 'json',
				success: function(data){
					$('#inp_kcmtn').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

    const save_continue_button	= document.getElementById('save_continue_button');
    const save_new_button		= document.getElementById('save_new_button');
    const save_exit_button		= document.getElementById('save_exit_button');
	const inputForm = document.getElementById('form_input');
    const fv = FormValidation.formValidation(inputForm, {
        fields: {
            inp_nama: {
                validators: {
                    notEmpty: {
                        message: 'Nama Dokter wajib diisi'
                    },
                    stringLength: {
                        min: 5,
                        message: 'Nama Dokter minimal 5 karakter'
                    }
                }
            },
			inp_email: {
				validators: {
					notEmpty: {
                        message: 'Email wajib diisi'
					},
					emailAddress: {
						message: 'The value is not a valid email address'
					}
				}
			},
			inp_almt: {
				validators: {
					notEmpty: {
                        message: 'Alamat wajib diisi'
					},
				}
			},
			inp_kcmtn: {
				validators: {
					notEmpty: {
                        message: 'Kecamatan wajib diisi'
					},
				}
			},
        },
        plugins: {
			trigger: new FormValidation.plugins.Trigger(),
			bootstrap: new FormValidation.plugins.Bootstrap(),

        },
    }).on('core.form.validating', function() {
    });

    save_continue_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_continue';
				document.forms["form_input"].submit();
			}
        });
    });

    save_new_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_new';
				document.forms["form_input"].submit();
			}
        });
    });

    save_exit_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_exit';
				document.forms["form_input"].submit();
			}
        });
    });

});
</script>
<script src="assets/js/pages/crud/file-upload/image-input.js?v=7.0.4"></script>
