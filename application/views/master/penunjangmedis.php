<style>
.select2-container {
width: 100% !important;
padding: 0;
}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<div class="row">
			<!-- ANAMNESA -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Anamnesa <span class="d-block text-muted pt-2 font-size-sm">Informasi Anamnesa</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddAnamnesaModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_anamnesa/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddAnamnesaModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_anamnesa" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_anam_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Anamnesa</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_anamnesa->result() as $row_anam){
									$no++;
									$ref_anamnesis_id	= $row_anam->ref_anamnesis_id;
									if($rNum == $ref_anamnesis_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_anamnesis_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_anam->ref_anamnesis_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- DIAGNOSA -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Diagnosa <span class="d-block text-muted pt-2 font-size-sm">Informasi Diagnosa</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum2 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddDiagnosaModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_diagnosa/?rNum2=<?php echo $rNum2?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddDiagnosaModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_diagnosa" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_diag_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Diagnosa</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_diagnosa->result() as $row_diag){
									$no++;
									$ref_diag_id	= $row_diag->ref_diag_id;
									if($rNum2 == $ref_diag_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_diag_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_diag->ref_diag_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- TINDAKAN -->
			<!--div class="col-md-6">
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Tindakan <span class="d-block text-muted pt-2 font-size-sm">Informasi Tindakan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum3 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddTindakanModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_tindakan/?rNum3=<?php echo $rNum3?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddTindakanModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_tindakan" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_tind_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" data-width="100" >Kode</th>
									<th data-sortable="true" >Tindakan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_tindakan->result() as $row_tind){
									$no++;
									$ref_tind_id	= $row_tind->ref_tind_id;
									if($rNum3 == $ref_tind_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_tind_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_tind->ref_tind_kode; ?></td>
									<td><?php echo $row_tind->ref_tind_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
			</div-->
			<!-- PERAWATAN -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Perawatan <span class="d-block text-muted pt-2 font-size-sm">Informasi Perawatan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum4 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddPerawatanModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_perawatan/?rNum4=<?php echo $rNum4?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddPerawatanModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_perawatan" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_prwtn_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Perawatan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_perawatan->result() as $row_prwtn){
									$no++;
									$ref_kondisi_id	= $row_prwtn->ref_kondisi_id;
									if($rNum4 == $ref_kondisi_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_kondisi_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_prwtn->ref_kondisi_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>

			<!-- HABITUS -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Habitus <span class="d-block text-muted pt-2 font-size-sm">Informasi Habitus</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum5 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddHabitusModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_habitus/?rNum5=<?php echo $rNum5?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddHabitusModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_habitus" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_habit_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Habitus</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_habitus->result() as $row_habit){
									$no++;
									$ref_kondisi_id	= $row_habit->ref_kondisi_id;
									if($rNum5 == $ref_kondisi_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_kondisi_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_habit->ref_kondisi_ket; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- PEMERIKSAAN LANJUTAN -->
			<div class="col-md-6">
				<!--begin::Card-->
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Pemeriksaan Lanjutan <span class="d-block text-muted pt-2 font-size-sm">Informasi Pemeriksaan Lanjutan</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum6 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddPeriksaModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_periksa/?rNum6=<?php echo $rNum6?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddPeriksaModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_periksa" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_prks_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" >Pemeriksaan Lanjutan</th>
									<th data-sortable="true" >Produk</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_periksa->result() as $row_periksa){
									$no++;
									$ref_prks_lnjt_id	= $row_periksa->ref_prks_lnjt_id;
									if($rNum6 == $ref_prks_lnjt_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $ref_prks_lnjt_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_periksa->ref_prks_lnjt_ket; ?></td>
									<td><?php echo $row_periksa->ref_prod_nama; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
				<!--end::Card-->
			</div>
			<!-- PRODUK -->
			<!--div class="col-md-12">
				<div class="card card-custom gutter-b">
					<div class="card-header flex-wrap py-3">
						<div class="card-title">
							<h3 class="card-label">Data Produk <span class="d-block text-muted pt-2 font-size-sm">Informasi Produk</span></h3>
						</div>
						<div class="card-toolbar">
							<?php 
							if($rNum7 > 0){
							?>
							<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddProdukModal">
								<i class="fa fa-user-edit icon-md"></i>
								Ubah Data
							</a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url().$class.'/'.$method;?>/del_peroduk/?rNum7=<?php echo $rNum7?>" class="btn btn-danger font-weight-bolder fix150" >
								<i class="fas fa-user-times icon-md"></i>
								Hapus
							</a>
							<?php
							}
							else{
							?>
							<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddProdukModal">
								<i class="fa fa-user-plus icon-md"></i>
								Tambah Data
							</a>
							<?php
							}
							?>
						</div>
					</div>
					<div class="card-body">
						<table id="table_produk" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_prod_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="35" data-align="right">No</th>
									<th data-sortable="true" data-width="100" >Jenis</th>
									<th data-sortable="true" data-width="300" >Nama Produk</th>
									<th data-sortable="true" data-width="500" >Link Tokopedia</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_produk->result() as $row_produk){
									$no++;
									$ref_prod_id = $row_produk->ref_prod_id;
									if($rNum7 == $ref_prod_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
								<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
									<td><?php echo $ref_prod_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_produk->ref_jns_prod_nama; ?></td>
									<td><?php echo $row_produk->ref_prod_nama; ?></td>
									<td><?php echo $row_produk->ref_prod_link_toped; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
			</div-->

		</div>
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<!-- Modal Anamnesa -->
<div class="modal fade" id="formAddAnamnesaModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_anamnesa" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_anamnesa/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelAnamnesa">Tambah Anamnesa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Anamnesa:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Anamnesa" name="inp_nama_anamnesa"  id="inp_nama_anamnesa" value="<?php echo $ref_anamnesis_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_anamnesa" name="submit_crud_anamnesa" value="simpan_anamnesa">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Diagnosa -->
<div class="modal fade" id="formAddDiagnosaModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_diagnosa" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_diagnosa/?rNum2=<?php echo $rNum2?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelDiagnosa">Tambah Diagnosa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Diagnosa:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Diagnosa" name="inp_nama_diagnosa"  id="inp_nama_diagnosa" value="<?php echo $ref_diag_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_diagnosa" name="submit_crud_diagnosa" value="simpan_diagnosa">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Tindakan -->
<div class="modal fade" id="formAddTindakanModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_tindakan" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_tindakan/?rNum3=<?php echo $rNum3?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelTindakan">Tambah Tindakan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Kode:</label>
							<input type="text" class="form-control" placeholder="Isi Kode Tindakan" name="inp_kode_tindakan"  id="inp_kode_tindakan" value="<?php echo $ref_tind_kode;?>" required/>
						</div>
						<div class="form-group">
							<label>Nama Tindakan:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Tindakan" name="inp_nama_tindakan"  id="inp_nama_tindakan" value="<?php echo $ref_tind_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_tindakan" name="submit_crud_tindakan" value="simpan_tindakan">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Perawatan -->
<div class="modal fade" id="formAddPerawatanModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_perawatan" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_perawatan/?rNum4=<?php echo $rNum4?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelPerawatan">Tambah Perawatan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Perawatan:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Perawatan" name="inp_nama_perawatan"  id="inp_nama_perawatan" value="<?php echo $ref_prwtn_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_perawatan" name="submit_crud_perawatan" value="simpan_perawatan">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Habitus -->
<div class="modal fade" id="formAddHabitusModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_habitus" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_habitus/?rNum5=<?php echo $rNum5 ?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelHabitus">Tambah Habitus</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Habitus:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Habitus" name="inp_nama_habitus"  id="inp_nama_habitus" value="<?php echo $ref_habit_ket;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_habitus" name="submit_crud_habitus" value="simpan_habitus">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- Modal Pemeriksaan Lanjutan -->
<div class="modal fade" id="formAddPeriksaModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_periksa" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_periksa/?rNum6=<?php echo $rNum6 ?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelPeriksa">Tambah Pemeriksaan Lanjutan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Pemeriksaan Lanjutan:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Pemeriksaan Lanjutan" name="inp_nama_periksa"  id="inp_nama_periksa" value="<?php echo $ref_prks_lnjt_ket;?>" required/>
						</div>
						<div class="form-group">
							<label>Produk:</label>
							<select class="form-control select2" name="inp_produk"  id="inp_produk" required >
								<option label="Label"></option>
								<?=$comboProduk;?>
							</select>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_periksa" name="submit_crud_periksa" value="simpan_periksa">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>
<!-- Modal Produk -->
<div class="modal fade" id="formAddProdukModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_produk" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_produk/?rNum7=<?php echo $rNum7 ?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelProduk">Tambah Produk</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Jenis Produk:</label>
							<select class="form-control select2" name="inp_jenis_produk"  id="inp_jenis_produk" required >
								<option label="Label"></option>
								<?=$comboJenisProduk;?>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Produk:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Produk" name="inp_nama_produk"  id="inp_nama_produk" value="<?php echo $ref_prod_nama;?>" required/>
						</div>
						<div class="form-group">
							<label>Link Tokopedia:</label>
							<input type="text" class="form-control" placeholder="Isi Link Tokopedia" name="inp_link_toped_produk"  id="inp_link_toped_produk" value="<?php echo $ref_prod_link_toped;?>" required/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_produk" name="submit_crud_produk" value="simpan_produk">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#table_anamnesa').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_anam_id);
	});

	$('#table_diagnosa').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum2='+row.row_diag_id);
	});

	$('#table_tindakan').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum3='+row.row_tind_id);
	});

	$('#table_perawatan').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum4='+row.row_prwtn_id);
	});

	$('#table_habitus').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum5='+row.row_habit_id);
	});

	$('#table_periksa').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum6='+row.row_prks_id);
	});

	$('#table_produk').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum7='+row.row_prod_id);
	});

	$('#inp_produk').select2({
		placeholder: "Pilih Produk",
		allowClear: true
	});	

	$('#inp_jenis_produk').select2({
		placeholder: "Pilih Jenis Produk",
		allowClear: true
	});	
</script>
