<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<style>
	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}

	table {
		table-layout: fixed;
	}

	.modal-dialog .select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pemilik
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Pemilik Hewan/Pasien</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum > 0){
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pemilik/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-edit icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/resetpass/?rNum=<?php echo $rNum?>" class="btn btn-success font-weight-bolder fix175" >
						<i class="fa fa-user-cog icon-md"></i>
						Reset Password
					</a>
					&nbsp;&nbsp;
					<?php
						if($pem_aktif =='t'){
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/disabled_pemilik/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-user-lock icon-md"></i>
							Non Aktifkan
						</a>
					<?php 
						}
						else{
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/enabled_pemilik/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-user icon-md"></i>
							Aktifkan
						</a>
					<?php
						}
					?>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete_pemilik/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
						<i class="fas fa-user-times icon-md"></i>
						Hapus
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pemilik" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-plus icon-md"></i>
						Tambah Data
					</a>

					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/pemilik_search/">
					<div class="form-group row">
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Nama" name="cr_nama"  id="cr_nama" /></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari No. HP"  name="cr_no_hp"  id="cr_no_hp"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Email" name="cr_email"  id="cr_email"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari NIK" name="cr_nik"  id="cr_nik" /></div>
					</div>
					<div class="form-group row">
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_prop"  id="cr_prop" >
								<option label="Label"></option>
								<?=$comboPropinsi;?>
							</select>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_kota"  id="cr_kota" >
								<option label="Label"></option>
								<?=$comboKota;?>
							</select>
						</div>
						<div class="col-lg-3 mb-5">
							<button class="btn btn-success mr-2 col-lg-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_pemilik" data-toggle="table" data-height="500" data-show-columns="false" data-search="true" data-show-toggle="false" data-pagination="false" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pem_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="250">Nama Pemilik</th>
							<th data-sortable="true" data-width="150">Komunitas</th>
							<th data-sortable="true" data-width="175">NIK</th>
							<th data-sortable="true" data-width="250">Email</th>
							<th data-sortable="false" data-width="225">No. Whatsapp</th>
							<th data-sortable="false" data-width="225">No. HP</th>
							<th data-sortable="false" data-width="175">TTL</th>
							<th data-sortable="false" data-width="75" data-align="center">JK</th>
							<th data-sortable="false" data-width="125" data-align="center">Foto Profile</th>
							<th data-sortable="false" data-width="125" data-align="center">Foto Identitas</th>
							<th data-sortable="false" data-width="600">Alamat</th>
							<th data-sortable="false" data-width="150">Propinsi</th>
							<th data-sortable="false" data-width="150">Kota</th>
							<th data-sortable="false" data-width="150">Kecamatan</th>
							<th data-sortable="false" data-width="75" data-align="center">Aktif ?</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Hewan</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= $offset;
						foreach($query_pemilik->result() as $row_pemilik){
							$no++;
							$pem_id		= $row_pemilik->pem_id;
							if($rNum == $pem_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pemilik->pem_asal == 1){
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pemilik->ref_asal_pem_ket.'</span>';
							}
							else{
								$asal = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pemilik->ref_asal_pem_ket.'</span>';
							}

							if($row_pemilik->pem_gender == 'P'){
								$gender = '<i class="fas fa-male"></i>';
							}
							else{
								$gender = '<i class="fas fa-female"></i>';
							}

							if($row_pemilik->pem_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
							<td><?php echo $pem_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_pemilik->pem_nama; ?></td>
							<td><?php echo $asal; ?></td>
							<td><?php echo $row_pemilik->pem_nik; ?></td>
							<td><?php echo $row_pemilik->pem_email; ?></td>
							<td><?php echo $row_pemilik->pem_no_hp_wa; ?></td>
							<td><?php echo $row_pemilik->pem_no_hp_backup; ?></td>
							<td><?php echo $row_pemilik->ttl; ?></td>
							<td><?php echo $gender; ?></td>
							<td><a class="ml-2 mr-2" data-caption="Foto Profile" data-fancybox="gallery" href="assets/file/foto_pemilik/<?php echo $row_pemilik->pem_photo ?>" >Lihat</a></td>
							<td><a class="ml-2 mr-2" data-caption="Foto Profile" data-fancybox="gallery" href="assets/file/foto_identitas/<?php echo $row_pemilik->pem_photo_identitas ?>" >Lihat</a></td>
							<td><?php echo $row_pemilik->pem_alamat; ?></td>
							<td><?php echo $row_pemilik->ref_prop_ket; ?></td>
							<td><?php echo $row_pemilik->ref_kota_ket; ?></td>
							<td><?php echo $row_pemilik->ref_kcmtn_ket; ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo $row_pemilik->count_pasien; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
		<!--begin::Pagination-->
		<?php
			if (isset($pagination)){
				echo $pagination ;
			}
		?>
		<!--end::Pagination-->
			</div>
		</div>
	</div>
</div>
<?php
if($rNum > 0){
?>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Hewan Peliharaan
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Hewan/Pasien yang dimiliki</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum2 > 0){
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pasien/?rNum=<?php echo $rNum?>&rNum2=<?php echo $rNum2?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-edit icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete_pasien_pemilik/?rNum=<?php echo $rNum?>&rNum2=<?php echo $rNum2?>" class="btn btn-danger font-weight-bolder fix150" >
						<i class="fas fa-user-times icon-md"></i>
						Hapus
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form_pasien/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-plus icon-md"></i>
						Tambah Data
					</a>
					
					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<table id="table_pasien" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pas_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="135">No RM</th>
							<th data-sortable="true" data-width="225">Nama Hewan</th>
							<th data-sortable="true" data-width="100" data-align="center">JK</th>
							<th data-sortable="true" data-width="135">Jenis Hewan</th>
							<th data-sortable="true" data-width="135">Ras Hewan</th>
							<th data-sortable="true" data-width="135">Warna Hewan</th>
							<th data-sortable="true" data-width="135"">Umur</th>
							<th data-sortable="true" data-width="75" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_pasien->result() as $row_pasien){
							$no++;
							$pas_id		= $row_pasien->pas_id;
							$pas_aktif	= $row_pasien->pas_aktif;
							if($rNum2 == $pas_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pasien->ref_gender_nama == 'J'){
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pasien->ref_gender_nama.'</span>';
							}
							else{
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pasien->ref_gender_nama.'</span>';
							}


							if($pas_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up icon-nm"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down icon-nm"></i>';
							}
						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $pas_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_pasien->pas_mrn?></td>
							<td><?php echo $row_pasien->pas_nama; ?></td>
							<td><?php echo $gender; ?></td>
							<td><?php echo $row_pasien->ref_jns_hwn_nama; ?></td>
							<td><?php echo $row_pasien->ref_ras_nama; ?></td>
							<td><?php echo $row_pasien->ref_warna_ket; ?></td>
							<td><?php echo $row_pasien->umur; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>
<?php
}
?>
<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
	
<script type="text/javascript">


	$('#cr_prop').select2({
		placeholder: "Pilih Propinsi",
		allowClear: true
	});

	$('#cr_kota').select2({
		placeholder: "Cari Kota/Kabupaten",
		allowClear: true
	});

	$('#cr_prop').on('change', function(){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_kota",
				type: "POST",
				data: {'sel_id' : sel_id},
				dataType: 'json',
				success: function(data){
					$('#cr_kota').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

	$('#table_pemilik').on('dbl-click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_pem_id);
	});

	$('#table_pasien').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum=<?php echo $rNum;?>&rNum2='+row.row_pas_id);
	});

	Fancybox.bind('[data-fancybox="gallery"]', {
	  //dragToClose: false,
	  Thumbs: false,

	  Image: {
		zoom: false,
		click: false,
		wheel: "slide",
	  },

	  on: {
		// Move caption inside the slide
		reveal: (f, slide) => {
		  slide.$caption && slide.$content.appendChild(slide.$caption);
		},
	  },
	});
</script>
