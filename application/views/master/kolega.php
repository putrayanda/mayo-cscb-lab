<style>
table {
	table-layout: fixed;
}

.select2-container {
	width: 100% !important;
	padding: 0;
}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Kolega
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Kolega</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum > 0){
					?>
					<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddDokter">
						<i class="fa fa-user-plus icon-md"></i>
						Tambah Dokter
					</a>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/form/?rNum=<?php echo $rNum?>" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fas fa-house-user icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<?php
						if($ph_aktif =='t'){
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/disabled/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-house-user icon-md"></i>
							Non Aktifkan
						</a>
					<?php 
						}
						else{
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/enabled/?rNum=<?php echo $rNum?>" class="btn btn-warning font-weight-bolder fix150" >
							<i class="fas fa-house-user icon-md"></i>
							Aktifkan
						</a>
					<?php
						}
					?>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete/?rNum=<?php echo $rNum?>" class="btn btn-danger font-weight-bolder fix150" >
						<i class="fas fa-house-user icon-md"></i>
						Hapus
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fas fa-house-user icon-md"></i>
						Tambah Data
					</a>					
					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<table id="table_hd" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="125">No. Kolega</th>
							<th data-sortable="true" data-width="300">Nama Kolega</th>
							<th data-sortable="true" data-width="150">No. Telp</th>
							<th data-sortable="true" data-width="150">Email</th>
							<th data-sortable="true" data-width="300">Alamat</th>
							<th data-sortable="true" data-width="150">Kecamatan</th>
							<th data-sortable="true" data-width="150">Kota</th>
							<th data-sortable="true" data-width="150">Propinsi</th>
							<th data-sortable="true" data-width="200">Contact Person Nama</th>
							<th data-sortable="true" data-width="200">Contact Person No. HP</th>
							<th data-sortable="true" data-width="85" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_hd){
							$no++;
							$ph_id		= $row_hd->ph_id;
							if($rNum == $ph_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_hd->ph_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $ph_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_hd->ph_nomor; ?></td>
							<td><?php echo $row_hd->ph_jenis_prefix; ?>. <?php echo $row_hd->ph_nama; ?></td>
							<td><?php echo $row_hd->ph_no_telp; ?></td>
							<td><?php echo $row_hd->ph_email; ?></td>
							<td><?php echo $row_hd->ph_alamat; ?></td>
							<td><?php echo $row_hd->ref_kcmtn_ket; ?></td>
							<td><?php echo $row_hd->ref_kota_ket; ?></td>
							<td><?php echo $row_hd->ref_prop_ket; ?></td>
							<td><?php echo $row_hd->ph_pic_nama; ?></td>
							<td><?php echo $row_hd->ph_pic_no_hp; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
			<?php 
			if($rNum > 0){
			?>

			<div class="card-body">
				<table id="table_dt" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="false" data-width="50" data-align="center">Hapus</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="325">Nama Dokter</th>
							<th data-sortable="true" data-width="150">No. HP/WA</th>
							<th data-sortable="true" data-width="200">Email</th>
							<th data-sortable="true" data-width="80" data-align="center">Aktif?</th>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						$query_dt	= $this->db->query("SELECT * FROM v_pihak_dokter WHERE ph_dok_pihak=".$rNum." ORDER BY dok_nama");
						foreach($query_dt->result() as $row_dt){
							$no++;
							$ph_dok_id		= $row_dt->ph_dok_id;
							if($rNum2 == $ph_dok_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_dt->dok_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $ph_dok_id; ?></td>
							<td>
								<a class="dropdown-item del_detail" data-header="<?php echo $ph_dok_id; ?>"><i class="fas fa-trash-alt"></i></a>
							</td>
							<td><?php echo $no?></td>
							<td><?php echo $row_dt->dok_nama; ?></td>
							<td><?php echo $row_dt->dok_no_hp_wa; ?></td>
							<td><?php echo $row_dt->dok_email; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
			<?php 
			}
			?>
		</div>
		<!--end::Card-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
<!-- Modal Dokter -->
<div class="modal fade" id="formAddDokter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_dokter" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_dokter/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelDokter">Tambah Dokter</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<div class="col-lg-12 col-sm-12">
								<label>Pilih Dokter:</label>
								<div class="input-group ">
									<select class="form-control select2" name="inp_dokter"  id="inp_dokter" required >
										<option label="Label"></option>
										<?=$comboDokter;?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_crud_dokter" name="submit_crud_dokter" value="simpan_dokter">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>
<script type="text/javascript">

	jQuery(document).ready(function() {
		$('#inp_dokter').select2({
			placeholder: "Pilih Dokter",
			allowClear: true
		});

		$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
			$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
		});

		$('.del_detail').click(function(){
			var rNum	= <?= $rNum?>;
			var rNum2	= $(this).attr('data-header');
			Swal.fire({
				title: "Anda Yakin?",
				text: "Anda tidak akan dapat mengembalikan ini!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Ya, hapus!"
			}).then(function(result) {
				if(result.value==true){
					window.location.href = "<?php echo base_url().$class.'/'.$method ?>/del_dokter/?rNum="+rNum+"&rNum2="+rNum2;
				}
			});
		});  
	});

</script>
