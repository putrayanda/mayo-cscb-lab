<style>
table {
	table-layout: fixed;
}
.select2-container {
	width: 100% !important;
	padding: 0;
}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pegawai
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Kepegawaian</span></h3>
				</div>
				<div class="card-toolbar">
					<?php 
					if($rNum > 0){
					?>
					<!--
					MODEL PAKAI MODAL
					<a href="#" class="btn btn-primary font-weight-bolder fix150" data-toggle="modal" data-target="#formAddModal">
						<i class="fa fa-user-edit icon-md"></i>
						Ubah Data
					</a>
					-->
					<a href="<?php echo base_url().$class.'/'.$method;?>/form/?rNum=<?php echo $rNum?>" class="btn btn-primary mb-3 font-weight-bolder fix150" >
						<i class="fa fa-user-edit icon-md"></i>
						Ubah Data
					</a>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/resetpass/?rNum=<?php echo $rNum?>" class="btn btn-success mb-3 font-weight-bolder fix175" >
						<i class="fa fa-user-cog icon-md"></i>
						Reset Password
					</a>
					&nbsp;&nbsp;
					<?php
						if($peg_aktif =='t'){
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/disabled/?rNum=<?php echo $rNum?>" class="btn btn-warning mb-3 font-weight-bolder fix150" >
							<i class="fas fa-user-lock icon-md"></i>
							Non Aktifkan
						</a>
					<?php 
						}
						else{
					?>
						<a href="<?php echo base_url().$class.'/'.$method;?>/enabled/?rNum=<?php echo $rNum?>" class="btn btn-warning mb-3 font-weight-bolder fix150" >
							<i class="fas fa-user icon-md"></i>
							Aktifkan
						</a>
					<?php
						}
					?>
					&nbsp;&nbsp;
					<a href="<?php echo base_url().$class.'/'.$method;?>/delete/?rNum=<?php echo $rNum?>" class="btn btn-danger mb-3 font-weight-bolder fix150" >
						<i class="fas fa-user-times icon-md"></i>
						Hapus
					</a>

					&nbsp;&nbsp;
					<a href="#" data-toggle="modal" data-target="#formModalMenu" class="btn btn-info mb-3 font-weight-bolder fix250">
						<i class="fas fa-plus-circle icon-md"></i>
						Tambah Grup Menu
					</a>

					&nbsp;&nbsp;
					<a href="#" data-toggle="modal" data-target="#formModalDinilai" class="btn btn-info mb-3 font-weight-bolder fix250">
						<i class="fas fa-plus-circle icon-md"></i>
						Tambah Peg Dinilai
					</a>

					<?php
					}
					else{
					?>
					<a href="<?php echo base_url().$class.'/'.$method;?>/form" class="btn btn-primary font-weight-bolder fix150" >
						<i class="fa fa-user-plus icon-md"></i>
						Tambah Data
					</a>
					
					<!--
					MODEL PAKAI MODAL
					<a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#formAddModal">
						<i class="fa fa-user-plus icon-md"></i>
						Tambah Data
					</a>
					-->
					<?php
					}
					?>
				</div>
			</div>
			<div class="card-body">
				<table id="table_hd" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<!--th data-width="100" data-align="center">Actions</th-->
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="200">Organisasi</th>
							<th data-sortable="true" data-width="175">Cabang</th>
							<th data-sortable="true" data-width="300">Nama Pegawai</th>
							<th data-sortable="true" data-width="200">Username</th>
							<th data-sortable="true" data-width="250">Email</th>
							<th data-sortable="false" data-width="225">No HP/WA</th>
							<th data-sortable="false" data-width="225">TTL</th>
							<th data-sortable="false" data-width="75" data-align="center">JK</th>
							<th data-sortable="false" data-width="1000">Alamat</th>
							<th data-sortable="false" data-width="75" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_hd){
							$no++;
							$peg_id		= $row_hd->peg_id;
							if($rNum == $peg_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_hd->peg_gender == 'P'){
								$gender = '<i class="fas fa-male"></i>';
							}
							else{
								$gender = '<i class="fas fa-female"></i>';
							}

							if($row_hd->peg_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down"></i>';
							}
						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $peg_id; ?></td>
							<!--td>
								<a href=<?php echo base_url().$class."/".$method."/"?> class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
									<i class="fa fa-user-edit icon-md"></i>
								</a>
								<a href=<?php echo base_url().$class."/".$method."/"?> class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Non Active">
									<i class="fas fa-user-lock"></i>
								</a>
							</td-->

							<td><?php echo $no?></td>
							<td><?php echo $row_hd->org_nama; ?></td>
							<td><?php echo $row_hd->ref_cab_nama; ?></td>
							<td><?php echo $row_hd->peg_nama; ?></td>
							<td><?php echo $row_hd->peg_username; ?></td>
							<td><?php echo $row_hd->peg_email; ?></td>
							<td><?php echo $row_hd->peg_no_hp_wa; ?></td>
							<td><?php echo $row_hd->ttl; ?></td>
							<td><?php echo $gender; ?></td>
							<td><?php echo $row_hd->alamat_lengkap; ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				
			<?php
			if($rNum > 0){
			?>
				<div class="card card-custom">
					<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
						<div class="ribbon-target mt-5">
							<span class="ribbon-inner bg-success"></span>Daftar Menu
						</div>
					</div>
					<div class="card-body">
						<table id="table_menu" data-toggle="table" data-height="300" data-show-columns="false" data-search="true" data-show-toggle="false" data-pagination="false" data-page-list="[1000, 5000, 10000]" data-page-size="1000" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_id" data-visible="false">ID</th>
									<th data-sortable="false" data-width="65" data-align="center">Action</th>
									<th data-sortable="true" data-width="500">Grup Menu</th>
									<th data-sortable="true" data-width="100">Urutan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($query_dt->result() as $row_dt){
									$ref_mn_peg_mn_id	= $row_dt->ref_mn_peg_mn_id;
									$ref_mn_grp_id	= $row_dt->ref_mn_grp_id;
								?>
								<tr class="tr-class-<?php echo $no?> table-primary font-weight-bold">
									<td><?php echo $ref_mn_peg_mn_id; ?></td>
									<td></td>
									<td><?php echo $row_dt->ref_mn_grp_judul; ?></td>
									<td><?php echo $row_dt->ref_mn_peg_no_urut; ?></td>
								</tr>
								<?php
									$query_child	= $this->db->query("SELECT * FROM v_grup_menu_pegawai WHERE ref_mn_grp_parent_id=".$ref_mn_grp_id." AND ref_mn_peg_peg_id =".$rNum." ORDER BY ref_mn_peg_no_urut");
									foreach($query_child->result() as $row_child){
										$ref_mn_peg_mn_id	= $row_child->ref_mn_peg_mn_id;
								?>
								<tr class="tr-class-<?php echo $no?>  ">
									<td><?php echo $ref_mn_peg_mn_id; ?></td>
									<td>
										<input type="hidden" name="row_id_<?php echo $no; ?>" id="row_id_<?php echo $no; ?>" value="<?php echo $ref_mn_peg_mn_id; ?>" />
										<span class="dropdown">
											<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
											  <i class="la la-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="Javascript: del_grupmenu(<?php echo $rNum; ?>,<?php echo $ref_mn_peg_mn_id; ?>)"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus</a>
											</div>
										</span>
									</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row_child->ref_mn_grp_judul; ?></td>
									<td><?php echo $row_child->ref_mn_peg_no_urut; ?></td>
								</tr>
								<?php
									}

								}
								?>
							</tbody>
						</table>				
					</div>
				</div>

				<div class="card card-custom">
					<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
						<div class="ribbon-target mt-5">
							<span class="ribbon-inner bg-success"></span>Daftar Pegawai Dinilai Performance
						</div>
					</div>
					<div class="card-body">
						<table id="table_dinilai" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[10, 50, 100]" data-page-size="10" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_id" data-visible="false">ID</th>
									<th data-sortable="false" data-width="65" data-align="center">Action</th>
									<th data-sortable="true" data-width="200">Organisasi</th>
									<th data-sortable="true" data-width="175">Cabang</th>
									<th data-sortable="true" data-width="300">Nama Pegawai</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_dinilai->result() as $row_dnl){
									$no++;
									$peg_nilai_perform_id		= $row_dnl->peg_nilai_perform_id;
								?>
								<tr class="tr-class-<?php echo$no?>">
									<td><?php echo $peg_nilai_perform_id; ?></td>
									<td>
										<input type="hidden" name="row_id_<?php echo $no; ?>" id="row_id_<?php echo $no; ?>" value="<?php echo $ref_mn_peg_mn_id; ?>" />
										<span class="dropdown">
											<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
											  <i class="la la-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="Javascript: del_dinilai(<?php echo $rNum; ?>,<?php echo $peg_nilai_perform_id; ?>)"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;Hapus</a>
											</div>
										</span>
									</td>
									<td><?php echo $row_dnl->org_dinilai; ?></td>
									<td><?php echo $row_dnl->cab_dinilai; ?></td>
									<td><?php echo $row_dnl->peg_dinilai; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>

			<?php
			}
			?>

			</div>
		</div>
		<!--end::Card-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->

<!-- MODAL MENU -->
<div class="modal fade" id="formModalMenu" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_menu/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelHeader">Tambah Grup Menu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body" id="modal_body_header"></div>
				<div class="modal-footer">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_hd" name="submit_simpan_hd" value="simpan_hd">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<!-- MODAL DINILAI -->
<div class="modal fade" id="formModalDinilai" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<form id="form_header_dinilai" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_dinilai/?rNum=<?php echo $rNum?>">
				<div class="modal-header">
					<h5 class="modal-title" id="modalLabelHeaderDinilai">Tambah Grup Menu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body" id="modal_body_header_dinilai"></div>
				<div class="modal-footer">
					<input type="hidden" id="action_crud_dinilai" name="action_crud" value="">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary font-weight-bold" id="submit_simpan_hd_dinilai" name="submit_simpan_hd_dinilai" value="simpan_hd">Simpan</button>
				</div>
			</form>            
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
	});

	$('#formModalMenu').on('show.bs.modal', function(e) {
		var rNum	= <?= $rNum?>;
		$.ajax({url: '<?php echo base_url().$class;?>/modal_grup_menu/?rNum='+rNum, success: function(result){
			$("#modal_body_header").html(result);
			$('.kt-select2').select2();
			
			$('#inp_grup_menu').select2({
				placeholder: "Pilih Grup Menu",
				allowClear: true
			});

			const inputForm = document.getElementById('form_header');
			const fv = FormValidation.formValidation(inputForm, {
				fields: {
					 inp_grup_menu: {
						validators: {
							notEmpty: {
								message: 'Grup Menu wajib diisi'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap(),

				},
			}).on('core.form.validating', function() {
			});

			const save_exit_button	= document.getElementById('submit_simpan_hd');
			save_exit_button.addEventListener('click', function() {
				fv.validate().then(function(status) {
					if(status=='Valid'){
						document.getElementById("action_crud").value = 'save_exit';
						document.forms["form_header"].submit();
					}
				});
			});
		}});
	});

	$('#formModalDinilai').on('show.bs.modal', function(e) {
		var rNum	= <?= $rNum?>;
		$.ajax({url: '<?php echo base_url().$class;?>/modal_peg_dinilai/?rNum='+rNum, success: function(result){
			$("#modal_body_header_dinilai").html(result);
			$('.kt-select2').select2();
			
			$('#inp_peg_dinilai').select2({
				placeholder: "Pilih Pegawai Dinilai",
				allowClear: true
			});

			const inputForm = document.getElementById('form_header_dinilai');
			const fv = FormValidation.formValidation(inputForm, {
				fields: {
					 inp_peg_dinilai: {
						validators: {
							notEmpty: {
								message: 'Pegawai Dinilai wajib diisi'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap(),

				},
			}).on('core.form.validating', function() {
			});

			const save_exit_button	= document.getElementById('submit_simpan_hd_dinilai');
			save_exit_button.addEventListener('click', function() {
				fv.validate().then(function(status) {
					if(status=='Valid'){
						document.getElementById("action_crud_dinilai").value = 'save_exit';
						document.forms["form_header_dinilai"].submit();
					}
				});
			});
		}});
	});


	function del_grupmenu(rNum, rNum2){
		Swal.fire({
			title: "Anda Yakin?",
			text: "Anda tidak akan dapat mengembalikan ini!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Ya, hapus!"
		}).then(function(result) {
			if(result.value==true){
				window.location.href = "<?php echo base_url().$class.'/'.$method ?>/delete_grupmenu/?rNum="+rNum+"&rNum2="+rNum2;
			}
		});
	}

	function del_dinilai(rNum, rNum3){
		Swal.fire({
			title: "Anda Yakin?",
			text: "Anda tidak akan dapat mengembalikan ini!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Ya, hapus!"
		}).then(function(result) {
			if(result.value==true){
				window.location.href = "<?php echo base_url().$class.'/'.$method ?>/delete_dinilai/?rNum="+rNum+"&rNum3="+rNum3;
			}
		});
	}
</script>
