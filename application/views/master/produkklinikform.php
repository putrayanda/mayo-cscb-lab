<link href="assets/bower_components/bootstrap-table/bootstrap-table.css" rel="stylesheet" type="text/css">
<link href="assets/bower_components/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<script src="assets/bower_components/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
<style>
	.select2-container {
	width: 100% !important;
	padding: 0;

	.kv-file-upload{
		color:#fff !important;
		visibility: hidden !important;
	}

	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}
}
</style>
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Input Produk
					<i class="mr-2"></i>
					<small class="">Untuk menambah/mengubah data Produk</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
					<div class="btn-group">
						<button type="button" class="btn btn-primary font-weight-bolder">
						<i class="ki ki-check icon-sm"></i>Save Form</button>
						<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
						<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
							<ul class="nav nav-hover flex-column">
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_continue_button">
										<i class="nav-icon flaticon2-reload"></i>
										<span class="nav-text">Save &amp; continue</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_new_button">
										<i class="nav-icon flaticon2-add-1"></i>
										<span class="nav-text">Save &amp; add new</span>
									</a>
								</li>
								<li class="nav-item">
									<a href="javascript:void(0)" class="nav-link"  id="save_exit_button">
										<i class="nav-icon flaticon2-power"></i>
										<span class="nav-text">Save &amp; exit</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form id="form_input" role="form" method="post"  enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud/?rNum=<?php echo $rNum?>">
					<input type="hidden" id="action_crud" name="action_crud" value="">
					<div class="form-group row">
						<div class="col-lg-4 col-sm-4 col-xs-12">
							<label>Kode Produk/KSU:</label>
							<input type="text" class="form-control" placeholder="Isi Kode Produk" name="inp_kode"  id="inp_kode" value="<?php echo $ref_prod_kode;?>" required/>
						</div>
						<div class="col-lg-8 col-sm-8">
							<label>Nama Produk:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Produk" name="inp_nama"  id="inp_nama" value="<?php echo $ref_prod_nama;?>" required/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-4 col-sm-4">
							<label>Jenis:</label>
								<select class="form-control select2" name="inp_jenis"  id="inp_jenis" required >
									<option label="Label"></option>
									<?=$comboJenis;?>
								</select>
						</div>
						<div class="col-lg-8 col-sm-8">
							<label>Link Toped:</label>
							<input type="email" class="form-control" placeholder="Isi Link Tokopedia" name="inp_link"  id="inp_link" value="<?php echo $ref_prod_link_toped;?>"/>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-2 col-sm-2">
							<label>Min Stok:</label>
							<input type="number" class="form-control text-right" placeholder="Isi Min Stok" name="inp_min_stok"  id="inp_min_stok" value="<?php echo $ref_prod_min_stok;?>" required/>
						</div>
						<div class="col-lg-2 col-sm-2">
							<label>Sat Beli:</label>
							<select class="form-control select2" name="inp_sat_beli"  id="inp_sat_beli" required >
								<option label="Label"></option>
								<?=$comboSatPmbln;?>
							</select>
						</div>
						<div class="col-lg-2 col-sm-2">
							<label>Setara:</label>
							<input type="number" class="form-control" placeholder="Isi Setara Jumlah" name="inp_rasio"  id="inp_rasio" value="<?php echo $ref_prod_rasio_beli_stok;?>" required/>
						</div>
						<div class="col-lg-2 col-sm-2">
							<label>Sat Stok/Jual:</label>
							<select class="form-control select2" name="inp_sat_stok"  id="inp_sat_stok" required >
								<option label="Label"></option>
								<?=$comboSatPnjln;?>
							</select>
						</div>
						<div class="col-lg-2 col-sm-2">
							<label>Harga Jual:</label>
							<input type="number" class="form-control text-right" placeholder="Isi Harga Jual" name="inp_hrg_jual"  id="inp_hrg_jual" value="<?php echo $ref_prod_harga;?>" required/>
						</div>
						<div class="col-lg-2 col-sm-2">
							<label>Harga Beli:</label>
							<input type="number" class="form-control text-right" placeholder="Isi Harga Beli" name="inp_hrg_beli"  id="inp_hrg_beli" value="<?php echo $ref_prod_harga_beli;?>" required/>
						</div>

					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-lg-4 col-sm-5">
							<label>Akun Pendapatan:</label>
							<select class="form-control select2" name="inp_coa_pndptn"  id="inp_coa_pndptn" required >
								<option label="Label"></option>
								<?=$comboCoaPndptn;?>
							</select>
						</div>
						<div class="col-lg-4 col-sm-5">
							<label>Akun Biaya:</label>
							<select class="form-control select2" name="inp_coa_biaya"  id="inp_coa_biaya" required >
								<option label="Label"></option>
								<?=$comboCoaBiaya;?>
							</select>
						</div>
						<div class="col-lg-4 col-sm-5">
							<label>Akun Persediaan:</label>
							<select class="form-control select2" name="inp_coa_prsdn"  id="inp_coa_prsdn" required >
								<option label="Label"></option>
								<?=$comboCoaPrsdn;?>
							</select>
						</div>
					</div>
					<div class="separator separator-dashed my-2"></div>
					<div class="form-group row">
						<div class="col-md-12 col-lg-12">
							<label>Foto Produk:</label>
							<div class="d-flex justify-content-center">
								<?php
								foreach($query_foto->result() as $row_foto){
									$ref_prod_fv_id = $row_foto->ref_prod_fv_id;
									if(preg_match('/^.*\.(mp4|mkv)$/i', $row_foto->ref_prod_fv_rm_url)) {
										$thumbnail = "https://localhost/cscb.leonvets.id/assets/file/foto_produk/videos.png";
									}
									else{
										$thumbnail = $row_foto->ref_prod_fv_rm_url;
									}
								?>
								<div>
									<div class="text-center mb-2">
										<a href="<?php echo base_url().$class.'/'.$method.'/del_foto/?rNum='.$rNum.'&rNum2='.$ref_prod_fv_id ?>" class="btn btn-icon btn-outline-danger btn-xs"><i class="fas fa-trash"></i></a>
									</div>
									<a class="ml-2 mr-2" data-caption="<?php echo $row_foto->ref_prod_fv_rm_keterangan ?>" data-fancybox="gallery" href="<?php echo $row_foto->ref_prod_fv_rm_url ?>"  >
										<img width="150px" class="rounded" src="<?php echo $thumbnail ?>" />
									</a>
								</div>
								<?php
								}
								?>
							</div>
							</br>
							<div class="file-loading">
								<input type="file" name="file_hasil[]" id="file_hasil" multiple data-allowed-file-extensions='[ "jpg", "jpeg", "png", "mkv", "mp4"]' />
							</div>
						</div>
					</div>
 					<div class="separator separator-dashed my-2"></div>

				</form>  
			</div>
		</div>
	</div>
</div>

<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
<script src="assets/js/pages/crud/file-upload/image-input.js?v=7.0.4"></script>
<script>
var arrows;
if (KTUtil.isRTL()) {
	arrows = {
		leftArrow: '<i class="la la-angle-right"></i>',
		rightArrow: '<i class="la la-angle-left"></i>'
	}
} else {
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
}

jQuery(document).ready(function() {
	
	$('#inp_jenis').select2({
		placeholder: "Pilih Propinsi",
		allowClear: true
	});

	$('#inp_sat_beli').select2({
		placeholder: "Pilih Satuan Beli",
		allowClear: true
	});

	$('#inp_sat_stok').select2({
		placeholder: "Pilih Satuan Stok/Jual",
		allowClear: true
	});	

	$('#inp_coa_pndptn').select2({
		placeholder: "Pilih Akun Pendapatan",
		allowClear: true
	});	

	$('#inp_coa_biaya').select2({
		placeholder: "Pilih Akun Biaya",
		allowClear: true
	});	

	$('#inp_coa_prsdn').select2({
		placeholder: "Pilih Akun Persediaan",
		allowClear: true
	});	

	$("#file_hasil").fileinput({
		'theme': 'explorer-fas',
		'showUpload': false
	});

    const save_continue_button	= document.getElementById('save_continue_button');
    const save_new_button		= document.getElementById('save_new_button');
    const save_exit_button		= document.getElementById('save_exit_button');
	const inputForm = document.getElementById('form_input');
    const fv = FormValidation.formValidation(inputForm, {
        fields: {
             inp_kode: {
                validators: {
                    notEmpty: {
                        message: 'Kode Produk/KSU wajib diisi'
                    },
                    stringLength: {
                        min: 5,
                        message: 'Kode Produk/KSU minimal 5 karakter'
                    }
                }
            },
			inp_nama: {
                validators: {
                    notEmpty: {
                        message: 'Nama Produk wajib diisi'
                    },
                    stringLength: {
                        min: 5,
                        message: 'Nama Produk minimal 5 karakter'
                    }
                }
            },
			inp_jenis: {
				validators: {
					notEmpty: {
                        message: 'Jenis Produk wajib diisi'
					},
				}
			},
			inp_min_stok: {
				validators: {
					notEmpty: {
                        message: 'Min Stok wajib diisi'
					}
				}
			},
			inp_sat_beli: {
				validators: {
					notEmpty: {
                        message: 'Satuan Beli wajib diisi'
					},
				}
			},
			inp_rasio: {
				validators: {
					notEmpty: {
                        message: 'Rasio Qty Beli - Stok wajib diisi'
					},
				}
			},
			inp_sat_stok: {
				validators: {
					notEmpty: {
                        message: 'Satuan Stok/Jual wajib diisi'
					},
				}
			},
			inp_hrg_jual: {
				validators: {
					notEmpty: {
                        message: 'Harga Jual wajib diisi'
					},
				}
			},
			inp_hrg_beli: {
				validators: {
					notEmpty: {
                        message: 'Harga Beli wajib diisi'
					},
				}
			},
        },
        plugins: {
			trigger: new FormValidation.plugins.Trigger(),
			bootstrap: new FormValidation.plugins.Bootstrap(),

        },
    }).on('core.form.validating', function() {
    });

    save_continue_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_continue';
				document.forms["form_input"].submit();
			}
        });
    });

    save_new_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_new';
				document.forms["form_input"].submit();
			}
        });
    });

    save_exit_button.addEventListener('click', function() {
        fv.validate().then(function(status) {
			if(status=='Valid'){
				document.getElementById("action_crud").value = 'save_exit';
				document.forms["form_input"].submit();
			}
        });
    });

});

Fancybox.bind('[data-fancybox="gallery"]', {
  //dragToClose: false,
  Thumbs: false,

  Image: {
	zoom: false,
	click: false,
	wheel: "slide",
  },

  on: {
	// Move caption inside the slide
	reveal: (f, slide) => {
	  slide.$caption && slide.$content.appendChild(slide.$caption);
	},
  },
});


</script>
