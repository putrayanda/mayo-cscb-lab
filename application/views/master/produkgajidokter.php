<style>
	table {
		table-layout: fixed;
	}
</style>
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Produk
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Produk</span></h3>
				</div>
				<div class="card-toolbar">
					<button class="btn btn-info font-weight-bolder fix150" id="submit_proses" value="proses"> 
						<i class="fas fa-check-circle icon-md"></i>
						Proses
					</button>
				</div>
			</div>
			<div class="card-body">
				<form id="form_crud" action="<?= base_url().$class.'/'.$method?>/crud/" method="post" class="" novalidate >
				<table id="table_hd" class="table_report" data-height="500" data-toggle="table" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="false" data-page-list="[500, 1000]" data-page-size="500" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="450">Produk Gaji</th>
							<th data-sortable="true" data-width="200">Jenis Produk</th>
							<th data-sortable="true" data-width="300">Nama Produk</th>
							<th data-sortable="true" data-width="300">Nama Alias/Di Invoice</th>
							<th data-sortable="true" data-width="150" data-align="right">Harga Jual</th>
							<th data-sortable="true" data-width="85" data-align="center">Aktif ?</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_hd){
							$no++;
							$ref_prod_id		= $row_hd->ref_prod_id;
							if($rNum == $ref_prod_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							($row_hd->ref_prod_aktif == 't') ? $status = '<i class="fas fa-thumbs-up">' : $status = '<i class="fas fa-thumbs-down"></i>';

							if($row_hd->ref_prod_fee_dokter_pasien_harian == 't'){
								$checkedNon				= '';
								$checkedHarian			= 'checked="checked"';
								$checkedOperasi			= '';
								$checkedSteril			= '';
							}
							elseif($row_hd->ref_prod_fee_dokter_operasi_vaksin == 't'){
								$checkedNon				= '';
								$checkedHarian			= '';
								$checkedOperasi			= 'checked="checked"';
								$checkedSteril			= '';
							}
							elseif($row_hd->ref_prod_fee_dokter_steril_komunitas == 't'){
								$checkedNon				= '';
								$checkedHarian			= '';
								$checkedOperasi			= '';
								$checkedSteril			= 'checked="checked"';
							}
							else{
								$checkedNon				= 'checked="checked"';
								$checkedHarian			= '';
								$checkedOperasi			= '';
								$checkedSteril			= '';
							}
						?>
						<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
							<td><?php echo $ref_prod_id; ?></td>
							<td><?php echo $no?></td>
							<td>
								<input type="hidden" name="row_id_<?php echo $no; ?>" id="row_id_<?php echo $no; ?>" value="<?php echo $ref_prod_id; ?>" />
								<div class="radio-inline">
									<label class="radio radio-success">
									<input type="radio" name="inp_status_<?php echo $no; ?>"  id="inp_status_non_<?php echo $no; ?>" value="1" <?php echo $checkedNon;?> />Non
									<span></span></label>
									<label class="radio radio-primary">
									<input type="radio" name="inp_status_<?php echo $no; ?>"  id="inp_status_harian_<?php echo $no; ?>" value="2" <?php echo $checkedHarian;?> />Pasien Harian
									<span></span></label>
									<label class="radio radio-warning">
									<input type="radio" name="inp_status_<?php echo $no; ?>"  id="inp_status_operasi_<?php echo $no; ?>" value="3" <?php echo $checkedOperasi;?> />Operasi
									<span></span></label>
									<label class="radio radio-danger">
									<input type="radio" name="inp_status_<?php echo $no; ?>"  id="inp_status_steril_<?php echo $no; ?>" value="4" <?php echo $checkedSteril;?> />Steril Komunitas
									<span></span></label>
								</div>

							</td>
							<td><?php echo $row_hd->ref_jns_prod_nama; ?></td>
							<td><?php echo $row_hd->ref_prod_nama; ?></td>
							<td><?php echo $row_hd->ref_prod_nama_invoice; ?></td>
							<td><?php echo number_format($row_hd->ref_prod_harga, 2, '.', ','); ?></td>
							<td><?php echo $status; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<input type="hidden" class="form-control" id="submit_crud" name="submit_crud" value="">
				<input type="hidden" class="form-control" id="count_hd" name="count_hd" value="<?php echo $no ?>">
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">	
	jQuery(document).ready(function() {
		$( "#submit_proses" ).click(function() {
			document.getElementById("submit_crud").value = 'proses';
			document.forms["form_crud"].submit();
		});
	});

</script>
