<style>
	table {
		table-layout: fixed;
	}

</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pasien/Hewan
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Pasien/Hewan Utama</span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/pasien_search/">
					<div class="form-group row">
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari No. RM"  name="cr_rm"  id="cr_rm"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Nama"  name="cr_nama"  id="cr_nama"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Pemilik"  name="cr_pemilik"  id="cr_pemilik"/></div>
						<div class="col-lg-3 mb-5">
							<button class="btn btn-success mr-2" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_pasien" data-toggle="table" data-height="500" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pas_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="125">No. RM</th>
							<th data-sortable="true" data-width="225">Nama Hewan</th>
							<th data-sortable="true" data-width="200">Pemilik Utama</th>
							<th data-sortable="true" data-width="135">Spesies</th>
							<th data-sortable="true" data-width="100" data-align="center">JK</th>
							<th data-sortable="true" data-width="135">Jenis Hewan</th>
							<th data-sortable="true" data-width="135">Ras Hewan</th>
							<th data-sortable="true" data-width="135">Warna Hewan</th>
							<th data-sortable="true" data-width="140">Umur</th>
							<th data-sortable="true" data-width="85" data-align="right">Berat (Kg)</th>
							<th data-sortable="true" data-width="135">Tanda Khusus</th>
							<th data-sortable="true" data-width="85" data-align="center">Aktif ?</th>
							<th data-sortable="true" data-width="200">Pemilik</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Pemilik</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Tele</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml RJ</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml RI</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= $offset;
						foreach($query_pasien->result() as $row_pasien){
							$no++;
							$pas_id		= $row_pasien->pas_id;
							$pas_aktif	= $row_pasien->pas_aktif;
							if($rNum == $pas_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pasien->ref_gender_nama == 'J'){
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pasien->ref_gender_nama.'</span>';
							}
							else{
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pasien->ref_gender_nama.'</span>';
							}


							if($pas_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up icon-nm"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down icon-nm"></i>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
							<td><?php echo $pas_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_pasien->pas_mrn?></td>
							<td><?php echo $row_pasien->pas_nama; ?></td>
							<td><?php echo $row_pasien->pem_nama; ?><br />(<?php echo $row_pasien->pem_no_hp_wa; ?>)</td>
							<td><?php echo $row_pasien->ref_spesies_nama; ?></td>
							<td><?php echo $gender; ?></td>
							<td><?php echo $row_pasien->ref_jns_hwn_nama; ?></td>
							<td><?php echo $row_pasien->ref_ras_nama; ?></td>
							<td><?php echo $row_pasien->ref_warna_ket; ?></td>
							<td><?php echo $row_pasien->umur; ?></td>
							<td><?php echo $row_pasien->pas_berat; ?></td>
							<td><?php echo $row_pasien->ref_tnd_khss_ket; ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo $row_pasien->nama_pemilik; ?></td>
							<td><?php echo $row_pasien->count_pemilik; ?></td>
							<td><?php echo $row_pasien->jumlah_kunjungan; ?></td>
							<td><?php echo $row_pasien->jumlah_telemedicine; ?></td>
							<td><?php echo $row_pasien->jumlah_rawatinap; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>	
			<?php
				if (isset($pagination)){
					echo $pagination ;
				}
			?>
			</div>
		</div>
	</div>
</div>
<?php
if($rNum > 0){
?>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label">Data Pasien/Hewan
					<span class="d-block text-muted pt-2 font-size-sm">Informasi Pasien/Hewan Yang Ingin Digabung</span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/pasien_merge_search/">
					<input type="hidden" name="rNum"  id="rNum" value="<?php echo $rNum;?>" />
					<div class="form-group row">
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari No. RM"  name="cr_rm"  id="cr_rm"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Nama"  name="cr_nama"  id="cr_nama"/></div>
						<div class="col-lg-3 mb-5"><input type="text" class="form-control" placeholder="Cari Pemilik"  name="cr_pemilik"  id="cr_pemilik"/></div>
						<div class="col-lg-3 mb-5">
							<button class="btn btn-success mr-2" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<form action="<?= base_url().$class.'/'.$method?>/crud/" method="post" class="form-horizontal form-label-left" novalidate >
				<table id="table_pasien_merge" data-toggle="table" data-height="500" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_pas_id" data-visible="false">ID</th>
							<th data-sortable="false" data-width="85" data-align="center">Merge</th>
							<th data-sortable="true" data-width="125">No. RM</th>
							<th data-sortable="true" data-width="225">Nama Hewan</th>
							<th data-sortable="true" data-width="200">Pemilik Utama</th>
							<th data-sortable="true" data-width="135">Spesies</th>
							<th data-sortable="true" data-width="100" data-align="center">JK</th>
							<th data-sortable="true" data-width="135">Jenis Hewan</th>
							<th data-sortable="true" data-width="135">Ras Hewan</th>
							<th data-sortable="true" data-width="135">Warna Hewan</th>
							<th data-sortable="true" data-width="140">Umur</th>
							<th data-sortable="true" data-width="85" data-align="right">Berat (Kg)</th>
							<th data-sortable="true" data-width="135">Tanda Khusus</th>
							<th data-sortable="true" data-width="85" data-align="center">Aktif ?</th>
							<th data-sortable="true" data-width="200">Pemilik</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Pemilik</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml Tele</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml RJ</th>
							<th data-sortable="true" data-width="100" data-align="center">Jml RI</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= $offset;
						foreach($query_pasien_merge->result() as $row_pasien_merge){
							$no++;
							$pas_id		= $row_pasien_merge->pas_id;
							$pas_aktif	= $row_pasien_merge->pas_aktif;
							if($rNum == $pas_id){
								$active = 'table-primary';
							}
							else{
								$active = '';
							}

							if($row_pasien_merge->ref_gender_nama == 'J'){
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-primary">'.$row_pasien_merge->ref_gender_nama.'</span>';
							}
							else{
								$gender = '<span class="label label-primary label-dot mr-2"></span> <span class="font-weight-bold text-danger">'.$row_pasien_merge->ref_gender_nama.'</span>';
							}


							if($pas_aktif == 't'){
								$status = '<i class="fas fa-thumbs-up icon-nm"></i>';
							}
							else{
								$status = '<i class="fas fa-thumbs-down icon-nm"></i>';
							}
						?>
						<tr class="tr-class-<?php echo $no?> <?php echo $active?> ">
							<td><?php echo $pas_id; ?></td>
							<td><input type="hidden" name="row_id_<?php echo $no; ?>" id="row_id_<?php echo $no; ?>" value="<?php echo $pas_id; ?>" /><input type="checkbox" name="checked_<?php echo $no; ?>" value="t" /></td>
							<td><?php echo $row_pasien_merge->pas_mrn?></td>
							<td><?php echo $row_pasien_merge->pas_nama; ?></td>
							<td><?php echo $row_pasien_merge->pem_nama; ?><br />(<?php echo $row_pasien_merge->pem_no_hp_wa; ?>)</td>
							<td><?php echo $row_pasien_merge->ref_spesies_nama; ?></td>
							<td><?php echo $gender; ?></td>
							<td><?php echo $row_pasien_merge->ref_jns_hwn_nama; ?></td>
							<td><?php echo $row_pasien_merge->ref_ras_nama; ?></td>
							<td><?php echo $row_pasien_merge->ref_warna_ket; ?></td>
							<td><?php echo $row_pasien_merge->umur; ?></td>
							<td><?php echo $row_pasien_merge->pas_berat; ?></td>
							<td><?php echo $row_pasien_merge->ref_tnd_khss_ket; ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo $row_pasien_merge->nama_pemilik; ?></td>
							<td><?php echo $row_pasien_merge->count_pemilik; ?></td>
							<td><?php echo $row_pasien_merge->jumlah_kunjungan; ?></td>
							<td><?php echo $row_pasien_merge->jumlah_telemedicine; ?></td>
							<td><?php echo $row_pasien_merge->jumlah_rawatinap; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>	
			</div>
			<div class="card-body">
					<input type="hidden" class="form-control" id="count_dt" name="count_dt" value="<?php echo $no ?>">
					<input type="hidden" name="rNum"  id="rNum" value="<?php echo $rNum;?>" />
					<div class="form-group row">
						<div class="col-lg-4 mb-5">&nbsp;</div>
						<div class="col-lg-4 mb-5">
							<button class="btn btn-success mr-2 col-lg-12" type="submit" > Merge
								<i class="fa fa-search"></i>
							</button>
						</div>
						<div class="col-lg-4 mb-5">&nbsp;</div>
					</div>
				</form>
			</div>

		</div>
	</div>
</div>
<?php
}
?>
<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
	
<script type="text/javascript">
	$('#table_pasien').on('dbl-click-row.bs.table', function (e, row, $element) {
		$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_pas_id);
	});


	Fancybox.bind('[data-fancybox="gallery"]', {
	  //dragToClose: false,
	  Thumbs: false,

	  Image: {
		zoom: false,
		click: false,
		wheel: "slide",
	  },

	  on: {
		// Move caption inside the slide
		reveal: (f, slide) => {
		  slide.$caption && slide.$content.appendChild(slide.$caption);
		},
	  },
	});
</script>
