<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/rekammedislabrad_search/">
					<div class="form-group row">
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="Nama Pasien/Hewan" name="cr_pasien"  id="cr_pasien" />
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="No. Rekam Medis" name="cr_mrn"  id="cr_mrn" />
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="Nama Pemilik"  name="cr_pemilik"  id="cr_pemilik"/>
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="No. HP Pemilik"  name="cr_no_hp"  id="cr_no_hp"/>
						</div>
						<div class="col-lg-3 mb-5">
							<div class='input-group' id='cr_daterangepicker'>
								<input type='text' class="form-control" name="cr_periode" id="cr_periode" readonly="readonly" placeholder="Pilih Periode" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_klinik_rujukan"  id="cr_klinik_rujukan" >
								<option label="Label"></option>
								<?=$comboKlinik;?>
							</select>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_dokter_rujukan"  id="cr_dokter_rujukan" >
								<option label="Label"></option>
								<?=$comboDokter;?>
							</select>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-success mr-2 col-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_hd" class="table_report" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[50, 100, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="150" data-align="center">T. Periksa</th>
							<th data-sortable="true" data-width="200">Nama Hewan</th>
							<th data-sortable="true" data-width="150">Spesies</th>
							<th data-sortable="true" data-width="200">Pemilik</th>
							<th data-sortable="true" data-width="200">Klinik</th>
							<th data-sortable="true" data-width="350">Dokter</th>
							<th data-sortable="true" data-width="200" data-align="center">Keterangan</th>
							<th data-sortable="true" data-width="100" data-align="right">Harga</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_rm){
							$no++;
							$rm_labrad_id	= $row_rm->rm_labrad_id;
							$rm_id			= $row_rm->rm_labrad_rm_id;
						?>
						<tr class="tr-class-<?php echo$no?> ">
							<td><?php echo $rm_labrad_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_rm->rm_tanggal; ?></td>
							<td><?php echo $row_rm->pas_nama; ?></td>
							<td><?php echo $row_rm->ref_spesies_nama?></td>
							<td><?php echo $row_rm->pem_nama?></td>
							<td><?php echo $row_rm->nama_klinik_rujukan?></td>
							<td><?php echo $row_rm->nama_dokter_rujukan?> (<?php echo $row_rm->no_hp_dokter_rujukan?>)</td>
							<td>
							<?php
							$query_hsl_usg	= "SELECT * FROM v_rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (78) AND rm_labrad_hsl_rm_id=".$rm_id." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan";

							$rhHslUSG		= $this->db->query($query_hsl_usg);
							foreach($rhHslUSG->result() as $rowHslUSG){
								$no++;
							?>
								<?php echo $rowHslUSG->lokasi_usg; ?></br>
							<?php
							}
							?>
							</td>
							<td><?php echo number_format($row_rm->rm_labrad_harga, 2, ',', '.')?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {

		$('#cr_klinik_rujukan').select2({
			placeholder: "Pilih Klinik Rujukan",
			allowClear: true
		});
			
		$('#cr_dokter_rujukan').select2({
			placeholder: "Pilih Dokter Rujukan",
			allowClear: true
		});
			
        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

		$('#cr_klinik_rujukan').on('change', function(){
			var sel_id = $(this).val();

			if(sel_id > 0) {
				$.ajax({
					url : "<?php echo base_url() ?>Ajax/create_list/get_dokter_klinik",
					type: "POST",
					data: {'sel_id' : sel_id},
					dataType: 'json',
					success: function(data){
						$('#cr_dokter_rujukan').html(data);
					},
					error: function(){
						//('Empty Data...!!');
					}
				});
			}
		});
	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
