<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<div class="card card-custom">
					<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
						<div class="ribbon-target mt-5">
							<span class="ribbon-inner bg-warning"></span>Daftar Deposit Pemilik
						</div>
					</div>
					<div class="card-body">	
						<table id="table_hd" class="table_report" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[50, 100, 1000]" data-page-size="100" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="60" data-align="right">No</th>
									<th data-sortable="true" data-width="300">Nama Pemilik</th>
									<th data-sortable="true" data-width="300">Nama Hewan</th>
									<th data-sortable="true" data-width="150" data-align="right">Jumlah Deposit</th>
									<th data-sortable="true" data-width="150" data-align="right">Jumlah Pakai</th>
									<th data-sortable="true" data-width="150" data-align="right">Jumlah Refund</th>
									<th data-sortable="true" data-width="150" data-align="right">Sisa</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_hd->result() as $row_hd){
									$no++;
									$pem_id	= $row_hd->pem_id;
									$sisa	= $row_hd->jumlah_deposit-($row_hd->jumlah_potong_deposit+$row_hd->jumlah_refund);

									if($rNum == $pem_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
								?>
									<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $pem_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_hd->pem_nama; ?></td>
									<td><?php echo $row_hd->nama_hewan; ?></td>
									<td><?php echo number_format($row_hd->jumlah_deposit, 2, ',', '.')?></td>
									<td><?php echo number_format($row_hd->jumlah_potong_deposit, 2, ',', '.')?></td>
									<td><?php echo number_format($row_hd->jumlah_refund, 2, ',', '.')?></td>
									<td><?php echo number_format($sisa, 2, ',', '.')?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>	
					</div>
				</div>
	
				<?php
					if($rNum > 0){
				?>
						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-success"></span>Daftar Penerimaan Deposit
								</div>
							</div>
							<div class="card-body">	
								<table id="table_penerimaan" class="table_report" data-toggle="table" data-height="300" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="200">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Terima</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="300">Cara bayar</th>
											<th data-sortable="true" data-width="300">Pihak</th>
											<th data-sortable="true" data-width="300">Pemilik</th>
											<th data-sortable="true" data-width="200">Asal</th>
											<th data-sortable="true" data-width="300">Akun Penerimaan</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="150">No. Kwitansi</th>
											<th data-sortable="true" data-width="100" data-align="center">Proses ?</th>
											<th data-sortable="true" data-width="200">Peg.Terima</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_penerimaan->result() as $row_trm){
											$no++;
											$pkb_id		= $row_trm->pkb_id;
											if($rNum == $pkb_id){
												$active = 'table-primary';
											}
											else{
												$active = '';
											}

											if($row_trm->pkb_flag_proses == 't'){
												$status = '<i class="fas fa-thumbs-up"></i>';
											}
											else{
												$status = '<i class="fas fa-thumbs-down"></i>';
											}
											$disabledCheck = '';
										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $pkb_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_trm->ref_cab_nama; ?></td>
											<td><?php echo $row_trm->pkb_tgl_terima; ?></td>
											<td><?php echo number_format($row_trm->pkb_jumlah, 2, ',', '.')?></td>
											<td><?php echo $row_trm->cara_bayar; ?></td>
											<td><?php echo $row_trm->ref_jns_phk_nama; ?> - <?php echo $row_trm->nama_pihak; ?></td>
											<td><?php echo $row_trm->nama_pemilik; ?></td>
											<td><?php echo $row_trm->rapkb_nama; ?></td>
											<td><?php echo $row_trm->kd_rkng_nama; ?></td>
											<td><?php echo $row_trm->pkb_keterangan; ?></td>
											<td><?php echo $row_trm->pkb_no_kwitansi; ?></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row_trm->peg_buat; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>
						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-primary"></span>Daftar Pemakaian Deposit
								</div>
							</div>
							<div class="card-body">	

							<table id="table_pemakaian" class="table_report" data-toggle="table" data-height="300" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
								<thead>
									<tr>
										<th data-field="row_id" data-visible="false">ID</th>
										<th data-sortable="true" data-width="60" data-align="right">No</th>
										<th data-sortable="true" data-width="200">Cabang</th>
										<th data-sortable="true" data-width="125">Tgl. Terima</th>
										<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
										<th data-sortable="true" data-width="300">Cara bayar</th>
										<th data-sortable="true" data-width="300">Pihak</th>
										<th data-sortable="true" data-width="300">Pemilik</th>
										<th data-sortable="true" data-width="200">Asal</th>
										<th data-sortable="true" data-width="300">Akun Penerimaan</th>
										<th data-sortable="true" data-width="300">Keterangan</th>
										<th data-sortable="true" data-width="150">No. Kwitansi</th>
										<th data-sortable="true" data-width="100" data-align="center">Proses ?</th>
										<th data-sortable="true" data-width="200">Peg.Terima</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no	= 0;
									foreach($query_pemakaian->result() as $row_pakai){
										$no++;
										$pkb_id		= $row_pakai->pkb_id;
										if($rNum == $pkb_id){
											$active = 'table-primary';
										}
										else{
											$active = '';
										}

										if($row_pakai->pkb_flag_proses == 't'){
											$status = '<i class="fas fa-thumbs-up"></i>';
										}
										else{
											$status = '<i class="fas fa-thumbs-down"></i>';
										}
										$disabledCheck = '';
									?>
									<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
										<td><?php echo $pkb_id; ?></td>
										<td><?php echo $no?></td>
										<td><?php echo $row_pakai->ref_cab_nama; ?></td>
										<td><?php echo $row_pakai->pkb_tgl_terima; ?></td>
										<td><?php echo number_format($row_pakai->pkb_jumlah, 2, ',', '.')?></td>
										<td><?php echo $row_pakai->cara_bayar; ?></td>
										<td><?php echo $row_pakai->ref_jns_phk_nama; ?> - <?php echo $row_pakai->nama_pihak; ?></td>
										<td><?php echo $row_pakai->nama_pemilik; ?></td>
										<td><?php echo $row_pakai->rapkb_nama; ?></td>
										<td><?php echo $row_pakai->kd_rkng_nama; ?></td>
										<td><?php echo $row_pakai->pkb_keterangan; ?></td>
										<td><?php echo $row_pakai->pkb_no_kwitansi; ?></td>
										<td><?php echo $status; ?></td>
										<td><?php echo $row_pakai->peg_buat; ?></td>
									</tr>
									<?php
									}
									?>
								</tbody>
							</table>				
							</div>
						</div>

						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-info"></span>Daftar Refund Deposit
								</div>
							</div>
							<div class="card-body">	
								<table id="table_refund" class="table_report"  data-toggle="table" data-height="300" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="200">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Bayar</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="300">Cara bayar</th>
											<th data-sortable="true" data-width="300">Pihak</th>
											<th data-sortable="true" data-width="200">Asal</th>
											<th data-sortable="true" data-width="300">Akun Pengeluaran</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="150">No. Kwitansi</th>
											<th data-sortable="true" data-width="100" data-align="center">Proses ?</th>
											<th data-sortable="true" data-width="200">Peg. Buat</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_refund->result() as $row_rfnd){
											$no++;
											$pkb_id		= $row_rfnd->pkb_id;
											if($rNum == $pkb_id){
												$active = 'table-primary';
											}
											else{
												$active = '';
											}

											if($row_rfnd->pkb_flag_proses == 't'){
												$status = '<i class="fas fa-thumbs-up"></i>';
											}
											else{
												$status = '<i class="fas fa-thumbs-down"></i>';
											}
											$disabledCheck = '';
										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $pkb_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_rfnd->ref_cab_nama; ?></td>
											<td><?php echo $row_rfnd->pkb_tgl_keluar; ?></td>
											<td><?php echo number_format($row_rfnd->pkb_jumlah, 2, ',', '.')?></td>
											<td><?php echo $row_rfnd->cara_bayar; ?></td>
											<td><?php echo $row_rfnd->ref_jns_phk_nama; ?> - <?php echo $row_rfnd->nama_pihak; ?></td>
											<td><?php echo $row_rfnd->rapkb_nama; ?></td>
											<td><?php echo $row_rfnd->kd_rkng_nama; ?></td>
											<td><?php echo $row_rfnd->pkb_keterangan; ?></td>
											<td><?php echo $row_rfnd->pkb_no_kwitansi; ?></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row_rfnd->peg_buat; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {

        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

		$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
			$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
		});
	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
