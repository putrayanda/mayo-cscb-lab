<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method ?>/statusrajal_search/">
					<div class="form-group row">
						<div class="col-lg-3 mb-5">
							<div class='input-group' id='cr_daterangepicker'>
								<input type='text' class="form-control" name="cr_periode" id="cr_periode" readonly="readonly" placeholder="Pilih Periode" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_cabang"  id="cr_cabang" >
								<option label="Label"></option>
								<?=$comboCabang;?>
							</select>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_perawatan"  id="cr_perawatan" >
								<option label="Label"></option>
								<?=$comboPerawatan;?>
							</select>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-success mr-2 col-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_hd" class="table_report" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[50, 100, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="200">Cabang</th>
							<th data-sortable="true" data-width="150">Perawatan</th>
							<th data-sortable="true" data-width="100">Tgl. RM</th>
							<th data-sortable="true" data-width="200">Nama Pasien/Hewan</th>
							<th data-sortable="true" data-width="250">Nama Pemilik</th>
							<th data-sortable="true" data-width="250">Nama Dokter</th>
							<th data-sortable="true" data-width="175">Pendaftaran</th>
							<th data-sortable="true" data-width="175">Sudah DiVerifikasi</th>
							<th data-sortable="true" data-width="175">Dalam Pemeriksaan</th>
							<th data-sortable="true" data-width="175">Pemeriksaan Selesai</th>
							<th data-sortable="true" data-width="175">Menunggu Obat</th>
							<th data-sortable="true" data-width="175">Verifikasi Dokter</th>
							<th data-sortable="true" data-width="175">Menunggu Invoice</th>
							<th data-sortable="true" data-width="175">Siap Pulang</th>
							<th data-sortable="true" data-width="175">Selesai</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_rm){
							$no++;
							$rm_id			= $row_rm->rm_id;
							$waktuDaftar	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=1 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuVerCRO	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=2 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuDalamPeriksa	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=3 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuSelesaiPeriksa= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=4 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuTungguObat	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=5 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuVerDokter		= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=9 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuTungguInvoice	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=6 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuSiapPulang	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=7 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$waktuSiapSelesai	= $this->Mainmodel->get_one_data("(rm_stat_log_time)::timestamp(0) without time zone","rekam_medis_status_log", "rm_stat_log_status_id=8 AND rm_stat_log_rm_id=".$rm_id." ORDER BY rm_stat_log_time LIMIT 1");
							$umur = 0;
						?>
						<tr class="tr-class-<?php echo$no?> ">
							<td><?php echo $rm_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_rm->ref_cab_nama; ?></td>
							<td><?php echo $row_rm->ref_prwtn_ket; ?></td>
							<td><?php echo $row_rm->rm_tanggal; ?></td>
							<td><?php echo $row_rm->pas_nama; ?></td>
							<td><?php echo $row_rm->pem_nama; ?></td>
							<td><?php echo $row_rm->nama_dokter; ?></td>
							<td><?php echo $waktuDaftar; ?></td>
							<td><?php echo $waktuVerCRO; ?></td>
							<td><?php echo $waktuDalamPeriksa; ?></td>
							<td><?php echo $waktuSelesaiPeriksa; ?></td>
							<td><?php echo $waktuTungguObat; ?></td>
							<td><?php echo $waktuVerDokter; ?></td>
							<td><?php echo $waktuTungguInvoice; ?></td>
							<td><?php echo $waktuSiapPulang; ?></td>
							<td><?php echo $waktuSiapSelesai; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {

		$('#cr_cabang').select2({
			placeholder: "Pilih Cabang",
			allowClear: true
		});
			
		$('#cr_status').select2({
			placeholder: "Pilih Status",
			allowClear: true
		});
			
		$('#cr_perawatan').select2({
			placeholder: "Pilih Perawatan",
			allowClear: true
		});
			
        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
