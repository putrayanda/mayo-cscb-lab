<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<div class="card card-custom">
					<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
						<div class="ribbon-target mt-5">
							<span class="ribbon-inner bg-primary"></span>Daftar Invoice Lab & Rad Kolega
						</div>
					</div>
					<div class="card-body">	
						<table id="table_invoice" class="table_report" data-toggle="table" data-height="400" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="60" data-align="right">No</th>
									<th data-sortable="true" data-width="200">Cabang</th>
									<th data-sortable="true" data-width="250">Klinik Kolega</th>
									<th data-sortable="true" data-width="125">Tgl. Inv</th>
									<th data-sortable="true" data-width="125">Tgl. JT</th>
									<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
									<th data-sortable="true" data-width="125" data-align="right">Diskon</th>
									<th data-sortable="true" data-width="125" data-align="right">PPN</th>
									<th data-sortable="true" data-width="125" data-align="right">Total</th>
									<th data-sortable="true" data-width="125" data-align="right">Terbayar</th>
									<th data-sortable="true" data-width="125" data-align="right">Sisa</th>
									<th data-sortable="true" data-width="150">No. Invoice</th>
									<th data-sortable="true" data-width="100" data-align="center">Status ?</th>
									<th data-sortable="true" data-width="200">Peg. Invoice</th>
									<th data-sortable="true" data-width="75" data-align="center">CJ</th>
							</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_tagihan->result() as $row_tgh){
									$no++;
									$inv_id		= $row_tgh->inv_id;
									$inv_rm_id	= $row_tgh->inv_rm_id;
									if($rNum == $inv_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}

									$nett	= $row_tgh->total_labrad-$row_tgh->inv_diskon_nominal;
									$ppn	= $nett*($row_tgh->inv_ppn_persen/100);
									$total	= $nett+$ppn;
									$terbayar	= $row_tgh->jumlah_bayar;
									$sisa		= $total-$terbayar;

									if($row_tgh->inv_status == 2){
										$status = '<i class="fas fa-thumbs-up"></i>';
									}
									else{
										$status = '<i class="fas fa-thumbs-down"></i>';
									}
									$disabledCheck = '';
								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $inv_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_tgh->ref_cab_nama; ?></td>
									<td><?php echo $row_tgh->nama_pihak; ?></td>
									<td><?php echo $row_tgh->inv_tanggal; ?></td>
									<td><?php echo $row_tgh->inv_tanggal_jt; ?></td>
									<td><?php echo number_format($row_tgh->total_labrad, 2, ',', '.')?></td>
									<td><?php echo number_format($row_tgh->inv_diskon_nominal, 2, ',', '.')?></td>
									<td><?php echo number_format($ppn, 2, ',', '.')?></td>
									<td><?php echo number_format($total, 2, ',', '.')?></td>
									<td><?php echo number_format($terbayar, 2, ',', '.')?></td>
									<td><?php echo number_format($sisa, 2, ',', '.')?></td>
									<td><?php echo $row_tgh->inv_nomor; ?></td>
									<td><?php echo $status; ?></td>
									<td><?php echo $row_tgh->peg_buat; ?></td>
									<td><?php echo $row_tgh->count_jurnal; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>				
					</div>
				</div>
	
				<?php
					if($rNum > 0){
				?>
						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-info"></span>Daftar Rekam Medis Lab & Rad
								</div>
							</div>
							<div class="card-body">	
								<table id="table_hd" class="table_report" data-toggle="table" data-height="300" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-page-list="[1000, 5000, 10000]" data-page-size="1000" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="150" data-align="center">T. Periksa</th>
											<th data-sortable="true" data-width="200">Nama Hewan</th>
											<th data-sortable="true" data-width="150">Spesies</th>
											<th data-sortable="true" data-width="250">Pemilik</th>
											<th data-sortable="true" data-width="200">Klinik</th>
											<th data-sortable="true" data-width="350">Dokter</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="125" data-align="right">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_rm->result() as $row_dt){
											$no++;
											$rm_id	= $row_dt->inv_dt_rm_id;

										?>
										<tr class="tr-class-<?php echo $no?> ">
											<td><?php echo $rm_id; ?></td>
											<td><?php echo $row_dt->rm_tanggal; ?></td>
											<td><?php echo $row_dt->pas_nama; ?></td>
											<td><?php echo $row_dt->ref_spesies_nama; ?></td>
											<td><?php echo $row_dt->pem_nama; ?></td>
											<td><?php echo $row_dt->nama_klinik_rujukan; ?></td>
											<td><?php echo $row_dt->nama_dokter_rujukan; ?></td>
											<td><?php echo $row_dt->nama_produk_tagihan; ?></td>
											<td><?php echo number_format($row_dt->tagihan_labrad, 2, '.', ','); ?> </td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>

						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-info"></span>Daftar Penerimaan Tagihan/Invoice
								</div>
							</div>
							<div class="card-body">	
								<table id="table_terima" class="table_report" data-toggle="table" data-height="400" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="200">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Terima</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="300">Cara bayar</th>
											<th data-sortable="true" data-width="300">Pihak</th>
											<th data-sortable="true" data-width="300">Pemilik</th>
											<th data-sortable="true" data-width="150">No. Kwitansi</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="100" data-align="center">Proses ?</th>
											<th data-sortable="true" data-width="200">Peg.Terima</th>
											<th data-sortable="true" data-width="75" data-align="center">CJ</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_terima->result() as $row_trm){
											$no++;
											$pkb_id		= $row_trm->pkb_id;
											if($rNum == $pkb_id){
												$active = 'table-primary';
											}
											else{
												$active = '';
											}

											if($row_trm->pkb_flag_proses == 't'){
												$status = '<i class="fas fa-thumbs-up"></i>';
											}
											else{
												$status = '<i class="fas fa-thumbs-down"></i>';
											}
											$disabledCheck = '';
										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $pkb_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_trm->ref_cab_nama; ?></td>
											<td><?php echo $row_trm->pkb_tgl_terima; ?></td>
											<td><?php echo number_format($row_trm->pkb_jumlah, 2, ',', '.')?></td>
											<td><?php echo $row_trm->cara_bayar; ?></td>
											<td><?php echo $row_trm->ref_jns_phk_nama; ?> - <?php echo $row_trm->nama_pihak; ?></td>
											<td><?php echo $row_trm->nama_pemilik; ?></td>
											<td><?php echo $row_trm->pkb_no_kwitansi; ?></td>
											<td><?php echo $row_trm->pkb_keterangan; ?></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row_trm->peg_buat; ?></td>
											<td><?php echo $row_trm->count_jurnal; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>

				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {
		$('#cr_cabang').select2({
			placeholder: "Pilih Cabang",
			allowClear: true
		});
			
        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

		$('#table_invoice').on('click-row.bs.table', function (e, row, $element) {
			$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
		});
	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
