<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<div class="card card-custom">
					<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
						<div class="ribbon-target mt-5">
							<span class="ribbon-inner bg-warning"></span>Daftar Pasien Rawat Inap
						</div>
					</div>
					<div class="card-body">	
						<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/rekammedis_search/">
							<input type="hidden" name="peg_jabatan"  id="peg_jabatan" value="<?= $peg_jabatan?>"/>
							<div class="form-group row">
								<div class="col-lg-3 mb-5">
									<input type="text" class="form-control" placeholder="Nama Pasien/Hewan" name="cr_pasien"  id="cr_pasien" />
								</div>
								<div class="col-lg-3 mb-5">
									<input type="text" class="form-control" placeholder="No. Rekam Medis" name="cr_mrn"  id="cr_mrn" />
								</div>
								<div class="col-lg-3 mb-5">
									<input type="text" class="form-control" placeholder="Nama Pemilik"  name="cr_pemilik"  id="cr_pemilik"/>
								</div>
								<div class="col-lg-3 mb-5">
									<div class='input-group' id='cr_daterangepicker'>
										<input type='text' class="form-control" name="cr_periode" id="cr_periode" readonly="readonly" placeholder="Pilih Periode" />
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-calendar-check-o"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="col-lg-3 mb-5">
									<select class="form-control select2" name="cr_cabang"  id="cr_cabang" >
										<option label="Label"></option>
										<?=$comboCabang;?>
									</select>
								</div>
								<div class="col-lg-3">
									<button class="btn btn-success mr-2 col-12" type="submit" > Search
										<i class="fa fa-search"></i>
									</button>
								</div>
							</div>
						</form>
						<table id="table_hd" class="table_report" data-toggle="table" data-height="300" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-page-list="[1000, 5000, 10000]" data-page-size="1000" data-show-export="true">
							<thead>
								<tr>
									<th data-field="row_id" data-visible="false">ID</th>
									<th data-sortable="true" data-width="60" data-align="right">No</th>
									<th data-sortable="true" data-width="225">Lokasi/Cabang</th>
									<th data-sortable="true" data-width="230" data-align="center">T. Inap</th>
									<th data-sortable="true" data-width="225">Nama Hewan</th>
									<th data-sortable="true" data-width="150">Spesies</th>
									<th data-sortable="true" data-width="300">Pemilik</th>
									<th data-sortable="true" data-width="125" data-align="right">Count Invoice</th>
									<th data-sortable="true" data-width="125" data-align="right">Total Tagihan</th>
									<th data-sortable="true" data-width="125" data-align="right">Deposit</th>
									<th data-sortable="true" data-width="125" data-align="right">Terbayar</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								foreach($query_rm->result() as $row_rm){
									$no++;
									$rm_id	= $row_rm->rm_id;
									if($rNum == $rm_id){
										$active = 'table-primary';
									}
									else{
										$active = '';
									}
									$count_tagihan	= $row_rm->count_tagihan_inap;
									$count_tinob	= $row_rm->count_tindakan_obat_ri;
									$tghn_tinob		= $row_rm->tagihan_tindakan_obat_ri;
									$jml_deposit		= $row_rm->jumlah_deposit_ri;
									$jml_bayar		= $row_rm->jumlah_pembayaran;

								?>
								<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
									<td><?php echo $rm_id; ?></td>
									<td><?php echo $no?></td>
									<td><?php echo $row_rm->ref_cab_nama; ?></td>
									<td><?php echo $row_rm->rm_tanggal; ?> - <?php echo $row_rm->tgl_akhir_inap; ?> (<?php echo $row_rm->lama_inap; ?> hr) </td>
									<td><?php echo $row_rm->pas_nama; ?> - <?php echo $row_rm->pas_mrn; ?></td>
									<td><?php echo $row_rm->ref_spesies_nama; ?></td>
									<td><?php echo $row_rm->pem_nama; ?> - <?php echo $row_rm->pem_no_hp_wa; ?></td>
									<td><?php echo $count_tagihan; ?></td>
									<td><?php echo number_format($tghn_tinob, 2, '.', ','); ?> (<?php echo $count_tinob; ?>)</td>
									<td><?php echo number_format($jml_deposit, 2, '.', ','); ?></td>
									<td><?php echo number_format($jml_bayar, 2, '.', ','); ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
	
				<?php
					if($rNum > 0){
				?>
						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-success"></span>Daftar Tindakan/Obat
								</div>
							</div>
							<div class="card-body">	
								<table id="table_tindakan" data-toggle="table" data-height="350" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="400">Nama Tindakan/Obat</th>
											<th data-sortable="true" data-width="100">Tgl. RM</th>
											<th data-sortable="true" data-width="100" data-align="right">Qty</th>
											<th data-sortable="true" data-width="100" data-align="right">Diskon</th>
											<th data-sortable="true" data-width="125" data-align="right">Harga</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_tindakan->result() as $row_tind){
											$no++;
											$rm_tinob_id	= $row_tind->rm_tinob_id;

										?>
										<tr class="tr-class-<?php echo $no?> ">
											<td><?php echo $rm_tinob_id; ?></td>
											<td><?php echo $row_tind->ref_prod_nama; ?></td>
											<td><?php echo $row_tind->rm_tanggal; ?></td>
											<td><?php echo number_format($row_tind->rm_tinob_qty, 2, '.', ','); ?></td>
											<td><?php echo number_format($row_tind->rm_tinob_disc, 2, '.', ','); ?></td>
											<td><?php echo number_format($row_tind->rm_tinob_harga, 2, '.', ','); ?></td>
											<td><?php echo number_format($row_tind->netto, 2, '.', ','); ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>

						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-info"></span>Daftar Penerimaan Deposit
								</div>
							</div>
							<div class="card-body">	
								<table id="table_terima" class="table_report" data-toggle="table" data-height="400" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="200">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Terima</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="300">Cara bayar</th>
											<th data-sortable="true" data-width="300">Pihak</th>
											<th data-sortable="true" data-width="300">Pemilik</th>
											<th data-sortable="true" data-width="200">Asal</th>
											<th data-sortable="true" data-width="300">Akun Penerimaan</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="150">No. Kwitansi</th>
											<th data-sortable="true" data-width="100" data-align="center">Proses ?</th>
											<th data-sortable="true" data-width="200">Peg.Terima</th>
											<th data-sortable="true" data-width="125" data-align="right">CJ</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_deposit->result() as $row_dpst){
											$no++;
											$pkb_id		= $row_dpst->pkb_id;
											if($rNum == $pkb_id){
												$active = 'table-primary';
											}
											else{
												$active = '';
											}

											if($row_dpst->pkb_flag_proses == 't'){
												$status = '<i class="fas fa-thumbs-up"></i>';
											}
											else{
												$status = '<i class="fas fa-thumbs-down"></i>';
											}
											$disabledCheck = '';
										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $pkb_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_dpst->ref_cab_nama; ?></td>
											<td><?php echo $row_dpst->pkb_tgl_terima; ?></td>
											<td><?php echo number_format($row_dpst->pkb_jumlah, 2, ',', '.')?></td>
											<td><?php echo $row_dpst->cara_bayar; ?></td>
											<td><?php echo $row_dpst->ref_jns_phk_nama; ?> - <?php echo $row_dpst->nama_pihak; ?></td>
											<td><?php echo $row_dpst->nama_pemilik; ?></td>
											<td><?php echo $row_dpst->rapkb_nama; ?></td>
											<td><?php echo $row_dpst->kd_rkng_nama; ?></td>
											<td><?php echo $row_dpst->pkb_keterangan; ?></td>
											<td><?php echo $row_dpst->pkb_no_kwitansi; ?></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row_dpst->peg_buat; ?></td>
											<td><?php echo $row_dpst->count_jurnal; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>

						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-info"></span>Daftar Pengeluaran Refund Deposit
								</div>
							</div>
							<div class="card-body">	
								<table id="table_terima" class="table_report" data-toggle="table" data-height="400" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="200">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Bayar</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="300">Cara  Bayar</th>
											<th data-sortable="true" data-width="300">Pihak</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="150">No. Kwitansi</th>
											<th data-sortable="true" data-width="125" data-align="right">CJ</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_refund->result() as $row_rfnd){
											$no++;
											$pkb_id		= $row_rfnd->pkb_id;

										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $pkb_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_rfnd->ref_cab_nama; ?></td>
											<td><?php echo $row_rfnd->pkb_tgl_keluar; ?></td>
											<td><?php echo number_format($row_rfnd->pkb_jumlah, 2, ',', '.')?></td>
											<td><?php echo $row_rfnd->cara_bayar; ?></td>
											<td><?php echo $row_rfnd->ref_jns_phk_nama; ?> - <?php echo $row_rfnd->nama_pihak; ?></td>
											<td><?php echo $row_rfnd->pkb_keterangan; ?></td>
											<td><?php echo $row_rfnd->pkb_no_kwitansi; ?></td>
											<td><?php echo $row_rfnd->count_jurnal; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>

						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-primary"></span>Daftar Invoice
								</div>
							</div>
							<div class="card-body">	
								<table id="table_invoice" class="table_report" data-toggle="table" data-height="400" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="225">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Inv</th>
											<th data-sortable="true" data-width="125">Tgl. JT</th>
											<th data-sortable="true" data-width="250">Pasien</th>
											<th data-sortable="true" data-width="350">Pemilik</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="125" data-align="right">Diskon</th>
											<th data-sortable="true" data-width="125" data-align="right">PPN</th>
											<th data-sortable="true" data-width="125" data-align="right">Total</th>
											<th data-sortable="true" data-width="125" data-align="right">Terbayar</th>
											<th data-sortable="true" data-width="125" data-align="right">Sisa</th>
											<th data-sortable="true" data-width="150">No. Invoice</th>
											<th data-sortable="true" data-width="100" data-align="center">Status ?</th>
											<th data-sortable="true" data-width="200">Peg. Invoice</th>
											<th data-sortable="true" data-width="75" data-align="center">CJ</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_tagihan->result() as $row_tgh){
											$no++;
											$inv_id		= $row_tgh->inv_id;
											$inv_rm_id	= $row_tgh->inv_rm_id;
											if($rNum == $inv_id){
												$active = 'table-primary';
											}
											else{
												$active = '';
											}
											$nett		= $row_tgh->total_netto_ri-$row_tgh->inv_diskon_nominal;
											$ppn		= $nett*($row_tgh->inv_ppn_persen/100);
											$total		= $nett+$ppn;
											$terbayar	= $row_tgh->jumlah_bayar;
											$sisa		= $total-$terbayar;

											if($row_tgh->inv_status == 2){
												$status = '<i class="fas fa-thumbs-up"></i>';
											}
											else{
												$status = '<i class="fas fa-thumbs-down"></i>';
											}
											$disabledCheck = '';
										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $inv_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_tgh->ref_cab_nama; ?></td>
											<td><?php echo $row_tgh->inv_tanggal; ?></td>
											<td><?php echo $row_tgh->inv_tanggal_jt; ?></td>
											<td><?php echo $row_tgh->pas_nama; ?> - <?php echo $row_tgh->pas_mrn; ?></td>
											<td><?php echo $row_tgh->pem_nama; ?> - <?php echo $row_tgh->pem_no_hp_wa; ?></td>
											<td><?php echo number_format($row_tgh->total_netto_ri, 2, ',', '.')?></td>
											<td><?php echo number_format($row_tgh->inv_diskon_nominal, 2, ',', '.')?></td>
											<td><?php echo number_format($ppn, 2, ',', '.')?></td>
											<td><?php echo number_format($total, 2, ',', '.')?></td>
											<td><?php echo number_format($terbayar, 2, ',', '.')?></td>
											<td><?php echo number_format($sisa, 2, ',', '.')?></td>
											<td><?php echo $row_tgh->inv_nomor; ?></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row_tgh->peg_buat; ?></td>
											<td><?php echo $row_tgh->count_jurnal; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>

						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-5">
									<span class="ribbon-inner bg-info"></span>Daftar Penerimaan Tagihan/Invoice
								</div>
							</div>
							<div class="card-body">	
								<table id="table_terima" class="table_report" data-toggle="table" data-height="400" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="true">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-sortable="true" data-width="60" data-align="right">No</th>
											<th data-sortable="true" data-width="200">Cabang</th>
											<th data-sortable="true" data-width="125">Tgl. Terima</th>
											<th data-sortable="true" data-width="125" data-align="right">Jumlah</th>
											<th data-sortable="true" data-width="300">Cara bayar</th>
											<th data-sortable="true" data-width="300">Pihak</th>
											<th data-sortable="true" data-width="300">Pemilik</th>
											<th data-sortable="true" data-width="150">No. Kwitansi</th>
											<th data-sortable="true" data-width="300">Keterangan</th>
											<th data-sortable="true" data-width="100" data-align="center">Proses ?</th>
											<th data-sortable="true" data-width="200">Peg.Terima</th>
											<th data-sortable="true" data-width="75" data-align="center">CJ</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										foreach($query_terima->result() as $row_trm){
											$no++;
											$pkb_id		= $row_trm->pkb_id;
											if($rNum == $pkb_id){
												$active = 'table-primary';
											}
											else{
												$active = '';
											}

											if($row_trm->pkb_flag_proses == 't'){
												$status = '<i class="fas fa-thumbs-up"></i>';
											}
											else{
												$status = '<i class="fas fa-thumbs-down"></i>';
											}
											$disabledCheck = '';
										?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $pkb_id; ?></td>
											<td><?php echo $no?></td>
											<td><?php echo $row_trm->ref_cab_nama; ?></td>
											<td><?php echo $row_trm->pkb_tgl_terima; ?></td>
											<td><?php echo number_format($row_trm->pkb_jumlah, 2, ',', '.')?></td>
											<td><?php echo $row_trm->cara_bayar; ?></td>
											<td><?php echo $row_trm->ref_jns_phk_nama; ?> - <?php echo $row_trm->nama_pihak; ?></td>
											<td><?php echo $row_trm->nama_pemilik; ?></td>
											<td><?php echo $row_trm->pkb_no_kwitansi; ?></td>
											<td><?php echo $row_trm->pkb_keterangan; ?></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row_trm->peg_buat; ?></td>
											<td><?php echo $row_trm->count_jurnal; ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>				
							</div>
						</div>

				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {
		$('#cr_cabang').select2({
			placeholder: "Pilih Cabang",
			allowClear: true
		});
			
        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

		$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
			$(location).attr('href','<?php echo current_url();?>/?rNum='+row.row_id);
		});
	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
