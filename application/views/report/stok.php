<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/rekammedislabrad_search/">
					<div class="form-group row">
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="Nama Barang" name="cr_barang"  id="cr_barang" />
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_cabang"  id="cr_cabang" >
								<option label="Label"></option>
								<?=$comboCabang;?>
							</select>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_vendor"  id="cr_vendor" >
								<option label="Label"></option>
								<?=$comboSupplier;?>
							</select>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-success mr-2 col-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_hd" class="table_report" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[50, 100, 1000]" data-page-size="100" data-show-export="true">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="150" data-align="center">Nama Barang</th>
							<th data-sortable="true" data-width="200">Tgl. Masuk</th>
							<th data-sortable="true" data-width="150">Harga</th>
							<th data-sortable="true" data-width="150">Jumlah Masuk</th>
							<th data-sortable="true" data-width="225">Jumlah Pakai</th>
							<th data-sortable="true" data-width="200">Jumlah Sisa</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_stok){
							$no++;
							$stok_id	= $row_stok->stok_id;
						?>
						<tr class="tr-class-<?php echo$no?> ">
							<td><?php echo $stok_id; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_stok->ref_prod_nama; ?></td>
							<td><?php echo $row_stok->stok_tgl_masuk; ?></td>
							<td><?php echo number_format($row_stok->stok_harga, 2, ',', '.')?></td>
							<td><?php echo number_format($row_stok->stok_jumlah_masuk, 2, ',', '.')?></td>
							<td><?php echo number_format($row_stok->stok_jumlah_pakai, 2, ',', '.')?></td>
							<td><?php echo number_format($row_stok->stok_jumlah_sisa, 2, ',', '.')?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {

		$('#cr_cabang').select2({
			placeholder: "Pilih Cabang",
			allowClear: true
		});
			
		$('#cr_vendor').select2({
			placeholder: "Pilih Vendor",
			allowClear: true
		});
			
        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
