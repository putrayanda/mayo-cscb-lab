<script src="assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<style>
	.table_report {
		table-layout: fixed;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-12" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!--end::Subheader-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<!--begin::Card-->
		<div class="card card-custom gutter-b">
			<div class="card-header flex-wrap py-3">
				<div class="card-title">
					<h3 class="card-label"><?= $title?>
					<span class="d-block text-muted pt-2 font-size-sm">Informasi <?= $title?></span></h3>
				</div>
				<div class="card-toolbar">
				</div>
			</div>
			<div class="card-body">
				<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/rekammedis_search/">
					<input type="hidden" name="peg_jabatan"  id="peg_jabatan" value="<?= $peg_jabatan?>"/>
					<div class="form-group row">
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="Nama Pasien/Hewan" name="cr_pasien"  id="cr_pasien" />
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="No. Rekam Medis" name="cr_mrn"  id="cr_mrn" />
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="Nama Pemilik"  name="cr_pemilik"  id="cr_pemilik"/>
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="No. HP Pemilik"  name="cr_no_hp"  id="cr_no_hp"/>
						</div>
						<div class="col-lg-3 mb-5">
							<input type="text" class="form-control" placeholder="No. Resep"  name="cr_no_resep"  id="cr_no_resep"/>
						</div>
						<div class="col-lg-3 mb-5">
							<div class='input-group' id='cr_daterangepicker'>
								<input type='text' class="form-control" name="cr_periode" id="cr_periode" readonly="readonly" placeholder="Pilih Periode" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_cabang"  id="cr_cabang" >
								<option label="Label"></option>
								<?=$comboCabang;?>
							</select>
						</div>
						<div class="col-lg-3 mb-5">
							<select class="form-control select2" name="cr_status"  id="cr_status" >
								<option label="Label"></option>
								<?=$comboStatus;?>
							</select>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-success mr-2 col-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
				<table id="table_hd" class="table_report" data-toggle="table" data-height="500" data-show-columns="true" data-search="true" data-show-toggle="false" data-pagination="false" data-page-list="[50, 100, 1000]" data-page-size="100" data-show-export="true" data-reorderable-columns="false">
					<thead>
						<tr>
							<th data-field="row_id" data-visible="false">ID</th>
							<th data-field="row_status_id" data-visible="false">Stat Id</th>
							<th data-sortable="true" data-width="60" data-align="right">No</th>
							<th data-sortable="true" data-width="150" data-align="center">T. Create</th>
							<th data-sortable="true" data-width="125" data-align="center">D. Create</th>
							<th data-sortable="true" data-width="125" data-align="center">M. Create</th>
							<th data-sortable="true" data-width="150" data-align="center">T. Rawat Jalan</th>
							<th data-sortable="true" data-width="150" data-align="center">D. Rawat Jalan</th>
							<th data-sortable="true" data-width="150" data-align="center">M. Rawat Jalan</th>
							<th data-sortable="true" data-width="200">Lokasi</th>
							<th data-sortable="true" data-width="150">Komunitas</th>
							<th data-sortable="true" data-width="150">Perawatan</th>
							<th data-sortable="true" data-width="200">Dokter</th>
							<th data-sortable="true" data-width="200">Nama Hewan</th>
							<th data-sortable="true" data-width="125">No. RM</th>
							<th data-sortable="true" data-width="135" data-align="right">Umur (Bulan)</th>
							<th data-sortable="true" data-width="135" data-align="right">Berat (Kg)</th>
							<th data-sortable="true" data-width="200">Pemilik</th>
							<th data-sortable="true" data-width="150">No Telp</th>
							<th data-sortable="true" data-width="200">Kota</th>
							<th data-sortable="true" data-width="200">Kecamatan</th>
							<th data-sortable="true" data-width="750">Keluhan / Gejala</th>
							<th data-sortable="true" data-width="500">Resep Obat</th>
							<th data-sortable="true" data-width="500">Tindakan/Obat (Billing)</th>
							<th data-sortable="true" data-width="150">No. Resep</th>
							<th data-sortable="true" data-width="150">Daftar Via</th>
							<th data-sortable="true" data-width="150">Status</th>
							<th data-sortable="true" data-width="150">Keputusan</th>
							<th data-sortable="true" data-width="100" data-align="right">No. Urut</th>
							<th data-sortable="true" data-width="500">Note Internal</th>
							<th data-sortable="true" data-width="200">Pre Groomer</th>
							<th data-sortable="true" data-width="200">Post Groomer</th>
							<th data-sortable="true" data-width="150">No. Invoice</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no	= 0;
						foreach($query_hd->result() as $row_rm){
							$no++;
							$rm_id	= $row_rm->rm_id;
							$umur	= ($row_rm->rm_umur_tahun*12)+$row_rm->rm_umur_bulan;
							if($row_rm->rm_from == 1){
								$asal = 'Halo Leo';
							}
							else{
								$asal = 'CSCB';
							}
							if($row_rm->rm_komunitas == 1){
								$komu = 'Ya';
							}
							else{
								$komu = 'Tidak';
							}
						?>
						<tr class="tr-class-<?php echo$no?> ">
							<td><?php echo $rm_id; ?></td>
							<td><?php echo $row_rm->rm_status; ?></td>
							<td><?php echo $no?></td>
							<td><?php echo $row_rm->rm_time; ?></td>
							<td><?php echo $row_rm->tgl_create; ?></td>
							<td><?php echo $row_rm->bln_create; ?></td>
							<td><?php echo $row_rm->rm_tanggal; ?></td>
							<td><?php echo $row_rm->tgl_rm; ?></td>
							<td><?php echo $row_rm->bln_rm; ?></td>
							<td><?php echo $row_rm->ref_cab_nama; ?></td>
							<td><?php echo $komu; ?></td>
							<td><?php echo $row_rm->ref_prwtn_ket; ?></td>
							<td><?php echo $row_rm->nama_dokter; ?></td>
							<td><?php echo $row_rm->pas_nama; ?></td>
							<td><?php echo $row_rm->pas_mrn; ?></td>
							<td><?php echo $umur; ?></td>
							<td><?php echo $row_rm->rm_berat; ?></td>
							<td><?php echo $row_rm->pem_nama; ?></td>
							<td><?php echo $row_rm->pem_no_hp_wa; ?></td>
							<td><?php echo $row_rm->ref_kota_ket; ?></td>
							<td><?php echo $row_rm->ref_kcmtn_ket; ?></td>
							<td><?php echo nl2br($row_rm->rm_anamnesis_ket) ?></td>
							<td><?php echo $row_rm->rm_resep_obat; ?></td>
							<td>
								<?php
								$query_rm_obat	= $this->db->query("SELECT * FROM v_rekam_medis_tindakan_obat WHERE ref_prod_jenis IN (1,9) AND rm_tinob_rm_id= ".$row_rm->rm_id);
								foreach($query_rm_obat->result() as $row_rm_obt){
								?>
									<?php echo $row_rm_obt->rm_tinob_qty.'&nbsp;'.$row_rm_obt->ref_prod_nama.' - '.$row_rm_obt->rm_tinob_petunjuk_pakai ?><br/>
								<?php
								}
								?>
							</td>
							<td><?php echo $row_rm->rm_no_resep; ?></td>
							<td><?php echo $asal; ?></td>
							<td><?php echo $row_rm->rm_status_ket; ?></td>
							<td><?php echo $row_rm->rm_kep_ket; ?></td>
							<td><?php echo $row_rm->rm_no_urut; ?></td>
							<td><?php echo $row_rm->rm_keterangan_internal; ?></td>
							<td><?php echo $row_rm->pre_groomer; ?></td>
							<td><?php echo $row_rm->post_groomer; ?></td>
							<td><?php echo $row_rm->inv_nomor; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#table_hd').on('click-row.bs.table', function (e, row, $element) {
		var peg_jabatan = $('#peg_jabatan').val();
		var method = 'cro';
		if(peg_jabatan==3){
			var method = 'dokter';
		}
		window.open('<?php echo base_url()?>'+method+'/grooming/?rNum='+row.row_id, '_blank');
	});

	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {

		$('#cr_cabang').select2({
			placeholder: "Pilih Cabang",
			allowClear: true
		});
			
		$('#cr_status').select2({
			placeholder: "Pilih Status",
			allowClear: true
		});
			
        $('#cr_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#cr_daterangepicker .form-control').val( start.format('DD/MM/YYYY') + ' - '  + end.format('DD/MM/YYYY'));
        });

	});
</script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
