<link href="assets/css/pages/wizard/wizard-kunjungan-cro.css?v=7.0.4" rel="stylesheet" type="text/css" />
<link href="assets/bower_components/bootstrap-table/bootstrap-table.css" rel="stylesheet" type="text/css">
<link href="assets/bower_components/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<script src="assets/bower_components/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
<style>
	.select2-container {
	width: 100% !important;
	padding: 0;

	.kv-file-upload{
		color:#fff !important;
		visibility: hidden !important;
	}

	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}
}
</style>


<link href="assets/css/pages/wizard/wizard-diagnosa-telemedicine-dokter.css?v=7.0.4" rel="stylesheet" type="text/css" />
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Hasil Lab & Rad
					<i class="mr-2"></i>
					<small class="">Untuk mengisi hasil laboratorium & radiologi</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method.'/?rNum='.$rNum;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
				</div>
			</div>
			<div class="card-body">
				<div class="accordion accordion-solid accordion-toggle-plus" id="accordionStatusPresent">
					<?php
					if($countRontgen > 0){
					?>
					<!-- RONTGEN -->
					<div class="card">
						<div class="card-header" id="headingRontgen">
							<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseRontgen">
							<i class="fas fa-fingerprint"></i>Rontgen</div>
						</div>
						<div id="collapseRontgen" class="collapse show" data-parent="#accordionStatusPresent">
							<form id="kt_form_rontgen" role="form" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_rontgen/?rNum=<?php echo $rNum?>">
							<div class="card-body">
								<div class="form-group row">
									<div class="col-lg-4 col-sm-4 col-xs-12">
										<label>Dokter:</label>
										<select class="form-control form-control-solid select2" name="inp_dokter_rontgen"  id="inp_dokter_rontgen">
											<option label="Label"></option>
											<?=$comboDokterRontgen;?>
										</select>
									</div>
									<div class="col-lg-4 col-sm-4 col-xs-12">
										<label>Teknisi:</label>
										<select class="form-control form-control-solid select2" name="inp_teknisi_rontgen"  id="inp_teknisi_rontgen">
											<option label="Label"></option>
											<?=$comboTeknisiRontgen;?>
										</select>
									</div>
									<div class="col-lg-2 col-sm-2">
										<label>Qty Film Pakai:</label>
										<input type="number" class="form-control text-right" placeholder="Isi Qty" name="inp_qty_film_pakai"  id="inp_qty_film_pakai" value="<?php echo $filmPakaiRontgen;?>" required/>
									</div>
									<div class="col-lg-2 col-sm-2">
										<label>Qty Film Error:</label>
										<input type="number" class="form-control text-right" placeholder="Isi Qty" name="inp_qty_film_error"  id="inp_qty_film_error" value="<?php echo $filmErrorRontgen;?>" required/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-12 col-sm-12 col-xs-12">
										<label>Kesimpulan:</label>
										<textarea class="form-control form-control-solid" id="inp_ket_rontgen" name="inp_ket_rontgen"  rows="2"><?php echo $ketHasilRontgen;?></textarea>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-12" align="center">
										<input type="hidden" name="id_labrad_rontgen"  id="id_labrad_rontgen" value="<?php echo $idRontgen;?>"/>
										<button type="submit" class="btn btn-primary font-weight-bolder" name="act_rontgen"  id="act_rontgen" value="simpan">
										<i class="flaticon-edit-1 icon-sm"></i> Simpan</button>
									</div>
								</div>
							</div>
							</form>
							<div class="card-body">
								<table id="table_hasil" class="table_report" data-toggle="table" data-height="300" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-show-export="false" data-reorderable-columns="false">
									<thead>
										<tr>
											<th data-field="row_id" data-visible="false">ID</th>
											<th data-width="200">Posisi</th>
											<th data-width="200">Regio</th>
											<th>Temuan</th>
										</tr>
									</thead>
									<tbody>
								<?php
									$no	= 0;
									$query_hsl_rontgen	= "SELECT * FROM v_rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (70) AND rm_labrad_hsl_rm_id=".$rNum." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan";
									$rhHslRontgen		= $this->db->query($query_hsl_rontgen);
									foreach($rhHslRontgen->result() as $rowHslRontgen){
										$no++;
										if($rNum2 == $rowHslRontgen->rm_labrad_hsl_id){
											$active = 'table-primary';
										}
										else{
											$active = '';
										}
									?>
										<tr class="tr-class-<?php echo$no?> <?php echo $active?> ">
											<td><?php echo $rowHslRontgen->rm_labrad_hsl_id; ?></td>
											<td><?php echo $rowHslRontgen->posisi_rontgen; ?></td>
											<td><?php echo $rowHslRontgen->regio_rontgen; ?></td>
											<td><?php echo $rowHslRontgen->rm_labrad_hsl_temuan; ?></td>
										</tr>
									<?php
									}
								?>
									</tbody>
								</table>
							</div>
							<form id="kt_form_rontgen_dt" role="form" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_dt_rontgen/?rNum=<?php echo $rNum?>">
							<div class="card-body">
								<div class="form-group row">
									<div class="col-lg-4 col-sm-2 col-xs-12">
										<label>Posisi:</label>
										<select class="form-control form-control-solid select2" name="inp_posisi_rontgen"  id="inp_posisi_rontgen">
											<option label="Label"></option>
											<?=$comboPosisiRontgen;?>
										</select>
									</div>
									<div class="col-lg-4 col-sm-2 col-xs-12">
										<label>Regio:</label>
										<select class="form-control form-control-solid select2" name="inp_regio_rontgen"  id="inp_regio_rontgen">
											<option label="Label"></option>
											<?=$comboRegioRontgen;?>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-lg-12 col-sm-12 col-xs-12">
										<label>Temuan:</label>
										<textarea class="form-control form-control-solid" id="inp_temuan_rontgen" name="inp_temuan_rontgen"  rows="2"><?php echo $temuanRontgen;?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label>Foto Hasil:</label>
									<div class="col-12 card-body">	
										<div class="d-flex justify-content-center">
										<?php
										$query_foto	= $this->db->query("SELECT * FROM rekam_medis_labrad_hasil_foto_video WHERE rmfv_labrad_hsl_rm_labrad_id= ".$idRontgen);

										foreach($query_foto->result() as $row_foto){
											$rmfv_labrad_hsl_id = $row_foto->rmfv_labrad_hsl_id;
											if(preg_match('/^.*\.(mp4|mkv)$/i', $row_foto->rmfv_labrad_hsl_rm_url)) {
												$thumbnail = "https://cscb.leonvets.id/assets/file/foto_rekam_medis/videos.png";
											}
											else{
												$thumbnail = $row_foto->rmfv_labrad_hsl_rm_url;
											}
										?>
										<div>
											<?php
											if($rm_status <= 8){
											?>
												<div class="mb-2 text-center">
													<a href="<?php echo base_url().$class.'/'.$method.'/del_foto/?rNum='.$rNum.'&rNum2='.$rmfv_labrad_hsl_id ?>" class="btn btn-icon btn-outline-danger btn-xs"><i class="fas fa-trash"></i></a>
												</div>
											<?php
											}
											?>
											<a class="ml-2 mr-2" data-caption="<?php echo $row_foto->rmfv_labrad_hsl_rm_keterangan ?>" data-fancybox="gallery" href="<?php echo $row_foto->rmfv_labrad_hsl_rm_url ?>"  >
												<img width="150px" class="rounded" src="<?php echo $thumbnail ?>" />
											</a>
										</div>
										<?php
										}
										?>
										</div>
									</div>
									<div class="col-12">
										<div class="file-loading">
											<input type="file" name="file_hasil_rontgen[]" id="file_hasil_rontgen" multiple data-allowed-file-extensions='[ "jpg", "jpeg", "png", "mkv", "mp4"]' />
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-12" align="center">
										<input type="hidden" name="id_labrad_rontgen_"  id="id_labrad_rontgen_" value="<?php echo $idRontgen;?>"/>
										<input type="hidden" name="id_hsl_labrad_rontgen"  id="id_hsl_labrad_rontgen" value="<?php echo $rNum2;?>"/>
										<button type="submit" class="btn btn-primary font-weight-bolder" name="act_rontgen_dt"  id="act_rontgen_dt_simpan" value="simpan"><i class="flaticon-edit-1 icon-sm"></i> <?php echo $textSubmitHasil ?> </button>
										<?php
										if($rNum2 > 0){
										?>
										&nbsp;&nbsp;
										<button type="submit" class="btn btn-danger font-weight-bolder" name="act_rontgen_dt"  id="act_rontgen_dt_hapus" value="Hapus"><i class="flaticon-delete icon-sm"></i> Hapus </button>
										<?php
										}
										?>
									</div>
								</div>
							</div>
							</form>
						</div>

					</div>
					<!-- END RONTGEN -->
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Entry-->


<script src="assets/js/pages/custom/wizard/wizard-diagnosa-telemedicine-dokter.js?v=7.0.4"></script>
<script src="assets/js/pages/crud/file-upload/image-input.js?v=7.0.4"></script>
<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
<script>
	jQuery(document).ready(function() {
		$('#table_hasil').on('click-row.bs.table', function (e, row, $element) {
			$(location).attr('href','<?php echo current_url();?>/?rNum=<?php echo $rNum ?>'+'&rNum2='+row.row_id);
		});

		$('#inp_dokter_rontgen').select2({
			placeholder: "Pilih",
			allowClear: true
		});

		$('#inp_teknisi_rontgen').select2({
			placeholder: "Pilih",
			allowClear: true
		});

		$('#inp_posisi_rontgen').select2({
			placeholder: "Pilih",
			allowClear: true
		});

		$('#inp_regio_rontgen').select2({
			placeholder: "Pilih",
			allowClear: true
		});

		$("#file_hasil_rontgen").fileinput({
			'theme': 'explorer-fas',
			'showUpload': false
		});
	});


Fancybox.bind('[data-fancybox="gallery"]', {
  //dragToClose: false,
  Thumbs: false,

  Image: {
	zoom: false,
	click: false,
	wheel: "slide",
  },

  on: {
	// Move caption inside the slide
	reveal: (f, slide) => {
	  slide.$caption && slide.$content.appendChild(slide.$caption);
	},
  },
});
</script>

