<link href="assets/css/pages/wizard/wizard-kunjungan-cro.css?v=7.0.4" rel="stylesheet" type="text/css" />
<link href="assets/bower_components/bootstrap-table/bootstrap-table.css" rel="stylesheet" type="text/css">
<link href="assets/bower_components/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<script src="assets/bower_components/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
<style>
	.select2-container {
	width: 100% !important;
	padding: 0;

	.kv-file-upload{
		color:#fff !important;
		visibility: hidden !important;
	}

	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}
}
</style>


<link href="assets/css/pages/wizard/wizard-diagnosa-telemedicine-dokter.css?v=7.0.4" rel="stylesheet" type="text/css" />
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="card card-custom gutter-b">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">Form Hasil Lab & Rad
					<i class="mr-2"></i>
					<small class="">Untuk mengisi hasil laboratorium & radiologi</small></h3>
				</div>
				<div class="card-toolbar">
					<a href="<?php echo base_url().$class.'/'.$method.'/?rNum='.$rNum;?>" class="btn btn-light-primary font-weight-bolder mr-2">
					<i class="ki ki-long-arrow-back icon-sm"></i>Back</a>
				</div>
			</div>
			<div class="card-body">
				<div class="accordion accordion-solid accordion-toggle-plus" id="accordionStatusPresent">
					<?php
					if($countCekDar > 0){
					?>
					<!-- PEMERIKSAAN DARAH -->
					<div class="card">
						<div class="card-header" id="headingKimiaDarah">
							<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseKimiaDarah">
							<i class="fas fa-fingerprint"></i>Pemeriksaan Darah</div>
						</div>
						<div id="collapseKimiaDarah" class="collapse show" data-parent="#accordionStatusPresent">
							<form id="kt_form_cek_darah" role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud_prks_darah/?rNum=<?php echo $rNum?>">
							<div class="card-body">
								<table id="table_hasil" class="table_report" data-toggle="table" data-height="300" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-show-export="false" data-reorderable-columns="false">
									<thead>
										<tr>
											<th data-width="200">Nama Pemeriksaan</th>
											<th data-width="150">Hasil</th>
											<th data-width="150">Nilai Rujukan <?php echo $ref_spesies_nama ?></th>
											<th data-width="100">Satuan</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no	= 0;
										$query_rm_labrad	= $this->db->query("SELECT * FROM rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (72,88,113) AND rm_labrad_hsl_rm_id=".$rNum." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan");
										foreach($query_rm_labrad->result() as $row_rm_labrad){
											$no++;
											if($pas_spesies == 1){
												$nilai_rujukan = $row_rm_labrad->rm_labrad_hsl_nilai_rujukan_anjing;
											}
											elseif($pas_spesies == 2){
												$nilai_rujukan = $row_rm_labrad->rm_labrad_hsl_nilai_rujukan_kucing;
											}
											else{
												$nilai_rujukan = $row_rm_labrad->rm_labrad_hsl_nilai_rujukan_lain;
											}
										?>
										<tr class="tr-class-<?php echo $no?> ">
											<td><?php echo $row_rm_labrad->rm_labrad_hsl_nama; ?><input type="hidden" name="row_id_<?php echo $no;?>"  id="row_id_<?php echo $no;?>" value="<?php echo $row_rm_labrad->rm_labrad_hsl_id;?>"/></td>
											<td><input type="text" class="form-control" name="inp_hasil_<?php echo $no;?>"  id="inp_hasil_<?php echo $no;?>" value="<?php echo $row_rm_labrad->rm_labrad_hsl_nilai_hasil;?>" required/></td>
											<td><input type="text" class="form-control" name="inp_rujukan_<?php echo $no;?>"  id="inp_rujukan_<?php echo $no;?>" value="<?php echo $nilai_rujukan;?>" required/></td>
											<td><?php echo $row_rm_labrad->rm_labrad_hsl_satuan; ?></td>
											<td><input type="text" class="form-control" name="inp_ket_<?php echo $no;?>"  id="inp_ket_<?php echo $no;?>" value="<?php echo $row_rm_labrad->rm_labrad_hsl_keterangan;?>" /></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
								<div class="form-group mt-5">
									<div class="col-lg-4 col-sm-4 col-xs-12">
										<label>Teknisi:</label>
										<select class="form-control form-control-solid select2" name="inp_teknisi_darah"  id="inp_teknisi_darah">
											<option label="Label"></option>
											<?=$comboTeknisiDarah;?>
										</select>
									</div>
									<div class="col-lg-8 col-sm-8 col-xs-12">
										<label>Kesimpulan:</label>
										<textarea class="form-control form-control-solid" id="inp_ket_cekdar" name="inp_ket_cekdar"  rows="4"><?php echo $ketHasilCekDar;?></textarea>
									</div>
								</div>	
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-12" align="center">
										<input type="hidden" class="form-control" id="count_dt" name="count_dt" value="<?php echo $no ?>">
										<input type="hidden" name="id_labrad_cekdar"  id="id_labrad_cekdar" value="<?php echo $idCekDar;?>"/>
										<button type="submit" class="btn btn-primary font-weight-bolder" name="act_kimia_darah"  id="act_kimia_darah" value="simpan">
										<i class="flaticon-edit-1 icon-sm"></i> Simpan</button>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
					<!-- END PEMERIKSAAN DARAH -->
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end::Entry-->


<script src="assets/js/pages/custom/wizard/wizard-diagnosa-telemedicine-dokter.js?v=7.0.4"></script>
<script src="assets/js/pages/crud/file-upload/image-input.js?v=7.0.4"></script>
<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
<script>
	jQuery(document).ready(function() {
		$('#inp_teknisi_darah').select2({
			placeholder: "Pilih",
			allowClear: true
		});
	});

</script>


