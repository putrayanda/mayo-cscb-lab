<?php
	if(empty($rrNum->nama_pemilik)){
		$pemilik = "Pemilik tidak diketahui";
	}
	else{
		$pemilik = $rrNum->nama_pemilik;
	}
?>
<!DOCTYPE html>
<!-- saved from url=(0105)https://www.tokopedia.com/invoice.pl?id=721052600&pdf=Invoice-1395752-1727084-20210225121734-c3Nzc3Nzc3M5 -->
<html lang="id"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hasil Pemeriksaan Darah <?php echo $row_rm->pas_nama; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="<?php echo base_url();?>">

	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" media="all">
	<!--end::Fonts-->
	<!--begin::Page Vendors Styles(used by this page)-->
	<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<!--end::Page Vendors Styles-->
	<!--begin::Global Theme Styles(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles-->
	<!--begin::Layout Themes(used by all pages)-->
	<link href="assets/css/themes/layout/header/base/light.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="assets/css/themes/layout/header/menu/light.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="assets/css/themes/layout/brand/light.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="assets/css/themes/layout/aside/dark.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<!--end::Layout Themes-->

	<link href="assets/bower_components/bootstrap-table/bootstrap-table.css" rel="stylesheet" type="text/css">
	<link href="assets/plugins/custom/uppy/uppy.bundle.css?v=7.0.4" rel="stylesheet" type="text/css" />
	<link href="assets/css/pages/wizard/wizard-diagnosa-telemedicine-dokter.css?v=7.0.4" rel="stylesheet" type="text/css" />

	<style>
		* {
			box-sizing: border-box;
			-webkit-box-sizing: border-box;
		}

		@media print {
			body {
				padding-top: 0;
			}

			#action-area {
				display: none;
			}
		}

		@media screen and (min-width: 1025px) {
			.btn-download {
				display: none !important;
			}

			.btn-back {
				display: none !important;
			}
		}

		@media screen and (max-width: 1024px) {
			.content-area>div {
				width: auto !important;
			}

			.btn-print {
				display: none !important;
			}
		}

		@media screen and (max-width: 720px) {
			.content-area>div {
				width: auto !important;
			}
		}

		@media screen and (max-width: 420px) {
			.content-area>div {
				width: 790px !important;
			}
		}

		@media screen and (max-width: 430px) {
			.content-area {
				transform: scale(0.59) translate(-35%, -35%)
			}

			.content-area>div {
				width: 720px !important;
			}

			.btn-print {
				display: none !important;
			}
		}

		@media screen and (max-width: 380px) {
			.content-area {
				transform: scale(0.45) translate(-58%, -62%);
			}

			.content-area>div {
				width: 790px !important;
			}

			.btn-print {
				display: none !important;
			}
		}

		@media screen and (max-width: 320px) {
			.content-area>div {
				width: 700px !important;
			}
		}

		.select2-container {
			width: 100% !important;
			padding: 0;
		}
	</style>
	<!--begin::Global Config(global config for global JS scripts)-->
	<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
	<!--end::Global Config-->
	<!--begin::Global Theme Bundle(used by all pages)-->
	<script src="assets/plugins/global/plugins.bundle.js?v=7.0.4"></script>
	<script src="assets/plugins/custom/prismjs/prismjs.bundle.js?v=7.0.4"></script>
	<script src="assets/js/scripts.bundle.js?v=7.0.4"></script>
	<!--end::Global Theme Bundle-->
	<!--begin::Page Vendors(used by this page)-->
	<script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.0.4"></script>
	<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM?v=7.0.4"></script>
	<script src="assets/plugins/custom/gmaps/gmaps.js?v=7.0.4"></script>
	<!--end::Page Vendors-->
	<!--begin::Page Scripts(used by this page)-->
	<script src="assets/js/pages/widgets.js?v=7.0.4"></script>
	<!--end::Page Scripts-->
	<script src="assets/bower_components/bootstrap-table/bootstrap-table.js"></script>
	<script src="assets/bower_components/bootstrap-table/extensions/resizable/bootstrap-table-resizable.js"></script>
	<script src="assets/bower_components/bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>
	<script src="assets/js/bootstrap-editable.js"></script>
	<script src="assets/bower_components/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
	<script src="assets/bower_components/tableExport/tableExport.js"></script>
	<script src="assets/bower_components/tableExport/libs/js-xlsx/xlsx.core.min.js"></script>
	<script src="assets/bower_components/bootstrap-table/extensions/filter/bootstrap-table-filter.js"></script>
	<script src="assets/js/pages/custom/wizard/wizard-diagnosa-telemedicine-dokter.js?v=7.0.4"></script>

	<body style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact; padding-top: 60px;">
		<div id="action-area">
			<div id="navbar-wrapper" style="padding: 12px 16px;font-size: 0;line-height: 1.4; box-shadow: 0 -1px 7px 0 rgba(0, 0, 0, 0.15); position: fixed; top: 0; left: 0; width: 100%; background-color: #FFF; z-index: 100;">
				<div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px;">
					<div class="btn-back" onclick="window.close();">
						<img src="assets/media/misc/back-invoice.png" width="20px" alt="Back" style="display: inline-block; vertical-align: middle;">
						<span style="display: inline-block; vertical-align: middle; margin-left: 16px; font-size: 16px; font-weight: bold; color: rgba(49, 53, 59, 0.96);">Memo Report</span>
					</div>
				</div>
				<div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px; text-align: right;">
					<a class="btn-download" href="javascript:window.print()" style="display: inline-block; vertical-align: middle;">
						<img src="assets/media/misc/download-invoice.png" alt="Download" width="20px" ;="">
					</a>
					<a class="btn-print" href="javascript:window.print()" style="height: 100%; display: inline-block; vertical-align: middle;">
						<button id="print-button" style="border: none; height: 100%; cursor: pointer;padding: 8px 40px;border-color: #03AC0E;border-radius: 8px;background-color: #03AC0E;margin-left: 16px;color: #fff;font-size: 12px;line-height: 1.333;font-weight: 700;">Cetak</button>
					</a>
				</div>
			</div>
		</div>
		<div class="content-area" style="background:#ffffff;">
			<div class="col-12" style="background:#ffffff;">
				<div class="card card-custom gutter-b card-stretch" style="background:#ffffff;">
					<div class="card-body">
						<table width="100%" border="0" style="font-family: Times New Roman">
							<tr>
								<td rowspan="2" width="15%" align="center"><img src="assets/media/logos/csbc500.png" width="200" height="200" border="0" alt=""></td>
								<td width="400" width="80%" align="center"><font size="7"><strong>Leo's Vet Lab by Leo n Vets Clinic</strong></font></td>
								<td width="5%"></td>
							</tr>
							<tr>
								<td align="center"><font size="3" >Jl. Raya Jatimekar No. 16 Jatimekar-Jatiasih, Bekasi 17422</br>081398669638 / 081297300464</font><</td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3" style="border: 2px solid black;width: 6em;"></td>
							</tr>
						</table>
						<table width="100%" border="0" style="font-family: Times New Roman;font-weight: bold;">
							<tr>
								<td colspan="6" align="center" style="padding-top:15px"><font size="5" color=""><strong>HASIL PEMERIKSAAN</strong></font></td>
							</tr>
							<tr>
								<td width="10%">Nama Hewan</td>
								<td width="2%">:</td>
								<td width="48%"><?php echo $row_rm->pas_nama; ?></td>
								<td width="10%">Nama Pemilik</td>
								<td width="2%">:</td>
								<td width="28%"><?php echo $row_rm->pem_nama; ?></td>
							</tr>
							<tr>
								<td width="10%">Jenis</td>
								<td width="2%">:</td>
								<td width="48%"><?php echo $row_rm->ref_spesies_nama; ?></td>
								<td width="10%">Dokter Hewan</td>
								<td width="2%">:</td>
								<td width="28%"><?php echo $row_rm->nama_dokter_rujukan; ?></td>
							</tr>
							<tr>
								<td width="10%">Tgl. Periksa</td>
								<td width="2%">:</td>
								<td width="48%"><?php echo $row_rm->rm_tanggal; ?></td>
								<td width="10%">Klinik</td>
								<td width="2%">:</td>
								<td width="28%"><?php echo $row_rm->nama_klinik_rujukan; ?></td>
							</tr>
							<tr>
								<td colspan="6" align="center" style="padding-top:15px"><font size="5" color=""><strong>HASIL PEMERIKSAAN DARAH</strong></font></td>
							</tr>
						</table>
						<table border="1" width="100%">
							<thead>
								<tr align="center">
									<th width="15%">Nama Pemeriksaan</th>
									<th width="15%">Hasil</th>
									<th width="15%">Nilai Rujukan</th>
									<th width="15%">Satuan</th>
									<th width="35%">Keterangan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no	= 0;
								$query_kimia_darah	= "SELECT * FROM rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (72,88,113) AND rm_labrad_hsl_rm_id=".$rNum." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan";
								$rhKimDar		= $this->db->query($query_kimia_darah);

								foreach($rhKimDar->result() as $rowKimDar){
									if($row_rm->pas_spesies == 1){
										$nilai_rujukan = $rowKimDar->rm_labrad_hsl_nilai_rujukan_anjing;
									}
									elseif($row_rm->pas_spesies == 2){
										$nilai_rujukan = $rowKimDar->rm_labrad_hsl_nilai_rujukan_kucing;
									}
									else{
										$nilai_rujukan = $rowKimDar->rm_labrad_hsl_nilai_rujukan_lain;
									}
								?>
								<tr>
									<td align="center"><?php echo $rowKimDar->rm_labrad_hsl_nama; ?></td>
									<td align="center"><?php echo $rowKimDar->rm_labrad_hsl_nilai_hasil; ?></td>
									<td align="center"><?php echo $nilai_rujukan; ?></td>
									<td align="center"><?php echo $rowKimDar->rm_labrad_hsl_satuan; ?></td>
									<td><?php echo $rowKimDar->rm_labrad_hsl_keterangan; ?></td>
								</tr>
								<?php
								}
								?>
							</tbody>
						</table>
						<table width="100%" border="0" style="font-family: Times New Roman;">
							<tr>
								<td style="font-weight: bold;" colspan="2">Kesimpulan:</td>
							</tr>
							<tr>
								<td colspan="2"><?php echo $ketHasilCekDar;?></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="70%"></td>
								<td width="30%" align="center">Bekasi,&nbsp;&nbsp;<?php echo date('d-m-Y') ?></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td width="70%"></td>
								<td width="30%" align="center">( .................................................. )</td>
							</tr>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>