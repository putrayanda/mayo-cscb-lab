<link href="assets/css/pages/wizard/wizard-labrad.css?v=7.0.4" rel="stylesheet" type="text/css" />
<style>
	.select2-container {
		width: 100% !important;
		padding: 0;
	}
</style>
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<!--begin::Info-->
		<div class="d-flex align-items-center flex-wrap mr-1">
			<!--begin::Page Heading-->
			<div class="d-flex align-items-baseline mr-5">
				<!--begin::Page Title-->
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
				<!--end::Page Title-->
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page Heading-->
		</div>
		<!--end::Info-->
	</div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<div class="card card-custom card-transparent">
			<div class="card-body p-0">
				<!--begin: Wizard-->
				<div class="wizard wizard-4" id="kt_wizard_rm" data-wizard-state="step-first" data-wizard-clickable="true">
					<!--begin: Wizard Nav-->
					<div class="wizard-nav">
						<div class="wizard-steps">
							<!--begin::Wizard Step 1 Nav-->
							<div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
								<div class="wizard-wrapper">
									<div class="wizard-number">1</div>
									<div class="wizard-label">
										<div class="wizard-title">Data Hewan/Pasien</div>
										<div class="wizard-desc">Kelengkapan data hewan/pasien</div>
									</div>
								</div>
							</div>
							<!--end::Wizard Step 1 Nav-->
							<!--begin::Wizard Step 2 Nav-->
							<div class="wizard-step" data-wizard-type="step">
								<div class="wizard-wrapper">
									<div class="wizard-number">2</div>
									<div class="wizard-label">
										<div class="wizard-title">Waktu & Jenis Pemeriksaan</div>
										<div class="wizard-desc">Isi Waktu, Lokasi, Kolega, dll</div>
									</div>
								</div>
							</div>
							<!--end::Wizard Step 2 Nav-->
							<!--begin::Wizard Step 3 Nav-->
							<div class="wizard-step" data-wizard-type="step">
								<div class="wizard-wrapper">
									<div class="wizard-number">3</div>
									<div class="wizard-label">
										<h3 class="wizard-title">Lengkap!</h3>
										<div class="wizard-desc">Tinjau dan Kirim</div>
									</div>
								</div>
							</div>
							<!--end::Wizard Step 3 Nav-->
						</div>
					</div>
					<!--end: Wizard Nav-->
					<!--begin: Wizard Body-->
					<div class="card card-custom card-shadowless rounded-top-0">
						<div class="card-body p-0">
							<div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
								<div class="col-xl-12 col-xxl-10">
									<!--begin: Wizard Form-->
									<form id="kt_form_rm" role="form" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().$class.'/'.$method;?>/crud/?rNum=<?php echo $rNum?>">
										<input type="hidden" id="action_crud" name="action_crud" value="">
										<input type="hidden"  class="form-control form-control-solid" name="inp_pas_id" id="inp_pas_id" dir="ltr" readonly value="<?php echo $pas_id?>"/>
										<!--begin: Wizard Step 1-->
										<div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
											<?php
											if(empty($rNum)){
											?>
											<div class="mb-10 font-weight-bold text-dark">Pilih Pasien/Hewan berdasarkan No. Rekam Medis atau Nama & Perbaharui Kelengkapan Datanya jika diperlukan </div>
											<div class="card bg-success">
												<div class="p-3">
													<div class="row">
														<div class="col-lg-2">
															<label>Status :</label>
															<div class="radio-inline">
																<label class="radio radio-primary">
																<input type="radio" name="inp_status"  id="inp_status_baru" value="1" checked="checked" />Baru
																<span></span></label>
																<label class="radio radio-warning">
																<input type="radio" name="inp_status"  id="inp_status_lama" value="2" />Lama
																<span></span></label>
															</div>
														</div>
														<div class="col-xl-5" id='d_cr_nama'>
															<label>No. Rekam Medis:</label>
															<div class="typeahead">
																<input class="form-control form-control-solid" placeholder="Cari/Pilih Hewanmu Berdasarkan No. Rekmed" name="cr_mrn" id="cr_mrn" dir="ltr" type="text" value="" />
															</div>
														</div>
														<div class="col-xl-5" id='d_cr_mrn'>
															<label>Nama Pasien/Hewan:</label>
															<div class="typeahead" >
																<input class="form-control form-control-solid" placeholder="Cari/Pilih Hewanmu Berdasarkan Nama" name="cr_nama" id="cr_nama" dir="ltr" type="text" value="" />
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="separator separator-dashed my-2"></div>
											<?php
											}
											?>
											<div class="row ">
												<div class="col-lg-4">
													<div class="form-group">
														<label>Nama Pasien/Hewan:</label>
														<input type="text" class="form-control" placeholder="Isi Nama Lengkap" name="inp_nama"  id="inp_nama" value="<?php echo $pas_nama;?>" required/>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="form-group">
														<label>Spesies:</label>
														<select class="form-control form-control-solid select2" name="inp_spesies"  id="inp_spesies" required>
															<option label="Label"></option>
															<?=$comboSpesies;?>
														</select>
													</div>													
												</div>
												<div class="col-lg-4">
													<div class="form-group">
														<label>Ras:</label>
														<select class="form-control select2" name="inp_ras"  id="inp_ras">
															<option label="Label"></option>
															<?=$comboRas;?>
														</select>
													</div>
												</div>
											</div>
											<div class="separator separator-dashed my-2"></div>
											<div class="row">
												<div class="col-xl-4">
													<div class="form-group">
														<label>Warna:</label>
														<select class="form-control form-control-solid select2" name="inp_warna"  id="inp_warna" required>
															<option label="Label"></option>
															<?=$comboWarna;?>
														</select>
													</div>
												</div>
												<div class="col-xl-4">
													<div class="form-group">
														<label>Jenis Kelamin:</label>
														<div class="radio-inline">
															<label class="radio radio-primary">
															<input type="radio" name="inp_jk"  id="inp_jantan" value="J" <?php echo $checkedJantan;?>/>Jantan&nbsp;&nbsp;
															<span></span></label>
															<label class="radio radio-success">
															<input type="radio" name="inp_jk"  id="inp_betina" value="B" <?php echo $checkedBetina;?>/>Betina&nbsp;&nbsp;
															<span></span></label>
														</div>
													</div>
												</div>
												<div class="col-xl-2">
													<div class="form-group">
														<label>Umur Tahun</label>
														<input type="number" class="form-control"  name="inp_umur_tahun"  id="inp_umur_tahun" value="<?php echo $rm_umur_tahun?>" required/>
													</div>
												</div>
												<div class="col-xl-2">
													<div class="form-group">
														<label>Bulan</label>
														<input type="number" class="form-control"  name="inp_umur_bulan"  id="inp_umur_bulan" value="<?php echo $rm_umur_bulan?>" required/>
													</div>
												</div>
											</div>
											<div class="separator separator-dashed my-2"></div>
											<div class="row ">
												<div class="col-lg-8">
													<div class="form-group">
														<label>Nama Pemilik:</label>
														<input type="text" class="form-control" name="inp_pem"  id="inp_pem" value="<?php echo $pem_nama;?>" required/>
													</div>
												</div>
											</div>
										</div>
										<!--end: Wizard Step 1-->
										<!--begin: Wizard Step 2-->
										<div class="pb-5" data-wizard-type="step-content">
											<div class="mb-10 font-weight-bold text-dark">Isi Waktu, Jenis Pemeriksaan dan Kolega beserta dokter rujukan</div>
											<div class="row">
												<div class="col-xl-3">
													<div class="form-group">
														<label>Tanggal:</label>
														<div class="input-group ">
															<div class="input-group-prepend">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
															<input type="text" class="form-control" placeholder="Isi Tanggal" name="inp_tgl"  id="inp_tgl" value="<?php echo $rm_tanggal;?>" required />
														</div>
													</div>
												</div>
												<div class="col-xl-3">
													<div class="form-group">
														<label>Lokasi Vet Lab:</label>
														<select class="form-control form-control-solid select2" name="inp_cabang"  id="inp_cabang" required>
															<option label="Label"></option>
															<?=$comboCabang;?>
														</select>
													</div>
												</div>
												<div class="col-xl-3">&nbsp;</div>
												<div class="col-xl-3">&nbsp;</div>
											</div>
											<div class="form-group">
												<label>Gejala Klinis:</label>
												<textarea class="form-control form-control-solid" id="inp_ket_anamnesis" name="inp_ket_anamnesis"  rows="4" cols="50"><?php echo $rm_anam_ket;?></textarea>
											</div>
											<div class="form-group row">
												<div class="col-lg-6">
													<label>Nama Klinik:</label>
													<div class="input-group ">
														<select class="form-control select2" name="inp_klinik"  id="inp_klinik" required >
															<option label="Label"></option>
															<?=$comboKlinik;?>
														</select>
													</div>
												</div>
												<div class="col-lg-6">
													<label>Dokter:</label>
													<div class="input-group ">
														<select class="form-control select2" name="inp_dokter"  id="inp_dokter" required >
															<option label="Label"></option>
															<?=$comboDokter;?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-6 col-form-label">
													<label>Jenis Pemeriksaan:</label>
													<div class="checkbox-list">
														<label class="checkbox">
															<input type="checkbox" name="cek_rontgen" id="cek_rontgen" value="1" <?=$checkedRontgen;?>/>
															<span></span>
															Rontgen
														</label>
														<label class="checkbox">
															<input type="checkbox" name="cek_usg" id="cek_usg" value="1" <?=$checkedUSG;?>/>
															<span></span>
															USG
														</label>
														<label class="checkbox">
															<input type="checkbox" name="cek_hema" id="cek_hema" value="1" <?=$checkedHema;?>/>
															<span></span>
															Haematology Rutin
														</label>
														<div class="checkbox-inline">
															<label class="checkbox">
																<input type="checkbox" name="cek_kimdar" id="cek_kimdar" value="1" <?=$checkedKimDar;?>/>
																<span></span>
																Kimia Darah 1
															</label>
															&nbsp;&nbsp;
															<label class="checkbox">
																<input type="checkbox" name="cek_kimdar_2" id="cek_kimdar_2" value="1" <?=$checkedKimDar2;?>/>
																<span></span>
																Kimia Darah 2
															</label>
														</div>
														<label class="checkbox">
															<input type="checkbox" name="cek_inhalasi" id="cek_inhalasi" value="1" <?=$checkedInhalasi;?>/>
															<span></span>
															Pemakaian Alat Inhalasi
														</label>

													</div>
												</div>
											</div>
										</div>
										<!--end: Wizard Step 2-->
										<!--begin: Wizard Step 3-->
										<div class="pb-5" data-wizard-type="step-content">
											<!--begin::Section-->
											<h4 class="mb-10 font-weight-bold text-dark">Tinjau Informasi/Rekam Medis Hewan dan Daftarkan!</h4>
											<div class="row">
												<div class="col-xl-6">
													<h6 class="font-weight-bolder mb-3">Info Pasien/Hewan:</h6>
													<div class="text-dark-50 line-height-lg" id="resume_pasien"></div>
												</div>
												<div class="col-xl-6">
													<h6 class="font-weight-bolder mb-3">Info Medis:</h6>
													<div class="text-dark-50 line-height-lg" id="resume_anamnesis"></div>
												</div>
											</div>
										</div>
										<!--end: Wizard Step 3-->
										<!--begin: Wizard Actions-->
										<div class="d-flex justify-content-between border-top mt-5 pt-10">
											<div class="mr-2">
												<button type="button" class="btn btn-light-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-prev">Sebelumnya</button>
											</div>
											<div>
												<button type="submit" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-submit">Daftar</button>
												<button type="button" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Lanjut</button>
											</div>
										</div>
										<!--end: Wizard Actions-->
									</form>
									<!--end: Wizard Form-->
								</div>
							</div>
						</div>
					</div>
					<!--end: Wizard Bpdy-->
				</div>
				<!--end: Wizard-->
			</div>
		</div>
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->


<script src="assets/js/pages/custom/wizard/wizard-labrad.js?v=7.0.4"></script>

<script>
var arrows;
if (KTUtil.isRTL()) {
	arrows = {
		leftArrow: '<i class="la la-angle-right"></i>',
		rightArrow: '<i class="la la-angle-left"></i>'
	}
} else {
	arrows = {
		leftArrow: '<i class="la la-angle-left"></i>',
		rightArrow: '<i class="la la-angle-right"></i>'
	}
}

jQuery(document).ready(function() {
	$('#d_cr_nama').hide();
	$('#d_cr_mrn').hide();

	$("input[name='inp_status']").change(function(){
		if($(this).val() == 2){
			$('#d_cr_nama').show();
			$('#d_cr_mrn').show();
		}
		else{
			$("#inp_pas_id").val(0);
			$('#d_cr_nama').hide();
			$('#d_cr_mrn').hide();
		}
	});

	var substringMatcher = function(strs) {
		return function findMatches(q, cb) {
			var matches, substringRegex;

			// an array that will be populated with substring matches
			matches = [];

			// regex used to determine if a string contains the substring `q`
			substrRegex = new RegExp(q, 'i');

			// iterate through the pool of strings and for any string that
			// contains the substring `q`, add it to the `matches` array
			$.each(strs, function(i, str) {
				if (substrRegex.test(str)) {
					matches.push(str);
				}
			});

			cb(matches);
		};
	};

	var pas_mrn = [<?= $var_pas_mrn; ?>];
	$('#cr_mrn').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'MRN',
		source: substringMatcher(pas_mrn)
	});

	var pas_nama = [<?= $var_pas_nama; ?>];
	$('#cr_nama').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'Nama_Hewan',
		source: substringMatcher(pas_nama)
	});

	jQuery('#cr_mrn').on('typeahead:selected', function (e, value) {
		var form_data = {};
		$.ajax({
			url:"<?php echo base_url().$class.'/'.$method;?>/get_data_mrn/?mrn="+value,
			type: 'POST',
			data: form_data,
			success: function(data){
				if($.parseJSON(data)['data'].pas_foto == null){
					var foto_url = "assets/media/users/cat.png";
				}
				else{
					var foto_url = "https://cscb.leonvets.id/assets/file/foto/"+$.parseJSON(data)['data'].pas_foto;
				}
				if($.parseJSON(data)['data'].pas_tanggal_lahir != ''){
					var today = new Date();
					var d1 = $.parseJSON(data)['data'].pas_tanggal_lahir;
					var d2 = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
					if(d1 != ""){
						var form_data = {};
						$.ajax({
							url:"<?php echo base_url() ?>Ajax/get_data/umur_bulan/?d1="+d1+"&d2="+d2,
							type: 'POST',
							data: form_data,
							success: function(data){
								if($.parseJSON(data)['data'] != null){
									var total_bulan = $.parseJSON(data)['data'].umur;
									var umur_tahun	= Math.floor(total_bulan/12);
									var umur_bulan	= total_bulan%12;
									 $('#inp_umur_tahun').val(umur_tahun);
									 $('#inp_umur_bulan').val(umur_bulan);
								}
							}
						});
					}
				}
				else{
					$("#inp_umur_tahun").val($.parseJSON(data)['data'].pas_umur_tahun);
					$("#inp_umur_bulan").val($.parseJSON(data)['data'].pas_umur_bulan);
				}
				$("#cr_nama").val($.parseJSON(data)['data'].pas_nama);
				$("#inp_pas_id").val($.parseJSON(data)['data'].pas_id);
				$("#inp_nama").val($.parseJSON(data)['data'].pas_nama);
				$("#inp_last_vaksin").val($.parseJSON(data)['data'].pas_last_vaksin);
				$('#inp_spesies').select2('destroy');
				$('#inp_spesies').val($.parseJSON(data)['data'].pas_spesies).select2();
				$("#inp_ras").val($.parseJSON(data)['data'].pas_ras).change();
				$("#inp_warna").val($.parseJSON(data)['data'].pas_warna).change();
				$("#inp_pem").val($.parseJSON(data)['data'].pem_nama);
				if($.parseJSON(data)['data'].pas_gender == 'B'){
					$('#inp_betina').prop('checked','checked');
				}
				else{ 
					$('#inp_jantan').prop('checked','checked');
				}
				$("input[name=inp_jns][value="+$.parseJSON(data)['data'].pas_jenis+"]").attr("checked", "checked");
				$("input[name=inp_jns][value="+$.parseJSON(data)['data'].pas_jenis+"]").attr("checked", "checked");
				$("#url_foto").replaceWith('<div class="image-input-wrapper" id="url_foto" style="background-image: url('+foto_url+')"></div>');
			}
		});
	});

	jQuery('#cr_nama').on('typeahead:selected', function (e, value) {
		var form_data = {};
		$.ajax({
			url:"<?php echo base_url().$class.'/'.$method;?>/get_data_nama/?nama="+value,
			type: 'POST',
			data: form_data,
			success: function(data){
				if($.parseJSON(data)['data'].pas_foto == null){
					var foto_url = "assets/media/users/cat.png";
				}
				else{
					var foto_url = "https://cscb.leonvets.id/assets/file/foto/"+$.parseJSON(data)['data'].pas_foto;
				}
				if($.parseJSON(data)['data'].pas_tanggal_lahir != ''){
					var today = new Date();
					var d1 = $.parseJSON(data)['data'].pas_tanggal_lahir;
					var d2 = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
					if(d1 != ""){
						var form_data = {};
						$.ajax({
							url:"<?php echo base_url() ?>Ajax/get_data/umur_bulan/?d1="+d1+"&d2="+d2,
							type: 'POST',
							data: form_data,
							success: function(data){
								if($.parseJSON(data)['data'] != null){
									var total_bulan = $.parseJSON(data)['data'].umur;
									var umur_tahun	= Math.floor(total_bulan/12);
									var umur_bulan	= total_bulan%12;
									 $('#inp_umur_tahun').val(umur_tahun);
									 $('#inp_umur_bulan').val(umur_bulan);
								}
							}
						});
					}
				}
				else{
					$("#inp_umur_tahun").val($.parseJSON(data)['data'].pas_umur_tahun);
					$("#inp_umur_bulan").val($.parseJSON(data)['data'].pas_umur_bulan);
				}
				$("#cr_mrn").val($.parseJSON(data)['data'].pas_mrn);
				$("#inp_pas_id").val($.parseJSON(data)['data'].pas_id);
				$("#inp_nama").val($.parseJSON(data)['data'].pas_nama);
				$('#inp_spesies').select2('destroy');
				$('#inp_spesies').val($.parseJSON(data)['data'].pas_spesies).select2();
				$("#inp_ras").val($.parseJSON(data)['data'].pas_ras).change();
				$("#inp_warna").val($.parseJSON(data)['data'].pas_warna).change();
				$("#inp_pem").val($.parseJSON(data)['data'].pem_nama);
				if($.parseJSON(data)['data'].pas_gender == 'B'){
					$('#inp_betina').prop('checked','checked');
				}
				else{
					$('#inp_jantan').prop('checked','checked');
				}
				$("input[name=inp_jns][value="+$.parseJSON(data)['data'].pas_jenis+"]").attr("checked", "checked");
				$("input[name=inp_jns][value="+$.parseJSON(data)['data'].pas_jenis+"]").attr("checked", "checked");
				$("#url_foto").replaceWith('<div class="image-input-wrapper" id="url_foto" style="background-image: url('+foto_url+')"></div>');
			}
		});
	});
		
	$('#inp_tgl').datepicker({
		rtl: KTUtil.isRTL(),
		orientation: "bottom left",
		todayHighlight: true,
		templates: arrows,
		format: 'dd/mm/yyyy',
	});

	$('#inp_cabang').select2({
		placeholder: "Pilih Cabang",
		allowClear: true
	});

	$('#inp_spesies').select2({
		placeholder: "Pilih Spesies",
		allowClear: true
	});

	$('#inp_spesies').on('change', function(){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_ras",
				type: "POST",
				data: {'sel_id' : sel_id},
				dataType: 'json',
				success: function(data){
					$('#inp_ras').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});

	$('#inp_ras').select2({
		placeholder: "Pilih Ras",
		allowClear: true
	});

	$('#inp_warna').select2({
		placeholder: "Pilih Warna",
		allowClear: true
	});

	$('#inp_klinik').select2({
		placeholder: "Pilih Klinik",
		allowClear: true
	});

	$('#inp_dokter').select2({
		placeholder: "Pilih Dokter",
		allowClear: true
	});

	$('#inp_klinik').on('change', function(){
		var sel_id = $(this).val();

		if(sel_id > 0) {
			$.ajax({
				url : "<?php echo base_url() ?>Ajax/create_list/get_dokter_klinik",
				type: "POST",
				data: {'sel_id' : sel_id},
				dataType: 'json',
				success: function(data){
					$('#inp_dokter').html(data);
				},
				error: function(){
					//('Empty Data...!!');
				}
			});
		}
	});
});

</script>

