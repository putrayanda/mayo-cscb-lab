<link href="assets/bower_components/fancybox/fancybox.css" rel="stylesheet" type="text/css">

<style>
	a[data-fancybox] img {
		cursor: zoom-in;
	}

	.fancybox__container {
		--fancybox-bg: rgba(17, 6, 25, 0.85);
	}

	.fancybox__container .fancybox__content {
		padding: 1rem;
		border-radius: 6px;
		color: #374151;
		background: #fff;
		box-shadow: 0 8px 23px rgb(0 0 0 / 50%);
	}

	.fancybox__content > .carousel__button.is-close {
		top: 0;
		right: -38px;
	}

	.fancybox__caption {
		margin-top: 0.75rem;
		padding-top: 0.75rem;
		padding-bottom: 0.25rem;
		width: 100%;
		border-top: 1px solid #ccc;
		font-size: 1rem;
		line-height: 1.5rem;

		/* Prevent opacity change when dragging up/down */
		--fancybox-opacity: 1;
	}

	table {
		table-layout: fixed;
	}

	.select2-container {
	width: 100% !important;
	padding: 0;
	}

	.isDisabled {
	  color: currentColor;
	  cursor: not-allowed;
	  opacity: 0.5;
	  text-decoration: none;
	}

</style>

<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-2 col-xl-2" >
			<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title?></h5>
			<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
				<li class="breadcrumb-item">
					<a href="<?php echo base_url().$class.'/'.$method ?>" class="text-muted"><?= $title ?></a>
				</li>
			</ul>
		</div>
		<div class="mr-8 mt-8 col-xl-10">
			<form role="form" method="post" accept-charset="utf-8" action="<?php echo base_url().$class;?>/rekammedis_search/">
				<div class="card-body">
					<div class="form-group row">
						<div class="col-xl-3">
							<input type="text" class="form-control" placeholder="Nama Pasien/Hewan" name="cr_nama"  id="cr_nama" />
						</div>
						<div class="col-xl-3">
							<input type="text" class="form-control" placeholder="No. RM"  name="cr_rm"  id="cr_rm"/>
						</div>
						<div class="col-xl-2">
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
								<input type="text" class="form-control" placeholder="Tanggal RM" name="cr_tgl_rm"  id="cr_tgl_rm"/>
							</div>
						</div>
						<div class="col-xl-2">
							<button class="btn btn-success mr-2 col-xl-12" type="submit" > Search
								<i class="fa fa-search"></i>
							</button>
						</div>
						<div class="col-xl-2">
							<a href="<?php echo base_url().$class.'/'.$method;?>/form" class="btn btn-light-primary font-weight-bold ml-2 col-xl-12" >
								<i class="fas fa-book-medical icon-md"></i>
								Tambah Daftar
							</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container">
		<!--begin::Row-->
		<div class="row">
			<?php
			$no	= 0;
			foreach($query_hd->result() as $row_rm){
				$no++;
				if(empty($row_rm->pas_foto)){
					$filename	= "assets/file/foto/unknown.jpg";
				}
				else{
					$filename	= "assets/file/foto/".$row_rm->pas_foto;
					if(!file_exists($filename)){
						$filename	= "assets/file/foto/unknown.jpg";
					}
				}

				if(empty($row_rm->pem_nama)){
					$pemilik = "Pemilik tidak diketahui";
				}
				else{
					$pemilik = $row_rm->pem_nama;
				}

				$query_pemeriksaan	= $this->db->query("select ref_prod_nama, ref_prod_nama_invoice from rekam_medis_labrad JOIN ref_produk ON ref_produk.ref_prod_id = rm_labrad_produk WHERE rm_labrad_rm_id=".$row_rm->rm_id);
				$pmrksaan	= '';
				$i			= 0;
				foreach($query_pemeriksaan->result() as $row_prks){
					if($i == 0){
						$koma = "";
					}
					else{
						$koma = ", ";
					}
					$pmrksaan =  $pmrksaan.$koma.$row_prks->ref_prod_nama_invoice;
					$i++;
				}
			?>
			<div class="col-xl-12">
				<div class="card card-custom gutter-b card-stretch">
					<div class="card-body">
						<div class="card card-custom">
							<div class="card-header ribbon ribbon-top ribbon-ver">
								<div class="ribbon-target bg-danger" style="top: -2px; right: 20px;"><?php echo $row_rm->rm_no_urut ?></div>
								<div class="ribbon-target bg-primary" style="top: -2px; right: 75px;"><?php echo $row_rm->rm_status_ket ?></div>
								<div class="flex-shrink-0 mr-4 symbol symbol-65 symbol-circle">
									<img src="<?php echo $filename;?>" alt="image" />
								</div>
								<div class="d-flex flex-column mr-auto">
									<a href="" class="card-title text-hover-primary font-weight-bolder font-size-h5 text-dark mb-1"><?php echo strtoupper($row_rm->pas_nama)?></a>
									<span class="text-muted font-weight-bold"><?php echo $row_rm->ref_spesies_nama?> - <?php echo $row_rm->ref_ras_nama?> - <?php echo $row_rm->ref_gender_nama?> - <?php echo $row_rm->ref_warna_ket?> - <?php echo $row_rm->rm_umur_tahun?> Thn, <?php echo $row_rm->rm_umur_bulan?> Bln</span>
									<span class="text-muted font-weight-bold"><?php echo $pemilik?></span>
								</div>
							</div>
						</div>
						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-2">
									<span class="ribbon-inner bg-warning"></span>Rekam Medis
								</div>
							</div>
							<div class="card-body">
								<div class="d-flex flex-wrap">
									<div class="d-flex flex-column mb-2 col-xl-2">
										<span class="d-block font-weight-bolder mb-2">Waktu Pemeriksaan</span>
										<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text"><?php echo $row_rm->rm_tanggal?></span>
									</div>
									<div class="d-flex flex-column mb-2 col-xl-3">
										<span class="d-block font-weight-bolder mb-2">Lokasi Vet Lab</span>
										<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text"><?php echo $row_rm->ref_cab_nama?></span>
									</div>
									<div class="d-flex flex-column mb-2 col-xl-3">
										<span class="d-block font-weight-bolder mb-2">Nama Klinik</span>
										<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text"><?php echo $row_rm->nama_klinik_rujukan?></span>
									</div>
									<div class="d-flex flex-column mb-2 col-xl-4">
										<span class="d-block font-weight-bolder mb-2">Dokter</span>
										<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text"><?php echo $row_rm->nama_dokter_rujukan?> (<?php echo $row_rm->no_hp_dokter_rujukan?>)</span>
									</div>
									<div class="d-flex flex-column mb-2 col-xl-6">
										<span class="d-block font-weight-bolder mb-2">Jenis Pemeriksaan</span>
										<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text"><?php echo $pmrksaan ?></span>
									</div>
									<div class="d-flex flex-column mb-2 col-xl-6">
										<span class="d-block font-weight-bolder mb-2">Gejala Klinis</span>
										<p class="mb-2 mt-3"><?php echo nl2br($row_rm->rm_anamnesis_ket) ?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="card card-custom">
							<div class="card-header card-header-right ribbon ribbon-clip ribbon-left">
								<div class="ribbon-target mt-2">
									<span class="ribbon-inner bg-success"></span>Pemeriksaan
								</div>
							</div>
							<div class="card-body">
								<div class="accordion accordion-solid accordion-toggle-plus" id="accordionHasilLabRad">
									<?php
									$query_inhalasi	= "	SELECT * FROM v_rekam_medis_labrad WHERE rm_labrad_produk=1338 AND rm_labrad_rm_id=".$row_rm->rm_id;
									$rhInhalasi		= $this->db->query($query_inhalasi);
									$countInhalasi	= $rhInhalasi->num_rows();
									if($countInhalasi > 0){
									?>
									<!-- INHALASI -->
									<div class="card">
										<div class="card-header" id="headingInhalasi">
											<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseInhalasi">
											<i class="fas fa-flask"></i>PEMAKAIAN ALAT INHALASI</div>
										</div>
									</div>
									<?php
									}
									?>
									<!-- END INHALASI -->
									<?php
									$query_rontgen	= "	SELECT * FROM v_rekam_medis_labrad WHERE rm_labrad_produk=70 AND rm_labrad_rm_id=".$row_rm->rm_id;
									$rhRontgen		= $this->db->query($query_rontgen);
									$countRontgen	= $rhRontgen->num_rows();
									if($countRontgen > 0){
										$rrRontgen	= $rhRontgen->row();
										$dokRontgen	= $rrRontgen->dok_labrad;
										$ketRontgen	= $rrRontgen->rm_labrad_hasil_ket;
										$tekRontgen	= $rrRontgen->peg_teknisi;
									?>
									<!-- RONTGEN -->
									<div class="card">
										<div class="card-header" id="headingRontgen">
											<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseRontgen">
											<i class="fas fa-flask"></i>Rontgen</div>
										</div>
										<div id="collapseRontgen" class="collapse" data-parent="#accordionHasilLabRad">
											<div class="card-body">
												<div class="d-flex flex-wrap">
													<div class="d-flex flex-column mb-2 col-xl-3">
														<span class="d-block font-weight-bolder">Dokter</span>
														<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $dokRontgen ?></span>
													</div>
													<div class="d-flex flex-column mb-2 col-xl-3">
														<span class="d-block font-weight-bolder">Teknisi</span>
														<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $tekRontgen ?></span>
													</div>
													<div class="d-flex flex-column mb-2 col-xl-6">
														<span class="d-block font-weight-bolder">Kesimpulan</span>
														<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $ketRontgen?></span>
													</div>
												</div>
												<?php
													$no	= 0;
													$query_hsl_rontgen	= "SELECT * FROM v_rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (70) AND rm_labrad_hsl_rm_id=".$row_rm->rm_id." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan";
													$rhHslRontgen		= $this->db->query($query_hsl_rontgen);
													foreach($rhHslRontgen->result() as $rowHslRontgen){
												?>
													<div class="d-flex flex-wrap">
														<div class="d-flex flex-column mb-2 col-xl-4">
															<span class="d-block font-weight-bolder">Posisi</span>
															<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $rowHslRontgen->posisi_rontgen ?></span>
														</div>
														<div class="d-flex flex-column mb-2 col-xl-4">
															<span class="d-block font-weight-bolder">Regio</span>
															<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $rowHslRontgen->regio_rontgen ?></span>
														</div>
														<div class="d-flex flex-column mb-2 col-xl-6">
															<span class="d-block font-weight-bolder">Temuan</span>
															<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $rowHslRontgen->rm_labrad_hsl_temuan?></span>
														</div>
													</div>
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<!-- END RONTGEN -->
									<?php
									}
									?>
									<?php
									$query_usg	= "	SELECT * FROM v_rekam_medis_labrad WHERE rm_labrad_produk=78 AND rm_labrad_rm_id=".$row_rm->rm_id;
									$rhUSG		= $this->db->query($query_usg);
									$countUSG	= $rhUSG->num_rows();
									if($countUSG > 0){
										$rrUSG	= $rhUSG->row();
										$dokUSG	= $rrUSG->dok_labrad;
										$ketUSG	= $rrUSG->rm_labrad_hasil_ket;
									?>
									<!-- USG -->
									<div class="card">
										<div class="card-header" id="headingUSG">
											<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseUSG">
											<i class="fas fa-flask"></i>USG</div>
										</div>
										<div id="collapseUSG" class="collapse" data-parent="#accordionHasilLabRad">
											<div class="card-body">
												<div class="d-flex flex-wrap">
													<div class="d-flex flex-column mb-2 col-xl-6">
														<span class="d-block font-weight-bolder">Dokter</span>
														<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $dokUSG ?></span>
													</div>
													<div class="d-flex flex-column mb-2 col-xl-6">
														<span class="d-block font-weight-bolder">Kesimpulan</span>
														<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $ketUSG?></span>
													</div>
												</div>
												<?php
													$no	= 0;
													$query_hsl_usg	= "SELECT * FROM v_rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (70) AND rm_labrad_hsl_rm_id=".$row_rm->rm_id." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan";
													$rhHslUsg		= $this->db->query($query_hsl_usg);
													foreach($rhHslUsg->result() as $rowHslUsg){
												?>
													<div class="d-flex flex-wrap">
														<div class="d-flex flex-column mb-2 col-xl-4">
															<span class="d-block font-weight-bolder">Lokasi</span>
															<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $rowHslUsg->lokasi_usg ?></span>
														</div>
														<div class="d-flex flex-column mb-2 col-xl-6">
															<span class="d-block font-weight-bolder">Temuan</span>
															<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $rowHslUsg->rm_labrad_hsl_temuan?></span>
														</div>
													</div>
												<?php
													}
												?>
											</div>
										</div>
									</div>
									<?php
									}
									?>
									<!-- END USG -->
									<?php
									$query_cek_darah	= "	SELECT * FROM v_rekam_medis_labrad WHERE rm_labrad_produk IN (72,88,113) AND rm_labrad_rm_id=".$row_rm->rm_id;
									$rhCekDarah		= $this->db->query($query_cek_darah);
									$countCekDarah	= $rhCekDarah->num_rows();
									if($countCekDarah > 0){
										$rrCekDarah	= $rhCekDarah->row();
										$ketCekDar	= $rrCekDarah->rm_labrad_hasil_ket;
										$tekCekDar	= $rrCekDarah->peg_teknisi;
									?>

									<!-- PEMERIKSAAN DARAH -->
									<div class="card">
										<div class="card-header" id="headingKimiaDarah">
											<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseKimiaDarah">
											<i class="fas fa-flask"></i>Pemeriksaan Darah</div>
										</div>
										<div id="collapseKimiaDarah" class="collapse show" data-parent="#accordionHasilLabRad">
											<div class="card-body">
												<table id="table_hasil_<?php echo $row_rm->rm_id?>" class="table_report" data-toggle="table" data-height="300" data-show-columns="false" data-search="false" data-show-toggle="false" data-pagination="false" data-show-export="false" data-reorderable-columns="false">
													<thead>
														<tr>
															<th data-width="200">Nama Pemeriksaan</th>
															<th data-width="150">Hasil</th>
															<th data-width="150">Nilai Rujukan</th>
															<th data-width="100">Satuan</th>
															<th>Keterangan</th>
														</tr>
													</thead>
													<tbody>
													<?php
														$no	= 0;
														$query_prks_darah	= "SELECT * FROM v_rekam_medis_labrad_hasil WHERE rm_labrad_hsl_prod_id IN (72,88,113) AND rm_labrad_hsl_rm_id=".$row_rm->rm_id." ORDER BY rm_labrad_hsl_prod_id,rm_labrad_hsl_urutan";
														$rhPrksDar		= $this->db->query($query_prks_darah);
															foreach($rhPrksDar->result() as $rowPrksDar){
															if($row_rm->pas_spesies == 1){
																$nilai_rujukan = $rowPrksDar->rm_labrad_hsl_nilai_rujukan_anjing;
															}
															elseif($row_rm->pas_spesies == 2){
																$nilai_rujukan = $rowPrksDar->rm_labrad_hsl_nilai_rujukan_kucing;
															}
															else{
																$nilai_rujukan = $rowPrksDar->rm_labrad_hsl_nilai_rujukan_lain;
															}
														?>
														<tr class="tr-class-<?php echo $no?> ">
															<td><?php echo $rowPrksDar->rm_labrad_hsl_nama; ?></td>
															<td><?php echo $rowPrksDar->rm_labrad_hsl_nilai_hasil; ?></td>
															<td><?php echo $nilai_rujukan; ?></td>
															<td><?php echo $rowPrksDar->rm_labrad_hsl_satuan; ?></td>
															<td><?php echo $rowPrksDar->rm_labrad_hsl_keterangan; ?></td>
														</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
											<div class="d-flex flex-wrap">
												<div class="d-flex flex-column mt-5 col-xl-4">
													<span class="d-block font-weight-bolder">Teknisi</span>
													<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $tekCekDar?></span>
												</div>
												<div class="d-flex flex-column mt-5 col-xl-8">
													<span class="d-block font-weight-bolder">Kesimpulan</span>
													<span class="btn btn-light-success btn-sm font-weight-bold btn-upper btn-text"><?php echo $ketCekDar?></span>
												</div>
											</div>

										</div>
									</div>
									<!-- END PEMERIKSAAN DARAH -->
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
					<?php
						$urlProses			= 'href="'.base_url().$class.'/'.$method.'/proses/?rNum='.$row_rm->rm_id.'"';
						$isDisabledProses	= '';

						$urlUbahData		= 'href="'.base_url().$class.'/'.$method.'/form/?rNum='.$row_rm->rm_id.'"';
						$isDisabledUbahData	= '';

						$urlHapusData		= 'href="'.base_url().$class.'/'.$method.'/delete/?rNum='.$row_rm->rm_id.'"';
						$isDisabledHapusData= '';
						
						if($row_rm->rm_status > 1){
							$urlUbahData		= '';
							$isDisabledUbahData	= 'isDisabled';

							$urlHapusData		= '';
							$isDisabledHapusData= 'isDisabled';

							$urlProses			= '';
							$isDisabledProses	= 'isDisabled';
						}

					?>
					<div class="card-footer" style="text-align:center;">
						<a <?php echo $urlUbahData ?> class="btn btn-light-primary btn-sm font-weight-bold mt-5 mt-sm-0 mr-auto mr-sm-0 ml-sm-auto fix150 <?php echo $isDisabledUbahData ?>" >
							<i class="fas fa-edit icon-md"></i>
							Ubah Data
						</a>&nbsp;&nbsp;
						<?php
							if($row_rm->rm_status == 1){
						?>
							<a <?php echo $urlHapusData ?> class="btn btn-light-danger btn-sm font-weight-bold mt-5 mt-sm-0 mr-auto mr-sm-0 ml-sm-auto fix150 <?php echo $isDisabledHapusData ?>">
								<i class="fas fa-save icon-md"></i>
								Hapus Data
							</a>&nbsp;&nbsp;
							<a <?php echo $urlProses ?> class="btn btn-light-warning btn-sm font-weight-bold mt-5 mt-sm-0 mr-auto mr-sm-0 ml-sm-auto fix150  <?php echo $isDisabledProses ?>" >
								<i class="fas fa-save icon-md"></i>
								Verifikasi
							</a>&nbsp;&nbsp;
						<?php
							}
						?>
					</div>
					<div class="card-footer"  style="text-align:center;">
						<?php
							if($row_rm->rm_status == 2 && $peg_id == 1){
						?>
							<a href="<?php echo base_url().$class.'/'.$method;?>/batal_proses/?rNum=<?php echo $row_rm->rm_id?>" class="btn btn-light-danger btn-sm font-weight-bold mt-5 mt-sm-0 mr-auto mr-sm-0 ml-sm-auto" >
								<i class="fas fa-edit icon-md"></i>
								Batal Verifikasi
							</a>&nbsp;&nbsp;
						<?php
							}
						?>
					</div>
				</div>
			</div>
			<?php
			}
			?>

		</div>
		<!--end::Row-->
		<!--begin::Pagination-->
		<?php
			if (isset($pagination)){
		?>
			<div class="card card-custom">
				<div class="container">
		<?php
				echo $pagination ;
		?>
				</div>
			</div>
		<?php
			}
		?>
		<!--end::Pagination-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
<script src="assets/bower_components/fancybox/fancybox.umd.js"></script>
<script>
	var arrows;
	if (KTUtil.isRTL()) {
		arrows = {
			leftArrow: '<i class="la la-angle-right"></i>',
			rightArrow: '<i class="la la-angle-left"></i>'
		}
	} else {
		arrows = {
			leftArrow: '<i class="la la-angle-left"></i>',
			rightArrow: '<i class="la la-angle-right"></i>'
		}
	}
	jQuery(document).ready(function() {

		$('#cr_tgl_rm').datepicker({
			rtl: KTUtil.isRTL(),
			orientation: "bottom left",
			todayHighlight: true,
			templates: arrows,
			format: 'dd/mm/yyyy',
		});
	});

	function htmlDecode(input) {
	  var doc = new DOMParser().parseFromString(input, "text/html");
	  return doc.documentElement.textContent;
	}

	Fancybox.bind('[data-fancybox="gallery"]', {
	  //dragToClose: false,
	  Thumbs: false,

	  Image: {
		zoom: false,
		click: false,
		wheel: "slide",
	  },

	  on: {
		// Move caption inside the slide
		reveal: (f, slide) => {
		  slide.$caption && slide.$content.appendChild(slide.$caption);
		},
	  },
	});
</script>

