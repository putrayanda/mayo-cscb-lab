<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title ?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Telemedicine Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowTelemedicineStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowTelemedicineStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowTelemedicineStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_telemed_stat">
						<img id="loading-image" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-danger py-5">
						<h3 class="card-title font-weight-bolder text-white">Kunjungan Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowKunjunganStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowKunjunganStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowKunjunganStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_kunjungan_stat">
						<img id="loading-image-kunjungan" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-xxl-12">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 py-5">
							<span class="card-label font-weight-bolder text-dark">New Pets</span>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowNewPets('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowNewPets('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_new_pets">
						<img id="loading-image-newpets" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$.ajax({
			url: '<?php echo base_url().$class;?>/telemedicine_stat/day', 
			beforeSend: function() {
              $("#loading-image").show();
           },
			success: function(result){
				$("#div_telemed_stat").html(result);
				$("#loading-image").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/kunjungan_stat/day', 
			beforeSend: function() {
              $("#loading-image-kunjungan").show();
           },
			success: function(result){
				$("#div_kunjungan_stat").html(result);
				$("#loading-image-kunjungan").hide();
			}
		});


		$.ajax({
			url: '<?php echo base_url().$class;?>/new_pets/day', 
			beforeSend: function() {
              $("#loading-image-newpets").show();
           },
			success: function(result){
				$("#div_new_pets").html(result);
				$("#loading-image-newpets").hide();
			}
		});

	});

	function ShowTelemedicineStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/telemedicine_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image").show();
           },
			success: function(result){
				$("#div_telemed_stat").html(result);
				$("#loading-image").hide();
			}
		});
	}

	function ShowKunjunganStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/kunjungan_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-kunjungan").show();
           },
			success: function(result){
				$("#div_kunjungan_stat").html(result);
				$("#loading-image-kunjungan").hide();
			}
		});
	}

	function ShowNewPets(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/new_pets/'+periode, 
			beforeSend: function() {
              $("#loading-image-newpets").show();
           },
			success: function(result){
				$("#div_new_pets").html(result);
				$("#loading-image-newpets").hide();
			}
		});
	}
</script>