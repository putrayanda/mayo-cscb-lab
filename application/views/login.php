<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<?php echo base_url();?>">
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<title>CSCB Leo n Vets | Login Page </title>
		<meta name="description" content="Selamat Datang di CSCB Leo n Vets, Web Application for Leo Group Staff" />
		<meta name="author" content="putrayanda"> 

		<meta property="og:site_name" content="CSCB Leo n Vets">
		<meta property="og:title" content="CSCB Leo n Vets" />
		<meta property="og:description" content="Selamat Datang di CSCB Leo n Vets, Web Application for Leo Group Staff"/>  
		<meta property="og:image" content="https://leonvets.id/assets/images/logo_275.png">
		<meta property="og:type" content="website" />
		<meta property="og:updated_time" content="1440432930" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />

		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<script src="assets/js/bootstrap.bundle.min.js"></script>
		<script src="assets/jquery.min.js"></script>

		<style>
		:root {
			--red: hsl(0, 78%, 62%);
			--cyan: hsl(180, 62%, 55%);
			--orange: hsl(34, 97%, 64%);
			--blue: hsl(212, 86%, 64%);
			--green: hsl(120, 86%, 54%);
			--varyDarkBlue: hsl(234, 12%, 34%);
			--grayishBlue: hsl(229, 6%, 66%);
			--veryLightGray: hsl(0, 0%, 98%);
			--weight1: 200;
			--weight2: 400;
			--weight3: 600;
		}
		body {
			color: #000;
			overflow-x: hidden;
			height: 100%;
			background-color: #B0BEC5;
			background-repeat: no-repeat
		}

		.card0 {
			box-shadow: 0px 4px 8px 0px #757575;
			border-radius: 0px
		}

		.card2 {
			margin: 0px 40px
		}

		.logo {
			width: 100px;
			height: 100px;
			margin-top: 20px;
			margin-left: 35px
		}

		.image {
			width: 360px;
			height: 327px
		}

		.border-line {
			border-right: 1px solid #EEEEEE
		}

		.facebook {
			background-color: #3b5998;
			color: #fff;
			font-size: 18px;
			padding-top: 5px;
			border-radius: 50%;
			width: 35px;
			height: 35px;
			cursor: pointer
		}

		.twitter {
			background-color: #1DA1F2;
			color: #fff;
			font-size: 18px;
			padding-top: 5px;
			border-radius: 50%;
			width: 35px;
			height: 35px;
			cursor: pointer
		}

		.linkedin {
			background-color: #2867B2;
			color: #fff;
			font-size: 18px;
			padding-top: 5px;
			border-radius: 50%;
			width: 35px;
			height: 35px;
			cursor: pointer
		}

		.line {
			height: 1px;
			width: 100%;
			background-color: #E0E0E0;
			margin-top: 10px
		}

		.or {
			width: 10%;
			font-weight: bold
		}

		.text-sm {
			font-size: 14px !important
		}

		::placeholder {
			color: #BDBDBD;
			opacity: 1;
			font-weight: 300
		}

		:-ms-input-placeholder {
			color: #BDBDBD;
			font-weight: 300
		}

		::-ms-input-placeholder {
			color: #BDBDBD;
			font-weight: 300
		}

		input,
		textarea {
			padding: 10px 12px 10px 12px;
			border: 1px solid lightgrey;
			border-radius: 2px;
			margin-bottom: 5px;
			margin-top: 2px;
			width: 100%;
			box-sizing: border-box;
			color: #2C3E50;
			font-size: 14px;
			letter-spacing: 1px
		}

		input:focus,
		textarea:focus {
			-moz-box-shadow: none !important;
			-webkit-box-shadow: none !important;
			box-shadow: none !important;
			border: 1px solid #304FFE;
			outline-width: 0
		}

		button:focus {
			-moz-box-shadow: none !important;
			-webkit-box-shadow: none !important;
			box-shadow: none !important;
			outline-width: 0
		}

		a {
			color: inherit;
			cursor: pointer
		}

		.btn-green {
			width: 150px;
			position: relative;
			display: inline-block;
			padding: 8px 20px;
			background: var(--green);
			border-radius: 5px;
			text-decoration: none;
			color: white;
			margin-top: 20px;
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
			transition: 0.5s;
		}

		.btn-green:hover {
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
			background: #fff;
			color: #000;
			cursor: pointer
		}

		.bg-green {
			color: #fff;
			background-color: var(--green)
		}

		.btn-red {
			width: 150px;
			position: relative;
			display: inline-block;
			padding: 8px 20px;
			background: var(--red);
			border-radius: 5px;
			text-decoration: none;
			color: white;
			margin-top: 20px;
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
			transition: 0.5s;
		}

		.btn-red:hover {
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
			background: #fff;
			color: #000;
			cursor: pointer
		}

		.bg-red {
			color: #fff;
			background-color: var(--red)
		}

		.btn-blue {
			width: 150px;
			position: relative;
			display: inline-block;
			padding: 8px 20px;
			background: var(--blue);
			border-radius: 5px;
			text-decoration: none;
			color: white;
			margin-top: 20px;
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
			transition: 0.5s;
		}

		.btn-blue:hover {
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
			background: #fff;
			color: #000;
			cursor: pointer
		}

		.bg-blue {
			color: #fff;
			background-color: var(--blue)
		}

		.btn-cyan {
			width: 150px;
			position: relative;
			display: inline-block;
			padding: 8px 20px;
			background: var(--cyan);
			border-radius: 5px;
			text-decoration: none;
			color: white;
			margin-top: 20px;
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
			transition: 0.5s;
		}

		.btn-cyan:hover {
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
			background: #fff;
			color: #000;
			cursor: pointer
		}

		.bg-cyan {
			color: #fff;
			background-color: var(--cyan)
		}

		.btn-orange {
			width: 150px;
			position: relative;
			display: inline-block;
			padding: 8px 20px;
			background: var(--orange);
			border-radius: 5px;
			text-decoration: none;
			color: white;
			margin-top: 20px;
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
			transition: 0.5s;
		}

		.btn-cyan:hover {
			box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
			background: #fff;
			color: #000;
			cursor: pointer
		}

		.bg-orange {
			color: #fff;
			background-color: var(--orange)
		}

		@media screen and (max-width: 991px) {
			.logo {
				margin-left: 0px
			}

			.image {
				width: 300px;
				height: 273px
			}

			.border-line {
				border-right: none
			}

			.card2 {
				border-top: 1px solid #EEEEEE !important;
				margin: 0px 15px
			}
		}
		</style>
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body>
		<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
			<div class="card card0 border-0">
				<div class="row d-flex">
					<div class="col-lg-6">
						<div class="card1 pb-5">
							<div class="row"> <img src="assets/media/logos/logo.png" class="logo"> </div>
							<div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="assets/media/vet_clinic_login.jpg" class="image"> </div>
						</div>
					</div>
					<div class="col-lg-6">
						<?php echo $msg?>
						<form class="kt-form" action="<?php echo base_url()?>loginauth/login/submit" method="post">
						<div class="card2 card border-0 px-4 py-5">
							<div class="row mb-4 px-3">
								<h6 class="mb-0 mr-4 mt-2">Sign in CSCB Leo n Vets</h6>
							</div>
							<div class="row px-3 mb-4">
								<div class="line"></div>
							</div>
							<div class="row px-3"> <label class="mb-1">
								<h6 class="mb-0 text-sm">Username</h6>
								</label> <input class="mb-4" type="text" placeholder="Username" name="username" id="username" > 
							</div>
							<div class="row px-3"> <label class="mb-1">
								<h6 class="mb-0 text-sm">Password</h6>
								</label> <input type="password" placeholder="Password" name="userpass" id="userpass"> 
							</div>
							<div class="row px-3 mb-4">
								<div class="custom-control custom-checkbox custom-control-inline"> 
									<input id="remember" type="checkbox" name="remember" class="custom-control-input"> 
									<label for="remember" class="custom-control-label text-sm">Remember me</label> 
								</div> 
							</div>
							<div class="row mb-3 px-3">
								<input type="hidden" name="app_id" id="app_id" value="1"> 
								<button type="submit" class="btn btn-orange text-center">Login</button> &nbsp;&nbsp;
								<a href="<?php echo base_url();?>/loginauth" class="btn btn-green text-center">Back</a> 
							</div>
						</div>
						</form>
					</div>
				</div>
				<div class="bg-orange py-4">
					<div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2022. All rights reserved.</small>
						<div class="social-contact ml-4 ml-sm-auto"> <span class="fa fa-facebook mr-4 text-sm"></span> <span class="fa fa-google-plus mr-4 text-sm"></span> <span class="fa fa-linkedin mr-4 text-sm"></span> <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span> </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>