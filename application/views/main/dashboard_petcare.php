<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title ?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Grooming Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowGroomingStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowGroomingStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowGroomingStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_grooming_stat">
						<img id="loading-image-grooming" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Cat Hotel Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowCatHotelStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowCatHotelStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowCatHotelStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_cathotel_stat">
						<img id="loading-image-cathotel" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-danger py-5">
						<h3 class="card-title font-weight-bolder text-white">Homeservice Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowHomeServiceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowHomeServiceStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowHomeServiceStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_homeservice_stat">
						<img id="loading-image-homeservice" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$.ajax({
			url: '<?php echo base_url().$class;?>/grooming_stat/day', 
			beforeSend: function() {
              $("#loading-image-grooming").show();
			},
			success: function(result){
				$("#div_grooming_stat").html(result);
				$("#loading-image-grooming").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/cathotel_stat/day', 
			beforeSend: function() {
              $("#loading-image-cathotel").show();
			},
			success: function(result){
				$("#div_cathotel_stat").html(result);
				$("#loading-image-cathotel").hide();
			}
		});


		$.ajax({
			url: '<?php echo base_url().$class;?>/homeservice_stat/day', 
			beforeSend: function() {
              $("#loading-image-homeservice").show();
			},
			success: function(result){
				$("#div_homeservice_stat").html(result);
				$("#loading-image-homeservice").hide();
			}
		});
		
	});

	function ShowGroomingStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/grooming_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-grooming").show();
			},
			success: function(result){
				$("#div_grooming_stat").html(result);
				$("#loading-image-grooming").hide();
			}
		});
	}

	function ShowCatHotelStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/cathotel_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-cathotel").show();
			},
			success: function(result){
				$("#div_cathotel_stat").html(result);
				$("#loading-image-cathotel").hide();
			}
		});
	}

	function ShowHomeServiceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/homeservice_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-homeservice").show();
			},
			success: function(result){
				$("#div_homeservice_stat").html(result);
				$("#loading-image-homeservice").hide();
			}
		});
	}
</script>