<div class="card-spacer">
	<div class="pt-1">
		<?php
		$no	= 0;
		foreach($query_hd->result() as $row_rm){
			$no++;
			if($mode == 'all'){
				$color	= "label-light-danger";
				$count[$row_rm->rm_status_id] = $this->Mainmodel->get_count_data("v_report_rekam_medis_labrad", "rm_labrad_produk=78 AND rm_status=".$row_rm->rm_status_id);
			}
			elseif($mode == 'month'){
				$color	= "label-light-info";
				$year	= date('Y');
				$month	= date('m');
				$count[$row_rm->rm_status_id] = $this->Mainmodel->get_count_data("v_report_rekam_medis_labrad", "rm_labrad_produk=78 AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." AND rm_status=".$row_rm->rm_status_id);
			}
			else{
				$color	= "label-light-success";
				$count[$row_rm->rm_status_id] = $this->Mainmodel->get_count_data("v_report_rekam_medis_labrad", "rm_labrad_produk=78 AND cast(rm_tanggal as date)='now()' AND rm_status=".$row_rm->rm_status_id);
			}
			$menu_report_url	= base_url().'report/rekammedisusg/'.$mode.'/?status='.$row_rm->rm_status_id;
			$menu_kunjungan_url	= base_url().'labrad/daftar/?mode_cari='.$mode.'&prod_cari=78&stat_cari='.$row_rm->rm_status_id;
		?>
		<div class="d-flex align-items-center pb-9">
			<div class="symbol symbol-45 symbol-light mr-4">
				<span class="symbol-label">
					<i class="<?php echo $row_rm->rm_status_icon?> icon-lg"></i>	
				</span>
			</div>
			<div class="d-flex flex-column flex-grow-1">
				<a href="<?php echo $menu_report_url?>" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank"><?php echo $row_rm->rm_status_ket?></a>
				<span class="text-muted font-weight-bold"><?php echo $row_rm->rm_status_deskripsi?></span>
			</div>
			<a href="<?php echo $menu_kunjungan_url?>" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank"><span class="font-weight-bolder label label-xl <?php echo $color ?> label-inline px-3 py-5 min-w-45px"><?php echo $count[$row_rm->rm_status_id]?></span></a>
		</div>
		<?php
		}
		?>
	</div>
</div>
