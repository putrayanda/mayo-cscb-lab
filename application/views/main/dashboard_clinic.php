<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title ?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Telemedicine Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowTelemedicineStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowTelemedicineStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowTelemedicineStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_telemed_stat">
						<img id="loading-image" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Pemeriksaan) Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowPemeriksaanStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowPemeriksaanStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowPemeriksaanStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_pemeriksaan_stat">
						<img id="loading-image-pemeriksaan" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-danger py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Vaksin) Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowVaksinStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowVaksinStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowVaksinStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_vaksin_stat">
						<img id="loading-image-vaksin" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-danger py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Steril) Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowSterilStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowSterilStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowSterilStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_steril_stat">
						<img id="loading-image-steril" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$.ajax({
			url: '<?php echo base_url().$class;?>/telemedicine_stat/day', 
			beforeSend: function() {
              $("#loading-image").show();
			},
			success: function(result){
				$("#div_telemed_stat").html(result);
				$("#loading-image").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/pemeriksaan_stat/day', 
			beforeSend: function() {
              $("#loading-image-pemeriksaan").show();
			},
			success: function(result){
				$("#div_pemeriksaan_stat").html(result);
				$("#loading-image-pemeriksaan").hide();
			}
		});


		$.ajax({
			url: '<?php echo base_url().$class;?>/vaksin_stat/day', 
			beforeSend: function() {
              $("#loading-image-vaksin").show();
			},
			success: function(result){
				$("#div_vaksin_stat").html(result);
				$("#loading-image-vaksin").hide();
			}
		});
		
		
		$.ajax({
			url: '<?php echo base_url().$class;?>/steril_stat/day', 
			beforeSend: function() {
              $("#loading-image-steril").show();
			},
			success: function(result){
				$("#div_steril_stat").html(result);
				$("#loading-image-steril").hide();
			}
		});
	});

	function ShowTelemedicineStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/telemedicine_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image").show();
			},
			success: function(result){
				$("#div_telemed_stat").html(result);
				$("#loading-image").hide();
			}
		});
	}

	function ShowPemeriksaanStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/pemeriksaan_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-pemeriksaan").show();
			},
			success: function(result){
				$("#div_pemeriksaan_stat").html(result);
				$("#loading-image-pemeriksaan").hide();
			}
		});
	}

	function ShowVaksinStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/vaksin_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-vaksin").show();
			},
			success: function(result){
				$("#div_vaksin_stat").html(result);
				$("#loading-image-vaksin").hide();
			}
		});
	}
	
	function ShowSterilStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/steril_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-steril").show();
			},
			success: function(result){
				$("#div_steril_stat").html(result);
				$("#loading-image-steril").hide();
			}
		});
	}

	function ShowNewPets(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/new_pets/'+periode, 
			beforeSend: function() {
              $("#loading-image-newpets").show();
			},
			success: function(result){
				$("#div_new_pets").html(result);
				$("#loading-image-newpets").hide();
			}
		});
	}
</script>