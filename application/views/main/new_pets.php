<link href="assets/bower_components/bootstrap-table/bootstrap-table.css" rel="stylesheet" type="text/css">
<script src="assets/bower_components/bootstrap-table/bootstrap-table.js"></script>
<script src="assets/bower_components/bootstrap-table/extensions/resizable/bootstrap-table-resizable.js"></script>
<script src="assets/bower_components/bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>
<script src="assets/js/bootstrap-editable.js"></script>
<script src="assets/bower_components/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script src="assets/bower_components/tableExport/tableExport.js"></script>
<script src="assets/bower_components/tableExport/libs/js-xlsx/xlsx.core.min.js"></script>
<script src="assets/bower_components/bootstrap-table/extensions/filter/bootstrap-table-filter.js"></script>
<div class="card-body">
	<table id="table_pasien" data-toggle="table" data-height="500" data-show-columns="false" data-search="true" data-show-toggle="false" data-pagination="true" data-page-list="[100, 500, 1000]" data-page-size="100" data-show-export="false">
		<thead>
			<tr>
				<th data-sortable="true" data-width="60" data-align="right">No</th>
				<th data-sortable="true" data-width="125">No. RM</th>
				<th data-sortable="true" data-width="225">Nama Hewan</th>
				<th data-sortable="true" data-width="200">Pemilik</th>
				<th data-sortable="true" data-width="135">Spesies</th>
				<th data-sortable="true" data-width="100" data-align="center">JK</th>
				<th data-sortable="true" data-width="135">Jenis Hewan</th>
				<th data-sortable="true" data-width="135">Ras Hewan</th>
				<th data-sortable="true" data-width="135">Warna Hewan</th>
				<th data-sortable="true" data-width="140">Umur</th>
				<th data-sortable="true" data-width="85" data-align="right">Berat (Kg)</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no	= 0;
			foreach($query_pasien->result() as $row_pasien){
				$no++;
				$pas_id		= $row_pasien->pas_id;
			?>
			<tr class="tr-class-<?php echo $no?>">
				<td><?php echo $no?></td>
				<td><?php echo $row_pasien->pas_mrn?></td>
				<td><?php echo $row_pasien->pas_nama; ?></td>
				<td><?php echo $row_pasien->nama_pemilik; ?></td>
				<td><?php echo $row_pasien->ref_spesies_nama; ?></td>
				<td><?php echo $row_pasien->ref_gender_nama; ?></td>
				<td><?php echo $row_pasien->ref_jns_hwn_nama; ?></td>
				<td><?php echo $row_pasien->ref_ras_nama; ?></td>
				<td><?php echo $row_pasien->ref_warna_ket; ?></td>
				<td><?php echo $row_pasien->umur; ?></td>
				<td><?php echo $row_pasien->pas_berat; ?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>	
</div>