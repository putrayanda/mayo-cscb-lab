<div class="card-spacer">
	<div class="pt-1">
		<div class="d-flex align-items-center pb-9">
			<div class="symbol symbol-45 symbol-light mr-4">
				<span class="symbol-label">
					<i class="flaticon2-line-chart icon-lg"></i>	
				</span>
			</div>
			<div class="d-flex flex-column flex-grow-1">
				<a href="" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank">Invoice</a>
			</div>
			<a href="<?=base_url().'finance/tagihanrawatjalan/?periode='.$mode?>" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank"><span class="font-weight-bolder label label-xl <?=$label_color?> label-inline px-3 py-5 min-w-45px"><?= number_format($countInvoice); ?></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="<?=base_url().'finance/tagihanrawatjalan/?periode='.$mode?>" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank"><span class="font-weight-bolder label label-xl <?=$label_color?> label-inline px-5 py-5 min-w-45px"><?= number_format($sumInvoice); ?></span></a>
		</div>
		<div class="d-flex align-items-center pb-9">
			<div class="symbol symbol-45 symbol-light mr-4">
				<span class="symbol-label">
					<i class="flaticon-analytics icon-lg"></i>	
				</span>
			</div>
			<div class="d-flex flex-column flex-grow-1">
				<a href="" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank">Penerimaan</a>
			</div>
			<a href="<?=base_url().'finance/kasirrawatjalan/?periode='.$mode?>" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank"><span class="font-weight-bolder label label-xl <?=$label_color?> label-inline px-3 py-5 min-w-45px"><?= number_format($countPenerimaan); ?></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="<?=base_url().'finance/kasirrawatjalan/?periode='.$mode?>" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder" target="_blank"><span class="font-weight-bolder label label-xl <?=$label_color?> label-inline px-5 py-5 min-w-45px"><?= number_format($sumPenerimaan); ?></span></a>
		</div>
	</div>
</div>
