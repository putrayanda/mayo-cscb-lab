<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title ?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan Finance Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="ShowRajalFinanceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowRajalFinanceStat('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_rajal_stat">
						<img id="loading-image-rajal" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Inap Finance Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="ShowRanapFinanceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowRanapFinanceStat('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_ranap_stat">
						<img id="loading-image-ranap" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
		</div>

		<!--div class="row">
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Pemeriksaan) Finance Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="ShowPemeriksaanFinanceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowPemeriksaanFinanceStat('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_pemeriksaan_stat">
						<img id="loading-image-pemeriksaan" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Vaksin) Finance Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="ShowVaksinFinanceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowVaksinFinanceStat('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_vaksin_stat">
						<img id="loading-image-vaksin" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Steril Non Komunitas) Finance Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="ShowSterilNonKomFinanceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowSterilNonKomFinanceStat('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_steril_non_kom_stat">
						<img id="loading-image-steril_non_kom" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Rawat Jalan (Steril Komunitas) Finance Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="ShowSterilKomunitasFinanceStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="ShowSterilKomunitasFinanceStat('month');">Month</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_steril_komunitas_stat">
						<img id="loading-image-steril_komunitas" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>
		</div-->
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$.ajax({
			url: '<?php echo base_url().$class;?>/rajal_finance_stat/day', 
			beforeSend: function() {
              $("#loading-image-rajal").show();
			},
			success: function(result){
				$("#div_rajal_stat").html(result);
				$("#loading-image-rajal").hide();
			}
		});
	
		$.ajax({
			url: '<?php echo base_url().$class;?>/ranap_finance_stat/day', 
			beforeSend: function() {
              $("#loading-image-ranap").show();
			},
			success: function(result){
				$("#div_ranap_stat").html(result);
				$("#loading-image-ranap").hide();
			}
		});		
		/*
		$.ajax({
			url: '<?php echo base_url().$class;?>/vaksin_finance_stat/day', 
			beforeSend: function() {
              $("#loading-image-vaksin").show();
			},
			success: function(result){
				$("#div_vaksin_stat").html(result);
				$("#loading-image-vaksin").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/steril_non_kom_finance_stat/day', 
			beforeSend: function() {
              $("#loading-image-steril_non_kom").show();
			},
			success: function(result){
				$("#div_steril_non_kom_stat").html(result);
				$("#loading-image-steril_non_kom").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/steril_komunitas_finance_stat/day', 
			beforeSend: function() {
              $("#loading-image-steril_komunitas").show();
			},
			success: function(result){
				$("#div_steril_komunitas_stat").html(result);
				$("#loading-image-steril_komunitas").hide();
			}
		});
		*/
	});

	function ShowRajalFinanceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/rajal_finance_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-rajal").show();
			},
			success: function(result){
				$("#div_rajal_stat").html(result);
				$("#loading-image-rajal").hide();
			}
		});
	}

	function ShowRanapFinanceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/ranap_finance_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-ranap").show();
			},
			success: function(result){
				$("#div_ranap_stat").html(result);
				$("#loading-image-ranap").hide();
			}
		});
	}

	/*
	function ShowPemeriksaanFinanceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/pemeriksaan_finance_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-pemeriksaan").show();
			},
			success: function(result){
				$("#div_pemeriksaan_stat").html(result);
				$("#loading-image-pemeriksaan").hide();
			}
		});
	}

	function ShowVaksinFinanceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/vaksin_finance_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-vaksin").show();
			},
			success: function(result){
				$("#div_vaksin_stat").html(result);
				$("#loading-image-vaksin").hide();
			}
		});
	}

	function ShowSterilNonKomFinanceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/steril_non_kom_finance_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-steril_non_kom").show();
			},
			success: function(result){
				$("#div_steril_non_kom_stat").html(result);
				$("#loading-image-steril_non_kom").hide();
			}
		});
	}

	function ShowSterilKomunitasFinanceStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/steril_komunitas_finance_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-steril_komunitas").show();
			},
			success: function(result){
				$("#div_steril_komunitas_stat").html(result);
				$("#loading-image-steril_komunitas").hide();
			}
		});
	}
	*/
</script>