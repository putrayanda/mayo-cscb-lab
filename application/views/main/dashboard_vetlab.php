<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
	<div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline mr-5">
				<h5 class="text-dark font-weight-bold my-2 mr-5"><?= $main_title ?></h5>
				<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
					<li class="breadcrumb-item">
						<a href="" class="text-muted"><?= $title ?></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="d-flex flex-column-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-primary py-5">
						<h3 class="card-title font-weight-bolder text-white">Rontgen Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowRontgenStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowRontgenStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowRontgenStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_rontgen_stat">
						<img id="loading-image-rontgen" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-info py-5">
						<h3 class="card-title font-weight-bolder text-white">USG Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowUsgStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowUsgStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowUsgStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_usg_stat">
						<img id="loading-image-usg" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Kimia Darah Stat</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowKimDarStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowKimDarStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowKimDarStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_kimdar_stat">
						<img id="loading-image-kimdar" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-xxl-6">
				<div class="card card-custom card-stretch gutter-b">
					<div class="card-header border-0 bg-warning py-5">
						<h3 class="card-title font-weight-bolder text-white">Hematologi</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowHemaStat('day');">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowHemaStat('month');">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab" href="javascript:void(0)" onclick="javascript:ShowHemaStat('all');">All</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-body p-0 position-relative overflow-hidden" id="div_hema_stat">
						<img id="loading-image-hema" src="assets/media/ajax-loader.gif" style="display:none;"/>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		$.ajax({
			url: '<?php echo base_url().$class;?>/rontgen_stat/day', 
			beforeSend: function() {
              $("#loading-image-rontgen").show();
			},
			success: function(result){
				$("#div_rontgen_stat").html(result);
				$("#loading-image-rontgen").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/usg_stat/day', 
			beforeSend: function() {
              $("#loading-image-usg").show();
			},
			success: function(result){
				$("#div_usg_stat").html(result);
				$("#loading-image-usg").hide();
			}
		});

		$.ajax({
			url: '<?php echo base_url().$class;?>/kimdar_stat/day', 
			beforeSend: function() {
              $("#loading-image-kimdar").show();
			},
			success: function(result){
				$("#div_kimdar_stat").html(result);
				$("#loading-image-kimdar").hide();
			}
		});


		$.ajax({
			url: '<?php echo base_url().$class;?>/hema_stat/day', 
			beforeSend: function() {
              $("#loading-image-hema").show();
			},
			success: function(result){
				$("#div_hema_stat").html(result);
				$("#loading-image-hema").hide();
			}
		});
	});

	function ShowRontgenStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/rontgen_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-rontgen").show();
			},
			success: function(result){
				$("#div_rontgen_stat").html(result);
				$("#loading-image-rontgen").hide();
			}
		});
	}

	function ShowUsgStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/usg_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-usg").show();
			},
			success: function(result){
				$("#div_usg_stat").html(result);
				$("#loading-image-usg").hide();
			}
		});
	}
	
	function ShowKimDarStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/kimdar_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-kimdar").show();
			},
			success: function(result){
				$("#div_kimdar_stat").html(result);
				$("#loading-image-kimdar").hide();
			}
		});
	}

	function ShowHemaStat(periode){
		$.ajax({
			url: '<?php echo base_url().$class;?>/hema_stat/'+periode, 
			beforeSend: function() {
              $("#loading-image-hema").show();
			},
			success: function(result){
				$("#div_hema_stat").html(result);
				$("#loading-image-hema").hide();
			}
		});
	}
</script>