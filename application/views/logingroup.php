<!DOCTYPE html>
<html lang="en">

<head>
	<base href="<?php echo base_url();?>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Selamat Datang di CSCB Leo n Vets, Web Application for Leo Group Staff" />
	<meta name="author" content="putrayanda"> 

	<meta property="og:site_name" content="CSCB Leo n Vets">
	<meta property="og:title" content="CSCB Leo n Vets" />
	<meta property="og:description" content="Selamat Datang di CSCB Leo n Vets, Web Application for Leo Group Staff"/>  
	<meta property="og:image" content="https://leonvets.id/assets/images/logo_275.png">
	<meta property="og:type" content="website" />
	<meta property="og:updated_time" content="1440432930" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />

	<title>CSCB | Leo n Vets</title>
	<link rel="stylesheet" href="styles.css">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;0,400;0,600;1,200;1,400;1,600&display=swap" rel="stylesheet">
	<style>
		:root {
			--red: hsl(0, 78%, 62%);
			--cyan: hsl(180, 62%, 55%);
			--orange: hsl(34, 97%, 64%);
			--blue: hsl(212, 86%, 64%);
			--varyDarkBlue: hsl(234, 12%, 34%);
			--grayishBlue: hsl(229, 6%, 66%);
			--veryLightGray: hsl(0, 0%, 98%);
			--weight1: 200;
			--weight2: 400;
			--weight3: 600;
		}

		body {
			font-size: 15px;
			font-family: 'Poppins', sans-serif;
			background-color: var(--veryLightGray);
		}

		.attribution { 
			font-size: 11px; text-align: center; 
		}
		.attribution a { 
			color: hsl(228, 45%, 44%); 
		}

		h1:first-of-type {
			font-weight: var(--weight1);
			color: var(--varyDarkBlue);

		}

		h1:last-of-type {
			color: var(--varyDarkBlue);
		}

		a {
		  position: relative;
		  display: inline-block;
		  padding: 8px 20px;
		  background: black;
		  border-radius: 5px;
		  text-decoration: none;
		  color: white;
		  margin-top: 20px;
		  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
		  transition: 0.5s;
		}

		a:hover {
		  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.6);
		  background: #fff;
		  color: #000;
		}

		@media (max-width: 400px) {
			h1 {
				font-size: 1.5rem;
			}
		}

		.header {
			text-align: center;
			line-height: 0.8;
			margin-bottom: 25px;
			margin-top: 25px;
		}

		.header p {
			margin: 0 auto;
			line-height: 2;
			color: var(--grayishBlue);
		}


		.box p {
			color: var(--grayishBlue);
		}

		.box {
			border-radius: 5px;
			box-shadow: 0px 30px 40px -20px var(--grayishBlue);
			padding: 30px;
			margin: 20px;  
		}

		.img {
			float: right;
		}

		@media (max-width: 450px) {
			.box {
				height: 200px;
			}
		}

		@media (max-width: 950px) and (min-width: 450px) {
			.box {
				text-align: center;
				height: 180px;
			}
		}

		.cyan {
			border-top: 3px solid var(--cyan);
		}

		.a_cyan {
		  background: var(--cyan);
		}

		.red {
			border-top: 3px solid var(--red);
		}

		.a_red {
		  background: var(--red);
		}

		.blue {
			border-top: 3px solid var(--blue);
		}

		.a_blue {
		  background: var(--blue);
		}

		.orange {
			border-top: 3px solid var(--orange);
		}

		.a_orange {
		  background: var(--orange);
		}

		h2 {
			color: var(--varyDarkBlue);
			font-weight: var(--weight3);
		}


		@media (min-width: 950px) {
			.row1-container {
				display: flex;
				justify-content: center;
				align-items: center;
			}
			
			.row2-container {
				display: flex;
				justify-content: center;
				align-items: center;
			}
			.box-down {
				position: relative;
				top: 150px;
			}
			.box {
				width: 20%;
			 
			}
			.header p {
				width: 30%;
			}			
		}
	</style>
</head>

<body>
  <div class="header">
    <h1><img src="assets/media/logos/logo.png" alt="" width="150px" /></h1>
    <h1>Customer Service Centre Based</h1>
  </div>
  <div class="row1-container">
    <div class="box box-down cyan">
      <h2>Leo Pet Care </a></h2>
      <p>Web App untuk pelayanan Grooming, Cat Hotel</p>
      <a href="loginauth/login/care" class="a_cyan">Login</a> <img class="img" src="assets/media/vet_care.png" alt="Leo Pet Care" width="60px" >
    </div>

    <div class="box red">
      <h2>Leo Vet Clinic</h2>
      <p>Web App untuk pelayanan Telemedicine, Rawat Jalan & Rawat Inap</p>
      <a href="loginauth/login/clinic" class="a_red">Login</a> <img class="img" src="assets/media/vet_clinic.png" alt="Leo Vet Clinic" width="60px" >
    </div>

    <div class="box box-down blue">
      <h2>Leo Vet Lab</h2>
      <p>Web App untuk pelayanan pemeriksaan lanjutan</p>
      <a href="loginauth/login/lab" class="a_blue">Login</a> <img class="img" src="assets/media/vet_lab.png" alt="Leo Vet Lab" width="60px" >
    </div>
  </div>
  <div class="row2-container">
    <div class="box orange">
      <h2>Apotek Leo n Vets</h2>
      <p>Web App untuk pelayanan obat & vitamin</p>
      <a href="loginauth/login/med" class="a_orange">Login</a> <img class="img" src="assets/media/vet_pharmacy.png" alt="Leo Vet Lab" width="60px" >
    </div>
  </div>
</body>
</html>