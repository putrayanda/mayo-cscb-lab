<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        if (!$this->session->userdata('userlogin')) {
			($this->input->get('rNum') > 0) ? $url = $this->uri->uri_string().'/?rNum='.$this->input->get('rNum') : $url = $this->uri->uri_string();
            $this->session->set_userdata('urlback', $url);
            redirect('loginauth/login');
        }
        $this->load->model('Mainmodel');
		$this->load->library('user_agent');
		$this->load->helper('form');
		$peg_id	= $this->session->userdata('peg_id');
		$this->rowPgw = $this->Mainmodel->get_row_data("v_pegawai","peg_id=".$peg_id);
    }

    public function index() {
		$this->dashboard();
    }

    function rekammedis($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= $this->input->get('hSQL');
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();

		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0 AND rm_status_id <= 10", "rm_status_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_id > 0", "ref_cab_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis WHERE rm_id > 0".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

    function rekammedistelemedicine($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0", "rm_status_id", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis WHERE rm_asal = 2".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

    function rekammediskunjungan($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$periode	= $this->input->get('periode');
		$perawatan	= $this->input->get('perawatan');
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && !empty($perawatan) && !empty($mode)){
				if($mode=='all'){
					$whereClause	= " AND rm_perawatan=".$perawatan." AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_perawatan=".$perawatan." AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_perawatan=".$perawatan." AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0 AND rm_status_id <= 10", "rm_status_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 1", "ref_cab_nama", 0);
		$data['comboPerawatan']	= $this->Mainmodel->show_combo("ref_kondisi", "ref_kondisi_id", "ref_kondisi_ket", "ref_kondisi_id IN (127, 128, 129) AND ref_kondisi_perawatan IS TRUE", "ref_kondisi_id", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_report_rekam_medis_rajal WHERE rm_asal = 1 AND rm_perawatan IN (10,127,128,129)".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

    function rekammedisrawatinap($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
				$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboKeputusan']	= $this->Mainmodel->show_combo_array("rekam_medis_keputusan", "rm_kep_id", "rm_kep_ket", "rm_kep_id IN (2,3,6,7)", "rm_kep_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 1", "ref_cab_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis WHERE rm_asal = 3".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

    function rekammedisrontgen($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
				$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboKlinik']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_jenis = 1", "ph_nama", 0);
		$data['comboDokter']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_report_rekam_medis_labrad WHERE rm_labrad_produk=70".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_labrad_id");
		$this->load->view('layout', $data);
	}

    function rekammedisusg($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
				$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboKlinik']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_jenis = 1", "ph_nama", 0);
		$data['comboDokter']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_report_rekam_medis_labrad WHERE rm_labrad_produk=78".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_labrad_id");
		$this->load->view('layout', $data);
	}

    function rekammedishema($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
				$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboKlinik']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_jenis = 1", "ph_nama", 0);
		$data['comboDokter']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_report_rekam_medis_labrad WHERE rm_labrad_produk IN (72)".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_labrad_id");
		$this->load->view('layout', $data);
	}

    function rekammedisdarah($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboKlinik']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_jenis = 1", "ph_nama", 0);
		$data['comboDokter']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_report_rekam_medis_labrad WHERE rm_labrad_produk IN (88,113)".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_labrad_id");
		$this->load->view('layout', $data);
	}

    function rekammedisinhalasi($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
				$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboKlinik']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_jenis = 1", "ph_nama", 0);
		$data['comboDokter']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_report_rekam_medis_labrad WHERE rm_labrad_produk IN (1346,1347,1348,1349,1350)".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_labrad_id");
		$this->load->view('layout', $data);
	}

    function rekammedisgrooming($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0 AND rm_status_id <= 10", "rm_status_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 3", "ref_cab_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis_grooming WHERE rm_asal = 1".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

    function rekammediscathotel($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0 AND rm_status_id <= 10", "rm_status_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 3", "ref_cab_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis WHERE rm_perawatan IN (126)".$whereClause." ORDER BY rm_tanggal, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

    function rekammedisdetail($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= $this->input->get('hSQL');
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
					$whereClause	= " AND rm_status= ".$status." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND rm_status= ".$status." AND rm_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
			}
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();

		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0 AND rm_status_id <= 10", "rm_status_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_id > 0", "ref_cab_nama", 0);
		$data['comboJenis']		= $this->Mainmodel->show_combo("ref_jenis_produk", "ref_jns_prod_id", "ref_jns_prod_nama", "ref_jns_prod_id > 0", "ref_jns_prod_nama", 0);
		$data['comboProduk']	= $this->Mainmodel->show_combo("ref_produk", "ref_prod_id", "ref_prod_nama_invoice", "ref_prod_nama_invoice <> ''", "ref_prod_nama_invoice", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis_detail WHERE rm_tinob_id > 0".$whereClause." ORDER BY rm_tanggal,rm_tinob_id");
		$this->load->view('layout', $data);
	}

    function homeservice($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			if(!empty($mode) && $status <> ''){
				if($mode=='all'){
					$whereClause	= " AND rm_status= ".$status;
				}
				elseif($mode=='month'){
					$year	= date('Y');
					$month	= date('m');
				$whereClause	= " AND hs_status= ".$status." AND date_part('year', hs_tanggal) = ".$year." AND date_part('month', hs_tanggal) = ".$month." ";
				}
				else{
					$whereClause	= " AND hs_status= ".$status." AND hs_tanggal='now()'";
				}
			}
			else{
				$year	= date('Y');
				$month	= date('m');
				$whereClause	= " AND date_part('year', hs_tanggal) = ".$year." AND date_part('month', hs_tanggal) = ".$month." ";
			}
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['query_hd']		= $this->db->query("SELECT * FROM v_home_service WHERE hs_id > 0".$whereClause." ORDER BY hs_tanggal");
		$this->load->view('layout', $data);
	}

    function stok($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;

		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 3", "ref_cab_nama", 0);
		$data['comboSupplier']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_jenis = 2", "ph_nama", 0);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_stok WHERE stok_jumlah_sisa > 0".$whereClause." ORDER BY stok_tgl_masuk");
		$this->load->view('layout', $data);
	}

    function deposit($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$rNum		= $this->input->get('rNum');
		if(empty($rNum)){
			$rNum = 0;
		}
		else{
			$data['query_penerimaan']	= $this->db->query("SELECT * FROM v_penerimaan_kas_bank WHERE pkb_asal_terima IN (51,52,59) AND id_pemilik=".$rNum." ORDER BY pkb_tgl_terima");
			$data['query_pemakaian']	= $this->db->query("SELECT * FROM v_penerimaan_kas_bank WHERE pkb_cara_bayar= 5 AND id_pemilik=".$rNum." ORDER BY pkb_tgl_terima");
			$data['query_refund']		= $this->db->query("SELECT * FROM v_pengeluaran_kas_bank WHERE pkb_asal_keluar = 88 AND id_pemilik=".$rNum." ORDER BY pkb_tgl_keluar");
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;
		$data['rNum']			= $rNum;
		$data['peg_id']			= $peg_id;

		$data['query_hd']		= $this->db->query("SELECT * FROM v_pemilik WHERE jumlah_deposit > 0 ORDER BY pem_nama");
		$this->load->view('layout', $data);
	}

    function rekaprawatinap($mode='') {
		$rNum		= $this->input->get('rNum');
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			$whereClause	= " AND tagihan_tindakan_obat_ri >=0  AND rm_tanggal >= '01-07-2022'";
		}		
		if(empty($rNum)){
			$rNum = 0;
		}
		else{
			$whereClause = ' AND rm_id='.$rNum;
			$data['query_tindakan']	= $this->db->query("SELECT * FROM v_rekam_medis_tindakan_obat WHERE rm_refer_rj_id=".$rNum." ORDER BY rm_tinob_id");
			$data['query_deposit']	= $this->db->query("SELECT * FROM v_penerimaan_kas_bank WHERE pkb_asal_terima = 52 AND pkb_rm_id=".$rNum." ORDER BY pkb_tgl_terima");
			$data['query_tagihan']	= $this->db->query("SELECT * FROM v_invoice_tagihan_ranap WHERE inv_jenis = 2 AND inv_rm_id=".$rNum." ORDER BY inv_tanggal");
			$data['query_terima']	= $this->db->query("SELECT * FROM v_penerimaan_kas_bank WHERE pkb_asal_terima = 2 AND pkb_inv_id IN (SELECT inv_id FROM invoice WHERE inv_rm_id=".$rNum.") ORDER BY pkb_tgl_terima");
			$data['query_refund']	= $this->db->query("SELECT * FROM v_pengeluaran_kas_bank WHERE pkb_asal_keluar = 88 AND pkb_id_rm_deposit=".$rNum." ORDER BY pkb_tgl_keluar");
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;
		$data['rNum']			= $rNum;
		$data['peg_id']			= $peg_id;

		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 1", "ref_cab_nama", 0);
		$data['query_rm']		= $this->db->query("SELECT *,tgl_akhir_inap::date - rm_tanggal as lama_inap FROM v_rekam_medis_tagihan_inap WHERE rm_asal = 1 AND rm_keputusan = 2 ".$whereClause." ORDER BY rm_tanggal");
		$this->load->view('layout', $data);
	}

    function rekaprawatjalan($mode='') {
		$rNum		= $this->input->get('rNum');
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			$month	= date('m');
			$year	= date('Y');
			$whereClause	= " AND date_part('year',rm_tanggal)=".$year." AND date_part('month',rm_tanggal)= ".$month;
		}		
		if(empty($rNum)){
			$rNum = 0;
		}
		else{
			$whereClause = ' AND rm_id='.$rNum;
			$data['query_tindakan']	= $this->db->query("SELECT * FROM v_rekam_medis_tindakan_obat WHERE rm_tinob_rm_id=".$rNum." ORDER BY rm_tinob_id");
			$data['query_tagihan']	= $this->db->query("SELECT * FROM v_invoice_rekam_medis WHERE inv_jenis = 1 AND inv_rm_id=".$rNum." ORDER BY inv_tanggal");
			$data['query_terima']	= $this->db->query("SELECT * FROM v_penerimaan_kas_bank WHERE pkb_asal_terima = 1 AND pkb_inv_id IN (SELECT inv_id FROM invoice WHERE inv_rm_id=".$rNum.") ORDER BY pkb_tgl_terima");
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;
		$data['rNum']			= $rNum;
		$data['peg_id']			= $peg_id;

		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 1", "ref_cab_nama", 0);
		$data['query_rm']		= $this->db->query("SELECT *FROM v_rekam_medis_tagihan WHERE rm_asal = 1 AND rm_perawatan IN (10,127,128,129) AND rm_status > 1 AND rm_status < 10 ".$whereClause." ORDER BY rm_tanggal");
		$this->load->view('layout', $data);
	}

    function rekaplabrad($mode='') {
		$rNum		= $this->input->get('rNum');
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$status		= $this->input->get('status');
		$whereClause = '';
		
		if(!empty($hSQL)){
            $whereClause	= base64_decode($hSQL);
		}
		else{
			$month	= date('m');
			$year	= date('Y');
			//$whereClause	= " AND date_part('year',inv_tanggal)=".$year." AND date_part('month',inv_tanggal)= ".$month;
		}		
		if(empty($rNum)){
			$rNum = 0;
		}
		else{
			$whereClause = ' AND inv_id='.$rNum;
			$data['query_terima']	= $this->db->query("SELECT * FROM v_penerimaan_kas_bank WHERE pkb_asal_terima = 3 AND pkb_inv_id =".$rNum." ORDER BY pkb_tgl_terima");
			$data['query_rm']		= $this->db->query("SELECT * FROM v_invoice_detail WHERE inv_dt_inv_id=".$rNum." ORDER BY inv_dt_id");
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_jabatan']	= $peg_jabatan;
		$data['rNum']			= $rNum;
		$data['peg_id']			= $peg_id;

		$data['query_tagihan']	= $this->db->query("SELECT * FROM v_invoice_tagihan_labrad WHERE inv_id > 0".$whereClause." ORDER BY inv_tanggal");
		$this->load->view('layout', $data);
	}

    function statusrajal($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= $this->input->get('hSQL');
		$status		= $this->input->get('status');
		$whereClause = '';
		$cr_cabang		= 0;
		$cr_perawatan	= 0;
		$cr_periode		= '';
		
		if($this->input->post('cr_cabang') > 0){
			$cr_cabang	= $this->input->post('cr_cabang');
			$whereClause .= " AND rm_cabang=".$cr_cabang;
		}

		if($this->input->post('cr_perawatan') > 0){
			$cr_perawatan	= $this->input->post('cr_perawatan');
			$whereClause .= " AND rm_perawatan=".$cr_perawatan;
		}

		if(!empty($this->input->post('cr_periode'))){
			$cr_periode		= $this->input->post('cr_periode');
			$periode		= explode("-", trim($this->input->post('cr_periode')));
			$cari_tgl_awl	= $periode[0];
			$cari_tgl_akh	= $periode[1];
			$whereClause	.= " AND cast(rm_tanggal as date) >= '" . $cari_tgl_awl . "' AND cast(rm_tanggal as date) <= '" . $cari_tgl_akh . "'";
		}
		else{
			$whereClause	.= " AND rm_tanggal='now()'";
		}
	
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();

		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_id > 0", "ref_cab_nama", $cr_cabang);
		$data['comboPerawatan']	= $this->Mainmodel->show_combo("ref_kondisi", "ref_kondisi_id", "ref_kondisi_ket", "ref_kondisi_id IN (127, 128, 129) AND ref_kondisi_perawatan IS TRUE", "ref_kondisi_id", $cr_perawatan);
		$data['cr_periode']		= $cr_periode;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis_minimalis WHERE rm_asal = 1 AND rm_perawatan IN (10,127,128,129) AND rm_status_proses_halo IS TRUE AND rm_status > 0".$whereClause." ORDER BY rm_cabang, rm_perawatan, rm_time, rm_no_urut, rm_id");
		$this->load->view('layout', $data);
	}

	function rekammedis_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$cr_pasien		= $this->input->post('cr_pasien');
		$cr_mrn			= $this->input->post('cr_mrn');
		$cr_pemilik		= $this->input->post('cr_pemilik');
		$cr_no_hp		= $this->input->post('cr_no_hp');
		$cr_no_resep	= $this->input->post('cr_no_resep');
		$cr_status		= $this->input->post('cr_status');
		$cr_cabang		= $this->input->post('cr_cabang');
		$cr_periode		= $this->input->post('cr_periode');
		$whereClause	= '';
		$cr_keputusan	= $this->input->post('cr_keputusan');
		$cr_prwtn		= $this->input->post('cr_prwtn');
		$cr_vaksin		= $this->input->post('cr_vaksin');
		$cr_steril		= $this->input->post('cr_steril');

		if (!empty($cr_pasien)) {
			$whereClause .= " AND pas_nama~*'" . $cr_pasien . "'";
		}

		if (!empty($cr_mrn)) {
			$whereClause .= " AND pas_mrn~*'" . $cr_mrn . "'";
		}

		if (!empty($cr_pemilik)) {
			$whereClause .= " AND pem_nama~*'" . $cr_pemilik . "'";
		}

		if (!empty($cr_no_hp)) {
			$whereClause .= " AND pem_no_hp_wa~*'" . $cr_no_hp . "'";
		}

		if (!empty($cr_no_resep)) {
			$whereClause .= " AND rm_no_resep~*'" . $cr_no_resep . "'";
		}

		if (!empty($cr_status)) {
			$whereClause .= " AND rm_status = " . $cr_status;
		}

		if (!empty($cr_cabang)) {
			$whereClause .= " AND rm_cabang = " . $cr_cabang;
		}

		if (!empty($cr_keputusan)) {
			$whereClause .= " AND rm_keputusan = " . $cr_keputusan;
		}

		if (!empty($cr_prwtn)) {
			$whereClause .= " AND rm_perawatan = " . $cr_prwtn;
		}

		if(!empty($cr_periode)){
			$periode		= explode("-", trim($cr_periode));
			$cari_tgl_awl	= $periode[0];
			$cari_tgl_akh	= $periode[1];
			$whereClause	.= " AND cast(rm_tanggal as date) >= '" . $cari_tgl_awl . "' AND cast(rm_tanggal as date) <= '" . $cari_tgl_akh . "'";
		}

		if (!empty($cr_vaksin)) {
			if($cr_vaksin == 1){
				$whereClause .= " AND last_vaksin_leo IS NOT NULL";
			}
			else{
				$whereClause .= " AND last_vaksin_leo IS NULL";
			}
		}

		if (!empty($cr_steril)) {
			if($cr_vaksin == 1){
				$whereClause .= " AND last_steril_leo IS NOT NULL";
			}
			else{
				$whereClause .= " AND last_steril_leo IS NULL";
			}
		}

		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}


	function rekammedisdetailsearch(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$cr_jenis		= $this->input->post('cr_jenis');
		$cr_produk		= $this->input->post('cr_produk');
		$cr_pasien		= $this->input->post('cr_pasien');
		$cr_mrn			= $this->input->post('cr_mrn');
		$cr_pemilik		= $this->input->post('cr_pemilik');
		$cr_no_hp		= $this->input->post('cr_no_hp');
		$cr_no_resep	= $this->input->post('cr_no_resep');
		$cr_status		= $this->input->post('cr_status');
		$cr_cabang		= $this->input->post('cr_cabang');
		$cr_periode		= $this->input->post('cr_periode');
		$whereClause	= '';
		$cr_keputusan	= $this->input->post('cr_keputusan');
		$cr_prwtn		= $this->input->post('cr_prwtn');
		$cr_vaksin		= $this->input->post('cr_vaksin');

		if (!empty($cr_jenis)) {
			$whereClause .= " AND ref_prod_jenis=" . $cr_jenis;
		}

		if (!empty($cr_produk)) {
			$whereClause .= " AND rm_tinob_produk=" . $cr_produk;
		}

		if (!empty($cr_pasien)) {
			$whereClause .= " AND pas_nama~*'" . $cr_pasien . "'";
		}

		if (!empty($cr_mrn)) {
			$whereClause .= " AND pas_mrn~*'" . $cr_mrn . "'";
		}

		if (!empty($cr_pemilik)) {
			$whereClause .= " AND pem_nama~*'" . $cr_pemilik . "'";
		}

		if (!empty($cr_no_hp)) {
			$whereClause .= " AND pem_no_hp_wa~*'" . $cr_no_hp . "'";
		}

		if (!empty($cr_no_resep)) {
			$whereClause .= " AND rm_no_resep~*'" . $cr_no_resep . "'";
		}

		if (!empty($cr_status)) {
			$whereClause .= " AND rm_status=" . $cr_status;
		}

		if (!empty($cr_cabang)) {
			$whereClause .= " AND rm_cabang=" . $cr_cabang;
		}

		if (!empty($cr_keputusan)) {
			$whereClause .= " AND rm_keputusan=" . $cr_keputusan;
		}

		if (!empty($cr_prwtn)) {
			$whereClause .= " AND rm_perawatan=" . $cr_prwtn;
		}

		if (!empty($cr_vaksin)) {
			if($cr_vaksin == 1){
				$whereClause .= " AND pas_last_vaksin IS NOT NULL";
			}
			else{
				$whereClause .= " AND pas_last_vaksin IS NULL";
			}
		}
		if(!empty($cr_periode)){
			$periode		= explode("-", trim($cr_periode));
			$cari_tgl_awl	= $periode[0];
			$cari_tgl_akh	= $periode[1];
			$whereClause	.= " AND cast(rm_tanggal as date) >= '" . $cari_tgl_awl . "' AND cast(rm_tanggal as date) <= '" . $cari_tgl_akh . "'";
		}

		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}

	function rekammedislabrad_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$cr_pasien		= $this->input->post('cr_pasien');
		$cr_mrn			= $this->input->post('cr_mrn');
		$cr_pemilik		= $this->input->post('cr_pemilik');
		$cr_no_hp		= $this->input->post('cr_no_hp');
		$cr_klinik_rujukan		= $this->input->post('cr_klinik_rujukan');
		$cr_dokter_rujukan		= $this->input->post('cr_dokter_rujukan');
		$cr_periode		= $this->input->post('cr_periode');
		$whereClause	= '';

		if (!empty($cr_pasien)) {
			$whereClause .= " AND pas_nama~*'" . $cr_pasien . "'";
		}

		if (!empty($cr_mrn)) {
			$whereClause .= " AND pas_mrn~*'" . $cr_mrn . "'";
		}

		if (!empty($cr_pemilik)) {
			$whereClause .= " AND pem_nama~*'" . $cr_pemilik . "'";
		}

		if (!empty($cr_no_hp)) {
			$whereClause .= " AND pem_no_hp_wa~*'" . $cr_no_hp . "'";
		}

		if (!empty($cr_klinik_rujukan)) {
			$whereClause .= " AND rm_klinik_rujukan = " . $cr_klinik_rujukan;
		}

		if (!empty($cr_dokter_rujukan)) {
			$whereClause .= " AND rm_dokter_rujukan = " . $cr_dokter_rujukan;
		}

		if(!empty($cr_periode)){
			$periode		= explode("-", trim($cr_periode));
			$cari_tgl_awl	= $periode[0];
			$cari_tgl_akh	= $periode[1];
			$whereClause	.= " AND cast(rm_tanggal as date) >= '" . $cari_tgl_awl . "' AND cast(rm_tanggal as date) <= '" . $cari_tgl_akh . "'";
		}

		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}

	function homeservice_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$cr_pemilik		= $this->input->post('cr_pemilik');
		$cr_no_hp		= $this->input->post('cr_no_hp');
		$cr_periode		= $this->input->post('cr_periode');
		$whereClause	= '';

		if (!empty($cr_pemilik)) {
			$whereClause .= " AND pem_nama~*'" . $cr_pemilik . "'";
		}

		if (!empty($cr_no_hp)) {
			$whereClause .= " AND pem_no_hp_wa~*'" . $cr_no_hp . "'";
		}

		if(!empty($cr_periode)){
			$periode		= explode("-", trim($cr_periode));
			$cari_tgl_awl	= $periode[0];
			$cari_tgl_akh	= $periode[1];
			$whereClause	.= " AND cast(hs_tanggal as date) >= '" . $cari_tgl_awl . "' AND cast(hs_tanggal as date) <= '" . $cari_tgl_akh . "'";
		}

		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}

    function rekapkomisireferral($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->Mainmodel->get_one_data("peg_jabatan", "pegawai", "peg_id=".$peg_id);
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$cr_bulan	= $this->input->get('cr_bulan');
		$cr_tahun	= $this->input->get('cr_tahun');
		$whereClause	= '';
		
		if(empty($cr_bulan)){
			$cr_bulan		= date('m');
			$cr_tahun		= date('Y');
		}

		$whereClause	= " AND date_part('month',rm_tanggal)= ".$cr_bulan." AND date_part('year',rm_tanggal)=".$cr_tahun."";
		
		if($rNum > 0){
			$whereClause	.= " AND rm_referral=".$rNum;
		}

		if($rNum2 > 0){
			$whereClause	.= " AND rm_tanggal='".$rNum2."'";
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;
		$data['whereClause']	= $whereClause;

		$data['comboBulan']		= $this->Mainmodel->show_combo("ref_bulan", "ref_bln_id", "ref_bln_nama", "ref_bln_id > 0", "ref_bln_id", $cr_bulan);
		$data['cr_tahun']		= $cr_tahun;
		$data['comboStatus']	= $this->Mainmodel->show_combo("rekam_medis_status", "rm_status_id", "rm_status_ket", "rm_status_id > 0 AND rm_status_id <= 10", "rm_status_id", 0);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 1", "ref_cab_nama", 0);
		
		$data['query_referral']	= $this->db->query("SELECT rm_referral, nama_referral, COUNT(*) AS rm_count FROM v_rekam_medis_referral WHERE rm_referral > 0 AND inv_id > 0".$whereClause." GROUP BY rm_referral,nama_referral ORDER BY nama_referral");
		$data['query_tgl']		= $this->db->query("SELECT rm_tanggal, COUNT(*) AS rm_count FROM v_rekam_medis_referral WHERE rm_referral > 0 AND inv_id > 0".$whereClause." GROUP BY rm_tanggal ORDER BY rm_tanggal");

		$data['query_referral_lok']	= $this->db->query("SELECT ref_cab_nama, nama_referral, COUNT(*) AS rm_count FROM v_rekam_medis_referral WHERE rm_referral > 0 AND inv_id > 0".$whereClause." GROUP BY ref_cab_nama, nama_referral ORDER BY nama_referral, ref_cab_nama");
		$data['query_tgl_lok']		= $this->db->query("SELECT ref_cab_nama, rm_tanggal, COUNT(*) AS rm_count FROM v_rekam_medis_referral WHERE rm_referral > 0 AND inv_id > 0".$whereClause." GROUP BY ref_cab_nama, rm_tanggal ORDER BY rm_tanggal, ref_cab_nama");
	
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis_referral WHERE rm_referral > 0 AND inv_id > 0".$whereClause." ORDER BY rm_tanggal, rm_id");
		$this->load->view('layout', $data);
	}
}
