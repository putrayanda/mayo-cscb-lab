<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ajax extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mainmodel');
    }

	function create_list($mode) {
		$sel_id  = $this->input->post('sel_id');
		$set_id  = $this->input->post('set_id');
		if(empty($sel_id)){
			$sel_id=0;
		}
		if(empty($set_id)){
			$set_id=0;
		}
		if($mode == 'get_propinsi'){
			$sql    = " SELECT	ref_prop_id,
								ref_prop_ket
						FROM	ref_propinsi
						WHERE	ref_prop_id_negara=".$sel_id."
						ORDER BY ref_prop_ket";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_prop_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_prop_id.'" '.$selected.'>'.$rrQ->ref_prop_ket.'</option>';
			}
		}
		elseif($mode == 'get_kota'){
			$sql    = " SELECT	ref_kota_id,
								ref_kota_ket,
								ref_kota_tipe
						FROM	ref_kota
						WHERE	ref_kota_id_prop=".$sel_id."
						ORDER BY ref_kota_ket";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_kota_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_kota_id.'" '.$selected.'>'.$rrQ->ref_kota_tipe.' '.$rrQ->ref_kota_ket.'</option>';
			}
		}
		elseif($mode == 'get_kcmtn'){
			$sql    = " SELECT	ref_kcmtn_id,
								ref_kcmtn_ket
						FROM	ref_kecamatan
						WHERE	ref_kcmtn_id_kota=".$sel_id."
						ORDER BY ref_kcmtn_ket";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_kcmtn_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_kcmtn_id.'" '.$selected.'>'.$rrQ->ref_kcmtn_ket.'</option>';
			}
		}
		elseif($mode == 'get_ras'){
			$sql    = " SELECT	ref_ras_id,
								ref_ras_nama
						FROM	ref_ras
						WHERE	ref_ras_spesies=".$sel_id."
						ORDER BY ref_ras_nama";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_ras_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_ras_id.'" '.$selected.'>'.$rrQ->ref_ras_nama.'</option>';
			}
		}
		elseif($mode == 'get_cabang'){
			$sql    = " SELECT	ref_cab_id,
								ref_cab_nama
						FROM	ref_cabang
						WHERE	ref_cab_organisasi=".$sel_id."
						ORDER BY ref_cab_nama";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_cab_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_cab_id.'" '.$selected.'>'.$rrQ->ref_cab_nama.'</option>';
			}
		}
		elseif($mode == 'get_dokter_klinik'){
			$sql    = " SELECT	ph_dok_id,
								dok_nama
						FROM	v_pihak_dokter
						WHERE	ph_dok_pihak=".$sel_id."
						ORDER BY dok_nama";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ph_dok_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ph_dok_id.'" '.$selected.'>'.$rrQ->dok_nama.'</option>';
			}
		}
		elseif($mode == 'get_pihak'){
			$rrJnsPihak = $this->Mainmodel->get_row_data("ref_jenis_pihak", "ref_jns_phk_id =".$sel_id);
			$table		= $rrJnsPihak->ref_jns_phk_table;
			$fieldId	= $rrJnsPihak->ref_jns_phk_field_id;
			$fieldName	= $rrJnsPihak->ref_jns_phk_field_name;
			$fieldClause= $rrJnsPihak->ref_jns_phk_field_clause;

			$sql    = " SELECT	".$fieldId.",
								".$fieldName."
						FROM	".$table."
						WHERE	".$fieldId." > 0 ".$fieldClause."
						ORDER BY ".$fieldName;
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->$fieldId){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->$fieldId.'" '.$selected.'>'.$rrQ->$fieldName.'</option>';
			}
		}
		elseif($mode == 'get_sub_akun'){
			$sql    = " SELECT	kd_rkng_id,
								kd_rkng_kode,
								kd_rkng_nama,
								kd_rkng_parent
						FROM	v_kode_rekening
						WHERE	kd_rkng_kategori=".$sel_id."
						ORDER BY kd_rkng_kode";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			$tabSpasi = '';
			$parent1  = 0;
			foreach($rhQ->result() as $rrQ){
				if($rrQ->kd_rkng_parent > 0){
					$parent1	= $this->Mainmodel->get_one_data("kd_rkng_parent","kode_rekening", "kd_rkng_id=".$rrQ->kd_rkng_parent);
					if($parent1 > 0){
						$parent2	= $this->Mainmodel->get_one_data("kd_rkng_parent","kode_rekening", "kd_rkng_id=".$parent1);
						if($parent2 > 0){
							$tabSpasi = '&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;';
						}
						else{
							$tabSpasi = '&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;';
						}

					}
					else{
						$tabSpasi = '';
					}
				}

				if($set_id==$rrQ->kd_rkng_id){
					$selected="selected";
				}
				else{
					$selected="";
				}

				$list	.= '<option value="'.$rrQ->kd_rkng_id.'" '.$selected.'>'.$tabSpasi.''.$rrQ->kd_rkng_kode.' - '.$rrQ->kd_rkng_nama.'</option>';
			}
		}
		elseif($mode == 'get_kamar_avail'){
			//Bekasi
			if($sel_id == 5){
				$clause = ' AND ref_kmr_cab_bekasi_rm_id IS NULL AND 5 = ANY (ref_kmr_avail_in_cabang)';
			}
			//Depok
			elseif($sel_id == 6){
				$clause = ' AND ref_kmr_cab_depok_rm_id IS NULL AND 6 = ANY (ref_kmr_avail_in_cabang)';
			}
			//P.Aren
			elseif($sel_id == 7){
				$clause = ' AND ref_kmr_cab_aren_rm_id IS NULL AND 7 = ANY (ref_kmr_avail_in_cabang)';
			}
			else{
				$clause = ' AND ref_kmr_id=0';
			}

			$sql    = " SELECT	ref_kmr_id,
								ref_kmr_nama
						FROM	ref_kamar
						WHERE	ref_kmr_id > 0 ".$clause."
						ORDER BY ref_kmr_id";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_kmr_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_kmr_id.'" '.$selected.'>'.$rrQ->ref_kmr_nama.'</option>';
			}
		}
		elseif($mode == 'get_kamar_gabung'){
			$data_pasien	= $this->Mainmodel->get_row_data("pasien", "pas_id=".$sel_id);
			$pemilik		= $data_pasien->pas_pemilik_utama;
			$sql    = " SELECT	rm_id,
								pas_nama
						FROM	v_rekam_medis
						WHERE	rm_perawatan = 126 AND
								rm_status <> 8 AND
								pas_pemilik_utama=".$pemilik."
						ORDER BY pas_nama";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->rm_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->rm_id.'" '.$selected.'>'.$rrQ->pas_nama.'</option>';
			}
		}
		elseif($mode == 'set_kamar_gabung'){
			$data_rm	= $this->Mainmodel->get_row_data("rekam_medis", "rm_id=".$sel_id);
			$kmr_gab	= $data_rm->rm_kamar;
			$sql    = " SELECT	ref_kmr_id,
								ref_kmr_nama
						FROM	ref_kamar
						WHERE	ref_kmr_id =".$kmr_gab."
						ORDER BY ref_kmr_id";
			$rhQ	= $this->db->query($sql);
			$list	= '<option label="Label"></option>';
			foreach($rhQ->result() as $rrQ){
				if($set_id==$rrQ->ref_kmr_id){
					$selected="selected";
				}
				else{
					$selected="";
				}
				$list	.= '<option value="'.$rrQ->ref_kmr_id.'" '.$selected.'>'.$rrQ->ref_kmr_nama.'</option>';
			}
		}
		echo json_encode($list);
	}

	function get_data($mode) {
		if($mode == 'pemilik_by_email'){
			$rNum	= $this->input->get('rNum');
			$email	= $this->input->get('email');
			if($rNum > 0){
				$data = $this->Mainmodel->get_row_data("pemilik", "pem_email='".$email."' AND pem_id!=".$rNum);
			}
			else{
				$data = $this->Mainmodel->get_row_data("pemilik", "pem_email='".$email."'");
			}

			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'pemilik_by_phone'){
			$rNum	= $this->input->get('rNum');
			$phone	= $this->input->get('phone');
			if($rNum > 0){
				$data = $this->Mainmodel->get_row_data("pemilik", "pem_no_hp_wa='".$phone."' AND pem_id!=".$rNum);
			}
			else{
				$data = $this->Mainmodel->get_row_data("pemilik", "pem_no_hp_wa='".$phone."'");
			}

			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'pemilik_old_password'){
			$rNum			= $this->input->get('rNum');
			$old_password	= $this->input->get('old_password');
			if($rNum > 0){
				$data = $this->Mainmodel->get_row_data("pemilik", "pem_password = crypt('".$old_password."',\"pem_password\") AND pem_id =".$rNum);
			}

			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'pegawai_old_password'){
			$rNum			= $this->input->get('rNum');
			$old_password	= $this->input->get('old_password');
			if($rNum > 0){
				$data = $this->Mainmodel->get_row_data("pegawai", "peg_passwd = crypt('".$old_password."',\"peg_passwd\") AND peg_id =".$rNum);
			}

			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'umur_bulan'){
			$d1	= $this->input->get('d1');
			$d2	= $this->input->get('d2');
			if(!empty($d1) && !empty($d2)){
				$sql	= "	SELECT get_bulan ('".$d1."','".$d2."') AS umur;";
				$rhQ	= $this->db->query($sql);
				$rrQ	= $rhQ->row();
				$response = array('data' => $rrQ);
				echo json_encode($response);
				exit;
			}
		}
		elseif($mode == 'get_tanggal'){
			$tahun	= (int) $this->input->get('tahun');
			$bulan	= (int) $this->input->get('bulan');
			
			$sql	= "	SELECT (now() - INTERVAL '".$tahun." YEAR' - INTERVAL '".$bulan." MONTH')::date AS tanggal_lahir";
			$rhQ	= $this->db->query($sql);
			$rrQ	= $rhQ->row();
			$response = array('data' => $rrQ);
			echo json_encode($response);
			exit;

		}
		elseif($mode == 'get_rm_daily'){
			$rm_id		= $this->input->get('rm_id');
			$tgl_check	= $this->input->get('tgl_check');
			
			$sql	= "	SELECT * FROM rekam_medis_daily WHERE rmd_tanggal='".$tgl_check."' AND rmd_rm_id=".$rm_id;
			$rhQ	= $this->db->query($sql);
			$rrQ	= $rhQ->row();
			$response = array('data' => $rrQ);
			echo json_encode($response);
			exit;

		}
	}

}
