<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        if (!$this->session->userdata('userlogin')) {
            $this->session->set_userdata('urlback', $this->uri->uri_string());
            redirect('loginauth/login');
        }

        $this->load->model('Mainmodel');
		$peg_id	= $this->session->userdata('peg_id');
		$this->rowPgw = $this->Mainmodel->get_row_data("v_pegawai","peg_id=".$peg_id);
    }

    public function index() {
		$this->dashboard_clinic();
    }

    function dashboard_clinic() {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		if($cntrl_menu == '/' || $cntrl_menu == 'main/' || $cntrl_menu == 'Main/'){
			$cntrl_menu = 'main/dashboard_clinic';
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_id']			= $peg_id;
		$this->load->view('layout', $data);
	}

    function dashboard_vetlab() {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_id']			= $peg_id;
		$this->load->view('layout', $data);
	}

    function dashboard_petcare() {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_id']			= $peg_id;
		$this->load->view('layout', $data);
	}

    function dashboard_finance() {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_id']			= $peg_id;
		$this->load->view('layout', $data);
	}

    function info_clinic() {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		if($cntrl_menu == '/' ||$cntrl_menu == 'main/'){
			$cntrl_menu = 'main/info_clinic';
		}
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_id']			= $peg_id;
		$this->load->view('layout', $data);
	}
	
	function profile($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$peg_id		= $this->session->userdata('peg_id');

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, 0, 0, 0);
		$data['kt_notification']= $this->Mainmodel->kt_notification($peg_id);
		$data['main_title']     = 'MAIN';
		$data['title']          = 'UPDATE PROFILE';
        $data['main_content']   = 'main/profile';
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['peg_id']			= $peg_id;

		$query_rnum		= "	SELECT * FROM v_pegawai WHERE peg_id=".$peg_id;
		$rhNum			= $this->db->query($query_rnum);
		$rrNum			= $rhNum->row();
		$peg_nama		= $rrNum->peg_nama;
		$peg_username	= $rrNum->peg_username;
		$peg_no_hp_wa	= $rrNum->peg_no_hp_wa;
		$peg_email		= $rrNum->peg_email;
		$peg_jabatan		= $rrNum->peg_jabatan;
		$peg_tempat_lahir	= $rrNum->peg_tempat_lahir;
		$peg_tanggal_lahir	= $rrNum->peg_tanggal_lahir;
		$peg_gender		= $rrNum->peg_gender;
		$peg_alamat		= $rrNum->peg_alamat;
		$peg_aktif		= $rrNum->peg_aktif;
		$peg_kecamatan	= $rrNum->ref_kcmtn_id;
		$peg_kota		= $rrNum->ref_kota_id;
		$peg_foto		= 'assets/file/foto_pegawai/'.$rrNum->peg_foto;
		$peg_propinsi	= $rrNum->ref_prop_id;
		if(empty($peg_propinsi)){
			$peg_propinsi = 0;
		}
		if(empty($peg_kota)){
			$peg_kota = 0;
		}
		$clauseComboKota	= ' AND ref_kota_id_prop='.$peg_propinsi;
		$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$peg_kota;

		if($peg_gender=='P'){
			$checkedPria	= 'checked="checked"';
			$checkedWanita	= '';
		}
		else{
			$checkedPria	= '';
			$checkedWanita	= 'checked="checked"';
		}

		if($mode == 'crud'){
			$action	= $this->input->post('action_crud');
			$rNum	= $this->input->post('peg_id');
			$data = array(
				'peg_nama'			=> "'".$this->input->post('inp_nama')."'",
				'peg_username'		=> "'".$this->input->post('inp_username')."'",
				'peg_no_hp_wa'		=> "'".$this->input->post('inp_hp')."'",
				'peg_email'			=> "'".$this->input->post('inp_email')."'",
				'peg_tempat_lahir'	=> "'".$this->input->post('inp_tmpt_lhr')."'",
				'peg_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lhr')."'",
				'peg_gender'		=> "'".$this->input->post('inp_jk')."'",
				'peg_alamat'		=> "'".$this->input->post('inp_almt')."'",
				'peg_kecamatan'		=> $this->input->post('inp_kcmtn'),
			);
			$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);

            $config['upload_path'] = './assets/file/foto_pegawai/';
            $config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']		= 1024;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('profile_avatar')){
				$fotoData	= $this->upload->data();
				$fotoName	= $fotoData['file_name'];
				$data = array(
					'peg_foto'			=> "'".pg_escape_string($fotoName)."'",
				);
				$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);
			}

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'changepass'){
			$action	= $this->input->post('action_crud_change');
			$rNum	= $this->input->post('peg_id_change');
			$data = array(
				'peg_passwd'	=> "(SELECT mk_passwd('".$this->input->post('reg_password')."'))"
			);
			$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$data['peg_id']			= $peg_id;
		$data['peg_nama']		= $peg_nama;
		$data['peg_username']	= $peg_username;
		$data['peg_no_hp_wa']	= $peg_no_hp_wa;
		$data['peg_email']		= $peg_email;
		$data['peg_alamat']		= $peg_alamat;
		$data['peg_tempat_lahir']	= $peg_tempat_lahir;
		$data['peg_tanggal_lahir']	= $peg_tanggal_lahir;
		$data['peg_email']		= $peg_email;
		$data['peg_foto']		= $peg_foto;
		$data['peg_aktif']		= $peg_aktif;
		$data['checkedPria']	= $checkedPria;
		$data['checkedWanita']	= $checkedWanita;
		$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $peg_propinsi);
		$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $peg_kota);
		$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $peg_kecamatan);

		$this->load->view('layout', $data);
	}

    function telemedicine_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,3,4,8) ORDER BY rm_status_id");
		$this->load->view('main/telemedicine_stat', $data);
	}

    function pemeriksaan_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,3,4,5,6,7,8) ORDER BY rm_status_id");
		$this->load->view('main/pemeriksaan_stat', $data);
	}

    function vaksin_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,3,4,5,6,7,8) ORDER BY rm_status_id");
		$this->load->view('main/vaksin_stat', $data);
	}
	
    function steril_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,3,4,5,6,7,8) ORDER BY rm_status_id");
		$this->load->view('main/steril_stat', $data);
	}

    function grooming_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_urutan_grooming IS NOT NULL ORDER BY rm_status_urutan_grooming");
		$this->load->view('main/grooming_stat', $data);
	}

    function cathotel_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,113,114, 8) ORDER BY rm_status_urutan_cathotel");
		$this->load->view('main/cathotel_stat', $data);
	}

    function homeservice_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM home_service_status WHERE hs_status_id IN (1,2,3,4,6) ORDER BY hs_status_id");
		$this->load->view('main/homeservice_stat', $data);
	}
	
    function rontgen_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,8) ORDER BY rm_status_id");
		$this->load->view('main/rontgen_stat', $data);
	}

    function usg_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,8) ORDER BY rm_status_id");
		$this->load->view('main/usg_stat', $data);
	}

	function kimdar_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,8) ORDER BY rm_status_id");
		$this->load->view('main/kimdar_stat', $data);
	}

	function hema_stat($mode) {
		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['query_hd']		= $this->db->query("SELECT * FROM rekam_medis_status WHERE rm_status_id IN (1,2,8) ORDER BY rm_status_id");
		$this->load->view('main/hema_stat', $data);
	}

	function new_pets($mode) {
		$data['current_url']	= current_url();
		$whereClause = '';
		if($mode=='month'){
			$year	= date('Y');
			$month	= date('m');
			$whereClause	= " AND date_part('year', pas_time_create) = ".$year." AND date_part('month', pas_time_create) = ".$month." ";
		}
		else{
			$whereClause	= " AND cast(pas_time_create as date)='now()'";
		}
		$peg_id		= $this->session->userdata('peg_id');
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['query_pasien']	= $this->db->query("SELECT * FROM v_pasien WHERE pas_id > 0 ".$whereClause." ORDER BY pas_id desc LIMIT 50");
		$this->load->view('main/new_pets', $data);
	}

    function rajal_finance_stat($mode) {
		if($mode == 'month'){
			$year	= date('Y');
			$month	= date('m');
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND inv_jenis = 1 AND inv_status = 2");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND inv_jenis = 1 AND inv_status = 2");
			
			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND pkb_asal_terima = 1 AND pkb_flag_proses IS TRUE");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND pkb_asal_terima = 1 AND pkb_flag_proses IS TRUE");
			$label_color	= "label-light-info";
		}
		else{
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND inv_jenis = 1 AND inv_status = 2");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND inv_jenis = 1 AND inv_status = 2");

			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND pkb_asal_terima = 1 AND pkb_flag_proses IS TRUE");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND pkb_asal_terima = 1 AND pkb_flag_proses IS TRUE");
			$label_color	= "label-light-success";
		}


		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['countInvoice']	= $countInvoice;
		$data['sumInvoice']		= $sumInvoice;
		$data['countPenerimaan']= $countPenerimaan;
		$data['sumPenerimaan']	= $sumPenerimaan;
		$data['label_color']	= $label_color;

		$this->load->view('main/rajal_finance_stat', $data);
	}

    function ranap_finance_stat($mode) {
		if($mode == 'month'){
			$year	= date('Y');
			$month	= date('m');
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_tagihan_ranap", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND inv_jenis = 2 AND inv_status = 2");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto_ri-inv_diskon_nominal)", "v_invoice_tagihan_ranap", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND inv_jenis = 2 AND inv_status = 2");
			
			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND pkb_asal_terima = 2 AND pkb_flag_proses IS TRUE");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND pkb_asal_terima = 2 AND pkb_flag_proses IS TRUE");
			$label_color	= "label-light-info";
		}
		else{
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_tagihan_ranap", "cast(inv_tanggal as date)='now()' AND inv_jenis = 2 AND inv_status = 2");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto_ri-inv_diskon_nominal)", "v_invoice_tagihan_ranap", "cast(inv_tanggal as date)='now()' AND inv_jenis = 2 AND inv_status = 2");

			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND pkb_asal_terima = 2 AND pkb_flag_proses IS TRUE");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND pkb_asal_terima = 2 AND pkb_flag_proses IS TRUE");
			$label_color	= "label-light-success";
		}


		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['countInvoice']	= $countInvoice;
		$data['sumInvoice']		= $sumInvoice;
		$data['countPenerimaan']= $countPenerimaan;
		$data['sumPenerimaan']	= $sumPenerimaan;
		$data['label_color']	= $label_color;

		$this->load->view('main/ranap_finance_stat', $data);
	}

    function pemeriksaan_finance_stat($mode) {
		if($mode == 'month'){
			$year	= date('Y');
			$month	= date('m');
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=128");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=128");
			
			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=128");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=128");
			$label_color	= "label-light-info";
		}
		else{
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=128");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=128");

			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=128");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=128");
			$label_color	= "label-light-success";
		}


		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['countInvoice']	= $countInvoice;
		$data['sumInvoice']		= $sumInvoice;
		$data['countPenerimaan']= $countPenerimaan;
		$data['sumPenerimaan']	= $sumPenerimaan;
		$data['label_color']	= $label_color;

		$this->load->view('main/pemeriksaan_finance_stat', $data);
	}

    function vaksin_finance_stat($mode) {
		if($mode == 'month'){
			$year	= date('Y');
			$month	= date('m');
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=127");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=127");
			
			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=127");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=127");
			$label_color	= "label-light-info";
		}
		else{
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=127");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=127");

			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=127");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=127");
			$label_color	= "label-light-success";
		}


		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['countInvoice']	= $countInvoice;
		$data['sumInvoice']		= $sumInvoice;
		$data['countPenerimaan']= $countPenerimaan;
		$data['sumPenerimaan']	= $sumPenerimaan;
		$data['label_color']	= $label_color;

		$this->load->view('main/vaksin_finance_stat', $data);
	}

    function steril_non_kom_finance_stat($mode) {
		if($mode == 'month'){
			$year	= date('Y');
			$month	= date('m');
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			
			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			$label_color	= "label-light-info";
		}
		else{
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");

			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas=0");
			$label_color	= "label-light-success";
		}


		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['countInvoice']	= $countInvoice;
		$data['sumInvoice']		= $sumInvoice;
		$data['countPenerimaan']= $countPenerimaan;
		$data['sumPenerimaan']	= $sumPenerimaan;
		$data['label_color']	= $label_color;

		$this->load->view('main/steril_non_kom_finance_stat', $data);
	}

    function steril_komunitas_finance_stat($mode) {
		if($mode == 'month'){
			$year	= date('Y');
			$month	= date('m');
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "date_part('year', inv_tanggal) = ".$year." AND date_part('month', inv_tanggal) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			
			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "date_part('year', pkb_tgl_terima) = ".$year." AND date_part('month', pkb_tgl_terima) = ".$month." AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			$label_color	= "label-light-info";
		}
		else{
			$countInvoice	= $this->Mainmodel->get_count_data("v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			$sumInvoice		= $this->Mainmodel->get_one_data("SUM(total_netto-inv_diskon_nominal)", "v_invoice_rekam_medis", "cast(inv_tanggal as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");

			$countPenerimaan= $this->Mainmodel->get_count_data("v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			$sumPenerimaan	= $this->Mainmodel->get_one_data("SUM(pkb_jumlah)", "v_penerimaan_kas_bank", "cast(pkb_tgl_terima as date)='now()' AND rm_asal = 1 AND rm_perawatan=129 AND rm_komunitas >0");
			$label_color	= "label-light-success";
		}


		$peg_id		= $this->session->userdata('peg_id');
		$peg_jabatan= $this->session->userdata('peg_jabatan');
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['mode']			= $mode;
		$data['peg_id']			= $peg_id;
		$data['peg_jabatan']	= $peg_jabatan;
		$data['countInvoice']	= $countInvoice;
		$data['sumInvoice']		= $sumInvoice;
		$data['countPenerimaan']= $countPenerimaan;
		$data['sumPenerimaan']	= $sumPenerimaan;
		$data['label_color']	= $label_color;

		$this->load->view('main/steril_komunitas_finance_stat', $data);
	}

}
