<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Master extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        if (!$this->session->userdata('userlogin')) {
			($this->input->get('rNum') > 0) ? $url = $this->uri->uri_string().'/?rNum='.$this->input->get('rNum') : $url = $this->uri->uri_string();
            $this->session->set_userdata('urlback', $url);
            redirect('loginauth/login');
        }
        $this->load->model('Mainmodel');
		$this->load->library('user_agent');
		$this->load->helper('form');
		$peg_id	= $this->session->userdata('peg_id');
		$this->rowPgw = $this->Mainmodel->get_row_data("v_pegawai","peg_id=".$peg_id);
    }

    public function index() {
		$this->dashboard();
    }

    function pegawai($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$rNum3		= $this->input->get('rNum3');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;
		$data['rNum3']			= $rNum3;

		$peg_nama		= '';
		$peg_username	= '';
		$peg_no_hp_wa	= '';
		$peg_email		= '';
		$peg_jabatan	= 0;
		$peg_tempat_lahir	= '';
		$peg_tanggal_lahir	= '';
		$peg_gender		= 'P';
		$peg_alamat		= '';
		$peg_aktif		= '';
		$peg_kecamatan	= 0;
		$peg_kota		= 0;
		$peg_propinsi	= 0;
		$peg_organisasi	= 0;
		$peg_cabang		= 0;
		$peg_foto		= "assets/file/foto_pegawai/001-boy.svg";
		$clauseComboKota	= ' AND ref_kota_id_prop=0';
		$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota=0';
		$clauseComboCabang	= ' AND ref_cab_id=0';
		$checkedPria	= '';
		$checkedWanita	= '';
		$orderByrNum	= " ORDER BY peg_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_pegawai WHERE peg_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$peg_nama		= $rrNum->peg_nama;
			$peg_username	= $rrNum->peg_username;
			$peg_no_hp_wa	= $rrNum->peg_no_hp_wa;
			$peg_email		= $rrNum->peg_email;
			$peg_jabatan		= $rrNum->peg_jabatan;
			$peg_tempat_lahir	= $rrNum->peg_tempat_lahir;
			$peg_tanggal_lahir	= $rrNum->peg_tanggal_lahir;
			$peg_gender		= $rrNum->peg_gender;
			$peg_alamat		= $rrNum->peg_alamat;
			$peg_aktif		= $rrNum->peg_aktif;
			$peg_propinsi	= $rrNum->ref_prop_id;
			$peg_kota		= $rrNum->ref_kota_id;
			$peg_kecamatan	= $rrNum->peg_kecamatan;
			$peg_organisasi	= $rrNum->org_id;
			$peg_cabang		= $rrNum->peg_cabang;
			
			$peg_foto		= 'assets/file/foto_pegawai/'.$rrNum->peg_foto;
			if(empty($peg_propinsi)){
				$peg_propinsi = 0;
			}
			if(empty($peg_kota)){
				$peg_kota = 0;
			}
			$clauseComboKota	= ' AND ref_kota_id_prop='.$peg_propinsi;
			$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$peg_kota;
			$clauseComboCabang	= ' AND ref_cab_organisasi='.$peg_organisasi;
			$orderByrNum		= " ORDER BY peg_id=".$rNum." desc";
			$data['query_dt']		= $this->db->query("SELECT * FROM v_grup_menu_pegawai WHERE ref_mn_grp_parent_id IS NULL AND ref_mn_peg_peg_id =".$rNum." ORDER BY ref_mn_peg_no_urut");
			$data['query_dinilai']	= $this->db->query("SELECT * FROM v_pegawai_penilaian_performance WHERE peg_nilai_perform_peg_penilai =".$rNum." ORDER BY peg_dinilai");
		}

		if($peg_gender=='P'){
			$checkedPria	= 'checked="checked"';
		}
		else{
			$checkedWanita	= 'checked="checked"';
		}

		if($mode == 'form'){
			$data['main_content']   = 'master/pegawaiform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
            $config['upload_path'] = './assets/file/foto_pegawai/';
            $config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']		= 1024;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

			$data = array(
				'peg_nama'			=> "'".$this->input->post('inp_nama')."'",
				'peg_username'		=> "'".$this->input->post('inp_username')."'",
				'peg_no_hp_wa'		=> "'".$this->input->post('inp_hp')."'",
				'peg_email'			=> "'".$this->input->post('inp_email')."'",
				'peg_tempat_lahir'	=> "'".$this->input->post('inp_tmpt_lhr')."'",
				'peg_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lhr')."'",
				'peg_gender'		=> "'".$this->input->post('inp_jk')."'",
				'peg_alamat'		=> "'".$this->input->post('inp_almt')."'",
				'peg_kecamatan'		=> $this->input->post('inp_kcmtn'),
				'peg_cabang'		=> $this->input->post('inp_cabang'),
				'peg_jabatan'		=> $this->input->post('inp_jabatan'),
			);

			if($this->upload->do_upload('profile_avatar')){
				$fotoData	= $this->upload->data();
				$fotoName	= $fotoData['file_name'];
				$data += ['peg_foto' => "'".pg_escape_string($fotoName)."'"];
			}

			if($rNum > 0){
				$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);
			}
			else{
				$data += ['peg_passwd' => "(SELECT mk_passwd('leo'))"];
				$rNum	= $this->Mainmodel->insert_table('pegawai', $data);

				$data_menu = array(
					'ref_mn_peg_peg_id'			=> $rNum,
					'ref_mn_peg_grup_menu_id'	=> 38,
				);
				 $this->Mainmodel->insert_table('ref_menu_pegawai', $data_menu);

				$data_menu = array(
					'ref_mn_peg_peg_id'			=> $rNum,
					'ref_mn_peg_grup_menu_id'	=> 39,
				);
				 $this->Mainmodel->insert_table('ref_menu_pegawai', $data_menu);

			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'resetpass'){
			$data = array(
				'peg_passwd'	=> "(SELECT mk_passwd('leo'))"
			);
			$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'disabled'){
			$data = array(
				'peg_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'peg_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('pegawai', 'peg_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_menu'){
			$data = array(
				'ref_mn_peg_peg_id'			=> $rNum,
				'ref_mn_peg_grup_menu_id'	=> $this->input->post('inp_grup_menu'),
				'ref_mn_peg_no_urut'		=> $this->input->post('inp_urut'),
			);	
			$this->Mainmodel->insert_table('ref_menu_pegawai', $data);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete_grupmenu'){
			$this->Mainmodel->delete_table('ref_menu_pegawai', 'ref_mn_peg_mn_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'crud_dinilai'){
			$data = array(
				'peg_nilai_perform_peg_penilai'	=> $rNum,
				'peg_nilai_perform_peg_dinilai'	=> $this->input->post('inp_peg_dinilai'),
			);	
			$this->Mainmodel->insert_table('pegawai_penilaian_performance', $data);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete_dinilai'){
			$this->Mainmodel->delete_table('pegawai_penilaian_performance', 'peg_nilai_perform_id='.$rNum3);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}

		$data['peg_nama']		= $peg_nama;
		$data['peg_username']	= $peg_username;
		$data['peg_no_hp_wa']	= $peg_no_hp_wa;
		$data['peg_email']		= $peg_email;
		$data['peg_alamat']		= $peg_alamat;
		$data['peg_tempat_lahir']	= $peg_tempat_lahir;
		$data['peg_tanggal_lahir']	= $peg_tanggal_lahir;
		$data['peg_email']		= $peg_email;
		$data['peg_foto']		= $peg_foto;
		$data['peg_aktif']		= $peg_aktif;
		$data['checkedPria']	= $checkedPria;
		$data['checkedWanita']	= $checkedWanita;
		$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $peg_propinsi);
		$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $peg_kota);
		$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $peg_kecamatan);
		$data['comboOrganisasi']= $this->Mainmodel->show_combo("organisasi", "org_id", "org_nama", "org_id > 0", "org_id", $peg_organisasi);
		$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_id > 0", "ref_cab_id", $peg_cabang);
		$data['comboJabatan']	= $this->Mainmodel->show_combo("ref_jabatan", "ref_jab_id", "ref_jab_ket", "ref_jab_id > 0", "ref_jab_id", $peg_jabatan);
		$data['query_hd']		= $this->db->query("SELECT * FROM v_pegawai WHERE peg_id > 0".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function modal_grup_menu($mode='') {
		$rNum			= $this->input->get('rNum');
		$comboMenu		= $this->Mainmodel->show_combo("v_ref_menu_grup", "ref_mn_grp_id", "nama_grup_menu", "ref_mn_grp_id NOT IN (SELECT ref_mn_peg_grup_menu_id FROM ref_menu_pegawai WHERE ref_mn_peg_peg_id=".$rNum.")", "ref_mn_grp_parent_id desc,ref_mn_grp_judul", 0);
		
		echo '	<input type="hidden" class="form-control" name="rNum" id="rNum" value="'.$rNum.'">
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Grup Menu:</label>
							<select class="form-control select2" name="inp_grup_menu"  id="inp_grup_menu" required >
								<option label="Label"></option>
								'.$comboMenu.'
							</select>
						</div>
						<div class="form-group">
							<label>Urutan:</label>
							<input type="number" class="form-control text-right" placeholder="Isi Urutan" name="inp_urut"  id="inp_urut" value="" required/>
						</div>
					</div>
				</div>';
	}

    function modal_peg_dinilai($mode='') {
		$rNum			= $this->input->get('rNum');
		$rNum2			= $this->input->get('rNum2');
		$comboPegawai	= $this->Mainmodel->show_combo("pegawai", "peg_id", "peg_nama", "peg_aktif IS TRUE", "peg_nama", $rNum2);
		
		echo '	<input type="hidden" class="form-control" name="rNum" id="rNum" value="'.$rNum.'">
				<input type="hidden" class="form-control" name="rNum2" id="rNum2" value="'.$rNum2.'">
				<div class="modal-body">
					<div class="card-body">
						<div class="form-group">
							<label>Peg. Dinilai:</label>
							<select class="form-control select2" name="inp_peg_dinilai"  id="inp_peg_dinilai" required >
								<option label="Label"></option>
								'.$comboPegawai.'
							</select>
						</div>
					</div>
				</div>';
	}

    function pasien($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
 		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;
		
		$pas_mrn		= '';
		$pas_nama		= '';
		$pas_gender		= 'J';
		$pas_pemilik_utama	= 0;
		$pas_spesies	= 0;
		$pas_jenis		= 1;
		$pas_ras		= 0;
		$pas_warna		= 0;
		$pas_tanda_khusus	= 0;
		$pas_last_vaksin= '';
		$pas_aktif		= '';
		$pas_tanggal_lahir= '';
		$pas_umur_tahun	= 0;
		$pas_umur_bulan	= 0;
		$pas_berat		= 0;
		$pas_foto		= "assets/file/foto/unknown.jpg";
		$checkedJantan	= '';
		$checkedBetina	= '';
		$checkedRas		= '';
		$checkedDomestik= '';
		$clauserNum		= ' AND pas_pem_pasien=0';
		$readonly_mrn	= 'readonly';
		if($rNum > 0){
			$query_rnum	= "	SELECT * FROM v_pasien WHERE pas_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$pas_mrn		= $rrNum->pas_mrn;
			$pas_nama		= $rrNum->pas_nama;
			$pas_tanggal_lahir	= $rrNum->pas_tanggal_lahir;
			$pas_umur_tahun	= $rrNum->pas_umur_tahun;
			$pas_umur_bulan	= $rrNum->pas_umur_bulan;
			$pas_berat		= $rrNum->pas_berat;
			$pas_gender		= $rrNum->pas_gender;
			$pas_pemilik_utama	= $rrNum->pas_pemilik_utama;
			$pas_spesies	= $rrNum->pas_spesies;
			$pas_jenis		= $rrNum->pas_jenis;
			$pas_ras		= $rrNum->pas_ras;
			$pas_warna		= $rrNum->pas_warna;
			$pas_tanda_khusus	= $rrNum->pas_tanda_khusus;
			$pas_last_vaksin= $rrNum->pas_last_vaksin;
			$pas_foto		= 'assets/file/foto/'.$rrNum->pas_foto;
			$pas_aktif		= $rrNum->pas_aktif;
			$clauserNum		= ' AND pas_pem_pasien='.$rNum;

			$pem_nama		= '';
			$pem_no_hp_wa	= '';
			$pem_no_hp_backup	= '';
			$pem_nik		= '';
			$pem_email		= '';
			$pem_tempat_lahir	= '';
			$pem_tanggal_lahir	= '';
			$pem_gender		= 'P';
			$pem_asal		= 1;
			$pem_alamat		= '';
			$pem_aktif		= '';
			$pem_kecamatan	= 0;
			$pem_kota		= 0;
			$pem_propinsi	= 0;
			$clauseComboKota	= ' AND ref_kota_id_prop=0';
			$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota=0';
			$checkedPria	= '';
			$checkedWanita	= '';
			$checkedNonKom	= '';
			$checkedKom		= '';
			if($rNum2 > 0){
				$query_rnum2		= "	SELECT * FROM v_pemilik WHERE pem_id=".$rNum2;
				$rhNum2			= $this->db->query($query_rnum2);
				$rrNum2			= $rhNum2->row();
				$pem_nama		= $rrNum2->pem_nama;
				$pem_no_hp_wa	= $rrNum2->pem_no_hp_wa;
				$pem_no_hp_backup	= $rrNum2->pem_no_hp_backup;
				$pem_nik		= $rrNum2->pem_nik;
				$pem_email		= $rrNum2->pem_email;
				$pem_tempat_lahir	= $rrNum2->pem_tempat_lahir;
				$pem_tanggal_lahir	= $rrNum2->pem_tanggal_lahir;
				$pem_gender		= $rrNum2->pem_gender;
				$pem_asal		= $rrNum2->pem_asal;
				$pem_alamat		= $rrNum2->pem_alamat;
				$pem_kecamatan	= $rrNum2->ref_kcmtn_id;
				$pem_kota		= $rrNum2->ref_kota_id;
				$pem_propinsi	= $rrNum2->ref_prop_id;
				$pem_aktif		= $rrNum2->pem_aktif;
				$clauseComboKota	= ' AND ref_kota_id_prop='.$pem_propinsi;
				$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$pem_kota;
			}

			if($pem_gender=='P'){
				$checkedPria	= 'checked="checked"';
			}
			else{
				$checkedWanita	= 'checked="checked"';
			}

			if($pem_asal==1){
				$checkedNonKom	= 'checked="checked"';
			}
			else{
				$checkedKom	= 'checked="checked"';
			}
			$data['pem_nama']		= $pem_nama;
			$data['pem_no_hp_wa']	= $pem_no_hp_wa;
			$data['pem_no_hp_backup']	= $pem_no_hp_backup;
			$data['pem_nik']		= $pem_nik;
			$data['pem_alamat']		= $pem_alamat;
			$data['pem_tempat_lahir']	= $pem_tempat_lahir;
			$data['pem_tanggal_lahir']	= $pem_tanggal_lahir;
			$data['pem_email']		= $pem_email;
			$data['pem_aktif']		= $pem_aktif;
			$data['checkedPria']	= $checkedPria;
			$data['checkedWanita']	= $checkedWanita;
			$data['checkedNonKom']	= $checkedNonKom;
			$data['checkedKom']		= $checkedKom;
			$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $pem_propinsi);
			$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $pem_kota);
			$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $pem_kecamatan);
			$whereClause	= " AND pas_id=".$rNum;
		}
		else{
			if(!empty($hSQL)){
				$whereClause	= base64_decode($hSQL);
			}
			else{
				$whereClause	= " AND pas_aktif IS TRUE";
			}

		}
		if($pas_gender=='J'){
			$checkedJantan	= 'checked="checked"';
		}
		else{
			$checkedBetina	= 'checked="checked"';
		}

		if($pas_jenis==1){
			$checkedRas	= 'checked="checked"';
		}
		else{
			$checkedDomestik	= 'checked="checked"';
		}

		if($mode == 'form_pasien'){
			$data['pas_mrn']		= $pas_mrn;
			$data['pas_nama']		= $pas_nama;
			$data['checkedJantan']	= $checkedJantan;
			$data['checkedBetina']	= $checkedBetina;
			$data['checkedRas']		= $checkedRas;
			$data['checkedDomestik']= $checkedDomestik;
			$data['comboPemilik']	= $this->Mainmodel->show_combo("pemilik", "pem_id", "pem_nama", "pem_id > 0", "pem_nama", $pas_pemilik_utama);
			$data['comboSpesies']	= $this->Mainmodel->show_combo("ref_spesies", "ref_spesies_id", "ref_spesies_nama", "ref_spesies_id > 0", "ref_spesies_nama", $pas_spesies);
			$data['comboRas']		= $this->Mainmodel->show_combo("ref_ras", "ref_ras_id", "ref_ras_nama", "ref_ras_id > 0", "ref_ras_nama", $pas_ras);
			$data['comboWarna']		= $this->Mainmodel->show_combo("ref_warna", "ref_warna_id", "ref_warna_ket", "ref_warna_id > 0", "ref_warna_ket", $pas_warna);
			$data['comboTndKhusus']	= $this->Mainmodel->show_combo("ref_tanda_khusus", "ref_tnd_khss_id", "ref_tnd_khss_ket", "ref_tnd_khss_id > 0", "ref_tnd_khss_ket", $pas_tanda_khusus);
			$data['pas_last_vaksin']= $pas_last_vaksin;
			$data['pas_mrn']		= $pas_mrn;
			$data['pas_tanggal_lahir']	= $pas_tanggal_lahir;
			$data['pas_umur_tahun']	= $pas_umur_tahun;
			$data['pas_umur_bulan']	= $pas_umur_bulan;
			$data['pas_berat']		= $pas_berat;
			$data['pas_foto']		= $pas_foto;		
			$data['readonly_mrn']	= $readonly_mrn;		
			$data['main_content']   = 'master/pasienform';
		}
		elseif($mode == 'crud_pasien'){
			$action		= $this->input->post('action_crud');
            $config['upload_path'] = './assets/file/foto/';
            $config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']		= 1024;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if(empty($this->input->post('inp_ras'))){
				$inp_ras = 'null';
			}
			else{
				$inp_ras = $this->input->post('inp_ras');
			}
			
			if(empty($this->input->post('inp_warna'))){
				$inp_warna = 'null';
			}
			else{
				$inp_warna = $this->input->post('inp_warna');
			}

			if(empty($this->input->post('inp_tanda_khusus'))){
				$inp_tanda = 'null';
			}
			else{
				$inp_tanda = $this->input->post('inp_tanda_khusus');
			}
 
			if(empty($this->input->post('inp_last_vaksin'))){
				$inp_last_vaksin = 'null';
			}
			else{
				$inp_last_vaksin = "'".$this->input->post('inp_last_vaksin')."'";
			}

			if($this->upload->do_upload('profile_avatar')){
				$fotoData	= $this->upload->data();
				$fotoName	= $fotoData['file_name'];
				$data = array(
					'pas_pemilik_utama'	=> $this->input->post('inp_pemilik'),
					'pas_nama'		=> "'".pg_escape_string($this->input->post('inp_nama'))."'",
					'pas_gender'	=> "'".$this->input->post('inp_jk')."'",
					'pas_spesies'	=> "'".$this->input->post('inp_spesies')."'",
					'pas_jenis'		=> $this->input->post('inp_jns'),
					'pas_warna'		=> $inp_warna,
					'pas_ras'		=> $inp_ras,
					'pas_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lahir')."'",
					'pas_umur_tahun'=> "'".$this->input->post('inp_umur_tahun')."'",
					'pas_umur_bulan'=> "'".$this->input->post('inp_umur_bulan')."'",
					'pas_berat'		=> "'".$this->input->post('inp_berat')."'",
					'pas_foto'		=> "'".pg_escape_string($fotoName)."'",
					'pas_tanda_khusus'	=> $inp_tanda,
					'pas_last_vaksin'	=> $inp_last_vaksin,
				);				
			}
			else{
				$data = array(
					'pas_pemilik_utama'	=> $this->input->post('inp_pemilik'),
					'pas_nama'		=> "'".pg_escape_string($this->input->post('inp_nama'))."'",
					'pas_gender'	=> "'".$this->input->post('inp_jk')."'",
					'pas_spesies'	=> "'".$this->input->post('inp_spesies')."'",
					'pas_jenis'		=> $this->input->post('inp_jns'),
					'pas_warna'		=> $inp_warna,
					'pas_ras'		=> $inp_ras,
					'pas_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lahir')."'",
					'pas_umur_tahun'=> "'".$this->input->post('inp_umur_tahun')."'",
					'pas_umur_bulan'=> "'".$this->input->post('inp_umur_bulan')."'",
					'pas_berat'		=> "'".$this->input->post('inp_berat')."'",
					'pas_tanda_khusus'	=> $inp_tanda,
					'pas_last_vaksin'	=> $inp_last_vaksin,
				);				
			}

			if($rNum > 0){
				$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$rNum);
				
				/*
				if(!empty($this->input->post('inp_nama'))){
					if($pas_mrn <> trim($this->input->post('inp_nama'))){
						$data = array(
							'pas_mrn'	=> "'".pg_escape_string($this->input->post('inp_mrn'))."'",
						);				
						$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$rNum);
					}
				}
				*/
				if($pas_pemilik_utama <> $this->input->post('inp_pemilik')){
					$this->Mainmodel->delete_table('pasien_pemilik', 'pas_pem_pasien='.$rNum.' AND pas_pem_pemilik='.$pas_pemilik_utama);
					$data_pem = array(
						'pas_pem_pasien'	=> $rNum,
						'pas_pem_pemilik'	=> $this->input->post('inp_pemilik')
					);		
					$this->Mainmodel->insert_table('pasien_pemilik', $data_pem);
				}
			}
			else{
				$rNum		= $this->Mainmodel->insert_table('pasien', $data);
				$data_pem = array(
					'pas_pem_pasien'	=> $rNum,
					'pas_pem_pemilik'	=> $this->input->post('inp_pemilik')
				);				
				$this->Mainmodel->insert_table('pasien_pemilik', $data_pem);
				
				$rrPemilik	= $this->Mainmodel->get_row_data("pemilik", "pem_id =".$this->input->post('inp_pemilik'));
				$lastNum	= $this->Mainmodel->get_mrn();
				$hurufAwalNamaPemilik	= substr($rrPemilik->pem_nama,0,1);
				$hurufAwalNamaHewan		= substr($this->input->post('inp_nama'),0,1);
				$noMrn					= $lastNum.".".$hurufAwalNamaHewan.".".$hurufAwalNamaPemilik.".".date('Y');
				$data_mrn= array(
					'pas_mrn'	=> "'".$noMrn."'"
				);				
				$this->Mainmodel->update_table('pasien', $data_mrn, 'pas_id='.$rNum);

			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pasien/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pasien/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'disabled_pasien'){
			$data = array(
				'pas_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled_pasien'){
			$data = array(
				'pas_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete_pasien'){
			$this->Mainmodel->delete_table('pasien_pemilik', 'pas_pem_pasien='.$rNum);
			$this->Mainmodel->delete_table('pasien', 'pas_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'form_pemilik'){
			$data['var_pem_nama']		= $this->Mainmodel->encode_var_js("pemilik", "pem_id", "pem_nama", "pem_aktif IS TRUE AND pem_nama IS NOT NULL", "pem_nama");
			$data['var_pem_nik']		= $this->Mainmodel->encode_var_js("pemilik", "pem_id", "pem_nik", "pem_aktif IS TRUE AND pem_nik IS NOT NULL", "pem_nik");
			$data['var_pem_no_hp_wa']	= $this->Mainmodel->encode_var_js("pemilik", "pem_id", "pem_no_hp_wa", "pem_aktif IS TRUE AND pem_nik IS NOT NULL", "pem_no_hp_wa");
			$data['main_content']   = 'master/pemilikpasienform';
		}
		elseif($mode == 'crud_pemilik'){
			$action	= $this->input->post('action_crud');
			$pem_id	= $this->input->post('inp_pem_id');
			if($pem_id > 0){
				$rNum2 = $pem_id;
			}

			if(empty($this->input->post('inp_email'))){
				$pem_email = 'null';
			}
			else{
				$pem_email = "'".$this->input->post('inp_email')."'";
			}
			$data = array(
				'pem_nama'			=> "'".$this->input->post('inp_nama')."'",
				'pem_no_hp_wa'		=> "'".$this->input->post('inp_wa')."'",
				'pem_no_hp_backup'	=> "'".$this->input->post('inp_hp')."'",
				'pem_nik'			=> "'".$this->input->post('inp_nik')."'",
				'pem_email'			=> $pem_email,
				'pem_tempat_lahir'	=> "'".$this->input->post('inp_tmpt_lhr')."'",
				'pem_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lhr')."'",
				'pem_gender'		=> "'".$this->input->post('inp_jk')."'",
				'pem_asal'			=> "'".$this->input->post('inp_asal')."'",
				'pem_alamat'		=> "'".$this->input->post('inp_almt')."'",
				'pem_kecamatan'		=> $this->input->post('inp_kcmtn'),
			);

			if($rNum2 > 0){
				$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum2);
			}
			else{
				$data += ['pem_user_create' => $peg_id,'pem_password'	=> "(SELECT mk_passwd('leo'))"];
				$rNum2	= $this->Mainmodel->insert_table('pemilik', $data);
			}

			$rhExist	= $this->db->query('SELECT * FROM pasien_pemilik WHERE pas_pem_pasien='.$rNum.' AND pas_pem_pemilik='.$rNum2);
			$rrExist	= $rhExist->row();
			if(empty($rrExist->pas_pem_id)){
				$data = array(
					'pas_pem_pasien'		=> $rNum,
					'pas_pem_pemilik'		=> $rNum2,
				);
				$this->Mainmodel->insert_table('pasien_pemilik', $data);
			}

			if(empty($pas_pemilik_utama)){
				$data = array(
					'pas_pemilik_utama'	=> $rNum2
				);
				$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$rNum);			
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pemilik/?rNum='.$rNum.'&rNum2='.$rNum2);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pemilik/?rNum='.$rNum);
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
			}
		}
		elseif($mode == 'delete_pemilik_pasien'){
			$this->Mainmodel->delete_table('pasien_pemilik', 'pas_pem_pasien='.$rNum.' AND pas_pem_pemilik='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'get_data_mrn'){
			$mrn	= $this->input->get('mrn');
			$data	= $this->Mainmodel->get_row_data("pasien", "pas_mrn='".$mrn."' AND pas_id <>".$rNum);
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'get_data_nama'){
			$nama	= $this->input->get('nama');
			$data	= $this->Mainmodel->get_row_data("v_pemilik", "pem_nama='".$nama."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'get_data_nik'){
			$nik	= $this->input->get('nik');
			$data	= $this->Mainmodel->get_row_data("v_pemilik", "pem_nik='".$nik."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'get_data_nohp'){
			$nohp	= $this->input->get('nohp');
			$data	= $this->Mainmodel->get_row_data("v_pemilik", "pem_no_hp_wa='".$nohp."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		else{
			$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM v_pasien WHERE pas_id > 0'.$whereClause);
			$num_row				= $queryNumRow->row();
			$count_data				= $num_row->count_data;

			(empty($this->input->get('pn'))) ? $page_number = 1 : $page_number = $this->input->get('pn');
			(empty($this->input->get('ps'))) ? $page_size = 100 : $page_size = $this->input->get('ps');
			($page_number > 1) ? $offset = ($page_number-1)*$page_size : $offset = 0;
			
			$data['offset']			= $offset;
			$data['query_pasien']	= $this->db->query("SELECT * FROM v_pasien WHERE pas_id > 0 ".$whereClause." ORDER BY pas_id desc LIMIT ".$page_size." OFFSET ".$offset);
			$data['query_pemilik']	= $this->db->query("SELECT * FROM v_pasien_pemilik WHERE pem_id > 0".$clauserNum);
			if($data['query_pasien']->num_rows() > 0){
				$data['pagination']		= $this->Mainmodel->pagination($hSQL, $page_number, $page_size, $count_data);
			}
		}

		$data['pas_aktif']		= $pas_aktif;
		$this->load->view('layout', $data);
	}

    function pemilik($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;
		
		$pem_photo				= "assets/file/foto_pemilik/unknown.jpg";
		$pem_photo_identitas	= "assets/file/foto_identitas/unknown.jpg";
		$pem_nama		= '';
		$pem_no_hp_wa	= '';
		$pem_no_hp_backup	= '';
		$pem_nik		= '';
		$pem_email		= '';
		$pem_tempat_lahir	= '';
		$pem_tanggal_lahir	= '';
		$pem_gender		= 'P';
		$pem_asal		= 1;
		$pem_alamat		= '';
		$pem_aktif		= '';
		$pem_kecamatan	= 0;
		$pem_kota		= 0;
		$pem_propinsi	= 0;
		$clauseComboKota	= ' AND ref_kota_id_prop=0';
		$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota=0';
		$checkedPria	= '';
		$checkedWanita	= '';
		$checkedNonKom	= '';
		$checkedKom		= '';
		$clauserNum		= ' AND pas_pem_id=0';
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_pemilik WHERE pem_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$pem_photo			= 'assets/file/foto_pemilik/'.$rrNum->pem_photo;
			$pem_photo_identitas= 'assets/file/foto_identitas/'.$rrNum->pem_photo_identitas;
			$pem_nama		= $rrNum->pem_nama;
			$pem_no_hp_wa	= $rrNum->pem_no_hp_wa;
			$pem_no_hp_backup	= $rrNum->pem_no_hp_backup;
			$pem_email		= $rrNum->pem_email;
			$pem_nik		= $rrNum->pem_nik;
			$pem_tempat_lahir	= $rrNum->pem_tempat_lahir;
			$pem_tanggal_lahir	= $rrNum->pem_tanggal_lahir;
			$pem_gender		= $rrNum->pem_gender;
			$pem_asal		= $rrNum->pem_asal;
			$pem_alamat		= $rrNum->pem_alamat;
			$pem_kecamatan	= $rrNum->ref_kcmtn_id;
			$pem_kota		= $rrNum->ref_kota_id;
			$pem_propinsi	= $rrNum->ref_prop_id;
			$pem_aktif		= $rrNum->pem_aktif;

			if(empty($pem_propinsi)){
				$pem_propinsi=0;
			}
			if(empty($pem_kota)){
				$pem_kota=0;
			}
			$clauseComboKota	= ' AND ref_kota_id_prop='.$pem_propinsi;
			$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$pem_kota;
			$clauserNum			= ' AND pas_pem_pemilik='.$rNum;

			$pas_mrn		= '';
			$pas_nama		= '';
			$pas_gender		= 'J';
			$pas_spesies	= 0;
			$pas_jenis		= 1;
			$pas_ras		= 0;
			$pas_warna		= 0;
			$pas_tanda_khusus	= 0;
			$pas_aktif		= '';
			$pas_tanggal_lahir= '';
			$pas_last_vaksin= '';
			$pas_umur_tahun	= 0;
			$pas_umur_bulan	= 0;
			$pas_berat		= 0;
			$pas_foto		= "assets/file/foto/unknown.jpg";
			$checkedJantan	= '';
			$checkedBetina	= '';
			$checkedRas		= '';
			$checkedDomestik= '';
			if($rNum2 > 0){
				$query_rnum2	= "	SELECT * FROM v_pasien WHERE pas_id=".$rNum2;
				$rhNum2			= $this->db->query($query_rnum2);
				$rrNum2			= $rhNum2->row();
				$pas_mrn		= $rrNum2->pas_mrn;
				$pas_nama		= $rrNum2->pas_nama;
				$pas_tanggal_lahir	= $rrNum2->pas_tanggal_lahir;
				$pas_last_vaksin	= $rrNum2->pas_last_vaksin;
				$pas_umur_tahun	= $rrNum2->pas_umur_tahun;
				$pas_umur_bulan	= $rrNum2->pas_umur_bulan;
				$pas_berat	= $rrNum2->pas_berat;
				$pas_gender		= $rrNum2->pas_gender;
				$pas_spesies	= $rrNum2->pas_spesies;
				$pas_jenis		= $rrNum2->pas_jenis;
				$pas_ras		= $rrNum2->pas_ras;
				$pas_warna		= $rrNum2->pas_warna;
				$pas_tanda_khusus	= $rrNum2->pas_tanda_khusus;
				$pas_foto		= 'assets/file/foto/'.$rrNum2->pas_foto;
				$pas_aktif		= $rrNum2->pas_aktif;
			}

			if($pas_gender=='J'){
				$checkedJantan	= 'checked="checked"';
			}
			else{
				$checkedBetina	= 'checked="checked"';
			}

			if($pas_jenis==1){
				$checkedRas	= 'checked="checked"';
			}
			else{
				$checkedDomestik	= 'checked="checked"';
			}

			$data['pas_mrn']		= $pas_mrn;
			$data['pas_nama']		= $pas_nama;
			$data['checkedJantan']	= $checkedJantan;
			$data['checkedBetina']	= $checkedBetina;
			$data['checkedRas']		= $checkedRas;
			$data['checkedDomestik']= $checkedDomestik;
			$data['comboSpesies']	= $this->Mainmodel->show_combo("ref_spesies", "ref_spesies_id", "ref_spesies_nama", "ref_spesies_id > 0", "ref_spesies_nama", $pas_spesies);
			$data['comboRas']		= $this->Mainmodel->show_combo("ref_ras", "ref_ras_id", "ref_ras_nama", "ref_ras_id > 0", "ref_ras_nama", $pas_ras);
			$data['comboWarna']		= $this->Mainmodel->show_combo("ref_warna", "ref_warna_id", "ref_warna_ket", "ref_warna_id > 0", "ref_warna_ket", $pas_warna);
			$data['comboTndKhusus']	= $this->Mainmodel->show_combo("ref_tanda_khusus", "ref_tnd_khss_id", "ref_tnd_khss_ket", "ref_tnd_khss_id > 0", "ref_tnd_khss_ket", $pas_tanda_khusus);
			$data['pas_aktif']		= $pas_aktif;
			$data['pas_tanggal_lahir']	= $pas_tanggal_lahir;
			$data['pas_last_vaksin']	= $pas_last_vaksin;
			$data['pas_umur_tahun']	= $pas_umur_tahun;
			$data['pas_umur_bulan']	= $pas_umur_bulan;
			$data['pas_foto']		= $pas_foto;
			$data['pas_berat']		= $pas_berat;
			$whereClause	= " AND pem_id=".$rNum;
		}
		else{

			if(!empty($hSQL)){
				$whereClause	= base64_decode($hSQL);
			}
			else{
				$whereClause	= " AND pem_aktif IS TRUE";
			}

		}

		if($pem_gender=='P'){
			$checkedPria	= 'checked="checked"';
		}
		else{
			$checkedWanita	= 'checked="checked"';
		}

		if($pem_asal==1){
			$checkedNonKom	= 'checked="checked"';
		}
		else{
			$checkedKom	= 'checked="checked"';
		}

		if($mode == 'form_pemilik'){
			$data['main_content']   = 'master/pemilikform';
		}
		elseif($mode == 'crud_pemilik'){
			$action		= $this->input->post('action_crud');
			if(empty($this->input->post('inp_email'))){
				$pem_email = 'null';
			}
			else{
				$pem_email = "'".$this->input->post('inp_email')."'";
			}	
			
			$data = array(
				'pem_nama'			=> "'".$this->input->post('inp_nama')."'",
				'pem_no_hp_wa'		=> "'".$this->input->post('inp_wa')."'",
				'pem_no_hp_backup'	=> "'".$this->input->post('inp_hp')."'",
				'pem_nik'			=> "'".$this->input->post('inp_nik')."'",
				'pem_email'			=> $pem_email,
				'pem_tempat_lahir'	=> "'".$this->input->post('inp_tmpt_lhr')."'",
				'pem_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lhr')."'",
				'pem_gender'		=> "'".$this->input->post('inp_jk')."'",
				'pem_asal'			=> "'".$this->input->post('inp_asal')."'",
				'pem_alamat'		=> "'".$this->input->post('inp_almt')."'",
				'pem_kecamatan'		=> $this->input->post('inp_kcmtn'),
			);

			if($rNum > 0){
				$data += ['pem_time_update' => "'now()'",  'pem_user_update' => $peg_id];
				$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum);
			}
			else{
				$data += ['pem_user_create' => $peg_id,'pem_password'	=> "(SELECT mk_passwd('leo'))"];
				$rNum	= $this->Mainmodel->insert_table('pemilik', $data);
			}
            $config['upload_path']	= './assets/file/foto_pemilik/';
            $config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']		= 1024;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('profile_avatar')){
				$fotoData	= $this->upload->data();
				$fotoName	= $fotoData['file_name'];
				$data = array(
					'pem_photo'		=> "'".pg_escape_string($fotoName)."'",
				);
				$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum);
			}

            $config2['upload_path']	= './assets/file/foto_identitas/';
            $config2['allowed_types'] = 'jpg|png|jpeg';
			$config2['max_size']		= 1024;
            $this->load->library('upload', $config2);
			$this->upload->initialize($config2);
			if($this->upload->do_upload('reg_foto_iden')){
				$fotoData2	= $this->upload->data();
				$fotoName2	= $fotoData2['file_name'];
				$data = array(
					'pem_photo_identitas'		=> "'".pg_escape_string($fotoName2)."'",
				);
				$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum);
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pemilik/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pemilik/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'resetpass'){
			$data = array(
				'pem_password'	=> "(SELECT mk_passwd('leo'))"
			);
			$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'disabled_pemilik'){
			$data = array(
				'pem_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled_pemilik'){
			$data = array(
				'pem_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('pemilik', $data, 'pem_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete_pemilik'){
			$this->Mainmodel->delete_table('pasien_pemilik', 'pas_pem_pemilik='.$rNum);
			$this->Mainmodel->delete_table('pemilik', 'pem_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'form_pasien'){
			$data['var_pas_mrn']	= $this->Mainmodel->encode_var_js("pasien", "pas_id", "pas_mrn", "pas_aktif IS TRUE AND pas_mrn IS NOT NULL", "pas_mrn");
			$data['var_pas_nama']	= $this->Mainmodel->encode_var_js("pasien", "pas_id", "pas_nama", "pas_aktif IS TRUE AND pas_nama IS NOT NULL", "pas_nama");
			$data['main_content']   = 'master/pasienpemilikform';
		}
		elseif($mode == 'crud_pasien'){
			$action		= $this->input->post('action_crud');
			$pas_id		= $this->input->post('inp_pas_id');
			if($pas_id > 0){
				$rNum2 = $pas_id;
			}
            $config['upload_path'] = './assets/file/foto/';
            $config['allowed_types'] = 'jpg|png|jpeg';
			$config['max_size']		= 1024;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

			if(empty($this->input->post('inp_ras'))){
				$inp_ras = 'null';
			}
			else{
				$inp_ras = $this->input->post('inp_ras');
			}

			if(empty($this->input->post('inp_tanda_khusus'))){
				$inp_tanda = 'null';
			}
			else{
				$inp_tanda = $this->input->post('inp_tanda_khusus');
			}
 
			if(empty($this->input->post('inp_last_vaksin'))){
				$inp_last_vaksin = 'null';
			}
			else{
				$inp_last_vaksin = "'".$this->input->post('inp_last_vaksin')."'";
			}

			if($this->upload->do_upload('profile_avatar')){
				$fotoData	= $this->upload->data();
				$fotoName	= $fotoData['file_name'];
				$data = array(
					'pas_nama'		=> "'".$this->input->post('inp_nama')."'",
					'pas_gender'	=> "'".$this->input->post('inp_jk')."'",
					'pas_spesies'	=> "'".$this->input->post('inp_spesies')."'",
					'pas_jenis'		=> $this->input->post('inp_jns'),
					'pas_warna'		=> $this->input->post('inp_warna'),
					'pas_tanda_khusus'	=> $inp_tanda,
					'pas_ras'		=> $inp_ras,
					'pas_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lahir')."'",
					'pas_umur_tahun'=> "'".$this->input->post('inp_umur_tahun')."'",
					'pas_umur_bulan'=> "'".$this->input->post('inp_umur_bulan')."'",
					'pas_berat'		=> "'".$this->input->post('inp_berat')."'",
					'pas_foto'		=> "'".pg_escape_string($fotoName)."'",
					'pas_last_vaksin'	=> $inp_last_vaksin,
				);
			}
			else{
				$data = array(
					'pas_nama'		=> "'".$this->input->post('inp_nama')."'",
					'pas_gender'	=> "'".$this->input->post('inp_jk')."'",
					'pas_spesies'	=> "'".$this->input->post('inp_spesies')."'",
					'pas_jenis'		=> $this->input->post('inp_jns'),
					'pas_warna'		=> $this->input->post('inp_warna'),
					'pas_tanda_khusus'	=> $inp_tanda,
					'pas_ras'		=> $inp_ras,
					'pas_tanggal_lahir'	=> "'".$this->input->post('inp_tgl_lahir')."'",
					'pas_umur_tahun'=> "'".$this->input->post('inp_umur_tahun')."'",
					'pas_umur_bulan'=> "'".$this->input->post('inp_umur_bulan')."'",
					'pas_berat'		=> "'".$this->input->post('inp_berat')."'",
					'pas_last_vaksin'	=> $inp_last_vaksin,
				);
			}

			if($rNum2 > 0){
				$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$rNum2);

				$query_rnum2		= "	SELECT * FROM v_pasien WHERE pas_id=".$rNum2;
				$rhNum2				= $this->db->query($query_rnum2);
				$rrNum2				= $rhNum2->row();
				$pas_pemilik_utama	= $rrNum2->pas_pemilik_utama;
				$pas_mrn			= $rrNum2->pas_mrn;
				if(empty($pas_pemilik_utama)){
					$data_pem_utama= array(
						'pas_pemilik_utama'	=> $rNum
					);				
					$this->Mainmodel->update_table('pasien', $data_pem_utama, 'pas_id='.$rNum2);
					$rhExist	= $this->db->query('SELECT * FROM pasien_pemilik WHERE pas_pem_pemilik='.$rNum.' AND pas_pem_pasien='.$rNum2);
					$rrExist	= $rhExist->row();
					if(empty($rrExist->pas_pem_id)){
						$data = array(
							'pas_pem_pemilik'	=> $rNum,
							'pas_pem_pasien'	=> $rNum2,
							'pas_pem_utama'		=> 'TRUE',
						);
						$this->Mainmodel->insert_table('pasien_pemilik', $data);
					}
				}
				else{
					$rhExist	= $this->db->query('SELECT * FROM pasien_pemilik WHERE pas_pem_pemilik='.$rNum.' AND pas_pem_pasien='.$rNum2);
					$rrExist	= $rhExist->row();
					if(empty($rrExist->pas_pem_id)){
						$data = array(
							'pas_pem_pemilik'		=> $rNum,
							'pas_pem_pasien'		=> $rNum2,
						);
						$this->Mainmodel->insert_table('pasien_pemilik', $data);
					}
				}

				if(empty($pas_mrn)){
					$rrPemilik	= $this->Mainmodel->get_row_data("pemilik", "pem_id =".$rNum);
					$lastNum	= $this->Mainmodel->get_mrn();
					$hurufAwalNamaPemilik	= substr($rrPemilik->pem_nama,0,1);
					$hurufAwalNamaHewan		= substr($this->input->post('inp_nama'),0,1);
					$noMrn					= $lastNum.".".$hurufAwalNamaHewan.".".$hurufAwalNamaPemilik.".".date('Y');
					$data_mrn= array(
						'pas_mrn'			=> "'".$noMrn."'"
					);				
					$this->Mainmodel->update_table('pasien', $data_mrn, 'pas_id='.$rNum2);

				}
			}
			else{
				$rNum2	= $this->Mainmodel->insert_table('pasien', $data);

				$rrPemilik	= $this->Mainmodel->get_row_data("pemilik", "pem_id =".$rNum);
				$lastNum	= $this->Mainmodel->get_mrn();
				$hurufAwalNamaPemilik	= substr($rrPemilik->pem_nama,0,1);
				$hurufAwalNamaHewan		= substr($this->input->post('inp_nama'),0,1);
				$noMrn					= $lastNum.".".$hurufAwalNamaHewan.".".$hurufAwalNamaPemilik.".".date('Y');
				$data_mrn= array(
					'pas_mrn'			=> "'".$noMrn."'",
					'pas_pemilik_utama'	=> $rNum
				);				
				$this->Mainmodel->update_table('pasien', $data_mrn, 'pas_id='.$rNum2);

				$data = array(
					'pas_pem_pemilik'	=> $rNum,
					'pas_pem_pasien'	=> $rNum2,
					'pas_pem_utama'		=> 'TRUE',
				);
				$this->Mainmodel->insert_table('pasien_pemilik', $data);
			}


			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pasien/?rNum='.$rNum.'&rNum2='.$rNum2);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_pasien/?rNum='.$rNum);
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
			}
		}
		elseif($mode == 'delete_pasien_pemilik'){
			$data = array(
				'pas_pemilik_utama'		=> 'null'
			);
			$this->Mainmodel->update_table('pasien', $data, 'pas_pemilik_utama='.$rNum);
			$this->Mainmodel->delete_table('pasien_pemilik', 'pas_pem_pemilik='.$rNum.' AND pas_pem_pasien='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'get_data_nama'){
			$nama	= $this->input->get('nama');
			$data	= $this->Mainmodel->get_row_data("pasien", "pas_nama='".$nama."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'get_data_mrn'){
			$mrn	= $this->input->get('mrn');
			$data	= $this->Mainmodel->get_row_data("pasien", "pas_mrn='".$mrn."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}

		$data['pem_photo']			= $pem_photo;
		$data['pem_photo_identitas']= $pem_photo_identitas;
		$data['pem_nama']		= $pem_nama;
		$data['pem_no_hp_wa']	= $pem_no_hp_wa;
		$data['pem_no_hp_backup']	= $pem_no_hp_backup;
		$data['pem_nik']		= $pem_nik;
		$data['pem_alamat']		= $pem_alamat;
		$data['pem_tempat_lahir']	= $pem_tempat_lahir;
		$data['pem_tanggal_lahir']	= $pem_tanggal_lahir;
		$data['pem_email']		= $pem_email;
		$data['pem_aktif']		= $pem_aktif;
		$data['checkedPria']	= $checkedPria;
		$data['checkedWanita']	= $checkedWanita;
		$data['checkedNonKom']	= $checkedNonKom;
		$data['checkedKom']		= $checkedKom;
		$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $pem_propinsi);
		$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $pem_kota);
		$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $pem_kecamatan);

		$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM v_master_pemilik WHERE pem_id > 0'.$whereClause);
		$num_row				= $queryNumRow->row();
		$count_data				= $num_row->count_data;

		(empty($this->input->get('pn'))) ? $page_number = 1 : $page_number = $this->input->get('pn');
		(empty($this->input->get('ps'))) ? $page_size = 100 : $page_size = $this->input->get('ps');
		($page_number > 1) ? $offset = ($page_number-1)*$page_size : $offset = 0;
		
		$data['offset']			= $offset;
		$data['query_pemilik']	= $this->db->query("SELECT * FROM v_master_pemilik WHERE pem_id > 0 ".$whereClause." ORDER BY pem_id desc LIMIT ".$page_size." OFFSET ".$offset);
		$data['query_pasien']	= $this->db->query("SELECT * FROM v_pasien_pemilik WHERE pas_id > 0".$clauserNum);
		if($data['query_pemilik']->num_rows() > 0){
			$data['pagination']		= $this->Mainmodel->pagination($hSQL, $page_number, $page_size, $count_data);
		}
		$this->load->view('layout', $data);
	}

    function penunjang($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum				= $this->input->get('rNum');
		$ref_spesies_nama	= '';
		$rNum2				= $this->input->get('rNum2');
		$ref_warna_ket		= '';
		$rNum3				= $this->input->get('rNum3');
		$ref_ras_spesies	= 0;
		$ref_ras_nama		= '';
		$rNum4				= $this->input->get('rNum4');
		$ref_ras_dt_nama		= '';
		$rNum5				= $this->input->get('rNum5');
		$ref_tnd_khss_ket		= '';

		if($rNum > 0){
			$query_spes			= "	SELECT * FROM ref_spesies WHERE ref_spesies_id=".$rNum;
			$rhSpes				= $this->db->query($query_spes);
			$rrSpes				= $rhSpes->row();
			$ref_spesies_nama	= $rrSpes->ref_spesies_nama;
		}

		if($rNum2 > 0){
			$query_warna		= "	SELECT * FROM ref_warna WHERE ref_warna_id=".$rNum2;
			$rhWrn				= $this->db->query($query_warna);
			$rrWrn				= $rhWrn->row();
			$ref_warna_ket		= $rrWrn->ref_warna_ket;
		}

		if($rNum3 > 0){
			$query_ras			= "	SELECT * FROM ref_ras WHERE ref_ras_id=".$rNum3;
			$rhRas				= $this->db->query($query_ras);
			$rrRas				= $rhRas->row();
			$ref_ras_spesies	= $rrRas->ref_ras_spesies;
			$ref_ras_nama		= $rrRas->ref_ras_nama;
			$whereRasDt			= " AND ref_ras_dt_ras_id=".$rNum3;

			if($rNum4 > 0){
				$query_ras_dt	= "	SELECT * FROM ref_ras_detail WHERE ref_ras_dt_id=".$rNum4;
				$rhRasDt		= $this->db->query($query_ras_dt);
				$rrRasDt		= $rhRasDt->row();
				$ref_ras_dt_nama	= $rrRasDt->ref_ras_dt_nama;
			}
		}
		else{
			$whereRasDt			= " AND ref_ras_dt_ras_id=0";
		}

		if($rNum5 > 0){
			$query_tanda		= "	SELECT * FROM ref_tanda_khusus WHERE ref_tnd_khss_id=".$rNum5;
			$rhTnd				= $this->db->query($query_tanda);
			$rrTnd				= $rhTnd->row();
			$ref_tnd_khss_ket	= $rrTnd->ref_tnd_khss_ket;
		}


		if($mode == 'crud_spesies'){
			$action		= $this->input->post('submit_crud_spesies');
			if($action == 'simpan_spesies'){
				$data = array(
					'ref_spesies_nama'		=> "'".$this->input->post('inp_nama_spesies')."'"
				);

				if($rNum > 0){
					$this->Mainmodel->update_table('ref_spesies', $data, 'ref_spesies_id='.$rNum);
				}
				else{
					$rNum	= $this->Mainmodel->insert_table('ref_spesies', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
			}
		}
		elseif($mode == 'del_spesies'){
			$this->Mainmodel->delete_table('ref_spesies', 'ref_spesies_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_warna'){
			$action		= $this->input->post('submit_crud_warna');
			if($action == 'simpan_warna'){
				$data = array(
					'ref_warna_ket'		=> "'".$this->input->post('inp_nama_warna')."'"
				);

				if($rNum2 > 0){
					$this->Mainmodel->update_table('ref_warna', $data, 'ref_warna_id='.$rNum2);
				}
				else{
					$rNum2	= $this->Mainmodel->insert_table('ref_warna', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum2='.$rNum2);
			}
		}
		elseif($mode == 'del_warna'){
			$this->Mainmodel->delete_table('ref_warna', 'ref_warna_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_ras'){
			$action		= $this->input->post('submit_crud_ras');
			if($action == 'simpan_ras'){
				$data = array(
					'ref_ras_spesies'	=> $this->input->post('inp_spesies_ras'),
					'ref_ras_nama'		=> "'".$this->input->post('inp_nama_ras')."'"
				);

				if($rNum3 > 0){
					$this->Mainmodel->update_table('ref_ras', $data, 'ref_ras_id='.$rNum3);
				}
				else{
					$rNum2	= $this->Mainmodel->insert_table('ref_ras', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum3='.$rNum3);
			}
		}
		elseif($mode == 'del_ras'){
			$this->Mainmodel->delete_table('ref_ras', 'ref_ras_id='.$rNum3);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_ras_dt'){
			$action		= $this->input->post('submit_crud_ras_dt');
			if($action == 'simpan_ras_dt'){
				$data = array(
					'ref_ras_dt_ras_id'		=> $rNum3,
					'ref_ras_dt_nama'		=> "'".$this->input->post('inp_nama_ras_dt')."'"
				);

				if($rNum4 > 0){
					$this->Mainmodel->update_table('ref_ras_detail', $data, 'ref_ras_dt_id='.$rNum4);
				}
				else{
					$rNum2	= $this->Mainmodel->insert_table('ref_ras_detail', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum3='.$rNum3.'&rNum4='.$rNum4);
			}
		}
		elseif($mode == 'del_ras_dt'){
			$this->Mainmodel->delete_table('ref_ras_detail', 'ref_ras_dt_id='.$rNum4);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_tanda'){
			$action		= $this->input->post('submit_crud_tanda');
			if($action == 'simpan_tanda'){
				$data = array(
					'ref_tnd_khss_ket'		=> "'".$this->input->post('inp_nama_tanda')."'"
				);

				if($rNum5 > 0){
					$this->Mainmodel->update_table('ref_tanda_khusus', $data, 'ref_warna_id='.$rNum5);
				}
				else{
					$rNum5	= $this->Mainmodel->insert_table('ref_tanda_khusus', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum5='.$rNum5);
			}
		}
		elseif($mode == 'del_tanda'){
			$this->Mainmodel->delete_table('ref_tanda_khusus', 'ref_tnd_khss_id='.$rNum5);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']				= $rNum;
		$data['ref_spesies_nama']	= $ref_spesies_nama;
		$data['rNum2']				= $rNum2;
		$data['ref_warna_ket']		= $ref_warna_ket;
		$data['rNum3']				= $rNum3;
		$data['ref_ras_nama']		= $ref_ras_nama;
		$data['rNum4']				= $rNum4;
		$data['ref_ras_dt_nama']	= $ref_ras_dt_nama;
		$data['rNum5']				= $rNum5;
		$data['ref_tnd_khss_ket']	= $ref_tnd_khss_ket;

		$data['query_spesies']	= $this->db->query("SELECT * FROM ref_spesies WHERE ref_spesies_id > 0");
		$data['query_warna']	= $this->db->query("SELECT * FROM ref_warna WHERE ref_warna_id > 0");
		$data['query_ras']		= $this->db->query("SELECT * FROM ref_ras LEFT JOIN ref_spesies ON ref_spesies_id=ref_ras_spesies WHERE ref_ras_id > 0");
		$data['comboSpesiesRas']	= $this->Mainmodel->show_combo("ref_spesies", "ref_spesies_id", "ref_spesies_nama", "ref_spesies_id > 0", "ref_spesies_nama", $ref_ras_spesies);
		$data['query_ras_detail']	= $this->db->query("SELECT * FROM ref_ras_detail WHERE ref_ras_dt_id > 0".$whereRasDt);
		$data['query_tanda']	= $this->db->query("SELECT * FROM ref_tanda_khusus WHERE ref_tnd_khss_id > 0");
		$this->load->view('layout', $data);
	}

    function penunjangmedis($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		
		$rNum				= $this->input->get('rNum');
		$ref_anamnesis_ket	= '';
		$orderByAnam		= " ORDER BY ref_anamnesis_ket";
		
		$rNum2				= $this->input->get('rNum2');
		$ref_diag_ket		= '';
		$orderByDiag		= " ORDER BY ref_diag_ket";
		
		$rNum3				= $this->input->get('rNum3');
		$ref_tind_kode		= '';
		$ref_tind_ket		= '';
		$orderByTind		= " ORDER BY ref_tind_ket";
		
		$rNum4				= $this->input->get('rNum4');
		$ref_prwtn_ket		= '';
		$orderByPrwtn		= " ORDER BY ref_kondisi_ket";
		
		$rNum5				= $this->input->get('rNum5');
		$ref_habit_ket		= '';
		$orderByHabit		= " ORDER BY ref_kondisi_ket";
		
		$rNum6				= $this->input->get('rNum6');
		$ref_prks_lnjt_produk		= 0;
		$ref_prks_lnjt_ket	= '';
		$orderByPeriksa		= " ORDER BY ref_prks_lnjt_ket";
		
		$rNum7				= $this->input->get('rNum7');
		$ref_prod_jenis		= 1;
		$ref_prod_nama		= '';
		$ref_prod_link_toped= '';
		$orderByProduk		= " ORDER BY ref_prod_nama";

		if($rNum > 0){
			$query_anamnesa		= "	SELECT * FROM ref_anamnesis WHERE ref_anamnesis_id=".$rNum;
			$rhAnam				= $this->db->query($query_anamnesa);
			$rrAnam				= $rhAnam->row();
			$ref_anamnesis_ket	= $rrAnam->ref_anamnesis_ket;
			$orderByAnam	= " ORDER BY ref_anamnesis_id=".$rNum." desc, ref_anamnesis_ket";
		}

		if($rNum2 > 0){
			$query_diagnosa		= "	SELECT * FROM ref_diagnosa WHERE ref_diag_id=".$rNum2;
			$rhDiag				= $this->db->query($query_diagnosa);
			$rrDiag				= $rhDiag->row();
			$ref_diag_ket		= $rrDiag->ref_diag_ket;
			$orderByDiag		= " ORDER BY ref_diag_id=".$rNum2." desc, ref_diag_ket";
		}

		if($rNum3 > 0){
			$query_tindakan		= "	SELECT * FROM ref_tindakan WHERE ref_tind_id=".$rNum3;
			$rhTind				= $this->db->query($query_tindakan);
			$rrTind				= $rhTind->row();
			$ref_tind_kode		= $rrTind->ref_tind_kode;
			$ref_tind_ket		= $rrTind->ref_tind_ket;
			$orderByTind		= " ORDER BY ref_tind_id=".$rNum3." desc, ref_tind_ket";
		}

		if($rNum4 > 0){
			$query_perawatan	= "	SELECT * FROM ref_kondisi WHERE ref_kondisi_perawatan IS TRUE AND ref_kondisi_id=".$rNum4;
			$rhPrwtn			= $this->db->query($query_perawatan);
			$rrPrwtn			= $rhPrwtn->row();
			$ref_prwtn_ket		= $rrPrwtn->ref_kondisi_ket;
			$orderByPrwtn		= " ORDER BY ref_kondisi_id=".$rNum4." desc, ref_kondisi_ket";
		}

		if($rNum5 > 0){
			$query_habitus		= "	SELECT * FROM ref_kondisi WHERE ref_kondisi_habitus IS TRUE AND ref_kondisi_id=".$rNum5;
			$rhHabit			= $this->db->query($query_habitus);
			$rrHabit			= $rhHabit->row();
			$ref_habit_ket		= $rrHabit->ref_kondisi_ket;
			$orderByHabit		= " ORDER BY ref_kondisi_id=".$rNum5." desc, ref_kondisi_ket";
		}

		if($rNum6 > 0){
			$query_periksa		= "	SELECT * FROM ref_pemeriksaan_lanjutan WHERE ref_prks_lnjt_id=".$rNum6;
			$rhPeriksa			= $this->db->query($query_periksa);
			$rrPeriksa			= $rhPeriksa->row();
			$ref_prks_lnjt_ket	= $rrPeriksa->ref_prks_lnjt_ket;
			$ref_prks_lnjt_produk	= $rrPeriksa->ref_prks_lnjt_produk;
			$orderByPeriksa		= " ORDER BY ref_prks_lnjt_id=".$rNum6." desc, ref_prks_lnjt_ket";
		}

		if($rNum7 > 0){
			$query_produk		= "	SELECT * FROM ref_produk WHERE ref_prod_id=".$rNum7;
			$rhProduk			= $this->db->query($query_produk);
			$rrProduk			= $rhProduk->row();
			$ref_prod_jenis		= $rrProduk->ref_prod_jenis;
			$ref_prod_nama		= $rrProduk->ref_prod_nama;
			$ref_prod_link_toped= $rrProduk->ref_prod_link_toped;
			$orderByProduk		= " ORDER BY ref_prod_id=".$rNum7." desc, ref_prod_nama";
		}

		if($mode == 'crud_anamnesa'){
			$action		= $this->input->post('submit_crud_anamnesa');
			if($action == 'simpan_anamnesa'){
				$data = array(
					'ref_anamnesis_ket'		=> "'".$this->input->post('inp_nama_anamnesa')."'"
				);

				if($rNum > 0){
					$this->Mainmodel->update_table('ref_anamnesis', $data, 'ref_anamnesis_id='.$rNum);
				}
				else{
					$rNum	= $this->Mainmodel->insert_table('ref_anamnesis', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_anamnesa'){
			$this->Mainmodel->delete_table('ref_anamnesis', 'ref_anamnesis_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_diagnosa'){
			$action		= $this->input->post('submit_crud_diagnosa');
			if($action == 'simpan_diagnosa'){
				$data = array(
					'ref_diag_ket'		=> "'".$this->input->post('inp_nama_diagnosa')."'"
				);

				if($rNum2 > 0){
					$this->Mainmodel->update_table('ref_diagnosa', $data, 'ref_diag_id='.$rNum);
				}
				else{
					$rNum2	= $this->Mainmodel->insert_table('ref_diagnosa', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_diagnosa'){
			$this->Mainmodel->delete_table('ref_diagnosa', 'ref_diag_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_tindakan'){
			$action		= $this->input->post('submit_crud_tindakan');
			if($action == 'simpan_tindakan'){
				$data = array(
					'ref_tind_kode'		=> "'".$this->input->post('inp_kode_tindakan')."'",
					'ref_tind_ket'		=> "'".$this->input->post('inp_nama_tindakan')."'"
				);

				if($rNum3 > 0){
					$this->Mainmodel->update_table('ref_tindakan', $data, 'ref_tind_id='.$rNum3);
				}
				else{
					$rNum3	= $this->Mainmodel->insert_table('ref_tindakan', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_tindakan'){
			$this->Mainmodel->delete_table('ref_tindakan', 'ref_tind_id='.$rNum3);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_perawatan'){
			$action		= $this->input->post('submit_crud_perawatan');
			if($action == 'simpan_perawatan'){
				$data = array(
					'ref_kondisi_perawatan' => "'true'",
					'ref_kondisi_ket'		=> "'".$this->input->post('inp_nama_perawatan')."'"
				);

				if($rNum4 > 0){
					$this->Mainmodel->update_table('ref_kondisi', $data, 'ref_kondisi_id='.$rNum4);
				}
				else{
					$rNum4	= $this->Mainmodel->insert_table('ref_kondisi', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_perawatan'){
			$this->Mainmodel->delete_table('ref_kondisi', 'ref_kondisi_id='.$rNum4);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_habitus'){
			$action		= $this->input->post('submit_crud_habitus');
			if($action == 'simpan_habitus'){
				$data = array(
					'ref_kondisi_habitus' => "'true'",
					'ref_kondisi_ket'		=> "'".$this->input->post('inp_nama_habitus')."'"
				);

				if($rNum4 > 0){
					$this->Mainmodel->update_table('ref_kondisi', $data, 'ref_kondisi_id='.$rNum5);
				}
				else{
					$rNum4	= $this->Mainmodel->insert_table('ref_kondisi', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_habitus'){
			$this->Mainmodel->delete_table('ref_kondisi', 'ref_kondisi_id='.$rNum5);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_periksa'){
			$action		= $this->input->post('submit_crud_periksa');
			if($action == 'simpan_periksa'){
				$data = array(
					'ref_prks_lnjt_ket'		=> "'".$this->input->post('inp_nama_periksa')."'",
					'ref_prks_lnjt_produk'		=> $this->input->post('inp_produk'),
				);

				if($rNum6 > 0){
					$this->Mainmodel->update_table('ref_pemeriksaan_lanjutan', $data, 'ref_prks_lnjt_id='.$rNum6);
				}
				else{
					$rNum6	= $this->Mainmodel->insert_table('ref_pemeriksaan_lanjutan', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_periksa'){
			$this->Mainmodel->delete_table('ref_pemeriksaan_lanjutan', 'ref_prks_lnjt_id='.$rNum6);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_produk'){
			$action		= $this->input->post('submit_crud_produk');
			if($action == 'simpan_produk'){
				$data = array(
					'ref_prod_jenis'		=> $this->input->post('inp_jenis_produk'),
					'ref_prod_nama'			=> "'".$this->input->post('inp_nama_produk')."'",
					'ref_prod_link_toped'	=> "'".$this->input->post('inp_link_toped_produk')."'"
				);

				if($rNum7 > 0){
					$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum7);
				}
				else{
					$rNum7	= $this->Mainmodel->insert_table('ref_produk', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'del_produk'){
			$this->Mainmodel->delete_table('ref_produk', 'ref_prod_id='.$rNum7);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']				= $rNum;
		$data['ref_anamnesis_ket']	= $ref_anamnesis_ket;
		$data['rNum2']				= $rNum2;
		$data['ref_diag_ket']		= $ref_diag_ket;
		$data['rNum3']				= $rNum3;
		$data['ref_tind_kode']		= $ref_tind_kode;
		$data['ref_tind_ket']		= $ref_tind_ket;
		$data['rNum4']				= $rNum4;
		$data['ref_prwtn_ket']		= $ref_prwtn_ket;
		$data['rNum5']				= $rNum5;
		$data['ref_habit_ket']		= $ref_habit_ket;
		$data['rNum6']				= $rNum6;
		$data['ref_prks_lnjt_ket']	= $ref_prks_lnjt_ket;
		$data['rNum7']				= $rNum7;
		$data['ref_prod_nama']		= $ref_prod_nama;
		$data['ref_prod_link_toped']= $ref_prod_link_toped;

		$data['comboJenisProduk']	= $this->Mainmodel->show_combo("ref_jenis_produk", "ref_jns_prod_id", "ref_jns_prod_nama", "ref_jns_prod_id > 0", "ref_jns_prod_nama", $ref_prod_jenis);
		$data['comboProduk']		= $this->Mainmodel->show_combo("ref_produk", "ref_prod_id", "ref_prod_nama", "ref_prod_aktif IS TRUE", "ref_prod_nama", $ref_prks_lnjt_produk);
		$data['query_anamnesa']	= $this->db->query("SELECT * FROM ref_anamnesis WHERE ref_anamnesis_id > 0".$orderByAnam);
		$data['query_diagnosa']	= $this->db->query("SELECT * FROM ref_diagnosa WHERE ref_diag_id > 0".$orderByDiag);
		$data['query_tindakan']	= $this->db->query("SELECT * FROM ref_tindakan WHERE ref_tind_id > 0".$orderByTind);
		$data['query_perawatan']= $this->db->query("SELECT * FROM ref_kondisi WHERE ref_kondisi_perawatan IS TRUE".$orderByPrwtn);
		$data['query_habitus']	= $this->db->query("SELECT * FROM ref_kondisi WHERE ref_kondisi_habitus IS TRUE".$orderByHabit);
		$data['query_periksa']	= $this->db->query("SELECT * FROM v_ref_pemeriksaan_lanjutan WHERE ref_prks_lnjt_id > 0".$orderByPeriksa);
		$data['query_produk']	= $this->db->query("SELECT * FROM v_produk WHERE ref_prod_id > 0".$orderByProduk);
		$this->load->view('layout', $data);
	}

    function penunjangproduk($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum				= $this->input->get('rNum');
		$ref_jns_prod_nama	= '';
		$rNum2				= $this->input->get('rNum2');
		$ref_sat_ket		= '';
		$ref_sat_kode		= '';

		if($rNum > 0){
			$query_jenis		= "	SELECT * FROM ref_jenis_produk WHERE ref_jns_prod_id=".$rNum;
			$rhJns				= $this->db->query($query_jenis);
			$rrJns				= $rhJns->row();
			$ref_jns_prod_nama	= $rrJns->ref_jns_prod_nama;
		}

		if($rNum2 > 0){
			$query_satuan		= "	SELECT * FROM ref_satuan WHERE ref_sat_id=".$rNum2;
			$rhSat				= $this->db->query($query_satuan);
			$rrSat				= $rhSat->row();
			$ref_sat_ket		= $rrSat->ref_sat_ket;
			$ref_sat_kode		= $rrSat->ref_sat_kode;
		}

		if($mode == 'crud_jenis'){
			$action		= $this->input->post('submit_crud_jenis');
			if($action == 'simpan_jenis'){
				$data = array(
					'ref_jns_prod_nama'		=> "'".$this->input->post('inp_nama_jenis')."'"
				);

				if($rNum > 0){
					$this->Mainmodel->update_table('ref_jenis_produk', $data, 'ref_jns_prod_id='.$rNum);
				}
				else{
					$rNum	= $this->Mainmodel->insert_table('ref_jenis_produk', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
			}
		}
		elseif($mode == 'del_jenis'){
			$this->Mainmodel->delete_table('ref_jenis_produk', 'ref_jns_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_satuan'){
			$action		= $this->input->post('submit_crud_satuan');
			if($action == 'simpan_satuan'){
				$data = array(
					'ref_sat_ket'		=> "'".$this->input->post('inp_nama_satuan')."'",
					'ref_sat_kode'		=> "'".$this->input->post('inp_kode_satuan')."'"
				);

				if($rNum2 > 0){
					$this->Mainmodel->update_table('ref_satuan', $data, 'ref_sat_id='.$rNum2);
				}
				else{
					$rNum2	= $this->Mainmodel->insert_table('ref_satuan', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum2='.$rNum2);
			}
		}
		elseif($mode == 'del_satuan'){
			$this->Mainmodel->delete_table('ref_satuan', 'ref_sat_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}


		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']				= $rNum;
		$data['ref_jns_prod_nama']	= $ref_jns_prod_nama;
		$data['rNum2']				= $rNum2;
		$data['ref_sat_ket']		= $ref_sat_ket;
 		$data['ref_sat_kode']		= $ref_sat_kode;

		$data['query_jenis']	= $this->db->query("SELECT * FROM ref_jenis_produk WHERE ref_jns_prod_id > 0");
		$data['query_satuan']	= $this->db->query("SELECT * FROM ref_satuan WHERE ref_sat_id > 0");
		$this->load->view('layout', $data);
	}

    function pemilikmerge($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$iSQL		= str_replace(' ', '+', $this->input->get('iSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;

		if($rNum > 0){
			$whereClause	= " AND pem_id=".$rNum;
			if(!empty($iSQL)){
				$whereClauseMerge	= base64_decode($iSQL);
			}
			else{
				$whereClauseMerge	= " AND pem_id= 0";
			}
			$data['query_pemilik_merge']	= $this->db->query("SELECT * FROM v_master_pemilik WHERE pem_id <>  ".$rNum.$whereClauseMerge);
		}
		else{
			if(!empty($hSQL)){
				$whereClause	= base64_decode($hSQL);
			}
			else{
				$whereClause	= " AND pem_aktif IS TRUE";
			}
		}

		if($mode == 'crud'){
			$rNum		= $this->input->post('rNum');
			$count_dt	= $this->input->post('count_dt');
			for ($i = 1; $i <= $count_dt; $i++) {
				$row_id		= $this->input->post('row_id_'.$i);
				$checked	= $this->input->post('checked_'.$i);
				if($checked=='t'){
					$this->db->query('UPDATE rekam_medis SET rm_owner_buat='.$rNum.' WHERE rm_owner_buat='.$row_id);
					$this->db->query('UPDATE pasien_pemilik SET pas_pem_pemilik='.$rNum.' WHERE pas_pem_pemilik='.$row_id);
					$this->db->query('UPDATE pasien SET pas_pemilik_utama='.$rNum.' WHERE pas_pemilik_utama='.$row_id);
					$this->db->query('INSERT INTO pemilik_merge SELECT *,'.$rNum.',now(),' . $peg_id . ' FROM pemilik WHERE pem_id=' . $row_id);
					$this->Mainmodel->delete_table('pemilik', 'pem_id='.$row_id);
				}
			} 
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM v_master_pemilik WHERE pem_id > 0'.$whereClause);
		$num_row				= $queryNumRow->row();
		$count_data				= $num_row->count_data;

		(empty($this->input->get('pn'))) ? $page_number = 1 : $page_number = $this->input->get('pn');
		(empty($this->input->get('ps'))) ? $page_size = 100 : $page_size = $this->input->get('ps');
		($page_number > 1) ? $offset = ($page_number-1)*$page_size : $offset = 0;
		
		$data['offset']			= $offset;
		$data['query_pemilik']	= $this->db->query("SELECT * FROM v_master_pemilik WHERE pem_id > 0 ".$whereClause." ORDER BY pem_time_create desc LIMIT ".$page_size." OFFSET ".$offset);
		if($data['query_pemilik']->num_rows() > 0){
			$data['pagination']		= $this->Mainmodel->pagination($hSQL, $page_number, $page_size, $count_data);
		}
		$this->load->view('layout', $data);
	}

    function pasienmerge($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$iSQL		= str_replace(' ', '+', $this->input->get('iSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;

		if($rNum > 0){
			$whereClause	= " AND pas_id=".$rNum;
			if(!empty($iSQL)){
				$whereClauseMerge	= base64_decode($iSQL);
			}
			else{
				$whereClauseMerge	= " AND pas_id= 0";
			}
			$data['query_pasien_merge']	= $this->db->query("SELECT * FROM v_pasien WHERE pas_id <>  ".$rNum.$whereClauseMerge);
		}
		else{
			if(!empty($hSQL)){
				$whereClause	= base64_decode($hSQL);
			}
			else{
				$whereClause	= " AND pas_aktif IS TRUE";
			}
		}

		if($mode == 'crud'){
			$rNum		= $this->input->post('rNum');
			$count_dt	= $this->input->post('count_dt');
			for ($i = 1; $i <= $count_dt; $i++) {
				$row_id		= $this->input->post('row_id_'.$i);
				$checked	= $this->input->post('checked_'.$i);
				if($checked=='t'){
					$this->db->query('UPDATE rekam_medis SET rm_pasien='.$rNum.' WHERE rm_pasien='.$row_id);
					$this->db->query('UPDATE pasien_pemilik SET pas_pem_pasien='.$rNum.' WHERE pas_pem_pasien='.$row_id);
					$this->db->query('INSERT INTO pasien_merge SELECT *,'.$rNum.',now(),' . $peg_id . ' FROM pasien WHERE pas_id=' . $row_id);
					$this->Mainmodel->delete_table('pasien', 'pas_id='.$row_id);
				}
			} 
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM v_pasien WHERE pas_id > 0'.$whereClause);
		$num_row				= $queryNumRow->row();
		$count_data				= $num_row->count_data;

		(empty($this->input->get('pn'))) ? $page_number = 1 : $page_number = $this->input->get('pn');
		(empty($this->input->get('ps'))) ? $page_size = 100 : $page_size = $this->input->get('ps');
		($page_number > 1) ? $offset = ($page_number-1)*$page_size : $offset = 0;
		
		$data['offset']			= $offset;
		$data['query_pasien']	= $this->db->query("SELECT * FROM v_pasien WHERE pas_id > 0 ".$whereClause." ORDER BY pas_id desc LIMIT ".$page_size." OFFSET ".$offset);
		if($data['query_pasien']->num_rows() > 0){
			$data['pagination']		= $this->Mainmodel->pagination($hSQL, $page_number, $page_size, $count_data);
		}
		$this->load->view('layout', $data);
	}

	function pasien_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$cr_rm	= $this->input->post('cr_rm');
		$cr_nama		= $this->input->post('cr_nama');
		$cr_pemilik		= $this->input->post('cr_pemilik');
		$whereClause	= '';

		if (!empty($cr_rm)) {
			$whereClause .= " AND pas_mrn~*'" . $cr_rm . "'";
		}

		if (!empty($cr_nama)) {
			$whereClause .= " AND pas_nama~*'" . $cr_nama . "'";
		}

		if (!empty($cr_pemilik)) {
			$whereClause .= " AND pem_nama~*'" . $cr_pemilik . "'";
		}
		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}

	function pemilik_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];

		$cr_nik			= $this->input->post('cr_nik');
		$cr_nama		= $this->input->post('cr_nama');
		$cr_email		= $this->input->post('cr_email');
		$cr_no_hp		= $this->input->post('cr_no_hp');
		$cr_prop		= $this->input->post('cr_prop');
		$cr_kota		= $this->input->post('cr_kota');
		$whereClause	= '';
		if (!empty($cr_nik)) {
			$whereClause .= " AND pem_nik~*'" . $cr_nik . "'";
		}

		if (!empty($cr_nama)) {
			$whereClause .= " AND pem_nama~*'" . $cr_nama . "'";
		}

		if (!empty($cr_email)) {
			$whereClause .= " AND pem_email~*'" . $cr_email . "'";
		}

		if (!empty($cr_no_hp)) {
			$whereClause .= " AND pem_no_hp_wa~*'" . $cr_no_hp . "'";
		}

		if ($cr_kota > 0) {
			$whereClause .= " AND ref_kota_id=" . $cr_kota;
		}
		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}

	function pemilik_merge_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$rNum			= $this->input->post('rNum');
		$cr_nik			= $this->input->post('cr_nik');
		$cr_nama		= $this->input->post('cr_nama');
		$cr_email		= $this->input->post('cr_email');
		$cr_no_hp		= $this->input->post('cr_no_hp');
		$whereClause	= '';
		if (!empty($cr_nik)) {
			$whereClause .= " AND pem_nik~*'" . $cr_nik . "'";
		}

		if (!empty($cr_nama)) {
			$whereClause .= " AND pem_nama~*'" . $cr_nama . "'";
		}

		if (!empty($cr_email)) {
			$whereClause .= " AND pem_email~*'" . $cr_email . "'";
		}

		if (!empty($cr_no_hp)) {
			$whereClause .= " AND pem_no_hp_wa~*'" . $cr_no_hp . "'";
		}

		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?rNum='.$rNum.'&iSQL='.$clause);
	}

	function pasien_merge_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$rNum			= $this->input->post('rNum');
		$cr_rm	= $this->input->post('cr_rm');
		$cr_nama		= $this->input->post('cr_nama');
		$cr_pemilik		= $this->input->post('cr_pemilik');
		$whereClause	= '';

		if (!empty($cr_rm)) {
			$whereClause .= " AND pas_mrn~*'" . $cr_rm . "'";
		}

		if (!empty($cr_nama)) {
			$whereClause .= " AND pas_nama~*'" . $cr_nama . "'";
		}

		if (!empty($cr_pemilik)) {
			$whereClause .= " AND pem_nama~*'" . $cr_pemilik . "'";
		}
		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?rNum='.$rNum.'&iSQL='.$clause);
	}

    function coa($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		if($mode == 'form'){
			$data['main_content']   = 'master/coaform';
		}
		elseif($mode == 'crud'){
			$tier		= $this->input->post('tier');
			$coa_parent= $this->input->post('coa_parent');
			$coa_id		= $this->input->post('coa_id');
			
			if($coa_id > 0){
				$data = array(
					'kd_rkng_nama'	=> "'".$this->input->post('inp_nama')."'"
				);

				if($tier == 4){
					if($this->input->post('cek_klinik') > 0){
						$data += ['kd_rkng_klinik'	=> 'TRUE'];
					}
					else{
						$data += ['kd_rkng_klinik'	=> 'FALSE'];
					}
					if($this->input->post('cek_labrad') > 0){
						$data += ['kd_rkng_labrad'	=> 'TRUE'];
					}
					else{
						$data += ['kd_rkng_labrad'	=> 'FALSE'];
					}
					if($this->input->post('cek_petcare') > 0){
						$data += ['kd_rkng_petcare' => 'TRUE'];
					}
					else{
						$data += ['kd_rkng_petcare'	=> 'FALSE'];
					}
					if($this->input->post('cek_apotek') > 0){
						$data += ['kd_rkng_apotek'	=> 'TRUE'];
					}
					else{
						$data += ['kd_rkng_apotek'	=> 'FALSE'];
					}
				}

				$this->Mainmodel->update_table('kode_rekening', $data, 'kd_rkng_id='.$coa_id);
			}
			else{
				die();
				$data = array(
					'kd_rkng_parent'=> $coa_parent,
					'kd_rkng_kode'	=> "'TEST'",
					'kd_rkng_nama'	=> "'".$this->input->post('inp_nama')."'"
				);

				if($tier == 3){
					if($this->input->post('cek_klinik') > 0){
						$data += ['kd_rkng_klinik'	=> 'TRUE'];
					}
					else{
						$data += ['kd_rkng_klinik'	=> 'FALSE'];
					}
					if($this->input->post('cek_labrad') > 0){
						$data += ['kd_rkng_labrad'	=> 'TRUE'];
					}
					else{
						$data += ['kd_rkng_labrad'	=> 'FALSE'];
					}
					if($this->input->post('cek_petcare') > 0){
						$data += ['kd_rkng_petcare' => 'TRUE'];
					}
					else{
						$data += ['kd_rkng_petcare'	=> 'FALSE'];
					}
					if($this->input->post('cek_apotek') > 0){
						$data += ['kd_rkng_apotek'	=> 'TRUE'];
					}
					else{
						$data += ['kd_rkng_apotek'	=> 'FALSE'];
					}
				}

				$coa_id	= $this->Mainmodel->insert_table('kode_rekening', $data);
			}
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'disabled'){
			$coa_id		= $this->input->post('coa_id');
			$data = array(
				'kd_rkng_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('kode_rekening', $data, 'kd_rkng_id='.$coa_id);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'enabled'){
			$coa_id		= $this->input->post('coa_id');
			$data = array(
				'kd_rkng_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('kode_rekening', $data, 'kd_rkng_id='.$coa_id);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'delete'){
			$coa_id		= $this->input->get('coa_id');
			$this->Mainmodel->delete_table('kode_rekening', 'kd_rkng_parent='.$coa_id);
			$this->Mainmodel->delete_table('kode_rekening', 'kd_rkng_id='.$coa_id);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$data['query_hd']		= $this->db->query("SELECT * FROM v_kode_rekening WHERE kd_rkng_parent IS NULL ORDER BY kd_rkng_kode");
		$this->load->view('layout', $data);
	}

    function modalcoa($mode='') {
		$tier		= $this->input->get('tier');
		$coa_parent	= $this->input->get('coa_parent');
		$coa_id		= $this->input->get('coa_id');
		$kd_rkng_nama = '';		
		$checkedKlinik = '';		
		$checkedLabrad = '';		
		$checkedPetcare = '';		
		$checkedApotek = '';	
		if($coa_id > 0){
			$query_rnum		= "	SELECT * FROM kode_rekening WHERE kd_rkng_id=".$coa_id;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$kd_rkng_nama	= $rrNum->kd_rkng_nama;
			($rrNum->kd_rkng_klinik == 't') ? $checkedKlinik = 'checked' : $checkedKlinik = '';
			($rrNum->kd_rkng_labrad == 't') ? $checkedLabrad = 'checked' : $checkedLabrad = '';
			($rrNum->kd_rkng_petcare == 't') ? $checkedPetcare = 'checked' : $checkedPetcare = '';
			($rrNum->kd_rkng_apotek == 't') ? $checkedApotek = 'checked' : $checkedApotek = '';
		}

		if($tier == 4 || ($tier == 3 && empty($coa_id))){
			echo '	<input type="hidden" class="form-control" name="tier" id="tier" value="'.$tier.'">
					<input type="hidden" class="form-control" name="coa_parent" id="coa_parent" value="'.$coa_parent.'">
					<input type="hidden" class="form-control" name="coa_id" id="coa_id" value="'.$coa_id.'">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Akun:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Akun" name="inp_nama"  id="inp_nama" value="'.$kd_rkng_nama.'" required/>
						</div>
						<div class="form-group">
							<label>Unit Bisnis/Organisasi:</label>
							<div class="checkbox-inline">
								<label class="checkbox">
									<input type="checkbox" name="cek_klinik" id="cek_klinik" value="1" '.$checkedKlinik.' />
									<span></span>
									Klinik
								</label>
								&nbsp;&nbsp;
								<label class="checkbox">
									<input type="checkbox" name="cek_labrad" id="cek_labrad" value="1" '.$checkedLabrad.' />
									<span></span>
									Lab & Rad
								</label>
								&nbsp;&nbsp;
								<label class="checkbox">
									<input type="checkbox" name="cek_petcare" id="cek_petcare" value="1" '.$checkedPetcare.' />
									<span></span>
									Petcare
								</label>
								&nbsp;&nbsp;
								<label class="checkbox">
									<input type="checkbox" name="cek_apotek" id="cek_apotek" value="1" '.$checkedApotek.' />
									<span></span>
									Apotek
								</label>
							</div>
						</div>
					</div>';

		}
		else{
			echo '	<input type="hidden" class="form-control" name="tier" id="tier" value="'.$tier.'">
					<input type="hidden" class="form-control" name="coa_parent" id="coa_parent" value="'.$coa_parent.'">
					<input type="hidden" class="form-control" name="coa_id" id="coa_id" value="'.$coa_id.'">
					<div class="card-body">
						<div class="form-group">
							<label>Nama Akun:</label>
							<input type="text" class="form-control" placeholder="Isi Nama Akun" name="inp_nama"  id="inp_nama" value="'.$kd_rkng_nama.'" required/>
						</div>
					</div>';
		}
	}

    function aset($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		$aktiva_cabang		= 1;
		$aktiva_jenis		= 0;
		$aktiva_nomor		= '';
		$aktiva_nama		= '';
		$aktiva_tanggal_peroleh	= '';
		$aktiva_nilai_peroleh	= 0;
		$aktiva_nilai_sisa		= 0;
		$aktiva_nilai_buku		= 0;
		$aktiva_umur_ekonomis	= 0;
		$aktiva_susut			= '';
		$aktiva_aktif			= '';
		$aktiva_coa_biaya_penyusutan		= 0;
		$aktiva_coa_akumulasi_penyusutan	= 0;
		$checkedYaSusut			= '';
		$checkedTidakSusut		= '';
		$orderByrNum			= " ORDER BY aktiva_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_aktiva WHERE aktiva_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$aktiva_cabang		= $rrNum->aktiva_cabang;
			$aktiva_jenis			= $rrNum->aktiva_jenis;
			$aktiva_nomor			= $rrNum->aktiva_nomor;
			$aktiva_nama			= $rrNum->aktiva_nama;
			$aktiva_tanggal_peroleh	= $rrNum->aktiva_tanggal_peroleh;
			$aktiva_nilai_peroleh	= $rrNum->aktiva_nilai_peroleh;
			$aktiva_nilai_sisa		= $rrNum->aktiva_nilai_sisa;
			$aktiva_nilai_buku		= $rrNum->aktiva_nilai_buku;
			$aktiva_umur_ekonomis	= $rrNum->aktiva_umur_ekonomis;
			$aktiva_aktif			= $rrNum->aktiva_aktif;
			$aktiva_susut			= $rrNum->aktiva_aktif;
			$aktiva_coa_biaya_penyusutan		= $rrNum->aktiva_coa_biaya_penyusutan;
			$aktiva_coa_akumulasi_penyusutan	= $rrNum->aktiva_coa_akumulasi_penyusutan;

			$orderByrNum		= " ORDER BY aktiva_id=".$rNum." desc";
		}

		if($aktiva_susut=='t'){
			$checkedYaSusut	= 'checked="checked"';
		}
		else{
			$checkedTidakSusut	= 'checked="checked"';
		}

		if($mode == 'form'){
			$data['comboCabang']			= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_id > 0", "ref_cab_id", $aktiva_cabang);
			$conf_coa_jenis_aset			= $this->Mainmodel->get_one_data('conf_coa_jenis_aset', 'config', 'conf_id=1');
			$data['comboJenisAset']			= $this->Mainmodel->show_combo("kode_rekening", "kd_rkng_id", "kd_rkng_nama", "kd_rkng_id IN (".$conf_coa_jenis_aset.")", "kd_rkng_kode", $aktiva_jenis);
			$conf_coa_biaya_susut			= $this->Mainmodel->get_one_data('conf_coa_biaya_penyusutan', 'config', 'conf_id=1');
			$data['comboBiayaSusut']		= $this->Mainmodel->show_combo("kode_rekening", "kd_rkng_id", "kd_rkng_nama", "kd_rkng_id IN (".$conf_coa_biaya_susut.")", "kd_rkng_id", $aktiva_coa_biaya_penyusutan);
			$conf_coa_akum_susut			= $this->Mainmodel->get_one_data('conf_coa_akumulasi_penyusutan', 'config', 'conf_id=1');
			$data['comboAkumSusut']			= $this->Mainmodel->show_combo("kode_rekening", "kd_rkng_id", "kd_rkng_nama", "kd_rkng_id IN (".$conf_coa_akum_susut.")", "kd_rkng_id", $aktiva_coa_akumulasi_penyusutan);
			$data['aktiva_nomor']			= $aktiva_nomor;
			$data['aktiva_nama']			= $aktiva_nama;
			$data['aktiva_tanggal_peroleh']	= $aktiva_tanggal_peroleh;
			$data['aktiva_nilai_peroleh']	= $aktiva_nilai_peroleh;
			$data['aktiva_nilai_sisa']		= $aktiva_nilai_sisa;
			$data['aktiva_nilai_buku']		= $aktiva_nilai_buku;
			$data['aktiva_umur_ekonomis']	= $aktiva_umur_ekonomis;
			$data['checkedYaSusut']			= $checkedYaSusut;
			$data['checkedTidakSusut']		= $checkedTidakSusut;
			$data['main_content']   = 'master/asetform';
			$data['query_foto']		= $this->db->query("SELECT * FROM aktiva_foto_video WHERE aktfv_aktiva_id= ".$rNum);
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');

			$data = array(
				'aktiva_cabang'			=> $this->input->post('inp_cab_aset'),
				'aktiva_jenis'				=> $this->input->post('inp_jenis_aset'),
				'aktiva_nomor'				=> "'".$this->input->post('inp_no_aset')."'",
				'aktiva_nama'				=> "'".$this->input->post('inp_nama_aset')."'",
				'aktiva_tanggal_peroleh'	=> "'".$this->input->post('inp_tgl_oleh_aset')."'",
				'aktiva_nilai_peroleh'		=> $this->input->post('inp_nilai_oleh_aset'),
				'aktiva_nilai_sisa'			=> $this->input->post('inp_nilai_sisa_aset'),
				'aktiva_nilai_buku'			=> $this->input->post('inp_nilai_buku_aset'),
				'aktiva_umur_ekonomis'		=> $this->input->post('inp_umur_eko_aset'),
				'aktiva_susut'				=> "'".$this->input->post('inp_susut')."'",
				'aktiva_coa_biaya_penyusutan'		=> $this->input->post('inp_coa_biaya_aset'),
				'aktiva_coa_akumulasi_penyusutan'	=> $this->input->post('inp_coa_akum_aset'),
			);

			if($rNum > 0){
				$this->Mainmodel->update_table('aktiva', $data, 'aktiva_id='.$rNum);
			}
			else{
				$rNum	= $this->Mainmodel->insert_table('aktiva', $data);
			}

			$inp_ket_foto	= "Foto Aset"; 
			$config['upload_path']		= 'assets/file/foto_aset/';
			$config['allowed_types']	= 'gif|jpg|jpeg|png|pdf|mp4|mkv';
			$config['remove_spaces']	= FALSE; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$files	= $_FILES;
			$cpt	= count($_FILES['file_hasil']['name']);
			for($i=0; $i<$cpt; $i++){           
				$_FILES['file_hasil']['name']		= $files['file_hasil']['name'][$i];
				$_FILES['file_hasil']['type']		= $files['file_hasil']['type'][$i];
				$_FILES['file_hasil']['tmp_name']	= $files['file_hasil']['tmp_name'][$i];
				$_FILES['file_hasil']['error']		= $files['file_hasil']['error'][$i];
				$_FILES['file_hasil']['size']		= $files['file_hasil']['size'][$i];    
				$url	= "https://cscb.leonvets.id/assets/file/foto_aset/".$_FILES['file_hasil']['name'];
				
				if($this->upload->do_upload('file_hasil')){
					$data = array(
						'aktfv_peg_id'			=> $peg_id,
						'aktfv_aktiva_id'		=> $rNum,
						'aktfv_rm_url'			=> "'".pg_escape_string($url)."'",
						'aktfv_rm_nama'			=> "'".pg_escape_string( $files['file_hasil']['name'][$i])."'",
						'aktfv_rm_keterangan'	=> "'".pg_escape_string($inp_ket_foto)."'",
					);
					$this->Mainmodel->insert_table('aktiva_foto_video', $data);
				}
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'disabled'){
			$data = array(
				'aktiva_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('aktiva', $data, 'aktiva_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'aktiva_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('aktiva', $data, 'aktiva_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('aktiva', 'aktiva_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$data['aktiva_aktif']			= $aktiva_aktif;
		$data['query_hd']				= $this->db->query("SELECT * FROM v_aktiva WHERE aktiva_id > 0".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function kolega($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;

		$ph_nomor		= '';
		$ph_nama		= '';
		$ph_jenis		= 1;
		$ph_no_telp		= '';
		$ph_email		= '';
		$ph_alamat		= '';
		$ph_kecamatan	= 0;
		$ph_kota		= 0;
		$ph_prop		= 0;
		$ph_pic_nama	= '';
		$ph_pic_no_hp	= '';
		$ph_aktif	= '';
		$clauseComboKota	= ' AND ref_kota_id_prop=0';
		$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota=0';
		$orderByrNum	= " ORDER BY ph_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_pihak WHERE ph_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$ph_nomor	= $rrNum->ph_nomor;
			$ph_nama	= $rrNum->ph_nama;
			$ph_jenis	= $rrNum->ph_jenis;
			$ph_no_telp	= $rrNum->ph_no_telp;
			$ph_email	= $rrNum->ph_email;
			$ph_alamat	= $rrNum->ph_alamat;
			$ph_kecamatan	= $rrNum->ph_kecamatan;
			$ph_kota		= $rrNum->ref_kota_id;
			$ph_prop		= $rrNum->ref_prop_id;
			$ph_pic_nama	= $rrNum->ph_pic_nama;
			$ph_pic_no_hp	= $rrNum->ph_pic_no_hp;
			$ph_aktif	= $rrNum->ph_aktif;
			$orderByrNum		= " ORDER BY ph_id=".$rNum." desc";
			$clauseComboKota	= ' AND ref_kota_id_prop='.$ph_prop;
			$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$ph_kota;
			$data['comboDokter']	= $this->Mainmodel->show_combo("dokter", "dok_id", "dok_nama", "dok_aktif IS TRUE", "dok_nama",0);
		}

		if($mode == 'form'){
			$data['ph_nomor']		= $ph_nomor;
			$data['ph_nama']		= $ph_nama;
			$data['ph_no_telp']		= $ph_no_telp;
			$data['ph_email']		= $ph_email;
			$data['ph_alamat']		= $ph_alamat;
			$data['ph_pic_nama']	= $ph_pic_nama;
			$data['ph_pic_no_hp']	= $ph_pic_no_hp;
			$data['comboJenis']		= $this->Mainmodel->show_combo("pihak_jenis", "ph_jenis_id", "ph_jenis_prefix", "ph_jenis_id > 0", "ph_jenis_id", $ph_jenis);
			$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $ph_prop);
			$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $ph_kota);
			$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $ph_kecamatan);
			$data['main_content']   = 'master/kolegaform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
			$data = array(
				'ph_nomor'		=> "'".$this->input->post('inp_nomor')."'",
				'ph_nama'		=> "'".$this->input->post('inp_nama')."'",
				'ph_jenis'		=> $this->input->post('inp_jenis'),
				'ph_no_telp'	=> "'".$this->input->post('inp_telp')."'",
				'ph_email'		=> "'".$this->input->post('inp_email')."'",
				'ph_alamat'		=> "'".$this->input->post('inp_almt')."'",
				'ph_kecamatan'	=> $this->input->post('inp_kcmtn'),
				'ph_pic_nama'	=> "'".$this->input->post('inp_pic_nama')."'",
				'ph_pic_no_hp'	=> "'".$this->input->post('inp_pic_no_hp')."'",
			);

			if($rNum > 0){
				$data += ['ph_time_update' => "'now()'",  'ph_peg_update' => $peg_id];
				$this->Mainmodel->update_table('pihak', $data, 'ph_id='.$rNum);
			}
			else{
				$data += ['ph_peg_buat' => $peg_id];
				$rNum	= $this->Mainmodel->insert_table('pihak', $data);
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'disabled'){
			$data = array(
				'ph_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('pihak', $data, 'ph_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'ph_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('pihak', $data, 'ph_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('pihak', 'ph_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'crud_dokter'){
			$action		= $this->input->post('submit_crud_dokter');
			if($action == 'simpan_dokter'){
				$data = array(
					'ph_dok_pihak'		=> $rNum,
					'ph_dok_dokter'		=> $this->input->post('inp_dokter')
				);

				if($rNum2 > 0){
					$this->Mainmodel->update_table('pihak_dokter', $data, 'ph_dok_id='.$rNum2);
				}
				else{
					$this->Mainmodel->insert_table('pihak_dokter', $data);
				}
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
			}
		}
		elseif($mode == 'del_dokter'){
			$this->Mainmodel->delete_table('pihak_dokter', 'ph_dok_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		$data['ph_aktif']		= $ph_aktif;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_pihak WHERE ph_kolega IS TRUE".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function dokter($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		$dok_nama		= '';
		$dok_no_hp_wa	= '';
		$dok_email		= '';
		$dok_tempat_lahir	= '';
		$dok_tanggal_lahir	= '';
		$dok_gender		= 'P';
		$dok_alamat		= '';
		$dok_kecamatan	= 0;
		$dok_kota		= 0;
		$dok_prop		= 0;
		$dok_aktif		= '';
		$checkedPria	= '';
		$checkedWanita	= '';
		$clauseComboKota	= ' AND ref_kota_id_prop=0';
		$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota=0';
		$orderByrNum		= " ORDER BY dok_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_dokter WHERE dok_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$dok_nama		= $rrNum->dok_nama;
			$dok_no_hp_wa	= $rrNum->dok_no_hp_wa;
			$dok_email		= $rrNum->dok_email;
			$dok_tempat_lahir	= $rrNum->dok_tempat_lahir;
			$dok_tanggal_lahir	= $rrNum->dok_tanggal_lahir;
			$dok_gender		= $rrNum->dok_gender;
			$dok_alamat		= $rrNum->dok_alamat;
			$dok_kecamatan	= $rrNum->dok_kecamatan;
			$dok_kota		= $rrNum->ref_kota_id;
			$dok_prop		= $rrNum->ref_prop_id;
			$dok_aktif		= $rrNum->dok_aktif;
			$orderByrNum		= " ORDER BY dok_id=".$rNum." desc";
			$clauseComboKota	= ' AND ref_kota_id_prop='.$dok_prop;
			$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$dok_kota;
			if($dok_gender=='P'){
				$checkedPria	= 'checked="checked"';
			}
			else{
				$checkedWanita	= 'checked="checked"';
			}
		}

		if($mode == 'form'){
			$data['dok_nama']		= $dok_nama;
			$data['dok_no_hp_wa']	= $dok_no_hp_wa;
			$data['dok_email']		= $dok_email;
			$data['dok_tempat_lahir']	= $dok_tempat_lahir;
			$data['dok_tanggal_lahir']	= $dok_tanggal_lahir;
			$data['dok_gender']		= $dok_gender;
			$data['dok_alamat']		= $dok_alamat;
			$data['checkedPria']	= $checkedPria;
			$data['checkedWanita']	= $checkedWanita;
			$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $dok_prop);
			$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $dok_kota);
			$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $dok_kecamatan);
			$data['main_content']   = 'master/dokterform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
			$data = array(
				'dok_nama'		=> "'".$this->input->post('inp_nama')."'",
				'dok_gender'	=> "'".$this->input->post('inp_jk')."'",
				'dok_no_hp_wa'	=> "'".$this->input->post('inp_telp')."'",
				'dok_email'		=> "'".$this->input->post('inp_email')."'",
				'dok_alamat'	=> "'".$this->input->post('inp_almt')."'",
				'dok_kecamatan'	=> $this->input->post('inp_kcmtn'),
			);

			if($rNum > 0){
				$data += ['dok_time_update' => "'now()'",  'dok_peg_update' => $peg_id];
				$this->Mainmodel->update_table('dokter', $data, 'dok_id='.$rNum);
			}
			else{
				$data += ['dok_peg_buat' => $peg_id];
				$rNum	= $this->Mainmodel->insert_table('dokter', $data);
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'disabled'){
			$data = array(
				'dok_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('dokter', $data, 'dok_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'dok_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('dokter', $data, 'dok_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('dokter', 'dok_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}

		$data['dok_aktif']		= $dok_aktif;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_dokter WHERE dok_id > 0".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function supplier($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		$ph_nomor		= '';
		$ph_nama		= '';
		$ph_jenis		= 1;
		$ph_no_telp		= '';
		$ph_email		= '';
		$ph_alamat		= '';
		$ph_kecamatan	= 0;
		$ph_kota		= 0;
		$ph_prop		= 0;
		$ph_pic_nama	= '';
		$ph_pic_no_hp	= '';
		$ph_aktif	= '';
		$clauseComboKota	= ' AND ref_kota_id_prop=0';
		$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota=0';
		$orderByrNum	= " ORDER BY ph_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_pihak WHERE ph_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$ph_nomor	= $rrNum->ph_nomor;
			$ph_nama	= $rrNum->ph_nama;
			$ph_jenis	= $rrNum->ph_jenis;
			$ph_no_telp	= $rrNum->ph_no_telp;
			$ph_email	= $rrNum->ph_email;
			$ph_alamat	= $rrNum->ph_alamat;
			$ph_kecamatan	= $rrNum->ph_kecamatan;
			$ph_kota		= $rrNum->ref_kota_id;
			$ph_prop		= $rrNum->ref_prop_id;
			$ph_pic_nama	= $rrNum->ph_pic_nama;
			$ph_pic_no_hp	= $rrNum->ph_pic_no_hp;
			$ph_aktif	= $rrNum->ph_aktif;
			$orderByrNum		= " ORDER BY ph_id=".$rNum." desc";
			$clauseComboKota	= ' AND ref_kota_id_prop='.$ph_prop;
			$clauseComboKcmtn	= ' AND ref_kcmtn_id_kota='.$ph_kota;
		}

		if($mode == 'form'){
			$data['ph_nomor']		= $ph_nomor;
			$data['ph_nama']		= $ph_nama;
			$data['ph_no_telp']		= $ph_no_telp;
			$data['ph_email']		= $ph_email;
			$data['ph_alamat']		= $ph_alamat;
			$data['ph_pic_nama']	= $ph_pic_nama;
			$data['ph_pic_no_hp']	= $ph_pic_no_hp;
			$data['comboJenis']		= $this->Mainmodel->show_combo("pihak_jenis", "ph_jenis_id", "ph_jenis_prefix", "ph_jenis_id > 0", "ph_jenis_id", $ph_jenis);
			$data['comboPropinsi']	= $this->Mainmodel->show_combo("ref_propinsi", "ref_prop_id", "ref_prop_ket", "ref_prop_id > 0", "ref_prop_id", $ph_prop);
			$data['comboKota']		= $this->Mainmodel->show_combo("ref_kota", "ref_kota_id", "ref_kota_ket", "ref_kota_id > 0".$clauseComboKota, "ref_kota_id", $ph_kota);
			$data['comboKecamatan']	= $this->Mainmodel->show_combo("ref_kecamatan", "ref_kcmtn_id", "ref_kcmtn_ket", "ref_kcmtn_id > 0".$clauseComboKcmtn, "ref_kcmtn_id", $ph_kecamatan);
			$data['main_content']   = 'master/supplierform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
			$data = array(
				'ph_nomor'		=> "'".$this->input->post('inp_nomor')."'",
				'ph_nama'		=> "'".$this->input->post('inp_nama')."'",
				'ph_jenis'		=> $this->input->post('inp_jenis'),
				'ph_no_telp'	=> "'".$this->input->post('inp_telp')."'",
				'ph_email'		=> "'".$this->input->post('inp_email')."'",
				'ph_alamat'		=> "'".$this->input->post('inp_almt')."'",
				'ph_kecamatan'	=> $this->input->post('inp_kcmtn'),
				'ph_pic_nama'	=> "'".$this->input->post('inp_pic_nama')."'",
				'ph_pic_no_hp'	=> "'".$this->input->post('inp_pic_no_hp')."'",
			);

			if($rNum > 0){
				$data += ['ph_time_update' => "'now()'",  'ph_peg_update' => $peg_id];
				$this->Mainmodel->update_table('pihak', $data, 'ph_id='.$rNum);
			}
			else{
				$data += ['ph_peg_buat' => $peg_id, 'ph_kolega' => "'FALSE'", 'ph_supplier' => "'TRUE'"];
				$rNum	= $this->Mainmodel->insert_table('pihak', $data);
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'disabled'){
			$data = array(
				'ph_aktif'	=> "'false'"
			);
			$this->Mainmodel->update_table('pihak', $data, 'ph_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'ph_aktif'	=> "'true'"
			);
			$this->Mainmodel->update_table('pihak', $data, 'ph_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('pihak', 'ph_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'del_foto'){
			$this->Mainmodel->delete_table('aktiva_foto_video', 'aktfv_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
		}

		$data['ph_aktif']		= $ph_aktif;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_pihak WHERE ph_supplier IS TRUE".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function produk($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
        $data['readonly_menu']  = $list_menu->ref_mn_to_grp_readonly;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		$ref_prod_kode					= '';
		$ref_prod_nama					= '';
		$ref_prod_nama_invoice			= '';
		$ref_prod_harga					= 0;
		$ref_prod_harga_beli			= 0;
		$ref_prod_link_toped			= '';
		$ref_prod_jenis					= 0;
		$ref_prod_min_stok				= 1;
		$ref_prod_satuan_beli			= 0;
		$ref_prod_satuan_stok			= 0;
		$ref_prod_satuan_pakai			= 0;
		$ref_prod_rasio_beli_stok		= 1;
		$ref_prod_rasio_stok_pakai		= 1;
		$ref_prod_coa_pendapatan		= 0;
		$ref_prod_coa_biaya				= 0;
		$ref_prod_coa_persediaan		= 0;
		$ref_prod_aktif					= '';
		$ref_prod_klinik				= 'f';
		$ref_prod_labrad				= 'f';
		$ref_prod_petcare				= 'f';
		$ref_prod_apotek				= 'f';

		$orderByrNum	= " ORDER BY ref_prod_klinik,ref_prod_labrad,ref_prod_petcare,ref_prod_apotek,ref_prod_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_produk WHERE ref_prod_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$ref_prod_kode					= $rrNum->ref_prod_kode;
			$ref_prod_nama					= $rrNum->ref_prod_nama;
			$ref_prod_nama_invoice			= $rrNum->ref_prod_nama_invoice;
			$ref_prod_harga					= $rrNum->ref_prod_harga;
			$ref_prod_harga_beli			= $rrNum->ref_prod_harga_beli;
			$ref_prod_link_toped			= $rrNum->ref_prod_link_toped;
			$ref_prod_jenis					= $rrNum->ref_prod_jenis;
			$ref_prod_min_stok				= $rrNum->ref_prod_min_stok;
			$ref_prod_satuan_beli			= $rrNum->ref_prod_satuan_beli;
			$ref_prod_satuan_stok			= $rrNum->ref_prod_satuan_stok;
			$ref_prod_satuan_pakai			= $rrNum->ref_prod_satuan_pakai;
			$ref_prod_rasio_beli_stok		= $rrNum->ref_prod_rasio_beli_stok;
			$ref_prod_rasio_stok_pakai		= $rrNum->ref_prod_rasio_stok_pakai;
			$ref_prod_coa_pendapatan		= $rrNum->ref_prod_coa_pendapatan;
			$ref_prod_coa_biaya				= $rrNum->ref_prod_coa_biaya;
			$ref_prod_coa_persediaan		= $rrNum->ref_prod_coa_persediaan;
			$ref_prod_aktif					= $rrNum->ref_prod_aktif;
			$ref_prod_klinik				= $rrNum->ref_prod_klinik;
			$ref_prod_labrad				= $rrNum->ref_prod_labrad;
			$ref_prod_petcare				= $rrNum->ref_prod_petcare;
			$ref_prod_apotek				= $rrNum->ref_prod_apotek;
			$orderByrNum		= " ORDER BY ref_prod_id=".$rNum." desc";
			$data['comboProduk']= $this->Mainmodel->show_combo("ref_produk", "ref_prod_id", "ref_prod_nama", "ref_prod_id NOT IN (SELECT ref_prod_dt_child FROM ref_produk_detail WHERE ref_prod_dt_parent=".$rNum.") AND ref_prod_id <> ".$rNum."", "ref_prod_nama", 0);
			$data['query_dt']	= $this->db->query("SELECT * FROM ref_produk_detail JOIN v_produk ON ref_prod_dt_child=ref_prod_id WHERE ref_prod_dt_parent =".$rNum);
		}

		if($mode == 'form'){
			($ref_prod_klinik == 't') ? $checkedKlinik = 'checked' : $checkedKlinik = '';
			($ref_prod_labrad == 't') ? $checkedLabrad = 'checked' : $checkedLabrad = '';
			($ref_prod_petcare == 't') ? $checkedPetcare = 'checked' : $checkedPetcare = '';
			($ref_prod_apotek == 't') ? $checkedApotek = 'checked' : $checkedApotek = '';

			$data['ref_prod_nama']				= $ref_prod_nama;
			$data['ref_prod_nama_invoice']		= $ref_prod_nama_invoice;
			$data['ref_prod_kode']				= $ref_prod_kode;
			$data['ref_prod_harga']				= $ref_prod_harga;
			$data['ref_prod_harga_beli']		= $ref_prod_harga_beli;
			$data['ref_prod_link_toped']		= $ref_prod_link_toped;
			$data['ref_prod_min_stok']			= $ref_prod_min_stok;
			$data['ref_prod_rasio_beli_stok']	= $ref_prod_rasio_beli_stok;
			$data['ref_prod_rasio_stok_pakai']	= $ref_prod_rasio_stok_pakai;
			$data['comboJenis']			= $this->Mainmodel->show_combo("ref_jenis_produk", "ref_jns_prod_id", "ref_jns_prod_nama", "ref_jns_prod_id > 0", "ref_jns_prod_nama", $ref_prod_jenis);
			$data['comboSatPmbln']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_beli);
			$data['comboSatPnjln']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_stok);
			$data['comboSatPakai']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_pakai);
			
			/*
			$conf_parent_coa_pendapatan	= $this->Mainmodel->get_one_data('conf_parent_coa_pendapatan', 'config', 'conf_id=1');
			$data['comboCoaPndptn']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_pendapatan.")", "kd_rkng_kode", $ref_prod_coa_pendapatan);
			$conf_parent_coa_biaya		= $this->Mainmodel->get_one_data('conf_parent_coa_biaya', 'config', 'conf_id=1');
			$data['comboCoaBiaya']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_biaya.")", "kd_rkng_kode", $ref_prod_coa_biaya);
			$conf_parent_coa_persediaan	= $this->Mainmodel->get_one_data('conf_parent_coa_persediaan', 'config', 'conf_id=1');
			$data['comboCoaPrsdn']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_persediaan.")", "kd_rkng_kode", $ref_prod_coa_persediaan);
			*/
			$data['comboCoaPndptn']		= $this->Mainmodel->show_combo_kode_rekening_full("", $ref_prod_coa_pendapatan);
			$data['comboCoaBiaya']		= $this->Mainmodel->show_combo_kode_rekening_full("", $ref_prod_coa_biaya);
			$data['comboCoaPrsdn']		= $this->Mainmodel->show_combo_kode_rekening_full("", $ref_prod_coa_persediaan);

			$data['checkedKlinik']		= $checkedKlinik;
			$data['checkedLabrad']		= $checkedLabrad;
			$data['checkedPetcare']		= $checkedPetcare;
			$data['checkedApotek']		= $checkedApotek;

			$data['query_foto']			= $this->db->query("SELECT * FROM ref_produk_foto_video WHERE ref_prod_fv_produk_id= ".$rNum);
			$data['main_content']		= 'master/produkform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
			$data = array(
				'ref_prod_kode'					=> "'".$this->input->post('inp_kode')."'",
				'ref_prod_nama'					=> "'".$this->input->post('inp_nama')."'",
				'ref_prod_nama_invoice'			=> "'".$this->input->post('inp_nama_invoice')."'",
				'ref_prod_jenis'				=> $this->input->post('inp_jenis'),
				'ref_prod_min_stok'				=> $this->input->post('inp_min_stok'),
				'ref_prod_satuan_beli'			=> $this->input->post('inp_sat_beli'),
				'ref_prod_rasio_beli_stok'		=> $this->input->post('inp_rasio'),
				'ref_prod_satuan_stok'			=> $this->input->post('inp_sat_stok'),
				'ref_prod_rasio_stok_pakai'		=> $this->input->post('inp_rasio_pakai'),
				'ref_prod_satuan_pakai'			=> $this->input->post('inp_sat_pakai'),
				'ref_prod_harga'				=> $this->input->post('inp_hrg_jual'),
				'ref_prod_harga_beli'			=> $this->input->post('inp_hrg_beli'),
			);
			if($this->input->post('cek_klinik') > 0){
				$data += ['ref_prod_klinik'	=> 'TRUE'];
			}
			else{
				$data += ['ref_prod_klinik'	=> 'FALSE'];
			}
			if($this->input->post('cek_labrad') > 0){
				$data += ['ref_prod_labrad'	=> 'TRUE'];
			}
			else{
				$data += ['ref_prod_labrad'	=> 'FALSE'];
			}
			if($this->input->post('cek_petcare') > 0){
				$data += ['ref_prod_petcare' => 'TRUE'];
			}
			else{
				$data += ['ref_prod_petcare'	=> 'FALSE'];
			}
			if($this->input->post('cek_apotek') > 0){
				$data += ['ref_prod_apotek'	=> 'TRUE'];
			}
			else{
				$data += ['ref_prod_apotek'	=> 'FALSE'];
			}

			if(strlen($this->input->post('inp_link')) > 0){
				$data += ['ref_prod_link_toped'	=> "'".$this->input->post('inp_link')."'"];
			}

			if($this->input->post('inp_coa_pndptn') > 0){
				$data += ['ref_prod_coa_pendapatan'	=> $this->input->post('inp_coa_pndptn')];
			}

			if($this->input->post('inp_coa_biaya') > 0){
				$data += ['ref_prod_coa_biaya'	=> $this->input->post('inp_coa_biaya')];
			}

			if($this->input->post('inp_coa_prsdn') > 0){
				$data += ['ref_prod_coa_persediaan'	=> $this->input->post('inp_coa_prsdn')];
			}
			
			if($rNum > 0){
				$data += ['ref_prod_time_update' => "'now()'",  'ref_prod_peg_update' => $peg_id];
				$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			}
			else{
				$data += ['ref_prod_peg_buat' => $peg_id];
				$rNum	= $this->Mainmodel->insert_table('ref_produk', $data);
			}

			$inp_ket_foto	= "Foto Produk"; 
			$config['upload_path']		= 'assets/file/foto_produk/';
			$config['allowed_types']	= 'gif|jpg|jpeg|png|pdf|mp4|mkv';
			$config['remove_spaces']	= FALSE; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$files	= $_FILES;
			$cpt	= count($_FILES['file_hasil']['name']);
			for($i=0; $i<$cpt; $i++){           
				$_FILES['file_hasil']['name']		= $files['file_hasil']['name'][$i];
				$_FILES['file_hasil']['type']		= $files['file_hasil']['type'][$i];
				$_FILES['file_hasil']['tmp_name']	= $files['file_hasil']['tmp_name'][$i];
				$_FILES['file_hasil']['error']		= $files['file_hasil']['error'][$i];
				$_FILES['file_hasil']['size']		= $files['file_hasil']['size'][$i];    
				$url	= "https://cscb.leonvets.id/assets/file/foto_produk/".$_FILES['file_hasil']['name'];
				
				if($this->upload->do_upload('file_hasil')){
					$data = array(
						'ref_prod_fv_peg_id'	=> $peg_id,
						'ref_prod_fv_produk_id'	=> $rNum,
						'ref_prod_fv_rm_url'	=> "'".pg_escape_string($url)."'",
						'ref_prod_fv_rm_nama'	=> "'".pg_escape_string( $files['file_hasil']['name'][$i])."'",
						'ref_prod_fv_rm_keterangan'	=> "'".pg_escape_string($inp_ket_foto)."'",
					);
					$this->Mainmodel->insert_table('ref_produk_foto_video', $data);
				}
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'crud_detail'){
			$data = array(
				'ref_prod_dt_parent'	=> $rNum,
				'ref_prod_dt_child'		=> $this->input->post('inp_dt_prod'),
				'ref_prod_dt_jumlah_pakai'		=> $this->input->post('inp_dt_jml'),
			);
			$this->Mainmodel->insert_table('ref_produk_detail', $data);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'del_detail'){
			$this->Mainmodel->delete_table('ref_produk_detail', 'ref_prod_dt_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);

		}
		elseif($mode == 'disabled'){
			$data = array(
				'ref_prod_aktif'		=> "'false'",
				'ref_prod_time_update'	=> "'now()'", 
				'ref_prod_peg_update'	=> $peg_id
			);
			$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'ref_prod_aktif'	=> "'true'",
				'ref_prod_time_update'	=> "'now()'", 
				'ref_prod_peg_update'	=> $peg_id
			);
			$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('ref_produk', 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'del_foto'){
			$this->Mainmodel->delete_table('ref_produk_foto_video', 'ref_prod_fv_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
		}

		$data['ref_prod_aktif']	= $ref_prod_aktif;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_produk WHERE ref_prod_id > 0".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function produkklinik($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		$ref_prod_kode					= '';
		$ref_prod_nama					= '';
		$ref_prod_harga					= 0;
		$ref_prod_harga_beli			= 0;
		$ref_prod_link_toped			= '';
		$ref_prod_jenis					= 0;
		$ref_prod_min_stok				= 0;
		$ref_prod_satuan_beli			= 0;
		$ref_prod_satuan_stok			= 0;
		$ref_prod_rasio_beli_stok		= 1;
		$ref_prod_coa_pendapatan		= 0;
		$ref_prod_coa_biaya				= 0;
		$ref_prod_coa_persediaan		= 0;
		$ref_prod_aktif					= '';
		$orderByrNum	= " ORDER BY ref_prod_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_produk WHERE ref_prod_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$ref_prod_kode					= $rrNum->ref_prod_kode;
			$ref_prod_nama					= $rrNum->ref_prod_nama;
			$ref_prod_harga					= $rrNum->ref_prod_harga;
			$ref_prod_harga_beli			= $rrNum->ref_prod_harga_beli;
			$ref_prod_link_toped			= $rrNum->ref_prod_link_toped;
			$ref_prod_jenis					= $rrNum->ref_prod_jenis;
			$ref_prod_min_stok				= $rrNum->ref_prod_min_stok;
			$ref_prod_satuan_beli			= $rrNum->ref_prod_satuan_beli;
			$ref_prod_satuan_stok			= $rrNum->ref_prod_satuan_stok;
			$ref_prod_rasio_beli_stok		= $rrNum->ref_prod_rasio_beli_stok;
			$ref_prod_coa_pendapatan		= $rrNum->ref_prod_coa_pendapatan;
			$ref_prod_coa_biaya				= $rrNum->ref_prod_coa_biaya;
			$ref_prod_coa_persediaan		= $rrNum->ref_prod_coa_persediaan;
			$ref_prod_aktif					= $rrNum->ref_prod_aktif;
			$orderByrNum		= " ORDER BY ref_prod_id=".$rNum." desc";
			$data['comboProduk']= $this->Mainmodel->show_combo("ref_produk", "ref_prod_id", "ref_prod_nama", "ref_prod_id NOT IN (SELECT ref_prod_dt_child FROM ref_produk_detail WHERE ref_prod_dt_parent=".$rNum.") AND ref_prod_id <> ".$rNum."", "ref_prod_nama", 0);
			$data['query_dt']	= $this->db->query("SELECT * FROM ref_produk_detail JOIN v_produk ON ref_prod_dt_child=ref_prod_id WHERE ref_prod_dt_parent =".$rNum);
		}

		if($mode == 'form'){
			$data['ref_prod_nama']				= $ref_prod_nama;
			$data['ref_prod_kode']				= $ref_prod_kode;
			$data['ref_prod_harga']				= $ref_prod_harga;
			$data['ref_prod_harga_beli']		= $ref_prod_harga_beli;
			$data['ref_prod_link_toped']		= $ref_prod_link_toped;
			$data['ref_prod_min_stok']			= $ref_prod_min_stok;
			$data['ref_prod_rasio_beli_stok']	= $ref_prod_rasio_beli_stok;
			$data['comboJenis']			= $this->Mainmodel->show_combo("ref_jenis_produk", "ref_jns_prod_id", "ref_jns_prod_nama", "ref_jns_prod_id > 0", "ref_jns_prod_nama", $ref_prod_jenis);
			$data['comboSatPnjln']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_stok);
			$data['comboSatPmbln']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_beli);
			$conf_parent_coa_pendapatan	= $this->Mainmodel->get_one_data('conf_parent_coa_pendapatan', 'config', 'conf_id=1');
			$data['comboCoaPndptn']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_pendapatan.")", "kd_rkng_kode", $ref_prod_coa_pendapatan);
			$conf_parent_coa_biaya		= $this->Mainmodel->get_one_data('conf_parent_coa_biaya', 'config', 'conf_id=1');
			$data['comboCoaBiaya']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_biaya.")", "kd_rkng_kode", $ref_prod_coa_biaya);
			$conf_parent_coa_persediaan	= $this->Mainmodel->get_one_data('conf_parent_coa_persediaan', 'config', 'conf_id=1');
			$data['comboCoaPrsdn']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_persediaan.")", "kd_rkng_kode", $ref_prod_coa_persediaan);
			$data['query_foto']			= $this->db->query("SELECT * FROM ref_produk_foto_video WHERE ref_prod_fv_produk_id= ".$rNum);
			$data['main_content']		= 'master/produkform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
			$data = array(
				'ref_prod_kode'					=> "'".$this->input->post('inp_kode')."'",
				'ref_prod_nama'					=> "'".$this->input->post('inp_nama')."'",
				'ref_prod_jenis'				=> $this->input->post('inp_jenis'),
				'ref_prod_min_stok'				=> $this->input->post('inp_min_stok'),
				'ref_prod_satuan_beli'			=> $this->input->post('inp_sat_beli'),
				'ref_prod_rasio_beli_stok'		=> $this->input->post('inp_rasio'),
				'ref_prod_satuan_stok'			=> $this->input->post('inp_sat_stok'),
				'ref_prod_harga'				=> $this->input->post('inp_hrg_jual'),
				'ref_prod_harga_beli'			=> $this->input->post('inp_hrg_beli'),
			);

			if(strlen($this->input->post('inp_link')) > 0){
				$data += ['ref_prod_link_toped'	=> "'".$this->input->post('inp_jenis')."'"];
			}

			if($this->input->post('inp_coa_pndptn') > 0){
				$data += ['ref_prod_coa_pendapatan'	=> $this->input->post('inp_coa_pndptn')];
			}

			if($this->input->post('inp_coa_biaya') > 0){
				$data += ['ref_prod_coa_biaya'	=> $this->input->post('inp_coa_biaya')];
			}

			if($this->input->post('inp_coa_prsdn') > 0){
				$data += ['ref_prod_coa_persediaan'	=> $this->input->post('inp_coa_prsdn')];
			}
			
			if($rNum > 0){
				$data += ['ref_prod_time_update' => "'now()'",  'ref_prod_peg_update' => $peg_id];
				$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			}
			else{
				$data += ['ref_prod_peg_buat' => $peg_id];
				$rNum	= $this->Mainmodel->insert_table('ref_produk', $data);
			}

			$inp_ket_foto	= "Foto Produk"; 
			$config['upload_path']		= 'assets/file/foto_produk/';
			$config['allowed_types']	= 'gif|jpg|jpeg|png|pdf|mp4|mkv';
			$config['remove_spaces']	= FALSE; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$files	= $_FILES;
			$cpt	= count($_FILES['file_hasil']['name']);
			for($i=0; $i<$cpt; $i++){           
				$_FILES['file_hasil']['name']		= $files['file_hasil']['name'][$i];
				$_FILES['file_hasil']['type']		= $files['file_hasil']['type'][$i];
				$_FILES['file_hasil']['tmp_name']	= $files['file_hasil']['tmp_name'][$i];
				$_FILES['file_hasil']['error']		= $files['file_hasil']['error'][$i];
				$_FILES['file_hasil']['size']		= $files['file_hasil']['size'][$i];    
				$url	= "https://cscb.leonvets.id/assets/file/foto_produk/".$_FILES['file_hasil']['name'];
				
				if($this->upload->do_upload('file_hasil')){
					$data = array(
						'ref_prod_fv_peg_id'	=> $peg_id,
						'ref_prod_fv_produk_id'	=> $rNum,
						'ref_prod_fv_rm_url'	=> "'".pg_escape_string($url)."'",
						'ref_prod_fv_rm_nama'	=> "'".pg_escape_string( $files['file_hasil']['name'][$i])."'",
						'ref_prod_fv_rm_keterangan'	=> "'".pg_escape_string($inp_ket_foto)."'",
					);
					$this->Mainmodel->insert_table('ref_produk_foto_video', $data);
				}
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'crud_detail'){
			$data = array(
				'ref_prod_dt_parent'	=> $rNum,
				'ref_prod_dt_child'		=> $this->input->post('inp_dt_prod'),
			);
			$this->Mainmodel->insert_table('ref_produk_detail', $data);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'del_detail'){
			$this->Mainmodel->delete_table('ref_produk_detail', 'ref_prod_dt_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);

		}
		elseif($mode == 'disabled'){
			$data = array(
				'ref_prod_aktif'		=> "'false'",
				'ref_prod_time_update'	=> "'now()'", 
				'ref_prod_peg_update'	=> $peg_id
			);
			$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'ref_prod_aktif'	=> "'true'",
				'ref_prod_time_update'	=> "'now()'", 
				'ref_prod_peg_update'	=> $peg_id
			);
			$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('ref_produk', 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'del_foto'){
			$this->Mainmodel->delete_table('ref_produk_foto_video', 'ref_prod_fv_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
		}

		$data['ref_prod_aktif']	= $ref_prod_aktif;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_produk WHERE ref_prod_id > 0".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function produklabrad($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;

		$ref_prod_kode					= '';
		$ref_prod_nama					= '';
		$ref_prod_harga					= 0;
		$ref_prod_harga_beli			= 0;
		$ref_prod_link_toped			= '';
		$ref_prod_jenis					= 0;
		$ref_prod_min_stok				= 0;
		$ref_prod_satuan_beli			= 0;
		$ref_prod_satuan_stok			= 0;
		$ref_prod_rasio_beli_stok		= 1;
		$ref_prod_coa_pendapatan		= 0;
		$ref_prod_coa_biaya				= 0;
		$ref_prod_coa_persediaan		= 0;
		$ref_prod_aktif					= '';
		$orderByrNum	= " ORDER BY ref_prod_nama";
		
		if($rNum > 0){
			$query_rnum		= "	SELECT * FROM v_produk WHERE ref_prod_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$ref_prod_kode					= $rrNum->ref_prod_kode;
			$ref_prod_nama					= $rrNum->ref_prod_nama;
			$ref_prod_harga					= $rrNum->ref_prod_harga;
			$ref_prod_harga_beli			= $rrNum->ref_prod_harga_beli;
			$ref_prod_link_toped			= $rrNum->ref_prod_link_toped;
			$ref_prod_jenis					= $rrNum->ref_prod_jenis;
			$ref_prod_min_stok				= $rrNum->ref_prod_min_stok;
			$ref_prod_satuan_beli			= $rrNum->ref_prod_satuan_beli;
			$ref_prod_satuan_stok			= $rrNum->ref_prod_satuan_stok;
			$ref_prod_rasio_beli_stok		= $rrNum->ref_prod_rasio_beli_stok;
			$ref_prod_coa_pendapatan		= $rrNum->ref_prod_coa_pendapatan;
			$ref_prod_coa_biaya				= $rrNum->ref_prod_coa_biaya;
			$ref_prod_coa_persediaan		= $rrNum->ref_prod_coa_persediaan;
			$ref_prod_aktif					= $rrNum->ref_prod_aktif;
			$orderByrNum		= " ORDER BY ref_prod_id=".$rNum." desc";
			$data['comboProduk']= $this->Mainmodel->show_combo("ref_produk", "ref_prod_id", "ref_prod_nama", "ref_prod_id NOT IN (SELECT ref_prod_dt_child FROM ref_produk_detail WHERE ref_prod_dt_parent=".$rNum.") AND ref_prod_id <> ".$rNum."", "ref_prod_nama", 0);
			$data['query_dt']	= $this->db->query("SELECT * FROM ref_produk_detail JOIN v_produk ON ref_prod_dt_child=ref_prod_id WHERE ref_prod_dt_parent =".$rNum);
		}

		if($mode == 'form'){
			$data['ref_prod_nama']				= $ref_prod_nama;
			$data['ref_prod_kode']				= $ref_prod_kode;
			$data['ref_prod_harga']				= $ref_prod_harga;
			$data['ref_prod_harga_beli']		= $ref_prod_harga_beli;
			$data['ref_prod_link_toped']		= $ref_prod_link_toped;
			$data['ref_prod_min_stok']			= $ref_prod_min_stok;
			$data['ref_prod_rasio_beli_stok']	= $ref_prod_rasio_beli_stok;
			$data['comboJenis']			= $this->Mainmodel->show_combo("ref_jenis_produk", "ref_jns_prod_id", "ref_jns_prod_nama", "ref_jns_prod_id > 0", "ref_jns_prod_nama", $ref_prod_jenis);
			$data['comboSatPnjln']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_stok);
			$data['comboSatPmbln']		= $this->Mainmodel->show_combo("ref_satuan", "ref_sat_id", "ref_sat_ket", "ref_sat_id > 0", "ref_sat_ket", $ref_prod_satuan_beli);
			$conf_parent_coa_pendapatan	= $this->Mainmodel->get_one_data('conf_parent_coa_pendapatan', 'config', 'conf_id=1');
			$data['comboCoaPndptn']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_pendapatan.")", "kd_rkng_kode", $ref_prod_coa_pendapatan);
			$conf_parent_coa_biaya		= $this->Mainmodel->get_one_data('conf_parent_coa_biaya', 'config', 'conf_id=1');
			$data['comboCoaBiaya']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_biaya.")", "kd_rkng_kode", $ref_prod_coa_biaya);
			$conf_parent_coa_persediaan	= $this->Mainmodel->get_one_data('conf_parent_coa_persediaan', 'config', 'conf_id=1');
			$data['comboCoaPrsdn']		= $this->Mainmodel->show_combo("v_kode_rekening", "kd_rkng_id", "full_kode", "kd_rkng_parent IN (".$conf_parent_coa_persediaan.")", "kd_rkng_kode", $ref_prod_coa_persediaan);
			$data['query_foto']			= $this->db->query("SELECT * FROM ref_produk_foto_video WHERE ref_prod_fv_produk_id= ".$rNum);
			$data['main_content']		= 'master/produkform';
		}
		elseif($mode == 'crud'){
			$action		= $this->input->post('action_crud');
			$data = array(
				'ref_prod_kode'					=> "'".$this->input->post('inp_kode')."'",
				'ref_prod_nama'					=> "'".$this->input->post('inp_nama')."'",
				'ref_prod_jenis'				=> $this->input->post('inp_jenis'),
				'ref_prod_min_stok'				=> $this->input->post('inp_min_stok'),
				'ref_prod_satuan_beli'			=> $this->input->post('inp_sat_beli'),
				'ref_prod_rasio_beli_stok'		=> $this->input->post('inp_rasio'),
				'ref_prod_satuan_stok'			=> $this->input->post('inp_sat_stok'),
				'ref_prod_harga'				=> $this->input->post('inp_hrg_jual'),
				'ref_prod_harga_beli'			=> $this->input->post('inp_hrg_beli'),
			);

			if(strlen($this->input->post('inp_link')) > 0){
				$data += ['ref_prod_link_toped'	=> "'".$this->input->post('inp_jenis')."'"];
			}

			if($this->input->post('inp_coa_pndptn') > 0){
				$data += ['ref_prod_coa_pendapatan'	=> $this->input->post('inp_coa_pndptn')];
			}

			if($this->input->post('inp_coa_biaya') > 0){
				$data += ['ref_prod_coa_biaya'	=> $this->input->post('inp_coa_biaya')];
			}

			if($this->input->post('inp_coa_prsdn') > 0){
				$data += ['ref_prod_coa_persediaan'	=> $this->input->post('inp_coa_prsdn')];
			}
			
			if($rNum > 0){
				$data += ['ref_prod_time_update' => "'now()'",  'ref_prod_peg_update' => $peg_id];
				$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			}
			else{
				$data += ['ref_prod_peg_buat' => $peg_id];
				$rNum	= $this->Mainmodel->insert_table('ref_produk', $data);
			}

			$inp_ket_foto	= "Foto Produk"; 
			$config['upload_path']		= 'assets/file/foto_produk/';
			$config['allowed_types']	= 'gif|jpg|jpeg|png|pdf|mp4|mkv';
			$config['remove_spaces']	= FALSE; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$files	= $_FILES;
			$cpt	= count($_FILES['file_hasil']['name']);
			for($i=0; $i<$cpt; $i++){           
				$_FILES['file_hasil']['name']		= $files['file_hasil']['name'][$i];
				$_FILES['file_hasil']['type']		= $files['file_hasil']['type'][$i];
				$_FILES['file_hasil']['tmp_name']	= $files['file_hasil']['tmp_name'][$i];
				$_FILES['file_hasil']['error']		= $files['file_hasil']['error'][$i];
				$_FILES['file_hasil']['size']		= $files['file_hasil']['size'][$i];    
				$url	= "https://cscb.leonvets.id/assets/file/foto_produk/".$_FILES['file_hasil']['name'];
				
				if($this->upload->do_upload('file_hasil')){
					$data = array(
						'ref_prod_fv_peg_id'	=> $peg_id,
						'ref_prod_fv_produk_id'	=> $rNum,
						'ref_prod_fv_rm_url'	=> "'".pg_escape_string($url)."'",
						'ref_prod_fv_rm_nama'	=> "'".pg_escape_string( $files['file_hasil']['name'][$i])."'",
						'ref_prod_fv_rm_keterangan'	=> "'".pg_escape_string($inp_ket_foto)."'",
					);
					$this->Mainmodel->insert_table('ref_produk_foto_video', $data);
				}
			}

			if($action == 'save_continue'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
			}
			elseif($action == 'save_new'){
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/');
			}
			else{
				redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
			}
		}
		elseif($mode == 'crud_detail'){
			$data = array(
				'ref_prod_dt_parent'	=> $rNum,
				'ref_prod_dt_child'		=> $this->input->post('inp_dt_prod'),
			);
			$this->Mainmodel->insert_table('ref_produk_detail', $data);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'del_detail'){
			$this->Mainmodel->delete_table('ref_produk_detail', 'ref_prod_dt_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);

		}
		elseif($mode == 'disabled'){
			$data = array(
				'ref_prod_aktif'		=> "'false'",
				'ref_prod_time_update'	=> "'now()'", 
				'ref_prod_peg_update'	=> $peg_id
			);
			$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'enabled'){
			$data = array(
				'ref_prod_aktif'	=> "'true'",
				'ref_prod_time_update'	=> "'now()'", 
				'ref_prod_peg_update'	=> $peg_id
			);
			$this->Mainmodel->update_table('ref_produk', $data, 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'delete'){
			$this->Mainmodel->delete_table('ref_produk', 'ref_prod_id='.$rNum);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'del_foto'){
			$this->Mainmodel->delete_table('ref_produk_foto_video', 'ref_prod_fv_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
		}

		$data['ref_prod_aktif']	= $ref_prod_aktif;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_produk WHERE ref_prod_id > 0".$orderByrNum);
		$this->load->view('layout', $data);
	}

    function produkgajidokter($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
        $data['readonly_menu']  = $list_menu->ref_mn_to_grp_readonly;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['rNum']			= $rNum;
		
		
		if($mode == 'crud'){
			$submit		= $this->input->post('submit_crud');
			$count_hd	= $this->input->post('count_hd');
			for ($i = 1; $i <= $count_hd; $i++) {
				$row_id		= $this->input->post('row_id_'.$i);
				$stat_gaji	= $this->input->post('inp_status_'.$i);
				if($stat_gaji == 2){
					$stat_harian	= 't';
					$stat_operasi	= 'f';
					$stat_steril	= 'f';
				}
				elseif($stat_gaji == 3){
					$stat_harian	= 'f';
					$stat_operasi	= 't';
					$stat_steril	= 'f';
				}
				elseif($stat_gaji == 4){
					$stat_harian	= 'f';
					$stat_operasi	= 'f';
					$stat_steril	= 't';
				}
				else{
					$stat_harian	= 'f';
					$stat_operasi	= 'f';
					$stat_steril	= 'f';
				}

				$data_upd = array(
					'ref_prod_fee_dokter_pasien_harian'		=> "'".$stat_harian."'",
					'ref_prod_fee_dokter_operasi_vaksin'	=> "'".$stat_operasi."'",
					'ref_prod_fee_dokter_steril_komunitas'	=> "'".$stat_steril."'",
				);
				$this->Mainmodel->update_table('ref_produk', $data_upd, 'ref_prod_id='.$row_id);
			} 
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		
		
		$data['query_hd']		= $this->db->query("SELECT * FROM v_produk WHERE ref_prod_id > 0 ORDER BY ref_prod_fee_dokter_pasien_harian,ref_prod_fee_dokter_operasi_vaksin,ref_prod_fee_dokter_steril_komunitas,ref_prod_nama");
		$this->load->view('layout', $data);
	}

}
