<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Labrad extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        if (!$this->session->userdata('userlogin')) {
            $this->session->set_userdata('urlback', $this->uri->uri_string());
            redirect('loginauth/login');
        }
        $this->load->model('Mainmodel');
		$this->load->library('user_agent');
		$this->load->helper('form');
		$peg_id	= $this->session->userdata('peg_id');
		$this->rowPgw = $this->Mainmodel->get_row_data("v_pegawai","peg_id=".$peg_id);
    }

    public function index() {
		$this->dashboard();
    }

    function daftar($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$mode_cari	= $this->input->get('mode_cari');
		$stat_cari	= $this->input->get('stat_cari');
		$prod_cari	= $this->input->get('prod_cari');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['comboCabangCari']= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 2", "ref_cab_nama", 0);
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;
		$data['peg_id']			= $peg_id;
		$whereClause			= '';
		$orderBy				= "rm_tanggal DESC, rm_no_urut, rm_id";
	
		$pas_id			= 0;
		$pas_mrn		= '';
		$pas_nama		= '';
		$pem_nama		= '';
		$pas_gender		= 'J';
		$pas_spesies	= 0;
		$pas_jenis		= 1;
		$pas_ras		= 0;
		$pas_warna		= 0;
		$pas_tanda_khusus	= 0;
		$pas_foto		= "assets/file/foto/unknown.jpg";
		$pas_tanggal_lahir = '';
		$rm_tanggal		= date('d-m-Y');
		$rm_cabang		= 3;
		$rm_no_urut		= 0;
		$rm_umur_tahun	= 0;
		$rm_umur_bulan	= 0;
		$rm_berat		= 0;
		$rm_last_vaksin	= '';
		$rm_anamnesis	= 277;
		$rm_anam_ket	= '';
		$rm_status		= '';
		$checkedJantan	= '';
		$checkedBetina	= '';
		$checkedRas		= '';
		$checkedDomestik= '';
		$rm_klinik_rujukan	= 1;
		$rm_dokter_rujukan	= 0;
		$checkedRontgen = '';	
		$checkedUSG = '';	
		$checkedHema	= '';	
		$checkedKimDar	= '';	
		$checkedKimDar2	= '';	
		$checkedInhalasi	= '';	
		
		$whereDokter		= " AND ph_dok_pihak=".$rm_klinik_rujukan;
		if($rNum > 0){
			$whereClause	= " AND rm_id=".$rNum;
			$orderBy		= "rm_id=".$rNum." DESC, rm_no_urut, rm_tanggal DESC";
			$query_rnum		= "	SELECT * FROM v_rekam_medis WHERE rm_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$pas_id			= $rrNum->pas_id;
			$pas_mrn		= $rrNum->pas_mrn;
			$pas_nama		= $rrNum->pas_nama;
			$pas_gender		= $rrNum->pas_gender;
			$pas_spesies	= $rrNum->pas_spesies;
			$pas_ras		= $rrNum->pas_ras;
			$pas_warna		= $rrNum->pas_warna;
			$pas_tanda_khusus	= $rrNum->pas_spesies;
			$pem_id			= $rrNum->pas_pemilik_utama;
			$pem_nama		= $rrNum->pem_nama;
			$pas_tanggal_lahir	= $rrNum->pas_tanggal_lahir;
			$rm_perawatan	= $rrNum->rm_perawatan;
			$rm_tanggal		= $rrNum->rm_tanggal;
			$rm_cabang		= $rrNum->rm_cabang;
			$rm_asal		= $rrNum->rm_asal;
			$rm_no_urut		= $rrNum->rm_no_urut;
			$rm_umur_tahun	= $rrNum->rm_umur_tahun;
			$rm_umur_bulan	= $rrNum->rm_umur_bulan;
			$rm_anam_ket	= str_replace("<br />","", $rrNum->rm_anamnesis_ket);
			$rm_status		= $rrNum->rm_status;
			$rm_klinik_rujukan	= $rrNum->rm_klinik_rujukan;
			$rm_dokter_rujukan	= $rrNum->rm_dokter_rujukan;
			$whereDokter	= " AND ph_dok_pihak=".$rm_klinik_rujukan;
			$rrRontgen	= $this->Mainmodel->get_row_data('rekam_medis_labrad', 'rm_labrad_produk=70 AND rm_labrad_rm_id='.$rNum);
			$rrUSG		= $this->Mainmodel->get_row_data('rekam_medis_labrad', 'rm_labrad_produk=78 AND rm_labrad_rm_id='.$rNum);
			$rrHema		= $this->Mainmodel->get_row_data('rekam_medis_labrad', 'rm_labrad_produk=72 AND rm_labrad_rm_id='.$rNum);
			$rrKimDar	= $this->Mainmodel->get_row_data('rekam_medis_labrad', 'rm_labrad_produk=88 AND rm_labrad_rm_id='.$rNum);
			$rrKimDar2	= $this->Mainmodel->get_row_data('rekam_medis_labrad', 'rm_labrad_produk=113 AND rm_labrad_rm_id='.$rNum);
			$rrInhalasi	= $this->Mainmodel->get_row_data('rekam_medis_labrad', 'rm_labrad_produk IN (1346,1347,1348,1349,1350) AND rm_labrad_rm_id='.$rNum);
		
			if(isset($rrRontgen)){
				$checkedRontgen = 'checked';	
			}
			if(isset($rrUSG)){
				$checkedUSG = 'checked';	
			}
			if(isset($rrHema)){
				$checkedHema = 'checked';	
			}
			if(isset($rrKimDar)){
				$checkedKimDar = 'checked';	
			}
			if(isset($rrKimDar2)){
				$checkedKimDar2 = 'checked';	
			}
			if(isset($rrInhalasi)){
				$checkedInhalasi = 'checked';	
			}
			if($pas_gender=='J'){
				$checkedJantan	= 'checked="checked"';
			}
			else{
				$checkedBetina	= 'checked="checked"';
			}
		}
		else{
			if(!empty($hSQL)){
				$whereClause	= base64_decode($hSQL);
			}
			else{
				if(!empty($mode_cari) && !empty($stat_cari)){

					if($mode_cari=='all'){
						$whereClause	= " AND rm_status= ".$stat_cari;
					}
					elseif($mode_cari=='month'){
						$year	= date('Y');
						$month	= date('m');
						$whereClause	= " AND rm_status= ".$stat_cari." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
					}
					else{
						$whereClause	= " AND rm_status= ".$stat_cari." AND rm_tanggal='now()'";
					}

					if($prod_cari == 70){
						$whereClause	.= " AND lab_rontgen > 0 ";
					}
					elseif($prod_cari == 72){
						$whereClause	.= " AND lab_kimdar > 0 ";
					}
					elseif($prod_cari == 78){
						$whereClause	.= " AND lab_usg > 0 ";
					}
				}
				else{
					$whereClause	= " AND rm_status < 8";
				}
			}
		}

		if($mode == 'form'){
			$data['pas_id']			= $pas_id;
			$data['pas_mrn']		= $pas_mrn;
			$data['pas_nama']		= $pas_nama;
			$data['pem_nama']		= $pem_nama;
			$data['rm_tanggal']		= $rm_tanggal;
			$data['rm_umur_tahun']	= $rm_umur_tahun;
			$data['rm_umur_bulan']	= $rm_umur_bulan;
			$data['rm_anam_ket']	= $rm_anam_ket;
			$data['rm_status']		= $rm_status;
			$data['checkedJantan']	= $checkedJantan;
			$data['checkedBetina']	= $checkedBetina;
			$data['comboCabang']	= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 2", "ref_cab_nama", $rm_cabang);
			$data['comboSpesies']	= $this->Mainmodel->show_combo("ref_spesies", "ref_spesies_id", "ref_spesies_nama", "ref_spesies_id > 0", "ref_spesies_nama", $pas_spesies);
			$data['comboRas']		= $this->Mainmodel->show_combo("ref_ras", "ref_ras_id", "ref_ras_nama", "ref_ras_id > 0", "ref_ras_nama", $pas_ras);
			$data['comboWarna']		= $this->Mainmodel->show_combo("ref_warna", "ref_warna_id", "ref_warna_ket", "ref_warna_id > 0", "ref_warna_ket", $pas_warna);
			$data['comboKlinik']	= $this->Mainmodel->show_combo("pihak", "ph_id", "ph_nama", "ph_kolega IS TRUE", "ph_nama", $rm_klinik_rujukan);
			$data['comboDokter']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE".$whereDokter, "dok_nama", $rm_dokter_rujukan);
			$data['var_pas_mrn']	= $this->Mainmodel->encode_var_js("pasien", "pas_id", "pas_mrn", "pas_aktif IS TRUE", "pas_mrn");
			$data['var_pas_nama']	= $this->Mainmodel->encode_var_js("pasien", "pas_id", "pas_nama", "pas_aktif IS TRUE", "pas_nama");
			$data['checkedRontgen']	= $checkedRontgen;
			$data['checkedUSG']		= $checkedUSG;
			$data['checkedHema']	= $checkedHema;
			$data['checkedKimDar']	= $checkedKimDar;
			$data['checkedKimDar2']	= $checkedKimDar2;
			$data['checkedInhalasi']= $checkedInhalasi;

			$data['main_content']   = 'labrad/daftar_form';
		}
		elseif($mode == 'crud'){
			if(empty($this->input->post('inp_ras'))){
				$inp_ras = 'null';
			}
			else{
				$inp_ras = $this->input->post('inp_ras');
			}

			$data = array(
				'pas_nama'		=> "'".$this->input->post('inp_nama')."'",
				'pas_spesies'	=> "'".$this->input->post('inp_spesies')."'",
				'pas_ras'		=> $inp_ras,
				'pas_warna'		=> $this->input->post('inp_warna'),
				'pas_gender'	=> "'".$this->input->post('inp_jk')."'",
				'pas_umur_tahun'=> $this->input->post('inp_umur_tahun'),
				'pas_umur_bulan'=> $this->input->post('inp_umur_bulan'),
				'pas_asal'		=> 2,
			);

			$data_pem = array(
				'pem_nama'		=> "'".$this->input->post('inp_pem')."'"
			);

			$inp_pas_id	= $this->input->post('inp_pas_id');
			if($rNum > 0){
				$inp_pas_id	 = $pas_id;
				$inp_pem_id	 = $pem_id;
			}

			if($inp_pas_id > 0){
				$this->Mainmodel->update_table('pasien', $data, 'pas_id='.$inp_pas_id);
				//$this->Mainmodel->update_table('pemilik', $data_pem, 'pem_id='.$inp_pem_id);
				}
			else{
				$inp_pem_id	= $this->Mainmodel->insert_table('pemilik', $data_pem);

				$data	+= ['pas_pemilik_utama' => $inp_pem_id];
				$inp_pas_id	= $this->Mainmodel->insert_table('pasien', $data);
			}
			
			$data = array(
				'rm_cabang'			=> $this->input->post('inp_cabang'),
				'rm_status'			=> 1,
				'rm_asal'			=> 1,
				'rm_peg_buat'		=> $peg_id,
				'rm_tanggal'		=> "'".$this->input->post('inp_tgl')."'",
				'rm_pasien'			=> $inp_pas_id,
				'rm_umur_tahun'		=> $this->input->post('inp_umur_tahun'),
				'rm_umur_bulan'		=> $this->input->post('inp_umur_bulan'),
				'rm_anamnesis_ket'	=> "'".pg_escape_string($this->input->post('inp_ket_anamnesis'))."'",
				'rm_perawatan'		=> 164,
				'rm_klinik_rujukan'	=> $this->input->post('inp_klinik'),
				'rm_dokter_rujukan'	=> $this->input->post('inp_dokter'),
			);

			if($rNum > 0){
				$data += ['rm_time_update' => "'now()'",  'rm_peg_update' => $peg_id];
				$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

				$data_log = array(
					'rm_stat_log_rm_id'		=> $rNum,
					'rm_stat_log_status_id'	=> 77,
					'rm_stat_log_user'		=> $peg_id,
				);
				$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);
			}
			else{
				$rNum	= $this->Mainmodel->insert_table('rekam_medis', $data);
				$data_log = array(
					'rm_stat_log_rm_id'		=> $rNum,
					'rm_stat_log_status_id'	=> 1,
					'rm_stat_log_user'		=> $peg_id,
				);
				$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);
			}

			if($this->input->post('cek_rontgen') > 0){
				if(!isset($rrRontgen)){
					$produk	= 70;
					$rrProd	= $this->Mainmodel->get_row_data('ref_produk', 'ref_prod_id='.$produk);
					$data = array(
						'rm_labrad_rm_id'	=> $rNum,
						'rm_labrad_produk'	=> $produk,
						'rm_labrad_harga'	=> $rrProd->ref_prod_harga_labrad,
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad', $data);
				}
			}
			else{
				if(isset($rrRontgen)){
					$this->Mainmodel->delete_table('rekam_medis_labrad', 'rm_labrad_produk=70 AND rm_labrad_rm_id='.$rNum);
				}
			}

			if($this->input->post('cek_usg') > 0){
				if(!isset($rrUSG)){
					$produk	= 78;
					$rrProd	= $this->Mainmodel->get_row_data('ref_produk', 'ref_prod_id='.$produk);
					$data = array(
						'rm_labrad_rm_id'	=> $rNum,
						'rm_labrad_produk'	=> $produk,
						'rm_labrad_harga'	=> $rrProd->ref_prod_harga_labrad,
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad', $data);
				}
			}
			else{
				if(isset($rrUSG)){
					$this->Mainmodel->delete_table('rekam_medis_labrad', 'rm_labrad_produk=78 AND rm_labrad_rm_id='.$rNum);
				}
			}

			if($this->input->post('cek_hema') > 0){
				if(!isset($rrHema)){
					$produk	= 72;
					$rrProd	= $this->Mainmodel->get_row_data('ref_produk', 'ref_prod_id='.$produk);
					$data = array(
						'rm_labrad_rm_id'	=> $rNum,
						'rm_labrad_produk'	=> $produk,
						'rm_labrad_harga'	=> $rrProd->ref_prod_harga_labrad,
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad', $data);
				}
			}
			else{
				if(isset($rrHema)){
					$this->Mainmodel->delete_table('rekam_medis_labrad', 'rm_labrad_produk=72 AND rm_labrad_rm_id='.$rNum);
				}
			}

			if($this->input->post('cek_kimdar') > 0){
				if(!isset($rrKimDar)){
					$produk	= 88;
					$rrProd	= $this->Mainmodel->get_row_data('ref_produk', 'ref_prod_id='.$produk);
					$data = array(
						'rm_labrad_rm_id'	=> $rNum,
						'rm_labrad_produk'	=> $produk,
						'rm_labrad_harga'	=> $rrProd->ref_prod_harga_labrad,
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad', $data);
				}
			}
			else{
				if(isset($rrKimDar)){
					$this->Mainmodel->delete_table('rekam_medis_labrad', 'rm_labrad_produk=88 AND rm_labrad_rm_id='.$rNum);
				}
			}

			if($this->input->post('cek_kimdar_2') > 0){
				if(!isset($rrKimDar2)){
					$produk	= 113;
					$rrProd	= $this->Mainmodel->get_row_data('ref_produk', 'ref_prod_id='.$produk);
					$data = array(
						'rm_labrad_rm_id'	=> $rNum,
						'rm_labrad_produk'	=> $produk,
						'rm_labrad_harga'	=> $rrProd->ref_prod_harga_labrad,
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad', $data);
				}
			}
			else{
				if(isset($rrKimDar2)){
					$this->Mainmodel->delete_table('rekam_medis_labrad', 'rm_labrad_produk=113 AND rm_labrad_rm_id='.$rNum);
				}
			}

			if($this->input->post('cek_inhalasi') > 0){
				if(!isset($rrInhalasi)){
					if($this->input->post('inp_spesies') == 1){
						$produk	= 1347;
					}
					elseif($this->input->post('inp_spesies') == 2){
						$produk	= 1346;
					}
					elseif($this->input->post('inp_spesies') == 5){
						$produk	= 1349;
					}
					elseif($this->input->post('inp_spesies') == 12){
						$produk	= 1348;
					}
					else{
						$produk	= 1350;
					}

					$rrProd	= $this->Mainmodel->get_row_data('ref_produk', 'ref_prod_id='.$produk);
					$data = array(
						'rm_labrad_rm_id'	=> $rNum,
						'rm_labrad_produk'	=> $produk,
						'rm_labrad_harga'	=> $rrProd->ref_prod_harga_labrad,
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad', $data);
				}
			}
			else{
				if(isset($rrInhalasi)){
					$this->Mainmodel->delete_table('rekam_medis_labrad', 'rm_labrad_produk IN (1346,1347,1348,1349,1350) AND rm_labrad_rm_id='.$rNum);
				}
			}

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'get_data_mrn'){
			$mrn	= $this->input->get('mrn');
			$data	= $this->Mainmodel->get_row_data("v_pasien", "pas_mrn='".$mrn."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'get_data_nama'){
			$nama	= $this->input->get('nama');
			$data	= $this->Mainmodel->get_row_data("v_pasien", "pas_nama='".$nama."'");
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'get_data_rm'){
			$rNum	= $this->input->get('rNum');
			$data	= $this->Mainmodel->get_row_data("rekam_medis", "rm_id =".$rNum);
			$response = array('data' => $data);
			echo json_encode($response);
			exit;
		}
		elseif($mode == 'delete'){
			$data = array(
				'rm_status'				=> 88,
				'rm_peg_update'			=> $peg_id,
				'rm_time_update'		=> "'now()'",
			);
			$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

			$data_log = array(
				'rm_stat_log_rm_id'		=> $rNum,
				'rm_stat_log_status_id'	=> 88,
				'rm_stat_log_user'		=> $peg_id,
			);
			$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'proses'){
			$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM rekam_medis_labrad_hasil WHERE rm_labrad_hsl_rm_id ='.$rNum);
			$num_row				= $queryNumRow->row();
			$count_data				= $num_row->count_data;
			if($count_data == 0){
				$query_rm_labrad	= $this->db->query("SELECT * FROM ref_hasil_labrad WHERE ref_hlr_prod_id IN (SELECT rm_labrad_produk FROM rekam_medis_labrad WHERE rm_labrad_rm_id=".$rNum.") ORDER BY ref_hlr_prod_id,ref_hlr_urutan");
				foreach($query_rm_labrad->result() as $row_rm_labrad){
					$data = array(
						'rm_labrad_hsl_rm_id'				=> $rNum,
						'rm_labrad_hsl_ref_id'				=> $row_rm_labrad->ref_hlr_id,
						'rm_labrad_hsl_prod_id'				=> $row_rm_labrad->ref_hlr_prod_id,
						'rm_labrad_hsl_nama'				=> "'".$row_rm_labrad->ref_hlr_nama."'",
						'rm_labrad_hsl_satuan'				=> "'".$row_rm_labrad->ref_hlr_satuan."'",
						'rm_labrad_hsl_nilai_rujukan_kucing'=> "'".$row_rm_labrad->ref_hlr_nilai_rujukan_kucing."'",
						'rm_labrad_hsl_nilai_rujukan_anjing'=> "'".$row_rm_labrad->ref_hlr_nilai_rujukan_anjing."'",
						'rm_labrad_hsl_nilai_rujukan_lain'	=> "'".$row_rm_labrad->ref_hlr_nilai_rujukan_lain."'",
						'rm_labrad_hsl_urutan'				=> $row_rm_labrad->ref_hlr_urutan
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad_hasil', $data);
				}
			}

			if($rm_no_urut > 0){
				$no_urut = $rm_no_urut;
			}
			else{
				$no_urut= $this->Mainmodel->get_no_urut_perawatan($rm_tanggal, $rm_cabang, $rm_perawatan);
				$data_no = array(
					'rm_no_urut_labrad_rm_id'	=> $rNum,
					'rm_no_urut_labrad_user'	=> $peg_id,
					'rm_no_urut_labrad_tanggal'	=> "'".$rm_tanggal."'",
					'rm_no_urut_labrad_nomor'	=> $no_urut
				);
				$this->Mainmodel->insert_table('rekam_medis_no_urut_labrad', $data_no);
			}

			$data = array(
				'rm_no_urut'		=> $no_urut,
				'rm_status'			=> 2,
			);
			$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

			$data_log = array(
				'rm_stat_log_rm_id'		=> $rNum,
				'rm_stat_log_status_id'	=> 2,
				'rm_stat_log_user'		=> $peg_id,
			);
			$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'batal_proses'){

			$this->Mainmodel->delete_table('rekam_medis_labrad_hasil', 'rm_labrad_hsl_rm_id='.$rNum);

			$data = array(
				'rm_status'			=> 1,
			);
			$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

			$data_log = array(
				'rm_stat_log_rm_id'		=> $rNum,
				'rm_stat_log_status_id'	=> 52,
				'rm_stat_log_user'		=> $peg_id,
			);
			$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
		}
		elseif($mode == 'batal'){
			$inp_ket_batal	= $this->input->post('inp_ket_batal');
			$rNum			= $this->input->post('rNumBatal');
			$data = array(
				'rm_status'				=> 88,
				'rm_batal_keterangan'	=> "'".$inp_ket_batal."'",
				'rm_peg_update'			=> $peg_id,
				'rm_time_update'		=> "'now()'",
			);
			$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

			$data_log = array(
				'rm_stat_log_rm_id'		=> $rNum,
				'rm_stat_log_status_id'	=> 88,
				'rm_stat_log_user'		=> $peg_id,
			);
			$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}

		$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM v_rekam_medis WHERE rm_asal = 1 AND rm_perawatan = 164'.$whereClause);
		$num_row				= $queryNumRow->row();
		$count_data				= $num_row->count_data;
		(empty($this->input->get('pn'))) ? $page_number = 1 : $page_number = $this->input->get('pn');
		(empty($this->input->get('ps'))) ? $page_size = 10 : $page_size = $this->input->get('ps');
		($page_number > 1) ? $offset = ($page_number-1)*$page_size : $offset = 0;
		
		$data['offset']			= $offset;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis WHERE rm_asal = 1 AND rm_perawatan = 164 ".$whereClause." ORDER BY ".$orderBy." LIMIT ".$page_size." OFFSET ".$offset);
		if($data['query_hd']->num_rows() > 0){
			$data['pagination']		= $this->Mainmodel->pagination($hSQL, $page_number, $page_size, $count_data);
		}

		$this->load->view('layout', $data);
	}

    function hasil($mode='') {
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$rNum2		= $this->input->get('rNum2');
		$mode_cari	= $this->input->get('mode_cari');
		$stat_cari	= $this->input->get('stat_cari');
		if(empty($rNum)){
			$rNum = 0;
		}
		$list_menu				= $this->Mainmodel->get_list_menu($cntrl_menu, $peg_id);
		$data['header_menu']	= $this->Mainmodel->header_menu($peg_id, $list_menu->ref_mn_grp_parent_id, $list_menu->ref_mn_grp_id, $list_menu->ref_menu_id);
		$data['main_title']     = $list_menu->ref_mn_grp_judul;
		$data['title']          = $list_menu->ref_menu_judul_template;
        $data['main_content']   = $list_menu->ref_menu_controller;
		$data['current_url']	= current_url();
		$data['class']			= $this->router->fetch_class();
		$data['method']			= $this->router->fetch_method();
		$data['comboCabangCari']= $this->Mainmodel->show_combo("ref_cabang", "ref_cab_id", "ref_cab_nama", "ref_cab_jenis_usaha = 2", "ref_cab_nama", 0);
		$data['rNum']			= $rNum;
		$data['rNum2']			= $rNum2;
		$data['peg_id']			= $peg_id;
		$whereClause			= '';
		$orderBy				= "rm_tanggal DESC, rm_no_urut, rm_id";
	
		$countRontgen	= 0;
		$countUSG		= 0;
		$countCekDar	= 0;
		$rm_tanggal		= date('d-m-Y');
		$rm_status		= '';
		$pas_spesies		= '';
		$ref_spesies_nama	= '';
		if($rNum > 0){
			$whereClause	= " AND rm_id=".$rNum;
			$orderBy		= "rm_id=".$rNum." DESC, rm_no_urut, rm_tanggal DESC";
			$query_rnum		= "	SELECT * FROM v_rekam_medis WHERE rm_id=".$rNum;
			$rhNum			= $this->db->query($query_rnum);
			$rrNum			= $rhNum->row();
			$rm_status		= $rrNum->rm_status;
			$rm_tanggal		= $rrNum->rm_tanggal;
			$rm_nomor		= $rrNum->rm_nomor;
			$rm_cabang		= $rrNum->rm_cabang;
			$rm_pasien		= $rrNum->rm_pasien;
			$pas_spesies	= $rrNum->pas_spesies;
			$rm_status		= $rrNum->rm_status;
			$ref_spesies_nama	= $rrNum->ref_spesies_nama;
			$rm_klinik_rujukan	= $rrNum->rm_klinik_rujukan;
		}
		else{
			if(!empty($hSQL)){
				$whereClause	= base64_decode($hSQL);
			}
			else{
				if(!empty($mode_cari) && !empty($stat_cari)){
					if($mode_cari=='all'){
						$whereClause	= " AND rm_status= ".$stat_cari;
					}
					elseif($mode_cari=='month'){
						$year	= date('Y');
						$month	= date('m');
						$whereClause	= " AND rm_status= ".$stat_cari." AND date_part('year', rm_tanggal) = ".$year." AND date_part('month', rm_tanggal) = ".$month." ";
					}
					else{
						$whereClause	= " AND rm_status= ".$stat_cari." AND rm_tanggal='now()'";
					}
				}
				else{
					$whereClause	= " AND rm_status > 1 AND rm_status < 8";
				}
			}
		}

		$data['rm_status']			= $rm_status;
		$data['pas_spesies']		= $pas_spesies;
		$data['ref_spesies_nama']	= $ref_spesies_nama;
		if($mode == 'form_rontgen'){
			$query_rontgen	= "	SELECT * FROM rekam_medis_labrad WHERE rm_labrad_produk=70 AND rm_labrad_rm_id=".$rNum;
			$rhRontgen		= $this->db->query($query_rontgen);
			$countRontgen	= $rhRontgen->num_rows();
			if($countRontgen > 0){
				$rrRontgen	= $rhRontgen->row();
				$dokRontgen	= $rrRontgen->rm_labrad_dokter;
				$tekRontgen	= $rrRontgen->rm_labrad_teknisi;
				if(empty($dokRontgen)){
					$dokRontgen = 15;
				}
				$data['idRontgen']			= $rrRontgen->rm_labrad_id;
				$data['ketHasilRontgen']	= $rrRontgen->rm_labrad_hasil_ket;
				$data['filmPakaiRontgen']	= $rrRontgen->rm_labrad_rontgen_qty_film_pakai;
				$data['filmErrorRontgen']	= $rrRontgen->rm_labrad_rontgen_qty_film_rusak;
				$data['comboDokterRontgen']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", $dokRontgen);
				$data['comboTeknisiRontgen']= $this->Mainmodel->show_combo("pegawai", "peg_id", "peg_nama", "peg_aktif IS TRUE", "peg_nama", $tekRontgen);
				
				$idHslRontgen	= 0;
				$posRontgen		= 0;
				$regRontgen		= 0;
				$temuanRontgen	= '';
				$textSubmitHasil= 'Tambah';
				if($rNum2 > 0){
					$query_hsl_rontgen	= "	SELECT * FROM rekam_medis_labrad_hasil WHERE rm_labrad_hsl_id=".$rNum2;
					$rhHslRontgen		= $this->db->query($query_hsl_rontgen);
					$rrHslRontgen		= $rhHslRontgen->row();
					$idHslRontgen		= $rrHslRontgen->rm_labrad_hsl_id;
					$posRontgen			= $rrHslRontgen->rm_labrad_hsl_rontgen_posisi;
					$regRontgen			= $rrHslRontgen->rm_labrad_hsl_rontgen_regio;
					$temuanRontgen		= $rrHslRontgen->rm_labrad_hsl_temuan;
					$textSubmitHasil	= 'Ubah';
				}
				$data['idHslRontgen']		= $idHslRontgen;
				$data['temuanRontgen']		= $temuanRontgen;
				$data['comboPosisiRontgen']	= $this->Mainmodel->show_combo("ref_kondisi", "ref_kondisi_id", "ref_kondisi_ket", "ref_kondisi_rontgen_posisi IS TRUE", "ref_kondisi_id", $posRontgen);
				$data['comboRegioRontgen']	= $this->Mainmodel->show_combo("ref_kondisi", "ref_kondisi_id", "ref_kondisi_ket", "ref_kondisi_rontgen_regio IS TRUE", "ref_kondisi_id", $regRontgen);
				$data['textSubmitHasil']	= $textSubmitHasil;

			}
			$data['countRontgen']	= $countRontgen;
			$data['main_content']   = 'labrad/hasil_form_rontgen';
		}
		elseif($mode == 'form_usg'){
			$query_usg	= "	SELECT * FROM rekam_medis_labrad WHERE rm_labrad_produk=78 AND rm_labrad_rm_id=".$rNum;
			$rhUSG		= $this->db->query($query_usg);
			$countUSG	= $rhUSG->num_rows();
			if($countUSG > 0){
				$rrUSG	= $rhUSG->row();
				$dokUSG	= $rrUSG->rm_labrad_dokter;
				$tekUSG	= $rrUSG->rm_labrad_teknisi;

				$data['idUSG']			= $rrUSG->rm_labrad_id;
				$data['ketHasilUSG']	= $rrUSG->rm_labrad_hasil_ket;
				//$data['comboDokterUSG']	= $this->Mainmodel->show_combo("v_pihak_dokter", "ph_dok_dokter", "dok_nama", "dok_aktif IS TRUE", "dok_nama", $dokUSG);
				$data['comboDokterUSG']	= $this->Mainmodel->show_combo("pegawai", "peg_id", "peg_nama", "peg_aktif IS TRUE AND peg_jabatan=3", "peg_nama", $dokUSG);

				$idHslUSG	= 0;
				$lokUSG		= 0;
				$temuanUSG	= '';
				$textSubmitHasil= 'Tambah';
				if($rNum2 > 0){
					$query_hsl_usg	= "	SELECT * FROM rekam_medis_labrad_hasil WHERE rm_labrad_hsl_id=".$rNum2;
					$rhHslUSG		= $this->db->query($query_hsl_usg);
					$rrHslUSG		= $rhHslUSG->row();
					$idHslUSG		= $rrHslUSG->rm_labrad_hsl_id;
					$lokUSG			= $rrHslUSG->rm_labrad_hsl_usg_lokasi;
					$temuanUSG		= $rrHslUSG->rm_labrad_hsl_temuan;
					$textSubmitHasil	= 'Ubah';
				}

				$data['idHslUSG']		= $idHslUSG;
				$data['temuanUSG']	= $temuanUSG;
				$data['comboLokasiUSG']	= $this->Mainmodel->show_combo("ref_kondisi", "ref_kondisi_id", "ref_kondisi_ket", "ref_kondisi_usg_lokasi IS TRUE", "ref_kondisi_id", $lokUSG);
				$data['textSubmitHasil']= $textSubmitHasil;
			}

			$data['countUSG']		= $countUSG;
			$data['main_content']   = 'labrad/hasil_form_usg';
		}
		elseif($mode == 'form_darah'){
			$query_cekdar	= "	SELECT * FROM rekam_medis_labrad WHERE rm_labrad_produk IN (72,88,113) AND rm_labrad_rm_id=".$rNum." LIMIT 1";
			$rhCekDar		= $this->db->query($query_cekdar);
			$countCekDar	= $rhCekDar->num_rows();
			if($countCekDar > 0){
				$rrCekDar	= $rhCekDar->row();
				$tekDarah	= $rrCekDar->rm_labrad_teknisi;
				$data['idCekDar']		= $rrCekDar->rm_labrad_id;
				$data['ketHasilCekDar']	= $rrCekDar->rm_labrad_hasil_ket;
				$data['comboTeknisiDarah']= $this->Mainmodel->show_combo("pegawai", "peg_id", "peg_nama", "peg_aktif IS TRUE", "peg_nama", $tekDarah);
			}

			$data['countCekDar']	= $countCekDar;
			$data['main_content']   = 'labrad/hasil_form_darah';
		}
		elseif($mode == 'crud_rontgen'){
			$data = array(
				'rm_labrad_dokter'					=> $this->input->post('inp_dokter_rontgen'),
				'rm_labrad_hasil_ket'				=> "'".$this->input->post('inp_ket_rontgen')."'",
				'rm_labrad_teknisi'					=> $this->input->post('inp_teknisi_rontgen'),
				'rm_labrad_qty'						=> $this->input->post('inp_qty_film_pakai'),
				'rm_labrad_rontgen_qty_film_pakai'	=> $this->input->post('inp_qty_film_pakai'),
				'rm_labrad_rontgen_qty_film_rusak'	=> $this->input->post('inp_qty_film_error'),
			);
			$this->Mainmodel->update_table('rekam_medis_labrad', $data, 'rm_labrad_id='.$this->input->post('id_labrad_rontgen'));
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_rontgen/?rNum='.$rNum);
		}
		elseif($mode == 'crud_dt_rontgen'){
			$act_rontgen_dt	= $this->input->post('act_rontgen_dt');

			$rNum2	= $this->input->post('id_hsl_labrad_rontgen');
			$data = array(
				'rm_labrad_hsl_rm_id'			=> $rNum,
				'rm_labrad_hsl_rm_labrad_id'	=> $this->input->post('id_labrad_rontgen_'),
				'rm_labrad_hsl_prod_id'			=> 70,
				'rm_labrad_hsl_rontgen_posisi'	=> $this->input->post('inp_posisi_rontgen'),
				'rm_labrad_hsl_rontgen_regio'	=> $this->input->post('inp_regio_rontgen'),
				'rm_labrad_hsl_temuan'			=> "'".$this->input->post('inp_temuan_rontgen')."'",
			);
			if($rNum2 > 0){
				if($act_rontgen_dt	== 'Hapus'){
					$this->Mainmodel->delete_table('rekam_medis_labrad_hasil', 'rm_labrad_hsl_id='.$rNum2);
				}
				else{
					$this->Mainmodel->update_table('rekam_medis_labrad_hasil', $data, 'rm_labrad_hsl_id='.$rNum2);
				}
			}
			else{
				$rNum2 = $this->Mainmodel->insert_table('rekam_medis_labrad_hasil', $data);
			}

			$inp_ket_hasil	= "Foto/Video Hasil Rontgen"; 
			$config['upload_path']		= 'assets/file/foto_labrad/';
			$config['allowed_types']	= 'gif|jpg|jpeg|png|pdf|mp4|mkv';
			$config['remove_spaces']	= FALSE; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$files	= $_FILES;
			$cpt	= count($_FILES['file_hasil_rontgen']['name']);
			for($i=0; $i<$cpt; $i++){           
				$_FILES['file_hasil_rontgen']['name']		= $files['file_hasil_rontgen']['name'][$i];
				$_FILES['file_hasil_rontgen']['type']		= $files['file_hasil_rontgen']['type'][$i];
				$_FILES['file_hasil_rontgen']['tmp_name']	= $files['file_hasil_rontgen']['tmp_name'][$i];
				$_FILES['file_hasil_rontgen']['error']		= $files['file_hasil_rontgen']['error'][$i];
				$_FILES['file_hasil_rontgen']['size']		= $files['file_hasil_rontgen']['size'][$i];    
				$url	= "https://cscb.leonvets.id/assets/file/foto_labrad/".$_FILES['file_hasil_rontgen']['name'];
				
				if($this->upload->do_upload('file_hasil_rontgen')){
					$data = array(
						'rmfv_labrad_hsl_rm_id'				=> $rNum,
						'rmfv_labrad_hsl_rm_labrad_id'		=> $this->input->post('id_labrad_rontgen_'),
						'rmfv_labrad_hsl_rm_labrad_hasil_id'=> $rNum2,
						'rmfv_labrad_hsl_peg_id'		=> $peg_id,
						'rmfv_labrad_hsl_rm_url'		=> "'".pg_escape_string($url)."'",
						'rmfv_labrad_hsl_rm_nama'		=> "'".pg_escape_string( $files['file_hasil_rontgen']['name'][$i])."'",
						'rmfv_labrad_hsl_rm_keterangan'	=> "'".pg_escape_string($inp_ket_hasil)."'",
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad_hasil_foto_video', $data);
				}
			}

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_rontgen/?rNum='.$rNum);
		}
		elseif($mode == 'crud_usg'){
			$data = array(
				'rm_labrad_dokter'		=> $this->input->post('inp_dokter_usg'),
				'rm_labrad_hasil_ket'	=> "'".$this->input->post('inp_ket_usg')."'",
			);
			$this->Mainmodel->update_table('rekam_medis_labrad', $data, 'rm_labrad_id='.$this->input->post('id_labrad_usg'));
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_usg/?rNum='.$rNum);
		}
		elseif($mode == 'crud_dt_usg'){
			$act_usg_dt	= $this->input->post('act_usg_dt');

			$rNum2	= $this->input->post('id_hsl_labrad_usg');
			$data = array(
				'rm_labrad_hsl_rm_id'			=> $rNum,
				'rm_labrad_hsl_rm_labrad_id'	=> $this->input->post('id_labrad_usg_'),
				'rm_labrad_hsl_prod_id'			=> 78,
				'rm_labrad_hsl_usg_lokasi'		=> $this->input->post('inp_lokasi_usg'),
				'rm_labrad_hsl_temuan'			=> "'".$this->input->post('inp_temuan_usg')."'",
			);
			if($rNum2 > 0){
				if($act_usg_dt	== 'Hapus'){
					$this->Mainmodel->delete_table('rekam_medis_labrad_hasil', 'rm_labrad_hsl_id='.$rNum2);
				}
				else{
					$this->Mainmodel->update_table('rekam_medis_labrad_hasil', $data, 'rm_labrad_hsl_id='.$rNum2);
				}
			}
			else{
				$rNum2 = $this->Mainmodel->insert_table('rekam_medis_labrad_hasil', $data);
			}

			$inp_ket_hasil	= "Foto/Video Hasil USG"; 
			$config['upload_path']		= 'assets/file/foto_labrad/';
			$config['allowed_types']	= 'gif|jpg|jpeg|png|pdf|mp4|mkv';
			$config['remove_spaces']	= FALSE; 
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$files	= $_FILES;
			$cpt	= count($_FILES['file_hasil_usg']['name']);
			for($i=0; $i<$cpt; $i++){           
				$_FILES['file_hasil_usg']['name']		= $files['file_hasil_usg']['name'][$i];
				$_FILES['file_hasil_usg']['type']		= $files['file_hasil_usg']['type'][$i];
				$_FILES['file_hasil_usg']['tmp_name']	= $files['file_hasil_usg']['tmp_name'][$i];
				$_FILES['file_hasil_usg']['error']		= $files['file_hasil_usg']['error'][$i];
				$_FILES['file_hasil_usg']['size']		= $files['file_hasil_usg']['size'][$i];    
				$url	= "https://cscb.leonvets.id/assets/file/foto_labrad/".$_FILES['file_hasil_usg']['name'];
				
				if($this->upload->do_upload('file_hasil_usg')){
					$data = array(
						'rmfv_labrad_hsl_rm_id'				=> $rNum,
						'rmfv_labrad_hsl_rm_labrad_id'		=> $this->input->post('id_labrad_usg_'),
						'rmfv_labrad_hsl_rm_labrad_hasil_id'=> $rNum2,
						'rmfv_labrad_hsl_peg_id'		=> $peg_id,
						'rmfv_labrad_hsl_rm_url'		=> "'".pg_escape_string($url)."'",
						'rmfv_labrad_hsl_rm_nama'		=> "'".pg_escape_string( $files['file_hasil_usg']['name'][$i])."'",
						'rmfv_labrad_hsl_rm_keterangan'	=> "'".pg_escape_string($inp_ket_hasil)."'",
					);
					$this->Mainmodel->insert_table('rekam_medis_labrad_hasil_foto_video', $data);
				}
			}

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_usg/?rNum='.$rNum);
		}
		elseif($mode == 'crud_prks_darah'){
			$data = array(
				'rm_labrad_hasil_ket'		=> "'".$this->input->post('inp_ket_cekdar')."'",
				'rm_labrad_teknisi'			=> $this->input->post('inp_teknisi_darah'),
			);
			$this->Mainmodel->update_table('rekam_medis_labrad', $data, 'rm_labrad_produk <> 70 AND rm_labrad_rm_id='.$rNum);

			$count_dt	= $this->input->post('count_dt');
			for ($i = 1; $i <= $count_dt; $i++) {
				$row_id		= $this->input->post('row_id_'.$i);
				$hasil		= $this->input->post('inp_hasil_'.$i);
				$rujukan	= $this->input->post('inp_rujukan_'.$i);
				$keterangan	= $this->input->post('inp_ket_'.$i);
				if($pas_spesies == 1){
					$field_rujukan = "rm_labrad_hsl_nilai_rujukan_anjing";
				}
				elseif($pas_spesies == 2){ 
					$field_rujukan = "rm_labrad_hsl_nilai_rujukan_kucing";
				}
				else{
					$field_rujukan = "rm_labrad_hsl_nilai_rujukan_lain";
				}
				$data = array(
					'rm_labrad_hsl_nilai_hasil'	=> "'".$hasil."'",
					$field_rujukan				=> "'".$rujukan."'",
					'rm_labrad_hsl_keterangan'	=> "'".$keterangan."'",
				);
				$this->Mainmodel->update_table('rekam_medis_labrad_hasil', $data, 'rm_labrad_hsl_id='.$row_id);
			}
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form_darah/?rNum='.$rNum);
		}
		elseif($mode == 'proses'){
			$data = array(
				'rm_status'			=> 8,
			);
			$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

			$data_log = array(
				'rm_stat_log_rm_id'		=> $rNum,
				'rm_stat_log_status_id'	=> 8,
				'rm_stat_log_user'		=> $peg_id,
			);
			$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'batal_proses'){
			$data = array(
				'rm_status'			=> 2,
			);
			$this->Mainmodel->update_table('rekam_medis', $data, 'rm_id='.$rNum);

			$data_log = array(
				'rm_stat_log_rm_id'		=> $rNum,
				'rm_stat_log_status_id'	=> 58,
				'rm_stat_log_user'		=> $peg_id,
			);
			$this->Mainmodel->insert_table('rekam_medis_status_log', $data_log);

			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/?rNum='.$rNum);
		}
		elseif($mode == 'del_foto'){
			$this->Mainmodel->delete_table('rekam_medis_labrad_hasil_foto_video', 'rmfv_labrad_hsl_id='.$rNum2);
			redirect($this->router->fetch_class().'/'.$this->router->fetch_method().'/form/?rNum='.$rNum);
		}

		$queryNumRow			= $this->db->query('SELECT COUNT(*) AS count_data FROM v_rekam_medis WHERE rm_asal = 1 AND rm_status_proses_halo IS TRUE AND rm_perawatan = 164'.$whereClause);
		$num_row				= $queryNumRow->row();
		$count_data				= $num_row->count_data;
		(empty($this->input->get('pn'))) ? $page_number = 1 : $page_number = $this->input->get('pn');
		(empty($this->input->get('ps'))) ? $page_size = 10 : $page_size = $this->input->get('ps');
		($page_number > 1) ? $offset = ($page_number-1)*$page_size : $offset = 0;
		
		$data['offset']			= $offset;
		$data['query_hd']		= $this->db->query("SELECT * FROM v_rekam_medis WHERE rm_asal = 1 AND rm_perawatan = 164 ".$whereClause." ORDER BY ".$orderBy." LIMIT ".$page_size." OFFSET ".$offset);
		if($data['query_hd']->num_rows() > 0){
			$data['pagination']		= $this->Mainmodel->pagination($hSQL, $page_number, $page_size, $count_data);
		}

		$this->load->view('layout', $data);
	}

	function rekammedis_search(){
		$previous_url	= $this->agent->referrer();
		$new_url		= preg_replace("(^https?://)", "", $previous_url );
		$new_url		= preg_replace("(localhost/)", "", $new_url );
		$url_segments	= explode('/',$new_url);
		$class			= $url_segments[1];
		$method			= $url_segments[2];
		$segment3		= $url_segments[3];
		$cr_cabang		= $this->input->post('cr_cabang');
		$cr_rm			= $this->input->post('cr_rm');
		$cr_nama		= $this->input->post('cr_nama');
		$cr_no_resep	= $this->input->post('cr_no_resep');
		$cr_tgl_rm		= $this->input->post('cr_tgl_rm');
		$whereClause	= '';

		if (!empty($cr_cabang)) {
			$whereClause .= " AND rm_cabang=" . $cr_cabang;
		}

		if (!empty($cr_rm)) {
			$whereClause .= " AND pas_mrn~*'" . $cr_rm . "'";
		}

		if (!empty($cr_nama)) {
			$whereClause .= " AND pas_nama~*'" . $cr_nama . "'";
		}

		if (!empty($cr_no_resep)) {
			$whereClause .= " AND rm_no_resep~*'" . $cr_no_resep . "'";
		}

		if (!empty($cr_tgl_rm)) {
			$whereClause .= " AND rm_tanggal='" . $cr_tgl_rm . "'";
		}

		$clause	= base64_encode($whereClause);
		redirect($class.'/'.$method.'/?hSQL='.$clause);
	}


	function print_hasil(){
		$peg_id		= $this->session->userdata('peg_id');
		$cntrl_menu	= $this->uri->segment(1) . '/' . $this->uri->segment(2);
		$hSQL		= str_replace(' ', '+', $this->input->get('hSQL'));
		$rNum		= $this->input->get('rNum');
		$cek		= $this->input->get('cek');
		$query_rnum	= "	SELECT * FROM v_rekam_medis WHERE rm_id=".$rNum;
		$rhNum		= $this->db->query($query_rnum);
		$rrNum		= $rhNum->row();

		$data['row_rm']	= $rrNum;
		$data['rNum']	= $rNum;
		if($cek == 1){
			$query_rontgen	= "	SELECT * FROM v_rekam_medis_labrad WHERE rm_labrad_produk=70 AND rm_labrad_rm_id=".$rNum;
			$rhRontgen		= $this->db->query($query_rontgen);
			$countRontgen	= $rhRontgen->num_rows();
			if($countRontgen > 0){
				$rrRontgen	= $rhRontgen->row();
				$data['idRontgen']		= $rrRontgen->rm_labrad_id;
				$data['dokterRontgen']	= $rrRontgen->dok_labrad;
				$data['ketHasilRontgen']= $rrRontgen->rm_labrad_hasil_ket;
			}
			$this->load->view('labrad/print_hasil_rontgen', $data);
		}
		elseif($cek == 2){
			$query_usg		= "	SELECT * FROM v_rekam_medis_labrad WHERE rm_labrad_produk=78 AND rm_labrad_rm_id=".$rNum;
			$rhUSG			= $this->db->query($query_usg);
			$countUSG	= $rhUSG->num_rows();
			if($countUSG > 0){
				$rrUSG	= $rhUSG->row();
				$data['idUSG']		= $rrUSG->rm_labrad_id;
				$data['dokterUSG']	= $rrUSG->dok_labrad;
				$data['ketHasilUSG']= $rrUSG->rm_labrad_hasil_ket;
			}
			$this->load->view('labrad/print_hasil_usg', $data);
		}
		else{
			$query_cekdar	= "	SELECT * FROM rekam_medis_labrad WHERE rm_labrad_produk IN (72,88,113) AND rm_labrad_rm_id=".$rNum." LIMIT 1";
			$rhCekDar		= $this->db->query($query_cekdar);
			$countCekDar	= $rhCekDar->num_rows();
			if($countCekDar > 0){
				$rrCekDar	= $rhCekDar->row();
				$data['ketHasilCekDar']	= $rrCekDar->rm_labrad_hasil_ket;
			}
			$this->load->view('labrad/print_hasil_darah', $data);
		}
	}

}
