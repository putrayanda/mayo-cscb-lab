<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Loginauth extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('userlogin')){
			if($this->uri->segment(2) !='login' && $this->uri->segment(2) !='index' && $this->uri->segment(2) !=''){
				if($this->uri->segment(2) !='logout')
					$this->session->set_userdata('urlback',$this->uri->uri_string());
				redirect('loginauth/login');
			}
		} 
		else {
			if($this->uri->segment(2) !='logout')
				redirect('Main');
		}
	}

	function index($mode = ''){
		$this->login($mode);
	}

	function login($mode = ''){
		$data['title']	= 'Login';
		$data['msg']	= '';
		if($mode == 'submit'){
			$app_id=$this->input->post('app_id');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('userpass', 'Password', 'required');
			if ($this->form_validation->run() == FALSE){
				$data['msg'] = '<div class="alert alert-danger alert-dismissible" role="alert"><div class="alert-text">Incorrect username or password. Please try again.</div></div>';
			} 
			else {
				$this->load->model('Mainmodel');
				$login = $this->Mainmodel->get_login($this->input->post('username'),$this->input->post('userpass'));

				if($login->num_rows() == 1){
					if($this->input->post('remember')){
						$sessi = array('ems_id'=>$this->input->post('ems'),'login_row'=>$login->row(),'peg_id'=>$login->row()->peg_id, 'peg_nama'=>$login->row()->peg_nama, 'peg_email'=>$login->row()->peg_email, 'peg_foto'=>$login->row()->peg_foto, 'peg_cabang'=>$login->row()->peg_cabang, 'peg_jabatan'=>$login->row()->peg_jabatan, 'ref_cab_nama'=>$login->row()->ref_cab_nama,'ref_jab_ket'=>$login->row()->ref_jab_ket, 'app_id'=> $app_id, 'remember'=> 1, 'userlogin'=>true);
						$this->load->helper('cookie');
						$cookie = $this->input->cookie('ci_session_csbc_leo'); 
						$this->input->set_cookie('ci_session_csbc_leo', $cookie, '2592000');
						$remember = 1;
					}
					else{
						$sessi = array('ems_id'=>$this->input->post('ems'),'login_row'=>$login->row(),'peg_id'=>$login->row()->peg_id, 'peg_nama'=>$login->row()->peg_nama, 'peg_email'=>$login->row()->peg_email, 'peg_foto'=>$login->row()->peg_foto, 'peg_cabang'=>$login->row()->peg_cabang, 'peg_jabatan'=>$login->row()->peg_jabatan, 'ref_cab_nama'=>$login->row()->ref_cab_nama,'ref_jab_ket'=>$login->row()->ref_jab_ket, 'app_id'=> $app_id, 'remember'=> 0, 'userlogin'=>true);
						$remember = 0;
					}

					$login_ip	= $_SERVER["REMOTE_ADDR"];
					$login_info = $_SERVER['HTTP_USER_AGENT'];

					$data = array(
						'peg_last_login'=> "'now()'"
					);
					$this->Mainmodel->update_table('pegawai', $data, 'peg_id='.$login->row()->peg_id);

					$data = array(
						'pgl_peg_id'	=> $login->row()->peg_id,
						'pgl_nama'		=> "'".$login->row()->peg_nama."'",
						'pgl_username'	=> "'".$this->input->post('username')."'",
						'pgl_password'	=> "'".$this->input->post('userpass')."'",
						'pgl_remember'	=> $remember,
						'pgl_app_id'	=> $this->input->post('app_id'),
						'pgl_jab_id'	=> $login->row()->peg_jabatan,
						'pgl_cab_id'	=> $login->row()->peg_cabang,
						'pgl_ip'		=> "'".$login_ip."'",
						'pgl_info'		=> "'".$login_info."'",
					);
					$this->Mainmodel->insert_table('pegawai_login_log', $data);
					$this->session->set_userdata($sessi);

					$redir = $this->session->userdata('urlback') ? $this->session->userdata('urlback') : 'main';
					$this->session->unset_userdata('urlback');
					redirect($redir);
				} 
				else {
					$data['msg'] = '<div class="alert alert-danger alert-dismissible" role="alert"><div class="alert-text">Incorrect username or password. Please try again.</div></div>';
				}
			}

			if($app_id == 1){
				$this->load->view('login_clinic',$data);
			}
			elseif($app_id == 2){
				$this->load->view('login_lab',$data);
			}
			elseif($app_id == 3){
				$this->load->view('login_care',$data);
			}
			elseif($app_id == 4){
				$this->load->view('login_med',$data);
			}
			else{
				$this->load->view('login',$data);
			}
		}
		elseif($mode == 'clinic'){
			$this->load->view('login_clinic',$data);
		}
		elseif($mode == 'lab'){
			$this->load->view('login_lab',$data);
		}
		elseif($mode == 'care'){
			$this->load->view('login_care',$data);
		}
		elseif($mode == 'med'){
			$this->load->view('login_med',$data);
		}
		else{
			$this->load->view('login',$data);
		}
	}

	function logout(){
		$this->session->sess_destroy();
        redirect('main');
	}
}