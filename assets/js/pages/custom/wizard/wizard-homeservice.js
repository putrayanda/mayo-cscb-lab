"use strict";

// Class definition
var KTWizard4 = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {
					if(wizard.getStep() == 2){
						$("#resume_pemilik").html("<div><strong>Nama Pemilik: </strong></div><div>"+$("#inp_nama").val()+"</div><div><strong>No.HP/WA: </strong></div><div>"+$("#inp_no_hp").val()+"</div><div><strong>Alamat: </strong></div><div>"+$("#inp_almt").val()+" Kec. "+$("#inp_kcmtn option:selected").text()+" "+$("#inp_kota option:selected").text()+" Prop. "+$("#inp_prop option:selected").text()+"</div><div>");
						$("#resume_homeservice").html("<div><strong>Jumlah Hewan: </strong></div><div>"+$("#inp_jml_hwn").val()+"</div><div><strong>Jarak: </strong></div><div>"+$("#inp_jarak").val()+" Km</div><div><strong>Keterangan: </strong></div><div>"+$("#inp_ket_daftar").val()+"</div>");
					}
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_nama: {
						validators: {
							notEmpty: {
								message: 'Nama Pasien/Hewan wajib diisi'
							},
						}
					},
					inp_no_hp: {
						validators: {
							notEmpty: {
								message: 'No. HP/WA wajib diisi'
							},
						}
					},
					inp_almt: {
						validators: {
							notEmpty: {
								message: 'Alamat wajib diisi'
							},
						}
					},	
					inp_prop: {
						validators: {
							notEmpty: {
								message: 'Propinsi wajib diisi'
							},
						}
					},
					inp_kota: {
						validators: {
							notEmpty: {
								message: 'Kota/Kab wajib diisi'
							},
						}
					},
					inp_kcmtn: {
						validators: {
							notEmpty: {
								message: 'Kecamatab wajib diisi'
							},
						}
					},
					

				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_tgl: {
						validators: {
							notEmpty: {
								message: 'Tanggal wajib diisi'
							}
						}
					},
					inp_jml_hwn: {
						validators: {
							notEmpty: {
								message: 'Jumlah Hewan wajib diisi'
							}
						}
					},
					inp_jarak: {
						validators: {
							notEmpty: {
								message: 'Jarak wajib diisi'
							}
						}
					}	
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
	}

	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_wizard_rm');
			_formEl = KTUtil.getById('kt_form_rm');

			initWizard();
			initValidation();
		}
	};
}();

jQuery(document).ready(function () {
	KTWizard4.init();
});
