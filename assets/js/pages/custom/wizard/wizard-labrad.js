"use strict";

// Class definition
var KTWizard4 = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {
					if(wizard.getStep() == 2){
						var strPeriksa	= '';
						var myStr	= $("#inp_ket_anamnesis").val();
						var strArray= myStr.replace(/-/g, "<br>");
						var anamnesa = [];
							$.each($("#inp_anamnesis option:selected"), function(){            
								anamnesa.push($(this).text());
							});
						var anam = anamnesa.join(", ");

						var rontgen	= $("#cek_rontgen").prop('checked');
						var usg		= $("#cek_usg").prop('checked');
						var hema	= $("#cek_hema").prop('checked');
						var kimdar	= $("#cek_kimdar").prop('checked');
						var kimdar2	= $("#cek_kimdar_2").prop('checked');
						if(rontgen==true){
							var strPeriksa	= strPeriksa+'<br> - Rontgen';
						}
						if(usg==true){
							var strPeriksa	= strPeriksa+'<br> - USG';
						}
						if(hema==true){
							var strPeriksa	= strPeriksa+'<br> - Haematology Rutin';
						}
						if(kimdar==true){
							var strPeriksa	= strPeriksa+'<br> - Kimia Darah 1';
						}
						if(kimdar2==true){
							var strPeriksa	= strPeriksa+'<br> - Kimia Darah 2';
						}
						$("#resume_pasien").html("<div><strong>Nama Pasien/Hewan: </strong></div><div>"+$("#inp_nama").val()+"</div><div><strong>Spesies: </strong></div><div>"+$("#inp_spesies option:selected").text()+"</div><div><strong>Jenis Kelamin: </strong></div><div>"+$("input[name='inp_jk']:checked").parent('label').text()+"</div><div><strong>Jenis: </strong></div><div>"+$("input[name='inp_jns']:checked").parent('label').text()+"</div><div><strong>Ras: </strong></div><div>"+$("#inp_ras option:selected").text()+"</div><div><strong>Warna: </strong></div><div>"+$("#inp_warna option:selected").text()+"</div><div><strong>Umur: </strong></div><div>"+$("#inp_umur_tahun").val()+" Tahun "+$("#inp_umur_bulan").val()+"</div><div><strong>Nama Pemilik: </strong></div><div>"+$("#inp_pem").val()+"</div>");
						$("#resume_anamnesis").html("<div><strong>Gejala Klinis: </strong></div><div>"+strArray+"</div><div><strong>Klinik: </strong></div><div>"+$("#inp_klinik option:selected").text()+"</div><div><strong>Dokter: </strong></div><div>"+$("#inp_dokter option:selected").text()+"</div><div><strong>Pemeriksaan: </strong></div><div>"+strPeriksa+"</div>");
					}
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_nama: {
						validators: {
							notEmpty: {
								message: 'Nama Pasien/Hewan wajib diisi'
							},
						}
					},
					inp_spesies: {
						validators: {
							notEmpty: {
								message: 'Spesies wajib diisi'
							},
						}
					},
					inp_ras: {
						validators: {
							notEmpty: {
								message: 'Ras wajib diisi'
							},
						}
					},	
					inp_warna: {
						validators: {
							notEmpty: {
								message: 'Warna wajib diisi'
							},
						}
					},
					inp_jk: {
						validators: {
							notEmpty: {
								message: 'Jenis Kelamin wajib diisi'
							},
						}
					},
					inp_pem: {
						validators: {
							notEmpty: {
								message: 'Nama Pemilik wajib diisi'
							},
						}
					},
					

				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_klinik: {
						validators: {
							notEmpty: {
								message: 'Klinik wajib diisi'
							}
						}
					},
					inp_dokter: {
						validators: {
							notEmpty: {
								message: 'Dokter wajib diisi'
							}
						}
					}					
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
	}

	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_wizard_rm');
			_formEl = KTUtil.getById('kt_form_rm');

			initWizard();
			initValidation();
		}
	};
}();

jQuery(document).ready(function () {
	KTWizard4.init();
});
