"use strict";

// Class definition
var KTWizard4 = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {
					if(wizard.getStep() == 3){
						var anamnesa = [];
							$.each($("#inp_anamnesis option:selected"), function(){            
								anamnesa.push($(this).text());
							});
						var anam = anamnesa.join(", ");
						$("#resume_pasien").html("<div><strong>No. Rekam Medis: </strong></div><div>"+$("#inp_mrn").val()+"</div><div><strong>Nama Pasien/Hewan: </strong></div><div>"+$("#inp_nama").val()+"</div><div><strong>Spesies: </strong></div><div>"+$("#inp_spesies option:selected").text()+"</div><div><strong>Jenis Kelamin: </strong></div><div>"+$("input[name='inp_jk']:checked").parent('label').text()+"</div><div><strong>Jenis: </strong></div><div>"+$("input[name='inp_jns']:checked").parent('label').text()+"</div><div><strong>Ras: </strong></div><div>"+$("#inp_ras option:selected").text()+"</div><div><strong>Warna: </strong></div><div>"+$("#inp_warna option:selected").text()+"</div><div><strong>Umur: </strong></div><div>"+$("#inp_umur_tahun").val()+" Tahun "+$("#inp_umur_tahun").val()+" Bulan</div><div><strong>Berat: </strong></div><div>"+$("#inp_berat").val()+" Kg</div>");
						$("#resume_anamnesis").html("<div><strong>Anamnesis: </strong></div><div>"+anam+"</div><div><strong>Keterangan: </strong></div><div>"+$("#inp_ket_anamnesis").val()+"</div>");
						$("#resume_diagnosa").html("<div><strong>Diagnosa: </strong></div><div>"+$("#inp_diagnosa option:selected").text()+"</div><div><strong>Keterangan: </strong></div><div>"+$("#inp_ket_diagnosa").val()+"</div>");
					}
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
			}
		));
		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
			}
		));
		// Step 3
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_diagnosa: {
						validators: {
							notEmpty: {
								message: 'Diagnosa wajibb diisi'
							}
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
	}

	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_wizard_rm');
			_formEl = KTUtil.getById('kt_form_rm');

			initWizard();
			initValidation();
		}
	};
}();

jQuery(document).ready(function () {
	KTWizard4.init();
});
