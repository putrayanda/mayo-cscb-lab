"use strict";

// Class definition
var KTWizard4 = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {
					if(wizard.getStep() == 3){
						$("#resume_pasien").html("<div><strong>No. Rekam Medis: </strong></div><div>"+$("#inp_mrn").val()+"</div><div><strong>Nama Pasien/Hewan: </strong></div><div>"+$("#inp_nama").val()+"</div><div><strong>Spesies: </strong></div><div>"+$("#inp_spesies option:selected").text()+"</div><div><strong>Jenis Kelamin: </strong></div><div>"+$("input[name='inp_jk']:checked").parent('label').text()+"</div><div><strong>Jenis: </strong></div><div>"+$("input[name='inp_jns']:checked").parent('label').text()+"</div><div><strong>Ras: </strong></div><div>"+$("#inp_ras option:selected").text()+"</div><div><strong>Warna: </strong></div><div>"+$("#inp_warna option:selected").text()+"</div><div><strong>Umur: </strong></div><div>"+$("#inp_umur_tahun").val()+" Tahun "+$("#inp_umur_tahun").val()+" Bulan</div>");
						$("#resume_anamnesis").html("<div><strong>Anamnesis: </strong></div><div>"+$("#inp_anamnesis option:selected").text()+"</div><div><strong>Keterangan: </strong></div><div>"+$("#inp_ket_anamnesis").val()+"</div>");
						$("#resume_present").html("<div><strong>Perawatan: </strong></div><div>"+$("#inp_rawat option:selected").text()+"</div><div><strong>Habitus: </strong></div><div>"+$("#inp_habitus option:selected").text()+"</div><div><strong>Gizi: </strong></div><div>"+$("input[name='inp_gizi']:checked").parent('label').text()+"</div><div><strong>Pertumbuhan Badan: </strong></div><div>"+$("input[name='inp_tumbuh']:checked").parent('label').text()+"</div><div><strong>Sikap Berdiri: </strong></div><div>"+$("#inp_sikap option:selected").text()+"</div><div><strong>Suhu: </strong></div><div>"+$("#inp_suhu").val()+"&#8451;</div><div><strong>Frekuensi Nadi: </strong></div><div>"+$("#inp_frek_nadi").val()+" x/menit</div><div><strong>Frekuensi Nafas: </strong></div><div>"+$("#inp_frek_nafas").val()+" x/menit</div>");
					}
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_mrn: {
						validators: {
							notEmpty: {
								message: 'No Rekam Medis wajib diisi'
							},
						}
					},
					inp_nama: {
						validators: {
							notEmpty: {
								message: 'Nama Lengkap wajib diisi'
							},
						}
					},
					inp_spesies: {
						validators: {
							notEmpty: {
								message: 'Spesies wajib diisi'
							},
						}
					},
					inp_jk: {
						validators: {
							notEmpty: {
								message: 'Jenis Kelamin wajib diisi'
							},
						}
					},
					inp_jns: {
						validators: {
							notEmpty: {
								message: 'Jenis wajib diisi'
							},
						}
					},						
					inp_warna: {
						validators: {
							notEmpty: {
								message: 'Warna wajib diisi'
							},
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_anamnesis: {
						validators: {
							notEmpty: {
								message: 'Anamnesis wajibb diisi'
							}
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					inp_rawat: {
						validators: {
							notEmpty: {
								message: 'Perawatan wajibb diisi'
							}
						}
					},
					inp_habitus: {
						validators: {
							notEmpty: {
								message: 'Habitus wajibb diisi'
							}
						}
					},
					inp_gizi: {
						validators: {
							notEmpty: {
								message: 'Gizi wajibb diisi'
							}
						}
					},
					inp_tumbuh: {
						validators: {
							notEmpty: {
								message: 'Pertumbuhan Badan wajibb diisi'
							}
						}
					},
					inp_sikap: {
						validators: {
							notEmpty: {
								message: 'Sikap Berdiri wajibb diisi'
							}
						}
					},
					inp_suhu: {
						validators: {
							notEmpty: {
								message: 'Suhu wajibb diisi'
							}
						}
					},
					inp_frek_nadi: {
						validators: {
							notEmpty: {
								message: 'Frekuensi Nadi wajibb diisi'
							}
						}
					},
					inp_frek_nafas: {
						validators: {
							notEmpty: {
								message: 'Frekuensi Nafas wajibb diisi'
							}
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

	}

	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_wizard_rm');
			_formEl = KTUtil.getById('kt_form_rm');

			initWizard();
			initValidation();
		}
	};
}();

jQuery(document).ready(function () {
	KTWizard4.init();
});
